function tastePlot(experiment,suffix)

vData = experiment.vData;
p=experiment.p;

figureSubFolder = [p.figureFolder filesep suffix];

if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep p.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || p.forceRedrawing
    

    
    lickSignal  = vData.optoPeriod;
    zoneSignal  = vData.inZone;
    
    iLicks = experiment.vData.licks.iFrame;
    iQuandrantLick = experiment.vData.licks.iQuandrant;
    
    x = vData.bodyX_cmS;
    y = vData.bodyY_cmS;
%     x = vData.xF_cmS;
%     y = vData.yF_cmS;
    nPos = size(x,1);
    
    inQuadrant = vData.inQuadrant;
    
    quadantColors(1,1:3) = [1 0.8 0.9];
    quadantColors(2,1:3) = [0.9 0.8 1];
    quadantColors(3,1:3) = [0.8 1 0.9];
    quadantColors(4,1:3) = [0.9 0.9 0.8];
    
    posQ.x = [-20 10 10 -20];
    posQ.y = [-20 -20 20 20];
    
    f=figure()
    subplot(4,3,[1 2 3 4 5 6 7 8 9])
    hold on
    set(gca, 'YDir','reverse')
    axis equal
    plot([0 0],[-25 25],'k:');
    plot([-25 25],[0 0],'k:');
    plot(x,y,'color',[0.9 0.9 0.9]);
    
    for i = 1:size(iLicks,1)
        iFrame = iLicks(i);
        iQ = iQuandrantLick(i);
        xMouse = x(iFrame);yMouse = y(iFrame);
        xWell = vData.landmarks_cmS.x(iQ);yWell = vData.landmarks_cmS.y(iQ);
        d = sqrt( (xMouse-xWell)^2 + (yMouse-yWell)^2 );
        j=iLicks(i);
        color_ = quadantColors(iQuandrantLick(i),:);
        if d>p.body2licko_distanceMax_cm
            color_ = [1 0 0];
        end
        plot([xMouse xWell],[yMouse yWell],'LineStyle',':','Color',color_,'LineWidth',2);
        plot(xMouse,yMouse,'Marker','o','MarkerFaceColor',color_,'MarkerEdgeColor',[0 0 0],'LineStyle','none','MarkerSize',5);
        text(xMouse+0.5,yMouse+0.5,num2str(i),'color',[0 0 0]);
    end
    
    for iQ=1:4
        ii = find(inQuadrant==iQ);
        %     plot(x(ii),y(ii),'Marker','.','MarkerFaceColor',[0 0 0],'MarkerEdgeColor',quadantColors(iQ,:),'LineStyle','none','MarkerSize',1);
        text(posQ.x(iQ),posQ.y(iQ),sprintf('Quandrant #%d: %s',iQ,vData.landmarksStr{iQ}));
        %     text(vData.landmarks_cmS.x(iQ)+1,vData.landmarks_cmS.y(iQ)+1,vData.landmarksStr{iQ});
        plot(vData.landmarks_cmS.x(iQ),vData.landmarks_cmS.y(iQ),'Marker','d','MarkerFaceColor',quadantColors(iQ,:),'MarkerEdgeColor',[0 0 0],'LineStyle','none','MarkerSize',10);
        %     plot(vData.landmarks_cmS.x(iQ),vData.landmarks_cmS.y(iQ),'Marker','+','MarkerFaceColor','none','MarkerEdgeColor',[0 0 0],'LineStyle','none','MarkerSize',10);
    end
    
    xlim([-25 25])
    ylim([-25 25])
    
    
    subplot(4,3,[10 11 12])
    hold on
    for i = 1:size(iLicks,1)
        iFrame=iLicks(i);
        iQ = iQuandrantLick(i);
        xMouse = x(iFrame);yMouse = y(iFrame);
        xWell = vData.landmarks_cmS.x(iQ);yWell = vData.landmarks_cmS.y(iQ);
        d = sqrt( (xMouse-xWell)^2 + (yMouse-yWell)^2 );
        plot([i i],[0 d],'Linestyle','-','Color',quadantColors(iQ,:))
    end
    plot([1 size(iLicks,1)],[p.body2licko_distanceMax_cm p.body2licko_distanceMax_cm],'r:')
    
    print(f,figName,'-djpeg');
    close(f);
end

