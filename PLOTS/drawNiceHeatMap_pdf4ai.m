load('F275.mat')

map = experiment.map.NormTransients.IO;
map = experiment.map.NormSig.IO;
map = experiment.map.Occ.IO;

ii = isnan(map);
ii = find(map==0);

map(ii)=0;
map = imgaussfilt(map,2);
map(ii)=nan;

figure();
hold on
colorbar
colormap([[1 1 1] ; jet(1024)])
imagesc(map)

fig = openfig('F275 EPM TransientMap GaussianSmoothing.fig')

print(gcf,'F275 EPM TransientMap GaussianSmoothing.pdf','-dpdf','-r2000','-painters')
% 
% SweetMaps = experiment.map.Occ.IO;
% ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
% 
% figure();
% hold on
% colorbar
% colormap([[1 1 1] ; jet(1024)])
% imagesc(SweetMaps)