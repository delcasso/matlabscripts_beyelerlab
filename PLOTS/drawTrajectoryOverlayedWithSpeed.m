function drawTrajectoryOverlayedWithSpeed(params,x,y,xMax,yMax)

suffix = 'speedTrace';

figureSubFolder = [p.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || params.forceRedrawing
    
    bg=getBackGroundQuick(params);
    f=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    
    %% Speed Calculation
    windowSize_sec = 1;
    windowSize_frame = windowSize_sec * params.behaviorCameraFrameRate_Hz;
    dx = diff(x);dy = diff(y);
    dist = sqrt(dx .^2 + dy .^2);
    speed = dist;
    speed = speed * 1000;
    
    c = colormap(jet(floor(max(dist))+1));
    f=figure();
    set(0, 'DefaultFigureRenderer', 'painters');
    hold on
    colormap(c);
    colorbar();
    axis equal
    xlim([0 xMax])
    ylim([0 yMax])
    nPos = size(x,1);
    for iPos=2:nPos
        if ~isnan(dist(iPos-1))
            plot([x(iPos-1) x(iPos)],[y(iPos-1) y(iPos)],'color',c(floor(dist(iPos-1))+1,:));
        end
    end
    print(f,figName,'-djpeg');
    close(f);
    
end

end