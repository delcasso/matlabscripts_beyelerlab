% function videoMean=getVideoMean0(videoPath)

videoPath = '/home/delcasso/tmp/F354.avi';
videoPath = format_filenames(videoPath);

videoMean=[];

[filepath,name,ext] = fileparts(videoPath);

if ~exist([filepath filesep name '-videoMean.mat'],'file')
    fprintf('%s ==> %s\n',[name ext],[name '-videoMean.mat']);
    videoInfo=getVideoInfo(videoPath);
    v = VideoReader(videoPath);
    videoMean=[];
    duration = v.Duration;
     w = waitbar(0,sprintf('%2.2f sec to process',duration));
     iFrame = 0;
     tic();
    while hasFrame(v)
        video = readFrame(v);
        videoMean=[videoMean mean2(video)];
        if mod(iFrame,100)
            waitbar(v.CurrentTime/duration,w,sprintf('conversion %2.2f / %2.2f',v.CurrentTime,duration));
        end
    end
    save(videoMean_Filename,'videoMean');
end
    
% end
% 
% f = waitbar(0,'Please wait...');
% pause(.5)
% 
% 

% videoMean_Filename = [params.dataRoot  filesep params.dataFileTag '-videoMean.mat'];
% if ~exist(videoMean_Filename)
%     videoInfo=getVideoInfo(videoPath);
%     v = VideoReader(videoPath);
%     videoMean=[];
%     while hasFrame(v)
%         video = readFrame(v);
%         videoMean=[videoMean mean2(video)];
%     end
%     save(videoMean_Filename,'videoMean');
% else
%     load(videoMean_Filename);
% end
% end
