function videoMean=getVideoMean(params)

videoPath = [params.dataRoot filesep params.dataFileTag '.' params.videoExtension];
videoMeanPath = [params.dataRoot  filesep params.dataFileTag '-videoMean.mat'];
if ~exist(videoMeanPath,'file')
    videoInfo=getVideoInfo(params);
    v = VideoReader(videoPath);
    videoMean=[];
    while hasFrame(v)
        video = readFrame(v);
        videoMean=[videoMean mean2(video)];
    end
    save(videoMeanPath,'videoMean');
else
    load(videoMeanPath);
end
end
