clc    
folder = 'Z:\Fiber-Photometry\01_DATA\20190107_IC-SweetBitter_G4\20190114_EPM';
videoPath = [folder filesep 'M360-01142019131708.avi'];

videoInfo=getVideoInfo(videoPath);
v = VideoReader(videoPath);
videoMean=[];
iFrame  = 0;
while hasFrame(v)
    iFrame  = iFrame  +1
    video = readFrame(v);
    videoMean=[videoMean mean2(video)];
end
save(videoMean_Filename,'videoMean');