function resynchedCamPlot(dataRoot,dataFileTag,B,B_synchronized)
figName = [dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    hold on
    plot(B.mainY,B.mainY,'color',[.7 .7 .7],'LineStyle','-','Marker','o');
    plot(B_synchronized.mainY,B_synchronized.mainY,'color',[.7 .2 .7],'LineStyle','-','Marker','+');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.dpdf'],'-dpdf');
    close(f6);
end
figName = [dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    subplot(2,2,[1 2])
    hold on
    title('All')
    plot(B.optoPeriod);
    subplot(2,2,3)
    hold on
    title('First Min')
    plot(B.optoPeriod(1:15*60));
    subplot(2,2,4)
    hold on
    title('Last Min')
    plot(B.optoPeriod(end-(15*60):end));
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.dpdf'],'-dpdf');
    close(f6);
end
end