function processMultipleFoldersForVideoMeanMeasure()

close all;clear;clc

tic;
t0=toc;

machine = 'SD_remote';
batchID= '20180926_iC-SweetBitter_AllTogether_TASTE';
p=[];
[dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig(batchID,machine,p);
p.outputFolder=outputFolder;
p.figureFolder = [p.outputFolder filesep 'figures'];
p.apparatus=apparatus;
p.videoExt=videoExt;
nFolders = size(dataRoot,2);

for iFolder=1:nFolders
    p.dataRoot = dataRoot{iFolder};
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    l = dir([dataRoot{iFolder} filesep '*.' videoExt{iFolder}]);
    n=size(l,1);
    for j=1:n
        p.filename = l(j).name;
        [p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        tProcessingStart=toc;
        fprintf('[%.1f sec.]\tProcessing File %d/%d [%s] ...',tProcessingStart,j,n,p.dataFileTag);
        videoMean=getVideoMean(p);
        tProcessingStop=toc;
        fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
    end
end