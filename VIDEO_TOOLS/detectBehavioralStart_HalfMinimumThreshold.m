function [iBackToZero,vData]=detectBehavioralStart_HalfMinimumThreshold(params,vData)

tmpName = [params.dataRoot filesep params.dataFileTag '-behstartmsec.txt'];

iBackToZero=[];
v=getVideoMean(params);
vData.vMean=v';
correctedV = v - median(v);   

if exist(tmpName,'file')    
    iBackToZero=[];     
    iBackToZero = load(tmpName);
    iBackToZero = floor(iBackToZero /1000);
    iBackToZero = iBackToZero * params.behaviorCameraFrameRate_Hz;    
else    
    iBackToZero=[];     
    thSig = correctedV > (min(correctedV)/2);  
    i0 = find(thSig==0);
    d_i0 = diff(i0);
    iBackToZero = find(correctedV(max(i0):end)>0,1,'first') + max(i0);
    if isempty(iBackToZero)
        iBackToZero=max(i0)+1;
end




suffix = 'behavioralStart';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

    if ~exist(figName,'file') || params.forceBehavioralStart
        fig=figure();
        subplot(2,1,1)
        hold on
        if ~exist('v','var')
            v=getVideoMean(params);
        end
        plot(v)
        plot([iBackToZero iBackToZero],[min(v) max(v)],'r');
        subplot(2,1,2)
        hold on
        plot(thSig)
        plot([iBackToZero iBackToZero],[min(thSig) max(thSig)],'r');          
        print(fig,figName,'-djpeg');
        close(fig);
    end

end





% 
% 
%   figure()
%     subplot(2,2,1)
%     hold on
%     plot(correctedV)
%     
%     thSig =correctedV > (min(correctedV)/2)
%     subplot(2,2,2)
%     hold on
%     plot(thSig)
%     
%     i0 = find(thSig==0);
%     d_i0 = diff(i0)
        










