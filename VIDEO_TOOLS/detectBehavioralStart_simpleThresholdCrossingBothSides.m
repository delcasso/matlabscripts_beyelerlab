function [iFirstCross,iCrossSup,iCrossInf,Th,vData]=detectBehavioralStart_simpleThresholdCrossingBothSides(params,vData)

% clear all; clc; close all;
% load('Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI\M258.mat');
% params  = experiment.p;

% if params.dataFileTag=='M279'
%     disp('here');
% end

v=getVideoMean(params);

% First Frame was white on First Recordings, they were converted to mkv
% format, so we can isolate them like this
v(1)=v(2); % sometimes the first frame has problem
n=size(v,2);

iHalf = floor(n/2);

vEnd = v(iHalf:end);
z=(v-mean(vEnd))./std(vEnd);
zEnd=(vEnd-mean(vEnd))./std(vEnd);

Th = 5;
iCrossSup=[];iCrossInf=[];
while Th>0 && isempty(iCrossSup) && isempty(iCrossInf)
    zSup = z>Th;iCrossSup = find(diff(zSup==1));
    zInf = z<-Th;iCrossInf = find(diff(zInf==1));
    Th=Th-1;
end

if isempty(iCrossSup)  && isempty(iCrossInf)
    iFirstCross =1;
else    
    iFirstCross = min([min(iCrossSup) min(iCrossInf)]);
end


vData.vMean = v';

suffix = 'behavioralStart';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || params.forceBehavioralStart   
    fig=figure();   
    hold on
    plot(z);
    plot([1 n],[5 5],'k:');
    plot([1 n],[-5 -5],'k:');    
    plot([iFirstCross iFirstCross],[min(z) max(z)]);
    print(fig,figName,'-djpeg');
    close(fig);    
end




