function [iBackToZero,vData]=detectBehavioralStart_sigmFit(params,vData)

tmpName = [params.dataRoot filesep params.dataFileTag '-behstartmsec.txt'];

iBackToZero=[];
v=getVideoMean(params);
vData.vMean=v';
correctedV = v - median(v);   


if exist(tmpName,'file')    
    iBackToZero=[];     
    iBackToZero = load(tmpName);
    iBackToZero = floor(iBackToZero /1000);
    iBackToZero = iBackToZero * params.behaviorCameraFrameRate_Hz;    
else    
    
    [param]=sigm_fit(1:size(correctedV,2),correctedV,[],[],0)%)       
    iBackToZero =floor(param(3));
end




suffix = 'behavioralStart';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

    if ~exist(figName,'file') || params.forceBehavioralStart
        fig=figure();
        subplot(2,1,1)
        hold on
        if ~exist('v','var')
            v=getVideoMean(params);
        end
        plot(v)
        plot([iBackToZero iBackToZero],[min(v) max(v)],'r');   
        print(fig,figName,'-djpeg');
        close(fig);
    end

end





% 
% 
%   figure()
%     subplot(2,2,1)
%     hold on
%     plot(correctedV)
%     
%     thSig =correctedV > (min(correctedV)/2)
%     subplot(2,2,2)
%     hold on
%     plot(thSig)
%     
%     i0 = find(thSig==0);
%     d_i0 = diff(i0)
        










