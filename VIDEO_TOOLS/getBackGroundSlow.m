function bg = getBackGroundSlow(params)

nFrames = 50;
beginningOffset_sec = 120;
interFrameGap_sec=0;

% nFrames = 50;
% beginningOffset_sec = 0;
% interFrameGap_sec=3;
suffix = 'bg';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName)
    videoPath = [params.dataRoot filesep params.dataFileTag '.' params.videoExtension];
    v = VideoReader(videoPath);
    duration_sec = v.Duration;
    if ~interFrameGap_sec
        interFrameGap_sec = floor((duration_sec - beginningOffset_sec)  / (nFrames+1) );
    end
    fprintf('\tinterFrame = %2.2f sec',interFrameGap_sec);
    v.CurrentTime = 0;
    vidFrame = readFrame(v);
    vidFrame = mean(vidFrame,3);
    imSize=size(vidFrame);
    imgBuffer = nan(imSize(1),imSize(2),nFrames);
    
    v.CurrentTime = beginningOffset_sec;
    
    for i=1:nFrames        
        try
        v.CurrentTime  = v.CurrentTime + interFrameGap_sec;
        vidFrame = readFrame(v);
        imgBuffer(:,:,i)=mean(vidFrame,3);
        catch
            warning('getBackGroundSlow try to get more frame than exisiting number');
        end
    end
    bgMat = nanmedian(imgBuffer,3);
    clearvars 'imgBuffer';
    bg = mat2gray(bgMat);
    imwrite(bg,figName,'jpg');
    clearvars 'bgImg'
end
    bg = imread(figName);





end
