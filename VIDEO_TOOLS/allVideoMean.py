import imageio
import os
import scipy.io as sio
import time
import sys

top = '/home/delcasso/nas1/Opto&Behavior/DATA/20181126_IC-SweetBitter-G2'

for path, _, files in os.walk(top):
    for f in files:
        p = os.path.splitext(f)
        ext = p[1]
        name = p[0]
        if ext == '.avi' or ext == '.mp4':
            matFileFullPath = path + os.sep + name + 'videoMean.mat'
            if os.path.isfile(matFileFullPath):
                print('already done : {0}'.format(matFileFullPath))
            else:
                i = name.find('-')
                if i == -1:
                    t0 = time.time()
                    fullpath = path + os.sep + f
                    print('processing : {0}'.format(fullpath))
                    try:
                        vid_reader = imageio.get_reader(fullpath)
                        videoMean = [im.mean() for i, im in enumerate(vid_reader)]
                        d = {}
                        d['videoMean'] = videoMean
                        sio.savemat(matFileFullPath, d)
                    except:
                        print('!!! error in execution')
                    t1 = time.time()
                    print('done in {0} min.'.format(int((t1 - t0) / 60)))
                else:
                    print('{0} skipped (bad name)'.format(f))
