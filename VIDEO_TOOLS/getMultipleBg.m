clear;clc;close all

% dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180905_TASTE';
% dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180904_TASTE';
dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180902 WATER PHOTO1';

fileList = dir([dataRoot filesep '*.avi']);
nFiles = size(fileList,1);

tic
for iFile = 1:nFiles
    n = fileList(iFile).name;
    tProcessingStart=toc;
    fprintf('[%.1f sec.]\tProcessing File %d/%d [%s] ...',tProcessingStart,iFile,nFiles,n);
    p = [dataRoot filesep n];   
    bg = getBackGroundSlow(p);  
    tProcessingStop=toc;
    fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
end
