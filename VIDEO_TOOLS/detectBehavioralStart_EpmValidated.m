function [iBackToZero,vData]=detectBehavioralStart_EpmValidated(params,vData)

iBackToZero=[];
v=getVideoMean(params);

% First Frame was white on First Recordings, they were converted to mkv
% format, so we can isolate them like this

v(1)=v(2); % sometimes the first frame has problem
n=size(v,2);

iHalf = floor(n/2);

vEnd = v(iHalf:end);
z=(v-mean(vEnd))./std(vEnd);
vMin=min(z);lowTH=vMin/2;
vMax=max(z);highTH=vMax/2;

lowSig = z<lowTH;dLowSig = diff(lowSig);
highSig = z>highTH;dHigSig = diff(highSig);
LowSigIdx = find(dLowSig==1);
HighSigIdx = find(dHigSig==1);

for j=1:size(LowSigIdx,2)
    iLow=LowSigIdx(j);
    iHigh=find(HighSigIdx>iLow,1,'first');
    if ~isempty(iHigh)
        iHigh=HighSigIdx(iHigh);
        iTmp=find(z(iHigh:end)<0,1,'first');
        if isempty(iTmp)
            iTmp=0;
        end
        iBackToZero(j) = iTmp + iHigh;
        ii=find(LowSigIdx(j+1:end)<iBackToZero(j) );
        LowSigIdx(ii)=[];
    end
    if isempty(LowSigIdx) || (j==size(LowSigIdx,2))
        break;
    end
end

lowSig = z<lowTH;dLowSig = diff(lowSig);
highSig = z<highTH;dHigSig = diff(highSig);
LowSigIdx = find(dLowSig==1);
HighSigIdx = find(dHigSig==1);





suffix = 'behavioralStart';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || params.forceBehavioralStart
    fig=figure();
    subplot(2,1,1)
    plot(v)
    subplot(2,1,2)
    hold on
    plot(z)
    plot([iHalf iHalf],[min(z) max(z)])
    plot([1 n],[lowTH lowTH],'m:');
    plot([1 n],[highTH highTH],'m:');
    for j=1:size(LowSigIdx,2)
        iLow=LowSigIdx(j);
        iHigh=find(HighSigIdx>iLow,1,'first');
        if ~isempty(iHigh)
            iHigh=HighSigIdx(iHigh);
            if (iHigh-iLow)>(params.HamamatsuFrameRate_Hz*(2*60))
                iHigh=find(z(iLow:end)>0,1,'first') + iLow;
            end
            iBackToZero(j) = find(z(iHigh:end)<0,1,'first') + iHigh;
            plot(iLow,z(iLow),'rv');
            plot(iHigh,z(iHigh),'g^');
            plot(iBackToZero(j),z(iBackToZero(j)),'ks');
            ii=find(LowSigIdx(j+1:end)<iBackToZero(j) );
            LowSigIdx(ii)=[];
        end
        if isempty(LowSigIdx) || (j==size(LowSigIdx,2))
            break;
        end
    end
    print(fig,figName,'-djpeg');
    close(fig);
end
vData.vMean=v';
end



% figure()
% hold on
% lVideo = dir([params.dataRoot filesep '*.avi']);
% nVideo = size(lVideo,1)
% for iVideo=1:nVideo
%     fprintf('Processing file %d/%d',iVideo,nVideo);
%     tic
%     n=lVideo(iVideo).name;
%     [~, params.dataFileTag, params.ext] = fileparts(n);
%     videoPath = [params.dataRoot filesep params.dataFileTag '.' params.videoExtension];
%     videoMean_Filename = [params.dataRoot  filesep params.dataFileTag '-videoMean.mat'];
%     if ~exist(videoMean_Filename)
%         videoInfo=getVideoInfo(videoPath);
%         v = VideoReader(videoPath);
%         videoMean=[];
%         while hasFrame(v)
%             video = readFrame(v);
%             videoMean=[videoMean mean2(video)];
%         end
%         save(videoMean_Filename,'videoMean');
%     else
%         load(videoMean_Filename);
%     end
%     normVideoMean = videoMean - min(videoMean);
%     normVideoMean / max(normVideoMean);
%     normVideoMean = normVideoMean + iVideo*10;
%     plot(normVideoMean);
%     colorOrder = get(gca, 'ColorOrder');
%      text(size(normVideoMean,2),mean(normVideoMean),params.dataFileTag,'color',colorOrder(mod(iVideo-1,7)+1,:));
%     toc
% end



