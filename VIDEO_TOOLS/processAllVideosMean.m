folder='Z:\Optogenetics & Behavior\DATA\20190318_ICa-ICp-G4\20190327_Sucrose\20190327_Sucrose_TOP view'
listing = dir([folder filesep '*.avi']);

s=size(listing,1);

for i=1:s
    n = listing(i).name        
    C = strsplit(n,'.');
    videoMeanFileName = [folder filesep C{1} '-videoMean.mat'];
    if ~exist(videoMeanFileName,'file')
        v = VideoReader([folder filesep n]); 
        while hasFrame(v)
            video = readFrame(v);
            videoMean(i)=mean(mean(mean(video)));
            i =i + 1;
        end    
        save([folder filesep C{1} '-videoMean.mat'],'videoMean');    
    end
end

