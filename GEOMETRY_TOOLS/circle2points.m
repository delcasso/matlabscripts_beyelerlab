function [x,y]=circle2points(xCenter,yCenter,radius,nPoints)
theta = linspace(0,2*pi,nPoints);
x = radius * cos(theta) + xCenter;
y = radius * sin(theta) + yCenter;
end

