function draw_linearizedActivityMaps_cmStandardPositive(params,outputPrefix,Map,armAligned, videoTrackingData)


suffix = ['LinearMaps_' outputPrefix];

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || params.forceRedrawing
    
    twoDim = armAligned.twoDim;
    oneDim =armAligned.oneDim;
    oneDimMin =armAligned.oneDimMin;
    oneDimMax =armAligned.oneDimMax;
    zones = videoTrackingData.zones_cmSP;
    
    for iZone=1:5
        zones(iZone).position = zones(iZone).position/params.MapScale_cmPerBin;
    end
    
    
    
    bg=getBackGroundQuick(params);
    f=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    
    subplot(4,4,[1 2 5 6 9 10 13 14])
    hold on
    axis equal
    axis off
    set(gca,'Ydir','reverse')
    c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
    load('C:\__Softwares\MatlabScripts_BeyelerLab\COLORMAPS\myCustomColormap.mat');
    colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);
    ii = isnan(Map);
    Map(ii)=0;
    %     gaussFilt_PercAvgSigMap = imgaussfilt(PercAvgSigMap,params.PhotometrySignalMap_sigmaGaussFilt);
    gaussFilt_PercAvgSigMap  = Map;
    gaussFilt_PercAvgSigMap(ii)=nan;
    im=imagesc(gaussFilt_PercAvgSigMap);
    bg=getBackGroundQuick(params);
    sMax = max(size(bg));
    %     xlim([0 sMax]);
    %     ylim([0 sMax]);
    colorbar();
    
    for iZones=1:5
        rectangle('Position',zones(iZones).position,'EdgeColor',zones(iZones).color,'LineStyle',zones(iZones).lineStyle);
    end
    
    for iZones=1:4
        subplot(4,4,iZones*4-1)
        hold on
        axis off
        axis equal
        imagesc([twoDim{iZones}(:,:)],[min(min(Map)) max(max(Map))+0.001]);
    end
    
    for iZones=1:4
        subplot(4,4,iZones*4)
        hold on
        xlim([0 80]);
        if ~isnan(oneDimMin) && ~isnan(oneDimMax)
            ylim([oneDimMin oneDimMax+0.001])
        end
        plot(oneDim{iZones},'color',zones(iZones).color,'LineStyle',zones(iZones).lineStyle);
    end
    
    print(f,figName,'-djpeg');
    %     print(f,[figName '.pdf'],'-dpdf');
    close(f);
    
end
end