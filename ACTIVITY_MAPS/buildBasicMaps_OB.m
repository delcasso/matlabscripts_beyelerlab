function experiment=buildBasicMaps_OB(experiment)
%% CREATING BASIC  ACTIVITY MAPS
p = experiment.p;vData = experiment.vData;
x = vData.mainX_cmSP;y = vData.mainY_cmSP;
map.Occ.IO=buildOccupancyMap_cmStandardPositive(x,y,p.xMax,p.yMax,p.MapScale_cmPerBin); %build a spatial map of the time spend in each pixel for the entire exp.
experiment.map=map;
end