function SigMap=buildSignalMap_cmStandardPositive(x,y,sig)

xMin=0;xMax=1500;yMin=0;yMax=1500;
SigMap = zeros(xMax,yMax);

nPos=size(x,1);
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos))+1;j=floor(x(iPos))+1;
        SigMap(i,j)= SigMap(i,j)+sig(iPos);
    end
end
end
