function OccupancyMap=buildOccupancyMap(x,y)


xMin=0;xMax=1500;
yMin=0;yMax=1500;

OccupancyMap = zeros(xMax,yMax);

nPos=size(x,1);

for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos))+1;j=floor(x(iPos))+1;
        OccupancyMap(i,j)=OccupancyMap(i,j)+1;
    end
end

end

