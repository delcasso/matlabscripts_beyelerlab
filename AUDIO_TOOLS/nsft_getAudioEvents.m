function audioevents_sec = nsft_getAudioEvents(params)

sideviewRoot = [params.dataRoot '-sideview'];

if ~exist(sideviewRoot,'dir')
    fprintf('sideview folder is  missing\n');
end

audioevents_path = [sideviewRoot filesep params.dataFileTag '-audioevents.mat'];

if ~exist(audioevents_path,'file')

sideview_videoPath = [sideviewRoot filesep params.dataFileTag '-sideview.mp4'];
if ~exist(sideview_videoPath,'file')
    fprintf('sideview video is missing\n');
end

sideview_synchroLedPath = [sideviewRoot filesep params.dataFileTag '-synchroLED.txt'];
if ~exist(sideview_synchroLedPath,'file')
    fprintf('sideview synchro LED bonsai  is missing\n');
end

sync_signal = load(sideview_synchroLedPath);
sync_signal =sync_signal - mean(sync_signal);

videoInfo = VideoReader(sideview_videoPath);
video_nSamples = size(sync_signal,1);
audiotrack = audioread(sideview_videoPath);
audiotrack = audiotrack - mean(audiotrack);
audio_nSamples = size(audiotrack,1);
audio_samplingRate = audio_nSamples / videoInfo.Duration;


norm_sync_signal  = sync_signal >(max(sync_signal )/10);
tmp =diff(norm_sync_signal );
iSync  = find(tmp ==1);
dSync = diff(iSync );
sync_gap = median(dSync);

ii = find(dSync>(sync_gap*1.5));
if ~isempty(ii)
    warning('synch signal lost')
end

% Cut Everything before first LED ON
i1 = iSync(1);
norm_sync_signal(1:i1-1)=[];
audio_nSamplePerVideoFrame = floor(audio_nSamples / video_nSamples);
audiotrack(1:(i1-1)*audio_nSamplePerVideoFrame)=[];

nSyncSignal =size(norm_sync_signal ,1);
tSyncSignal = 1:nSyncSignal;
tSyncSignal = tSyncSignal / videoInfo.FrameRate;

nAudio =size(audiotrack ,1);
tAudio = 1:nAudio;
tAudio = tAudio / audio_samplingRate;
tAudio_period = 1/audio_samplingRate;

audioTH = 4* std(audiotrack);

norm_audio = audiotrack>audioTH;
diff_norm_audio = diff(norm_audio);
iUp_audio  = find(diff_norm_audio ==1);

audioevents_sec =tAudio(iUp_audio);
d_audioEvents = diff(audioevents_sec);

audioDetectionGap_sec = 1;

ii = find(d_audioEvents< (tAudio_period*audio_samplingRate*audioDetectionGap_sec));
audioevents_sec(ii)=[];

save(audioevents_path,'audioevents_sec');

else
    load(audioevents_path)
end


