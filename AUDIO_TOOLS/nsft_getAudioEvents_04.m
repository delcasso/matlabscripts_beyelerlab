function audio = nsft_getAudioEvents_04(params)

sideviewRoot = [params.dataRoot '-sideview'];

if ~exist(sideviewRoot,'dir')
    fprintf('sideview folder is  missing\n');
end

audioevents_path = [sideviewRoot filesep params.dataFileTag '-audio.mat'];

if 1%~exist(audioevents_path,'file')
    
    sideview_videoPath = [sideviewRoot filesep params.dataFileTag '-sideview.mp4'];
    if ~exist(sideview_videoPath,'file')
        fprintf('sideview video is missing\n');
    end
    
    sideview_synchroLedPath = [sideviewRoot filesep params.dataFileTag '-synchroLED.txt'];
    if ~exist(sideview_synchroLedPath,'file')
        fprintf('sideview synchro LED bonsai  is missing\n');
    end
    
    % Average Signals on Zero
    synchroSig = load(sideview_synchroLedPath);
    synchroSig =synchroSig - mean(synchroSig);
    synchroSig_size = size(synchroSig,1);
    videoInfo = VideoReader(sideview_videoPath);
    audioSig = audioread(sideview_videoPath);
    audioSig = audioSig - mean(audioSig);
    audioSig_size =size(audioSig ,1);
    audio_nSamplePerVideoFrame = floor(audioSig_size / (synchroSig_size));
%     audio_sfreq = audioSig_size / videoInfo.Duration;
    
    audio_sfreq = (audioSig_size / (synchroSig_size-1)) * videoInfo.FrameRate;
    
    synchroSig  = synchroSig >(max(synchroSig )/10);
    tmp =diff(synchroSig );iSync  = find(tmp ==1)+1;dSync = diff(iSync );
    sync_gap = median(dSync);
    
    ii = find(dSync>(sync_gap*1.5));
    if ~isempty(ii), warning('synch signal lost');end
    
    % Cut Everything before first LED ON
%     i1 = iSync(1);
%     synchroSig(1:i1-1)=[];
%     synchroSig_size =size(synchroSig ,1);
%     audioSig(1:(i1-1)*audio_nSamplePerVideoFrame)=[];
%     audioSig_size =size(audioSig ,1);
    
    tSyncSignal = 1:synchroSig_size;
    tSyncSignal = tSyncSignal / videoInfo.FrameRate;
    
    tAudio = 1:audioSig_size;
    tAudio = tAudio / audio_sfreq;
    tAudio_period = 1/audio_sfreq;
    
    %     audioSig = audioSig(1:44000*30);
    %     tAudio= tAudio(1:44000*30);
    %
    
    %Smart downsampling to avoid loosing peaks
    nSamples = size(audioSig,1);
    downSamplingFactor = 1000;
    nLines = floor(nSamples/downSamplingFactor);
    nSamples = nLines * downSamplingFactor;
    tmp = audioSig(1:nSamples);
    tmp = reshape(tmp',downSamplingFactor,nLines);
    [tmp,i] = max(tmp);
    tmp2 = tAudio(1:nSamples);
    tmp2 = reshape(tmp2',downSamplingFactor,nLines);
    tmp3 = nan(nLines,1);
    for j=1:nLines
        tmp3(j)=tmp2(i(j),j);
    end
    
    audioDown_values = tmp;
    audioDown_ts = tmp3;
    audioDown_sfreq = audio_sfreq / downSamplingFactor;
    
    % x = tmp3;
    % y = tmp + max(y);
    % plot(x,y)
    
    
    
    figure()
    
    subplot(3,1,1)
    hold on
    plot(tAudio,audioSig)
    
    subplot(3,1,2)
    hold on
    plot(audioDown_ts,audioDown_values)
    
    subplot(3,1,3)
    figure()
    hold on
    tic;audioDown_values_f = highpass(audioDown_values,0.25,audioDown_sfreq);toc
    plot(audioDown_ts,audioDown_values_f)
    
    for nSTD=1:6
        audioTH = nSTD* std(audioDown_values);
        plot([audioDown_ts(1) audioDown_ts(end)],[audioTH audioTH],'k')
    end
    
    [pks,locs] = findpeaks(audioDown_values_f);
    
    nSTD=4;
    audioTH = nSTD* std(audioDown_values);
    plot([audioDown_ts(1) audioDown_ts(end)],[audioTH audioTH],'r')
    
    ii = find(pks<audioTH);
    pks(ii)=[];locs(ii)=[];
    
    plot(audioDown_ts(locs),audioDown_values_f(locs),'Marker','d','MarkerFacecolor','r','MarkerEdgecolor','none','LineStyle','none');
    
    audioevents_sec =audioDown_ts(locs);
    
    diff_audioevents_sec = diff(audioevents_sec);
    audioDetectionGap_sec = .1;
    ii = find(diff_audioevents_sec<audioDetectionGap_sec);
    audioevents_sec(ii+1) = []
    locs(ii+1)=[];
    audioevents_idx = locs;
    plot(audioDown_ts(audioevents_idx),audioDown_values_f(audioevents_idx),'Marker','d','MarkerFacecolor','g','MarkerEdgecolor','none','LineStyle','none');
    
     borisData = getBorisData([sideviewRoot filesep 'bitesEvents' filesep params.dataFileTag '.tsv']);
     
     bitesEvents_sec = borisData.Time;
%      bitesEvents_sec = bitesEvents_sec - bitesEvents_sec(1);
     bitesEvents_sec(1)=[];
     
     nbitesEvents = size(bitesEvents_sec,1);
     
     for i=1: nbitesEvents
     plot([bitesEvents_sec(i) bitesEvents_sec(i)] ,[-0.1 0.1],'m');
     end
%          plot(bitesEvents_sec,ones(size(bitesEvents_sec,1))*max(audioDown_values_f),'Marker','d','MarkerFacecolor','m','MarkerEdgecolor','none','LineStyle','none');
% 
    
    audioDown_ts = audioDown_ts - (iSync(1)/videoInfo.FrameRate);

    save(audioevents_path,'audio');
    
else
    load(audioevents_path)
end

end

    function borisData = getBorisData(filename, startRow, endRow)
        %IMPORTFILE Import numeric data from a text file as a matrix.
        %   F251 = IMPORTFILE(FILENAME) Reads data from text file FILENAME for the
        %   default selection.
        %
        %   F251 = IMPORTFILE(FILENAME, STARTROW, ENDROW) Reads data from rows
        %   STARTROW through ENDROW of text file FILENAME.
        %
        % Example:
        %   F251 = importfile('F251.tsv', 17, 72);
        %
        %    See also TEXTSCAN.
        
        % Auto-generated by MATLAB on 2019/08/22 17:15:35
        
        %% Initialize variables.
        delimiter = '\t';
        if nargin<=2
            startRow = 17;
            endRow = inf;
        end
        
        %% Format for each line of text:
        %   column1: double (%f)
        %	column2: categorical (%C)
        %   column3: double (%f)
        %	column4: double (%f)
        %   column5: text (%s)
        %	column6: double (%f)
        %   column7: text (%s)
        %	column8: text (%s)
        %   column9: categorical (%C)
        % For more information, see the TEXTSCAN documentation.
        formatSpec = '%f%C%f%f%s%f%s%s%C%[^\n\r]';
        
        %% Open the text file.
        fileID = fopen(filename,'r');
        
        %% Read columns of data according to the format.
        % This call is based on the structure of the file used to generate this
        % code. If an error occurs for a different file, try regenerating the code
        % from the Import Tool.
        dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines', startRow(1)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
        for block=2:length(startRow)
            frewind(fileID);
            dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines', startRow(block)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
            for col=1:length(dataArray)
                dataArray{col} = [dataArray{col};dataArrayBlock{col}];
            end
        end
        
        %% Close the text file.
        fclose(fileID);
        
        %% Post processing for unimportable data.
        % No unimportable data rules were applied during the import, so no post
        % processing code is included. To generate code which works for
        % unimportable data, select unimportable cells in a file and regenerate the
        % script.
        
        %% Create output variable
        borisData = table(dataArray{1:end-1}, 'VariableNames', {'Time','Mediafilepath','Totallength','FPS','Subject','Behavior','Behavioralcategory','Comment','Status'});
    end








