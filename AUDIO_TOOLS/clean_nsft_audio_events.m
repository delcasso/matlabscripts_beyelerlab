function experiment=clean_nsft_audio_events(experiment)

params = experiment.p;
sideviewRoot = [params.dataRoot '-sideview'];
if ~exist(sideviewRoot,'dir'), fprintf('sideview folder is  missing\n');end
audioEvents_path = [sideviewRoot filesep params.dataFileTag '-audioEvents.mat'];


audioEvents_sec = experiment.audioEvents_sec;
timeLag = experiment.audioEvents_timeLag_sec;
nSTD = experiment.audioEvents_nSTD;
audioDetectionGap_sec = experiment.audioEvents_audioDetectionGap_sec;

nE = size(audioEvents_sec,1);

t_pos = experiment.vData.t0;
n_pos = size(t_pos,1);
x = experiment.vData.mainX_cmS;
y = experiment.vData.mainY_cmS;

audio_evts_idx_cleaned = [];
audio_evts_idx = [];
audio_evts_ts_cleaned = [];


for iE=1:nE
    t = audioEvents_sec(iE);
    [~,i] = min(abs(t_pos-t));
    if ~(i==1 || i==n_pos)
        audio_evts_idx = [audio_evts_idx i];
        if abs(x(i))<5 && abs(y(i))<5
            audio_evts_idx_cleaned = [audio_evts_idx_cleaned i];
            audio_evts_ts_cleaned = [audio_evts_ts_cleaned t];
        end
    end
end

save(audioEvents_path,'audioEvents_sec','timeLag','nSTD','audioDetectionGap_sec','audio_evts_idx_cleaned','audio_evts_ts_cleaned');


% figure()
% hold on
% plot(x,y)
% plot(x(audio_evts_idx),y(audio_evts_idx),'Marker','o','MarkerFaceColor',[1 0 0],'MarkerEdgeColor' ,'none','LineStyle','none');
% plot(x(audio_evts_idx_cleaned),y(audio_evts_idx_cleaned),'Marker','o','MarkerFaceColor',[0 1 0],'MarkerEdgeColor' ,'none','LineStyle','none');



sfreq = experiment.p.HamamatsuFrameRate_Hz;
nFrames = experiment.pData.nFrames;
edges_msec = experiment.p.eventBasedAnalysisEdges_msec;
edges_sec = edges_msec/1000;
zeroPosition = find(edges_sec==0);
zeroPositionPerc = (zeroPosition/size(edges_sec,2))*100;
windowSize_sec = edges_sec(end)-edges_sec(1);
nColumns = ceil(windowSize_sec*sfreq);
edges_idx = 1:nColumns;
zeroPosition = floor((size(edges_idx,2)*zeroPositionPerc)/100);
edges_idx = edges_idx - zeroPosition;


bulkSignal = experiment.pData.mainSig;
idx_transients = [experiment.pData.transients(:).iMax];


experiment.audio_evts_idx_cleaned = audio_evts_idx_cleaned;
n_idx_synchro = size(audio_evts_idx_cleaned,2);
% if n_idx_synchro>6
%     idx_synchro = idx_synchro(1:6);
%     n_idx_synchro = size(idx_synchro,2);
% end


   bulkMatrix = nan(n_idx_synchro,nColumns);

if n_idx_synchro
    
    tansientsMatrix=get_peth(audio_evts_idx_cleaned/sfreq,idx_transients/sfreq,edges_sec);
 
    for j=1:n_idx_synchro
        i = audio_evts_idx_cleaned(j);
        idx = edges_idx + i;
        if idx(1)>0 && idx(end)<nFrames
            bulkMatrix(j,:)=bulkSignal(idx);
        end
    end
    
end


m = nanmean(bulkMatrix);
sem = nanstd(bulkMatrix) / sqrt(size(bulkMatrix,1));
experiment.pData.avgBulkSignalAroundFood = m;

suffix = 'nsftaudio';

params = experiment.p;

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || params.forceBehavioralStart
    f=figure()
    subplot(2,2,1)
    hold on
    axis equal
    title('position')
    xlabel('x (cm)')
    ylabel('y (cm)')
    plot(x,y,'k')
    for iE=1:nE
        t = audioEvents_sec(iE);
        [~,i] = min(abs(t_pos-t));
        if ~(i==1 || i==n_pos)
            if abs(x(i))<5 && abs(y(i))<5
                plot(x(i),y(i),'Marker' ,'o','MarkerEdgeColor',[30 30 30]/255,'MarkerFaceColor',[173 255 47]/255,'MarkerSize',6);
            else
                plot(x(i),y(i),'Marker' ,'o','MarkerEdgeColor','none','MarkerFaceColor',[255 99 171]/255,'MarkerSize',4);
            end
        end
        
    end
    
    subplot(2,2,2)
    hold on
    title('bulk (food, t=0)')
    xlabel('Time (s)')
    ylabel('# Trial')
    peth_step = median(diff(edges_sec));
    time = edges_sec(1):peth_step:edges_sec(end)
    imagesc(time,1:n_idx_synchro,bulkMatrix)
    c=colorbar();
    c.Label.String = 'Bulk Signal (% DFF)';
    subplot(2,2,3)
    hold on
    title('bulk (food, t=0)')
    xlabel('Time (s)')
    ylabel('Averaged Bulk Signal (% DFF)')
    
    plot(time,m,'k')
    plot(time,m+sem,'k:')
    plot(time,m-sem,'K:')
    
    
    print(f,figName,'-djpeg');
    close(f);
    
end

