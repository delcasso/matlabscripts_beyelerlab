
% clear all
% clc
% close all
% tic();
% tmpFolder = 'C:\Users\Delcasso\Desktop';
% mouse = 'M257';
% load([tmpFolder filesep mouse '.mat'])
% sideview_videoPath = [tmpFolder filesep mouse '-sideview.mp4']
% synchroLedPath   = [tmpFolder filesep mouse '-synchroLED.txt']
% sync_signal = load(synchroLedPath);
% sync_signal =sync_signal - mean(sync_signal);

% videoInfo = VideoReader(sideview_videoPath);
% video_nSamples = size(sync_sideview,1);
% audiotrack = audioread(sideview_videoPath);
% audiotrack = audiotrack - mean(audiotrack);
% audio_nSamples = size(audiotrack,1);
% audio_samplingRate = audio_nSamples / videoInfo.Duration;

norm_sync_signal  = sync_signal >(max(sync_signal )/10);
tmp =diff(norm_sync_signal );
iSync  = find(tmp ==1);
dSync = diff(iSync );
sync_gap = median(dSync);

ii = find(dSync>(sync_gap*1.5));
if ~isempty(ii)
    warning('synch signal lost')
end

% Cut Everything before first LED ON
i1 = iSync(1);
norm_sync_signal(1:i1-1)=[];
audio_nSamplePerVideoFrame = floor(audio_nSamples / video_nSamples);
audiotrack(1:(i1-1)*audio_nSamplePerVideoFrame)=[];

nSyncSignal =size(norm_sync_signal ,1);
tSyncSignal = 1:nSyncSignal;
tSyncSignal = tSyncSignal / videoInfo.FrameRate;

nAudio =size(audiotrack ,1);
tAudio = 1:nAudio;
tAudio = tAudio / audio_samplingRate;
tAudio_period = 1/audio_samplingRate;

audioTH = 3* std(audiotrack);

norm_audio = audiotrack>audioTH;
iUp_audio  = find(norm_audio ==1);

audioEvents_ts =tAudio(iUp_audio);
d_audioEvents = diff(audioEvents_ts);

audioDetectionGap_sec = 0.3;

ii = find(d_audioEvents< (tAudio_period*audio_samplingRate*audioDetectionGap_sec));
audioEvents_ts(ii)=[];

figure()
subplot(2,2,1)
hold on
title('sync events')
plot(tSyncSignal,norm_sync_signal,'k')
subplot(2,2,2)
hold on
title('audio track')
plot(tAudio,audiotrack,'k')
plot([tAudio(1) tAudio(end)],[audioTH audioTH],'r')
subplot(2,2,3)
hold on
title('audio norm')
histogram(diff(audioEvents_ts))











% 
% 
% 
% 
% 
% dSync=diff(norm_sync);
% iUp = find(dSync==1);
% dFrame = median(diff(iUp));
% nSynchro=size(iUp,1);

% for i=1:nSynchro
%     i1=iUp(i);i2=i1+dFrame;
%     videoTrackingData2.mainX = [videoTrackingData2.mainX ; interp1(x,videoTrackingData.mainX(i1:i2),xq)'];
%     videoTrackingData2.mainY = [videoTrackingData2.mainY ; interp1(x,videoTrackingData.mainY(i1:i2),xq)'];
%     videoTrackingData2.mouseAngle = [videoTrackingData2.mouseAngle ; interp1(x,videoTrackingData.mouseAngle(i1:i2),xq)'];
%     videoTrackingData2.mouseMajorAxisLength = [videoTrackingData2.mouseMajorAxisLength ; interp1(x,videoTrackingData.mouseMajorAxisLength(i1:i2),xq)'];
%     videoTrackingData2.mouseMinorAxisLength = [videoTrackingData2.mouseMinorAxisLength ; interp1(x,videoTrackingData.mouseMinorAxisLength(i1:i2),xq)'];
%     videoTrackingData2.mouseArea = [videoTrackingData2.mouseArea ; interp1(x,videoTrackingData.mouseArea(i1:i2),xq)'];
%     videoTrackingData2.optoPeriod = [videoTrackingData2.optoPeriod ; interp1(x,videoTrackingData.optoPeriod(i1:i2),xq)'];
% end




% 
% videoInfo = VideoReader([tmpFolder filesep videoPath]);
% audiotrack = audioread([tmpFolder filesep videoPath]);
% 
% video_nSamplesEstimated = floor(videoInfo.Duration * videoInfo.FrameRate);
% video_nSamplesReal = size(sync,1);
% 
% audio_nSamplesReal = size(audiotrack,1);
% audio_samplingRate = audio_nSamplesReal/ videoInfo.Duration;
% 
% window = [];
% noverlap = [];
% f=[.1:200:8000];
% fs = audio_samplingRate;
% 
% t_audio = (1:size(audiotrack,1))/audio_samplingRate ;
% 
% eventTime_sec = 187;
% windowSize_sec = 5;
% idx_sec = [eventTime_sec-windowSize_sec eventTime_sec+windowSize_sec]
% idx = floor(idx_sec * audio_samplingRate)
% 
% % x = audiotrack(idx(1):idx(2));
% x = audiotrack;
% % f=.1:50:5000;
% f = 700:10:900;
% [s,f,t] = spectrogram(x,2400,2000,f,fs,'yaxis');
% 
% sReal = real(10*log10(s));
% 
% m = mean(sReal,2);
% 
% m = repmat(m,1,size(sReal,2));
% 
% blsub_spectro = sReal-m; 
% 
% figure()
% subplot(3,1,1)
% hold on
% title('baseline substracted spectrogram')
% imagesc(t,f,blsub_spectro)
% subplot(3,1,2)
% hold on
% title('median')
% plot(t,median(blsub_spectro))
% subplot(3,1,3)
% hold on
% title('mean')
% plot(t,mean(blsub_spectro))
% 
% 
% 
% figure()
% hold on
% title('raw trace')
% plot(t_audio,audiotrack)
% 
% 
% toc