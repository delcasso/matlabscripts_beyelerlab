

%% 
% sound is not going to work
% [a,Fs] = audioread('F354.mp4');
% nSamples = size(a);
% t = 1:nSamples;
% t = t./Fs;
% t(1:Fs*65)=[];
% a(1:Fs*65)=[];
% t(Fs*15:end)=[];
% a(Fs*15:end)=[];
% plot(t-65,a);
% sound(a,Fs)
% spectrogram(a,floor(Fs/100),floor(Fs/200),[],Fs)
close all
clear
clc

dataRoot = 'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190131_QUININE-SUCROSE sideview';

cd(dataRoot);

v = VideoReader('F359.mp4');
d = v.Duration;
sFreq = v.FrameRate;
v.CurrentTime = 0;

idx = 0;
idxStop = sFreq * 60 * 5; 
mean_servoCrop=[];


w = waitbar(0,'reading data from video file');


f = figure();

while hasFrame(v)
            
    idx = idx+1;    
    
    t=1:idx;
    t=t./sFreq;
            
    imgTmp = readFrame(v);
    tmp = mean(mean(mean(imgTmp(578:601,558:588,:))));
    mean_servoCrop(idx) = tmp;
    
    if ~mod(idx,sFreq*60)
        plot(t,mean_servoCrop);
        waitbar(idx/(d*sFreq),w);
    end
  
end

save([dataRoot filesep 'F354-servoCrop.mat'],'mean_servoCrop','t','v');


fig = figure();

subplot(4,1,1)
plot(t,mean_servoCrop);
th = 23;
n = mean_servoCrop>th;

subplot(4,1,2)
plot(t,n);
ylim([-1 2])

subplot(4,1,3)
d = diff(n);
plot(t(2:end),d)
ylim([-2 2])

subplot(4,1,4)
iDown = find(d==-1);
d_iDown = diff(t(iDown));
histogram(d_iDown,[0:1:60])












