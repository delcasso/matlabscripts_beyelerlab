function audio = nsft_getAudioEvents_03(params)

sideviewRoot = [params.dataRoot '-sideview'];

if ~exist(sideviewRoot,'dir')
    fprintf('sideview folder is  missing\n');
end

audioevents_path = [sideviewRoot filesep params.dataFileTag '-audio.mat'];

if 1%~exist(audioevents_path,'file')
    
    sideview_videoPath = [sideviewRoot filesep params.dataFileTag '-sideview.mp4'];
    if ~exist(sideview_videoPath,'file')
        fprintf('sideview video is missing\n');
    end
    
    sideview_synchroLedPath = [sideviewRoot filesep params.dataFileTag '-synchroLED.txt'];
    if ~exist(sideview_synchroLedPath,'file')
        fprintf('sideview synchro LED bonsai  is missing\n');
    end
    
    sync_signal = load(sideview_synchroLedPath);
    sync_signal =sync_signal - mean(sync_signal);
    
    videoInfo = VideoReader(sideview_videoPath);
    video_nSamples = size(sync_signal,1);
    audiotrack = audioread(sideview_videoPath);
    audiotrack0 = audiotrack;
       
    n0 =size(audiotrack0 ,1);
    audio_samplingRate = n0 / videoInfo.Duration;
    t0 = 1:n0;
    t0 = t0 / audio_samplingRate;

    audiotrack = audiotrack - mean(audiotrack);
    audio_nSamples = size(audiotrack,1);
    audio_samplingRate = audio_nSamples / videoInfo.Duration;
    
    
    norm_sync_signal  = sync_signal >(max(sync_signal )/10);
    tmp =diff(norm_sync_signal );
    iSync  = find(tmp ==1)+1;
    dSync = diff(iSync );
    sync_gap = median(dSync);
    
    ii = find(dSync>(sync_gap*1.5));
    if ~isempty(ii)
        warning('synch signal lost')
    end
    
    % Cut Everything before first LED ON
    i1 = iSync(1);
    norm_sync_signal(1:i1-1)=[];
    audio_nSamplePerVideoFrame = floor(audio_nSamples / video_nSamples);
    audiotrack(1:(i1-1)*audio_nSamplePerVideoFrame)=[];
    
    nSyncSignal =size(norm_sync_signal ,1);
    tSyncSignal = 1:nSyncSignal;
    tSyncSignal = tSyncSignal / videoInfo.FrameRate;
    
    nAudio =size(audiotrack ,1);
    tAudio = 1:nAudio;
    tAudio = tAudio / audio_samplingRate;
    tAudio_period = 1/audio_samplingRate;
    
    
    [pks,locs] = findpeaks(audiotrack);
    audioTH = 4* std(audiotrack);
    ii = find(pks<audioTH);
    pks(ii)=[];
    locs(ii)=[];
    
    
    audioevents_sec =tAudio(locs);
    
    % figure()
    % hold on
    % plot(tAudio,audiotrack)
    % plot(audioevents_sec,1,'o')
    
    
    d_audioEvents_dx = diff(locs);
    
    audioDetectionGap_sec = .1;
    audioDetectionGap_idx = audioDetectionGap_sec * audio_samplingRate;
    
    ii = find(d_audioEvents_dx>audioDetectionGap_idx)+1;
    
    
    
    audioevents_sec = audioevents_sec(ii);
    audioevents_idx = locs(ii);
    
    
        
%     figure()
%     hold on
%     plot(tAudio,audiotrack,'b')
%     plot(audioevents_sec,audiotrack(audioevents_idx),'ok')
    
    
%      audio.iSync = iSync;        
%     audio.nSamplePerVideoFrame = audio_nSamplePerVideoFrame;
%     audio.samplingRate = audio_samplingRate;
%     audio.track0 = audiotrack0;
%     audio.n0 = n0;
%     audio.t0 = t0;    
%     
%     audio.track = audiotrack;
%     auido.n = nAudio;
%     audio.t =tAudio;
    audio.evt_idx = audioevents_idx;
    audio.evt_sec = audioevents_sec;
       
    % figure()
    % hold on
    % plot(tAudio,audiotrack)
    % plot(audioevents_sec,1,'o')
    
    save(audioevents_path,'audio');
    
else
    load(audioevents_path)
end


