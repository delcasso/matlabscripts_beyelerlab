function [dataRoot,outputFolder,apparatus,videoExt,analysisParameters]=getBatchAnalysisConfig_PB(batchID,machine,analysisParameters,outputFolder)



switch batchID
    
    case 'ICa-ICp_EPM'
        %% EPM SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20180809_ICa-ICp_G1\20180813_EPM';
        dataRoot{2} =  'Z:\PhotometryAndBehavior\01_DATA\20180827_ICa-ICp_G2\20180827_EPM';
        dataRoot{3} =  'Z:\PhotometryAndBehavior\01_DATA\20181106_ICa-ICp_G3\20181108_EPM';
        dataRoot{4} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190114_EPM';
        
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\EPM' ;
        end
        
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\EPM' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='EPM';
        apparatus{1}.Model='Ugo Basile version 1';
        apparatus{1}.OA_cm = 80; % Open Arms Envergure
        apparatus{1}.CA_cm = 75; % Closed Arms Envergure
        apparatus{1}.W_cm = 5;     % Arms Width
        
        apparatus{2}.type='EPM';
        apparatus{2}.Model='Ugo Basile version 1';
        apparatus{2}.OA_cm = 80; % Open Arms Envergure
        apparatus{2}.CA_cm = 75; % Closed Arms Envergure
        apparatus{2}.W_cm = 5;     % Arms Width
        
        apparatus{3}.type='EPM';
        apparatus{3}.Model='Ugo Basile version 2';
        apparatus{3}.OA_cm = 75; % Open Arms Envergure
        apparatus{3}.CA_cm = 75; % Closed Arms Envergure
        apparatus{3}.W_cm = 5.3;     % Arms Width
        
        apparatus{4}.type='EPM';
        apparatus{4}.Model='Ugo Basile version 2';
        apparatus{4}.OA_cm = 75; % Open Arms Envergure
        apparatus{4}.CA_cm = 75; % Closed Arms Envergure
        apparatus{4}.W_cm = 5.3;     % Arms Width
        
        apparatus{5}.type='EPM';
        apparatus{5}.Model='ForNormalization';
        apparatus{5}.OA_cm = 80; % Open Arms Envergure
        apparatus{5}.CA_cm = 80; % Closed Arms Envergure
        apparatus{5}.W_cm = 5;     % Arms Width
        
        %         apparatus{4}.type='EPM';
        %         apparatus{4}.Model='ForNormalization';
        %         apparatus{4}.OA_cm = 100; % Open Arms Envergure
        %         apparatus{4}.CA_cm = 100; % Closed Arms Envergure
        %         apparatus{4}.W_cm = 10;     % Arms Width
        
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        
    case 'ICa-ICp_NSFT'
        %% NSFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20180809_ICa-ICp_G1\20180817_NSFT';
        dataRoot{2} =  'Z:\PhotometryAndBehavior\01_DATA\20180827_ICa-ICp_G2\20180830_NSFT';
        dataRoot{3} =  'Z:\PhotometryAndBehavior\01_DATA\20181106_ICa-ICp_G3\20181113_NSFT';
        dataRoot{4} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190117_NSFT';
        
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\NSFT' ;
        end
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\NSFT' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='NSFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='NSFT';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        apparatus{3}.type='NSFT';
        apparatus{3}.Model='Pierre Feugas';
        apparatus{3}.side_cm = 60; % Open Arms Envergure
        
        apparatus{4}.type='NSFT';
        apparatus{4}.Model='Pierre Feugas';
        apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        
    case 'ICa-ICp_NSFT-CONTROL'
        %% OFT SPECIFIC PARAMETERS
        %% NSFT CONTROL IN HOMe CAGE SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190122_NSFT-CONTROL';
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\NSFT-CONTROL' ;
        end
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\NSFT-CONTROL' ;
        
        analysisParameters.apparatusNormalizationRequested = 0;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='HOMECAGE-FD';
        apparatus{1}.Model='Adrien Verite';
        apparatus{1}.side1_cm = 42.5; % Open Arms Envergure
        apparatus{1}.side2_cm = 26.4; % Open Arms Envergure
        
        videoExt{1}='avi';
        
    case 'ICa-ICp_OFT'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20180809_ICa-ICp_G1\20180814_OFT';
        dataRoot{2} =  'Z:\PhotometryAndBehavior\01_DATA\20180827_ICa-ICp_G2\20180828_OFT';
        dataRoot{3} =  'Z:\PhotometryAndBehavior\01_DATA\20181106_ICa-ICp_G3\20181109_OFT';
        dataRoot{4} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190115_OFT';
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\OFT' ;
        end
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\OFT' ;
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.5;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='OFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='OFT';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        apparatus{3}.type='OFT';
        apparatus{3}.Model='Pierre Feugas';
        apparatus{3}.side_cm = 60; % Open Arms Envergure
        
        apparatus{4}.type='OFT';
        apparatus{4}.Model='Pierre Feugas';
        apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        
    case 'ICa-ICp_OFT-CONTROL'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190123_OFT-CONTROL';
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\OFT-CONTROL' ;
        end
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\OFT-CONTROL' ;
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='OFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        
    case 'ICa-ICp_SUCROSE'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190124_SUCROSE';
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\SUCROSE' ;
        end
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\SUCROSE' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='OFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        
    case 'ICa-ICp_QUININE-SUCROSE'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190131_QUININE-SUCROSE';
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\QUININE-SUCROSE' ;
        end
        journalFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\QUININE-SUCROSE' ;
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='NSFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        
        
        
        
        
        
        
        
        
        
        
    case 'ICa-ICp_SI'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\PhotometryAndBehavior\01_DATA\20180809_ICa-ICp_G1\20180815_SI';
        dataRoot{2} =  'Z:\PhotometryAndBehavior\01_DATA\20180827_ICa-ICp_G2\20180829_SI';
        dataRoot{3} =  'Z:\PhotometryAndBehavior\01_DATA\20181106_ICa-ICp_G3\20181112_SI';
        dataRoot{4} =  'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190116_SI';
        
        if isempty(outputFolder)
            outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\SI' ;
        end
        journalFolder ='Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\SI' ;
        analysisParameters.socialDistance_cm = 5;
        analysisParameters.MouseCoordinatesCentroid = 'Nose';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='SI';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60;
        apparatus{1}.side2_cm = 30;
        apparatus{1}.cageDiameter_cm = 8;
        
        apparatus{2}.type='SI';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60;
        apparatus{2}.side2_cm = 30;
        apparatus{2}.cageDiameter_cm = 8;
        
        apparatus{3}.type='SI';
        apparatus{3}.Model='Pierre Feugas';
        apparatus{3}.side_cm = 60;
        apparatus{3}.side2_cm = 30;
        apparatus{2}.cageDiameter_cm = 8;
        
        apparatus{4}.type='SI';
        apparatus{4}.Model='Pierre Feugas';
        apparatus{4}.side_cm = 60;
        apparatus{4}.side2_cm = 30;
        apparatus{4}.cageDiameter_cm = 8;
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        
    case '20180926_ICa-ICp_AllTogether_TASTE'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_ICa-ICp_G1\20180905_TASTE';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_ICa-ICp_G2\20180904_TASTE';
                if isempty(outputFolder)
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_ICa-ICp_AllTogether\TASTE' ;
                end
                journalFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_ICa-ICp_AllTogether\TASTE' ;
        analysisParameters.MouseCoordinatesCentroid = 'Nose';
        analysisParameters.MapScale_cmPerBin = 0.25;
        analysisParameters.tasteDistance_cm = 2;
        analysisParameters.zoningMethod = 'port';
        %        analysisParameters.zoningMethod = 'quadrant';
        
        apparatus{1}.type='TASTE';
        apparatus{1}.Model='4 ports';
        apparatus{1}.side_cm = 18;
        apparatus{1}.side2_cm = 9;
        apparatus{1}.licko_led = 'yes';
        
        apparatus{2}.type='TASTE';
        apparatus{2}.Model='4 ports';
        apparatus{2}.side_cm = 18;
        apparatus{2}.side2_cm = 9;
        apparatus{2}.licko_led = 'yes';
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        
    case 'Luigi Bellocchio STR-SNr Exp1'
        %% NSFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180724_SIGNALCHECK';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180726_LOCOMOTION';
        dataRoot{3} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180817_LOCOMOTION';
        dataRoot{4} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180920_SALINE';
        dataRoot{5} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180925_SKF';
        dataRoot{6} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180925_SKF2';
        if isempty(outputFolder)
            outputFolder = 'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20181012_resultsAllCombined' ;
        end
        journalFolder = 'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20181012_resultsAllCombined' ;
        analysisParameters.apparatusZoningRatio = 3;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        videoExt{1}='';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        videoExt{5}='avi';
        videoExt{6}='avi';
        apparatus{1}.type='';
        apparatus{1}.Model='';
        apparatus{1}.side_cm = 0;
        
        apparatus{2}.type='OFT';
        apparatus{2}.Model='Sebastien Delcasso AB1';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        apparatus{3}.type='OFT';
        apparatus{3}.Model='Sebastien Delcasso AB1';
        apparatus{3}.side_cm = 60; % Open Arms Envergure
        
        apparatus{4}.type='OFT';
        apparatus{4}.Model='Sebastien Delcasso AB1';
        apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        apparatus{5}.type='OFT';
        apparatus{5}.Model='Sebastien Delcasso AB1';
        apparatus{5}.side_cm = 60; % Open Arms Envergure
        
        apparatus{6}.type='OFT';
        apparatus{6}.Model='Sebastien Delcasso AB1';
        apparatus{6}.side_cm = 60; % Open Arms Envergure
        
end









switch machine
    case 'SD_local'
        fprintf('Drive is C:\n');
        for i=1:size(dataRoot,2)
            dataRoot{i} = strrep(dataRoot{i},'Z:\','C:\');
        end
        outputFolder = strrep(outputFolder,'Z:\','C:\');
    case 'SD_remote'
        fprintf('Drive is Z:\n')
end


analysisParameters.journal = readtable([journalFolder filesep 'Journal.xlsx']);
analysisParameters = getConfig_PB(analysisParameters,outputFolder,apparatus,videoExt);







