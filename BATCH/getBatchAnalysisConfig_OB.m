function [dataRoot,outputFolder,apparatus,videoExt,analysisParameters]=getBatchAnalysisConfig_OB(batchID,machine)

switch batchID
    
    case 'ICa-ICp_CAV2Cre_EPM'
        %% EPM SPECIFIC PARAMETERS
        tpmStr = 'Z:\OptogeneticsAndBehavior\DATA';
        %         dataRoot{1} =  [tpmStr filesep '20190701_ICa-BLA-G1' filesep '20190701_EPM'];
        %         dataRoot{2} =  [tpmStr filesep '20190701_ICa-BLA-G2' filesep '20190701_EPM'];
        dataRoot{1} =  [tpmStr filesep '20190715_ICp-CeM-G1' filesep '20190715_EPM'];
        dataRoot{2} =  [tpmStr filesep '20190715_ICp-CeM-G2' filesep '20190715_EPM'];
        
        outputFolder = 'Z:\OptogeneticsAndBehavior\ANALYSIS\20190919_ICa-ICp_CAV2Cre\EPM' ;
        
        analysisParameters.journalPath = [outputFolder filesep 'Journal.xlsx'];
        
        analysisParameters.apparatusNormalizationRequested = 0;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='EPM';
        apparatus{1}.Model='Ugo Basile version 2';
        apparatus{1}.OA_cm = 75; % Open Arms Envergure
        apparatus{1}.CA_cm = 75; % Closed Arms Envergure
        apparatus{1}.W_cm = 5.3;     % Arms Width
        
        apparatus{2}.type='EPM';
        apparatus{2}.Model='Ugo Basile version 2';
        apparatus{2}.OA_cm = 75; % Open Arms Envergure
        apparatus{2}.CA_cm = 75; % Closed Arms Envergure
        apparatus{2}.W_cm = 5.3;     % Arms Width
        
        %         apparatus{3}.type='EPM';
        %         apparatus{3}.Model='Ugo Basile version 2';
        %         apparatus{3}.OA_cm = 75; % Open Arms Envergure
        %         apparatus{3}.CA_cm = 75; % Closed Arms Envergure
        %         apparatus{3}.W_cm = 5.3;     % Arms Width
        %
        %         apparatus{4}.type='EPM';
        %         apparatus{4}.Model='Ugo Basile version 2';
        %         apparatus{4}.OA_cm = 75; % Open Arms Envergure
        %         apparatus{4}.CA_cm = 75; % Closed Arms Envergure
        %         apparatus{4}.W_cm = 5.3;     % Arms Width
        
        %         apparatus{5}.type='EPM';
        %         apparatus{5}.Model='ForNormalization';
        %         apparatus{5}.OA_cm = 80; % Open Arms Envergure
        %         apparatus{5}.CA_cm = 80; % Closed Arms Envergure
        %         apparatus{5}.W_cm = 5;     % Arms Width
        
        videoExt{1}='avi';
        videoExt{2}='mp4';
        videoExt{3}='avi';
        videoExt{4}='mp4';
        
    case 'ICa-ICp_CAV2Cre_OFT'
        %% EPM SPECIFIC PARAMETERS
        tpmStr = 'Z:\OptogeneticsAndBehavior\DATA';
        %         dataRoot{1} =  [tpmStr filesep '20190701_ICa-BLA-G1' filesep '20190702_OFT'];
        %         dataRoot{2} =  [tpmStr filesep '20190701_ICa-BLA-G2' filesep '20190702_OFT'];
        dataRoot{1} =  [tpmStr filesep '20190715_ICp-CeM-G1' filesep '20190716_OFT'];
        dataRoot{2} =  [tpmStr filesep '20190715_ICp-CeM-G2' filesep '20190716_OFT'];
        
        outputFolder = 'Z:\OptogeneticsAndBehavior\ANALYSIS\20190919_ICa-ICp_CAV2Cre\OFT' ;
        analysisParameters.journalPath = [outputFolder filesep 'Journal.xlsx'];
        
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.5;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        resultats_fields = {'TimeCenter_sec','TimeBorder_sec'};
        
        apparatus{1}.type='OFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='OFT';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        %         apparatus{3}.type='OFT';
        %         apparatus{3}.Model='Pierre Feugas';
        %         apparatus{3}.side_cm = 60; % Open Arms Envergure
        %
        %         apparatus{4}.type='OFT';
        %         apparatus{4}.Model='Pierre Feugas';
        %         apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        videoExt{2}='mp4';
        %         videoExt{3}='avi';
        %         videoExt{4}='mp4';
                              
case 'ICa-ICp_CAV2Cre_RTPP'
        %% EPM SPECIFIC PARAMETERS
        tpmStr = 'Z:\OptogeneticsAndBehavior\DATA';
        %         dataRoot{1} =  [tpmStr filesep '20190701_ICa-BLA-G1' filesep '20190702_OFT'];
        %         dataRoot{2} =  [tpmStr filesep '20190701_ICa-BLA-G2' filesep '20190702_OFT'];
        dataRoot{1} =  [tpmStr filesep '20190715_ICp-CeM-G1' filesep '20190722_RTPPA'];
        dataRoot{2} =  [tpmStr filesep '20190715_ICp-CeM-G2' filesep '20190722_RTPP'];
        
        outputFolder = 'Z:\OptogeneticsAndBehavior\ANALYSIS\20190919_ICa-ICp_CAV2Cre\RTPP' ;
        analysisParameters.journalPath = [outputFolder filesep 'Journal.xlsx'];
                
        analysisParameters.apparatusNormalizationRequested = 0;
        analysisParameters.apparatusCenterZoneSize_propOfTotalArea = 0.5;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        resultats_fields = {'TimeCenter_sec','TimeBorder_sec'};
        
        apparatus{1}.type='OFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='OFT';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        %         apparatus{3}.type='OFT';
        %         apparatus{3}.Model='Pierre Feugas';
        %         apparatus{3}.side_cm = 60; % Open Arms Envergure
        %
        %         apparatus{4}.type='OFT';
        %         apparatus{4}.Model='Pierre Feugas';
        %         apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        videoExt{2}='mp4';
        %         videoExt{3}='avi';
        %         videoExt{4}='mp4';
        
        
                
        
        
        
        
        
        
    case '20181126_IC-SweetBitter-G2_NSFT'
        %% NSFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Opto&Behavior\DATA\20181126_IC-SweetBitter-G2\20181203_NSFT';
        outputFolder = 'Z:\Opto&Behavior\ANALYSIS\20181126_IC-SweetBitter-G2\NSFT' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusZoningRatio = 3;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='NSFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        
        
    case 'ICa-ICp_RTPP'
        %% NSFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Optogenetics&Behavior\DATA\20181126_ICa-ICp-G1\20181206_RTPP';
        dataRoot{2}=   'Z:\Optogenetics&Behavior\DATA\20190204_ICa-ICp-G2\20190214_RTPP' ;
        dataRoot{3}=   'Z:\Optogenetics&Behavior\DATA\20190225_ICa-ICp-G3\20190307_RTPP' ;
        dataRoot{4}=   'Z:\Optogenetics&Behavior\DATA\20190318_ICa-ICp-G4\20190326_RTPP';
        
        outputFolder = 'Z:\Optogenetics&Behavior\ANALYSIS\20181126_iCa-ICp_allGroups\RTPP';
        
        analysisParameters.journalPath = 'Z:\Optogenetics&Behavior\ANALYSIS\20181126_iCa-ICp_allGroups\Journal.xlsx';
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='RTPP';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='RTPP';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        apparatus{3}.type='RTPP';
        apparatus{3}.Model='Pierre Feugas';
        apparatus{3}.side_cm = 60; % Open Arms Envergure
        
        apparatus{4}.type='RTPP';
        apparatus{4}.Model='Pierre Feugas';
        apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        
end


switch machine
    case 'SD_local'
        fprintf('Drive is C:\n');
        for i=1:size(dataRoot,2)
            dataRoot{i} = strrep(dataRoot{i},'Z:\','C:\');
        end
        outputFolder = strrep(outputFolder,'Z:\','C:\');
    case 'SD_remote'
        fprintf('Drive is Z:\n')
end




% results = analysisParameters.journal;
% resultats_fields = results.Properties.VariableNames;
% [r,c]=size(results);
% nFields = size(resultats_fields,2);
% for iField=1:nFields
%     cmd = sprintf('results.%s = nan(r,1);',resultats_fields{iField});
%     eval(cmd);
% end
% analysisParameters.results = results;
analysisParameters = getConfig_OB(analysisParameters,outputFolder,apparatus,videoExt);
