close all;clear;clc

%% DATA LIBRARIES
dataRoot{1} =  '/media/delcasso/shares/nas1/Fiber-Photometry/01_DATA/20180809_IC-SweetBitter_G1/20180905_TASTE';
dataRoot{2} =  '/media/delcasso/shares/nas1/Fiber-Photometry/01_DATA/20180827_IC-SweetBitter_G2/20180904_TASTE';

videoExt{1}='avi';
videoExt{2}='avi';


%% PARAMETERS


%% TO PROCESS ALL FOLDERS
for iFolder=1:2
    p.dataRoot = dataRoot{iFolder} 
    p.videoExtension=videoExt{iFolder};
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);
    nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;
        [p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
       videoMean=getVideoMean(p);                              
    end
end

