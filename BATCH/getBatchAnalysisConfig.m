function [dataRoot,outputFolder,apparatus,videoExt,analysisParameters]=getBatchAnalysisConfig(batchID,machine,analysisParameters)


switch batchID
    
    case '20180926_iC-SweetBitter_AllTogether_EPM'
        %% EPM SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
        dataRoot{3} =  'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180316_EPM-preFC';
        dataRoot{4} =  'Z:\Fiber-Photometry\01_DATA\20181106_IC-SweetBitter_G3\20181108_EPM';
        
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.MouseCoordinatesCentroid = 'Body';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='EPM';
        apparatus{1}.Model='Ugo Basile version 1';
        apparatus{1}.OA_cm = 80; % Open Arms Envergure
        apparatus{1}.CA_cm = 75; % Closed Arms Envergure
        apparatus{1}.W_cm = 5;     % Arms Width
        
        apparatus{2}.type='EPM';
        apparatus{2}.Model='Ugo Basile version 1';
        apparatus{2}.OA_cm = 80; % Open Arms Envergure
        apparatus{2}.CA_cm = 75; % Closed Arms Envergure
        apparatus{2}.W_cm = 5;     % Arms Width
        
        apparatus{3}.type='EPM';
        apparatus{3}.Model='Cajal School, Noldus, version 1';
        apparatus{3}.OA_cm = 80; % Open Arms Envergure
        apparatus{3}.CA_cm = 80; % Closed Arms Envergure
        apparatus{3}.W_cm = 6;     % Arms Width
        
        apparatus{4}.type='EPM';
        apparatus{4}.Model='Cajal School, Noldus, version 1';
        apparatus{4}.OA_cm = 80; % Open Arms Envergure
        apparatus{4}.CA_cm = 80; % Closed Arms Envergure
        apparatus{4}.W_cm = 6;     % Arms Width        
        
        apparatus{5}.type='EPM';
        apparatus{5}.Model='ForNormalization';
        apparatus{5}.OA_cm = 80; % Open Arms Envergure
        apparatus{5}.CA_cm = 80; % Closed Arms Envergure
        apparatus{5}.W_cm = 5;     % Arms Width
        
        %         apparatus{4}.type='EPM';
        %         apparatus{4}.Model='ForNormalization';
        %         apparatus{4}.OA_cm = 100; % Open Arms Envergure
        %         apparatus{4}.CA_cm = 100; % Closed Arms Envergure
        %         apparatus{4}.W_cm = 10;     % Arms Width
        
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='mkv';
        videoExt{4}='avi';
        
    case '20180926_iC-SweetBitter_AllTogether_NSFT'
        %% NSFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180817_NSFT';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180830_NSFT';
        
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\NSFT' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusZoningRatio = 3;
        analysisParameters.MouseCoordinatesCentroid = 'Body';     
        analysisParameters.MapScale_cmPerBin = 0.5;
                
        apparatus{1}.type='NSFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='NSFT';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        
    case '20180926_iC-SweetBitter_AllTogether_OFT'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180814_OFT';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180828_OFT';
        dataRoot{3} =  'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180307_OFT-preFC';
        
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\OFT' ;
        
        analysisParameters.apparatusNormalizationRequested = 1;
        analysisParameters.apparatusZoningRatio = 3;
        analysisParameters.MouseCoordinatesCentroid = 'Body';    
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='OFT';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60; % Open Arms Envergure
        
        apparatus{2}.type='OFT';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        apparatus{3}.type='OFT';
        apparatus{3}.Model='Pierre Feugas';
        apparatus{3}.side_cm = 60; % Open Arms Envergure
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='avi';
                        
    case '20180926_iC-SweetBitter_AllTogether_SI'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180815_SI';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180829_SI';
        dataRoot{3} =  'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180315_SI-preFC';
        
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI' ;
        
        analysisParameters.socialDistance_cm = 5;
        analysisParameters.MouseCoordinatesCentroid = 'Nose';
        analysisParameters.MapScale_cmPerBin = 0.5;
        
        apparatus{1}.type='SI';
        apparatus{1}.Model='Pierre Feugas';
        apparatus{1}.side_cm = 60;
        apparatus{1}.side2_cm = 30;
        apparatus{1}.cageDiameter_cm = 8;
        
        apparatus{2}.type='SI';
        apparatus{2}.Model='Pierre Feugas';
        apparatus{2}.side_cm = 60;
        apparatus{2}.side2_cm = 30;
        apparatus{2}.cageDiameter_cm = 8;              
        
        apparatus{3}.type='SI';
        apparatus{3}.Model='Pierre Feugas';
        apparatus{3}.side_cm = 60;
        apparatus{3}.side2_cm = 30;
        apparatus{2}.cageDiameter_cm = 8;              
        
        videoExt{1}='avi';
        videoExt{2}='avi';
        videoExt{3}='mkv';

    case '20180926_iC-SweetBitter_AllTogether_TASTE'
        %% OFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180905_TASTE';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180904_TASTE';
        
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\TASTE' ;        
        analysisParameters.MouseCoordinatesCentroid = 'Nose';
       analysisParameters.MapScale_cmPerBin = 0.25;      
       analysisParameters.tasteDistance_cm = 2;
       analysisParameters.zoningMethod = 'port';
%        analysisParameters.zoningMethod = 'quadrant';
       
        apparatus{1}.type='TASTE';
        apparatus{1}.Model='4 ports';
        apparatus{1}.side_cm = 18;
        apparatus{1}.side2_cm = 9;
        apparatus{1}.licko_led = 'yes';

        apparatus{2}.type='TASTE';
        apparatus{2}.Model='4 ports';
        apparatus{2}.side_cm = 18;
        apparatus{2}.side2_cm = 9;
        apparatus{2}.licko_led = 'yes';

        videoExt{1}='avi';
        videoExt{2}='avi';
                
    case 'Luigi Bellocchio STR-SNr Exp1'
        %% NSFT SPECIFIC PARAMETERS
        dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180724_SIGNALCHECK';
        dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180726_LOCOMOTION';
        dataRoot{3} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180817_LOCOMOTION';
        dataRoot{4} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180920_SALINE';
        dataRoot{5} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180925_SKF';
        dataRoot{6} =  'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20180925_SKF2';
        
        outputFolder = 'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20181012_resultsAllCombined' ;
        
        analysisParameters.apparatusZoningRatio = 3;
        analysisParameters.MouseCoordinatesCentroid = 'Body';    
        analysisParameters.MapScale_cmPerBin = 0.5;
                
        videoExt{1}='';
        videoExt{2}='avi';
        videoExt{3}='avi';
        videoExt{4}='avi';
        videoExt{5}='avi';
        videoExt{6}='avi';
        apparatus{1}.type='';
        apparatus{1}.Model='';
        apparatus{1}.side_cm = 0;
        
        apparatus{2}.type='OFT';
        apparatus{2}.Model='Sebastien Delcasso AB1';
        apparatus{2}.side_cm = 60; % Open Arms Envergure
        
        apparatus{3}.type='OFT';
        apparatus{3}.Model='Sebastien Delcasso AB1';
        apparatus{3}.side_cm = 60; % Open Arms Envergure
        
        apparatus{4}.type='OFT';
        apparatus{4}.Model='Sebastien Delcasso AB1';
        apparatus{4}.side_cm = 60; % Open Arms Envergure
        
        apparatus{5}.type='OFT';
        apparatus{5}.Model='Sebastien Delcasso AB1';
        apparatus{5}.side_cm = 60; % Open Arms Envergure
        
        apparatus{6}.type='OFT';
        apparatus{6}.Model='Sebastien Delcasso AB1';
        apparatus{6}.side_cm = 60; % Open Arms Envergure
        
end









switch machine
    case 'SD_local'
        fprintf('Drive is C:\n');
        for i=1:size(dataRoot,2)
            dataRoot{i} = strrep(dataRoot{i},'Z:\','C:\');
        end
        outputFolder = strrep(outputFolder,'Z:\','C:\');
    case 'SD_remote'
        fprintf('Drive is Z:\n')
end