function p = getConfig_PB(p,outputFolder,apparatus,videoExt)


%% PARAMETERS
c = clock();
p.batch_ouputFile = [outputFolder filesep sprintf('%04d%02d%02d-%02d%02d%02d',c(1),c(2),c(3),c(4),c(5),floor(c(6))) '.txt'];
%warning p.batch_outputFile is deprecated, I am replacing it with an
%extansion of the journal in results

p.bach_resultFile =  [outputFolder filesep sprintf('results_%04d%02d%02d-%02d%02d%02d',c(1),c(2),c(3),c(4),c(5),floor(c(6))) '.xlsx'];
p.batch_ouputFile_headerWritten=0;
p.outputFolder=outputFolder;
p.figureFolder = [p.outputFolder filesep 'figures'];
p.apparatus=apparatus;
p.videoExt=videoExt;
p.cameraMode = 'synchronous';
p.ledDetectionThreshold = 50; %percent of led signal max
p.HamamatsuFrameRate_Hz= 20;
p.behaviorCameraFrameRate_Hz=20;
p.speedThreshold=50; % Use to clean the position data automatically, based on abnormal animal speed
p.body2licko_distanceMax_cm = 6;
p.savePDF = 0;
p.saveFIG = 0;
p.savePNG = 0;
p.forceRedrawing = 0;
p.forceBehavioralStart = 0;
p.getVideoTrackingData_plot=0;
p.getVideoTrackingData_force=0;
p.forceGetBodyParts=0;
p.OccupancyMap_sigmaGaussFilt=5;
p.PhotometrySignalMap_sigmaGaussFilt=5;
p.deltaFF_slidingWindowWidth = 1200;
p.lookingForMouse = '';
p.bonzaiDone = 1;
p.eventBasedAnalysisEdges_msec = [-30000:50:30000];
% p.eventBasedAnalysisEdges_msec = [-60000:50:60000];
p.firstLickTh_msec = 500;
p.results = p.journal;

save([p.outputFolder filesep 'analysisParams.mat'],'p');