clear all
close all
p = 'S:\_Maxime\Experiment\M2_project\collateral\Picture\ICp';
tic

if ~ exist([p filesep 'db.mat'],'file')
    
    l = dir([p filesep '*.tif']);n = size(l,1);
    db.pic_green = nan(2048,2048,n);
    db.pic_names = {}
    
    for i=1:n
        db.pic_names{i} =  l(i).name;
        clear tmp
        tmp = imread([p filesep l(i).name]);
        tmp(1500:2048,1500:2048,:) = 0;
        tmp =tmp(:,:,2);
        tmp = double(tmp);
        tmp = tmp ./(power(2,8)-1);
        db.pic_green(:,:,i) = tmp;
    end
    clear tmp;
    save([p filesep 'db.mat'],'db');
    
end

load([p filesep 'db.mat']);
n = size(db.pic_names,2);

histEdges = 0:0.025:1;
histRes = nan(n,size(histEdges,2)-1);
for i=1:n
    tmp = reshape(db.pic_green(:,:,i),1,2048*2048);
    histRes(i,:) = histcounts(tmp,histEdges,'Normalization','probability');
end

threshold_plot(histEdges,histRes,db);

for i=1:n
threshold_plot_cellbycell(histEdges,histRes,db,i,p)
end


fod = fopen('res.xls','w');
for i=1:n
    tmp=db.pic_green(:,:,i);    
    tmp = tmp>0;
    fprintf(fod,'%s\t%d\n',db.pic_names{i},sum(sum(tmp)));
end
fclose(fod);



function threshold_plot_cellbycell(histEdges,histRes,db,iPic,p)

n = size(db.pic_names,2);
x = histEdges(1:size(histEdges,2)-1)

fig=figure()

cmap = zeros(1024,3);
cmap(:,2) = (1:1024)/1024;
colormap(cmap)

subplot(2,3,1)
hold on
title('raw')
axis off
imagesc(db.pic_green(:,:,iPic))

subplot(2,3,2)
hold on
xlabel('Pixel intensity')
ylabel('Pixel cum sum (million)')
y = histRes(iPic,:)';
y_million = y .*(2048*2048)/1000000
plot(x,cumsum(y_million))

subplot(2,3,3)
hold on
xlabel('Pixel intensity')
ylabel('Pixel cum sum slope')
y = histRes(iPic,:)';
y_million = y .*(2048*2048)/1000000
plot(x(1:end-1),diff(cumsum(y_million)))


th = [0.3 0.5 0.8];
for i=1:3
    subplot(2,3,i+3)
    hold on
    title(sprintf('th=%2.2f',th(i)))
    axis off
    a = db.pic_green(:,:,iPic);
    ii = find(a<th(i));
    a(ii)=0;
    imagesc(a)
end

n=db.pic_names{iPic};
[filepath,name,ext] = fileparts(n);
print(fig,[p filesep 'image_analysis' filesep name '.png'],'-dpng')
close(fig);

end


% end
    









function threshold_plot(histEdges,histRes,db)

n = size(db.pic_names,2);
legend_={}
for i=1:n
    legend_{i} = num2str(i);
end



figure()
ax1=subplot(3,3,1)
hold on
ylim([0 1]);xlim([0 1]);
xlabel('Pixel intensity')
ylabel('Pixel proportion')
% legend(legend_);

    
ax2=subplot(3,3,2)
hold on
ylim([0 1]);xlim([0 1]);
ylabel('Pixel intensity')
xlabel('Pixel proportion')

ax3=subplot(3,3,3)
hold on
ylim([0 1])
ylabel('Pixel intensity')
xlabel('Pixel number (million)')
    
ax4=subplot(3,3,4)
hold on
ylabel('Pixel intensity')
xlabel('Pixel cum sum (million)')

ax5=subplot(3,3,5)
hold on
xlabel('Pixel intensity')
ylabel('Pixel cum sum (million)')

ax6=subplot(3,3,6)
hold on
xlabel('Pixel intensity')
ylabel('Pixel cum sum slope')

ax7=subplot(3,3,[7 8 9])
hold on
xlabel('Pixel intensity')
ylabel('Pixel cum sum slope')

x = histEdges(1:size(histEdges,2)-1)
  
for iPic = 1:n
    y = histRes(iPic,:)';          
    axes(ax1);
    plot(x,y);    
    axes(ax2);
    plot(y,x);    
    axes(ax3);
    y_million = y .*(2048*2048)/1000000
    plot(y_million,x);    
    axes(ax4);
    plot(cumsum(y_million),x)    
    axes(ax5);
    plot(x,cumsum(y_million))    
    axes(ax6);
    plot(x(1:end-1),diff(cumsum(y_million)))    
    axes(ax7);
    plot(cumsum(y_million)+ (5*iPic),x)     
end

end



