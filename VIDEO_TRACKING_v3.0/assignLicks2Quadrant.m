function experiment=assignLicks2Quadrant(experiment)

vData = experiment.vData;
p=experiment.p;
inQuadrant = experiment.vData.inQuadrant;

lickSignal  = vData.optoPeriod;
zoneSignal  = vData.inZone;
lickSignal = lickSignal - min(lickSignal);
lickSignal = lickSignal /max(lickSignal);
lickSignal = lickSignal>0.5;
diff_lickSignal = diff(lickSignal);
iLicks = find(diff_lickSignal==1);

experiment.vData.licks.iFrame = iLicks;
experiment.vData.licks.iQuandrant = inQuadrant(iLicks);

 tastePlot(experiment,'licks-raw')
 
 % Remove lick when body is to far from well (> p.body2licko_distanceMax_cm  = 4)
iLicks = experiment.vData.licks.iFrame;
iQuandrantLick = experiment.vData.licks.iQuandrant;
x = vData.bodyX_cmS;
y = vData.bodyY_cmS;
% x = vData.xF_cmS;
% y = vData.yF_cmS;

 toRemove = [];
 for i = 1:size(iLicks,1)
     iFrame = iLicks(i);
     iQ = iQuandrantLick(i);
     xMouse = x(iFrame);yMouse = y(iFrame);
     xWell = vData.landmarks_cmS.x(iQ);yWell = vData.landmarks_cmS.y(iQ);
     d = sqrt( (xMouse-xWell)^2 + (yMouse-yWell)^2 );
     j=iLicks(i);
     if d>p.body2licko_distanceMax_cm
        toRemove = [toRemove i];
     end
 end
 
 if ~isempty(toRemove)
     iLicks(toRemove)=[];iQuandrantLick(toRemove)=[];
 end
 
 % We want to detect the first lick so if dt_licks < p.firstLickTh_msec  % 500 ms,
 % the second is not  a first lick;

p.firstLickTh_nFrames = (p.firstLickTh_msec/1000) * p.HamamatsuFrameRate_Hz;
diff_iLicks = diff(iLicks);
toRemove = [];
toRemove = find(diff_iLicks< p.firstLickTh_nFrames );
  
 if ~isempty(toRemove)
     iLicks(toRemove)=[];iQuandrantLick(toRemove)=[];
 end
    
experiment.vData.licks.iFrame=iLicks;
experiment.vData.licks.iQuandrant=iQuandrantLick;

n = size(experiment.vData.landmarksStr,2);

for i=1:n
    eventCategory(i).name = experiment.vData.landmarksStr{i};
    eventCategory(i).id = i;
    ii = find(iQuandrantLick==i);
    eventCategory(i).frame_idx = iLicks(ii);
    eventCategory(i).nb = size(ii,1);
    eventCategory(i).perc = (eventCategory(i).nb / size(iQuandrantLick,1)) * 100.0;
end

experiment.eventCategory= eventCategory;

 tastePlot(experiment,'licks-cleaned')

end