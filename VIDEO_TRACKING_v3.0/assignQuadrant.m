function experiment=assignQuadrant(experiment)
vData = experiment.vData;p=experiment.p;
% x = vData.bodyX_cmS;y = vData.bodyY_cmS;
x = vData.xF_cmS;y = vData.yF_cmS;
nPos = size(x,1);
LR = x>0;TD = y>0;
inQuadrant = zeros(nPos,1);
inQuadrant(LR & TD)=3;
inQuadrant(LR & ~TD)=2;
inQuadrant(~LR & ~TD)=1;
inQuadrant(~LR & TD)=4;
experiment.vData.inQuadrant = inQuadrant;
