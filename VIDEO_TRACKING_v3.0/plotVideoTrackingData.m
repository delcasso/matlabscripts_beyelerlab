% function plotVideoTackingData()

clear all
clc
close all

f=figure();

params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180905_TASTE';
params.dataFileTag = 'F260';
bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
params.outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI' ;
params.figureFolder = [params.outputFolder filesep 'figures'];
params.videoExtension ='avi';
params.MouseCoordinatesCentroid = 'Nose';

vData = getVideoTrackingData(params);

x = vData.bodyX;y = vData.bodyY;
xL = vData.xL;yL = vData.yL;
xR = vData.xR;yR = vData.yR;
xF = vData.xF;yF = vData.yF;
xB = vData.xB;yB = vData.yB;

nSamples = size(x,1);

vidObj = VideoReader([params.dataRoot filesep params.dataFileTag '.' params.videoExtension]);

i1=1691;
vidObj.CurrentTime=84.55;

% i1=1;
% vidObj.CurrentTime=0.05;

for i=i1:nSamples        
    clf(f);
    hold on
    axis off      
    vidFrame = readFrame(vidObj);
    image(vidFrame);     
    
    xlim([0 size(vidFrame,2)]);
    ylim([0  size(vidFrame,1)]);
    
    t=vidObj.CurrentTime;
    msg = sprintf('t = %2.2f, fr = %d',t,i);
   fprintf('%s\n',msg)
    text(10,1000,msg,'color',[1 1 1]);
    
    currAxes.Visible = 'off';
    if i>20
        jj=i:i+50;
        plot(x(jj),y(jj),'Marker','o','MarkerEdgeColor','none','MarkerFaceColor','m','MarkerSize',2,'LineStyle','none');
    end   
    patch([xL(i) xF(i) xR(i)],[yL(i) yF(i) yR(i)],'m');
    patch([xL(i) xB(i) xR(i)],[yL(i) yB(i) yR(i)],'k');   
    pause(.001)
            
end
