% function bonsai_output=getVideoTrackingData(params)

clear all;clc;close all

f=figure();

flipNeeded=0;

params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180905_TASTE';
params.dataFileTag = 'F260';
bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
params.outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI' ;
params.figureFolder = [params.outputFolder filesep 'figures'];
params.videoExtension ='avi';
params.MouseCoordinatesCentroid = 'Nose';

bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
bonsai_output = getBonsaiData(bonsaiPath);
vidObj = VideoReader([params.dataRoot filesep params.dataFileTag '.' params.videoExtension]);

nSamples = size(bonsai_output.mouseX,1);

% i1=1691;vidObj.CurrentTime=84.55;
i1=1999;vidObj.CurrentTime=100.00;
% i1=1;vidObj.CurrentTime=0.05;

[x0,y0,maxL0,minL0,theta0,xF0,yF0,xB0,yB0,xL0,yL0,xR0,yR0]=depackBonsaiData(bonsai_output,i1-1,flipNeeded);

for i=i1:nSamples
    clf(f);
    hold on
    vidFrame = readFrame(vidObj);image(vidFrame);
    xlim([0 size(vidFrame,2)]);ylim([0  size(vidFrame,1)]);
    
    [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai_output,i,flipNeeded);
       
    dC=getDistanceBetweenObjects(x0,y0,x,y);    
    
    if dC>10
        [centroid_thetaDiff,centroid_rhoDiff] = cart2pol(x-x0,y-y0);
        [F_thetaDiff,F_rhoDiff] = cart2pol(xF-x,yF-y);
        if (abs(F_thetaDiff-centroid_thetaDiff)>(pi/2))
            msg = 'Walking Bckward ?';
            xTmp = xF;xF = xB; xB=xTmp;
            yTmp = yF;yF = yB; yB=yTmp;
            flipNeeded = flipNeeded+pi;            
        end
    end
    
    
    dF=getDistanceBetweenObjects(xF0,yF0,xF,yF);
    dB=getDistanceBetweenObjects(xB0,yB0,xB,yB);
    dFB=getDistanceBetweenObjects(xF,yF,xB,yB);
    dFB0=getDistanceBetweenObjects(xF0,yF0,xB0,yB0);
    
    if (dF > dFB0*4/5) && (dB>dFB0*4/5)
        msg = '!!!';
        xTmp = xF;xF = xB; xB=xTmp;
        yTmp = yF;yF = yB; yB=yTmp;
        flipNeeded = flipNeeded+pi;
    else
        msg = '';
    end    
    
    
    plot([x xF],[y yF],'m');
    plot([x xB],[y yB],'w');
    
    t=vidObj.CurrentTime;
    %     msg = [msg sprintf('t = %2.2f, fr = %d, dF = %2.2f, dB = %2.2f, dFB = %2.2f',t,i,dF,dB,dFB)];
    msg = [msg sprintf('t = %2.2f, fr = %d, dC=%2.2f, F[%2.2f,%2.2f], B[%2.2f,%2.2f]',t,i,dC,xF,yF,xB,yB)];
    fprintf('%s\n',msg);
    text(10,1000,msg,'color',[1 1 1])
    plot(x,y,'gx');
    
    

    
    pause(.01)
    
    
    
    x0=x;y0=y;
    xF0=xF;yF0=yF;
    xB0=xB;yB0=yB;
    
end

function bonsai_output = getBonsaiData(bonsaiPath)

%% get Bonsai Data
bonsai_output=[];
if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    bonsai_output.bodyX = bonsai_output.mouseX;
    bonsai_output.bodyY = bonsai_output.mouseY;
    rmfield(bonsai_output,'mouseX');rmfield(bonsai_output,'mouseY');
end
end
function [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai_output,i,flipNeeded)
x=bonsai_output.bodyX(i);
y=bonsai_output.bodyY(i);
maxL=bonsai_output.mouseMajorAxisLength(i);
minL=bonsai_output.mouseMinorAxisLength(i);
theta=bonsai_output.mouseAngle(i) + flipNeeded;
[xF,yF] = pol2cart(theta,maxL/2);xF=xF+x;yF=yF+y;
[xB,yB] = pol2cart(theta+pi,maxL/2);xB=xB+x;yB=yB+y;
[xL,yL] = pol2cart(theta+pi/2,minL/2);xL=xL+x;yL=yL+y;
[xR,yR] = pol2cart(theta-pi/2,minL/2);xR=xR+x;yR=yR+y;
end
function distance=getDistanceBetweenObjects(x1,y1,x2,y2)
dx = x2-x1;dy = y2-y1;distance = sqrt((dx.*dx)+(dy.*dy));
end

