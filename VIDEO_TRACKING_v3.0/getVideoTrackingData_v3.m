function [params,vData]=getVideoTrackingData_v3(params,vData)

% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180905_TASTE';
% params.dataFileTag = 'F260';
% bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
% params.outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI' ;
% params.figureFolder = [params.outputFolder filesep 'figures'];
% params.videoExtension ='avi';
% params.MouseCoordinatesCentroid = 'Nose';

filename=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
if exist(filename,'file')
    params.videoTrackingDataType = 'bonsai';
    vData=getCleanBonsaiData(params);
else
    filename=[params.dataRoot filesep params.dataFileTag '-deeplabcutFiltered.csv'];
    if exist(filename,'file')
    params.videoTrackingDataType = 'deeplabcutFiltered';
    vData = getDeepLabCutData(params);
    else
        warning('there is no videotrack file for this mouse !!');
        pause
    end
end



end
