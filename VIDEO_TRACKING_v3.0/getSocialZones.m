function boundaries=getSocialZones(params)

% matlabFile = [params.dataRoot filesep params.dataFileTag '-' matFileSuffix '.mat'];

% suffix =matFileSuffix;

% figureSubFolder = [params.figureFolder filesep matFileSuffix];
% if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end
% figName = [figureSubFolder filesep params.dataFileTag '-' matFileSuffix '.jpeg'];

% clear;
% clc;
% close all;


% params.dataRoot='Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180315_SI-preFC';
% params.dataFileTag='F002';
% params.outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI' ;
% params.figureFolder = [params.outputFolder filesep 'figures'];
% params.videoExtension ='mkv';

% if ~exist(matlabFile,'file')

videoTrackingData = getVideoTrackingData(params);
bg=getBackGroundQuick(params);



videoResolution = size(bg);
zoom = floor(videoResolution(2)/5);




boundaryFile = [params.dataRoot filesep params.dataFileTag '-boundaries.mat'];


if ~exist(boundaryFile)

    f1=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    hold on
    axis equal
    axis off
    set(gca,'Ydir','reverse')
    colormap(gray(1024));
    
    
imagesc(bg);
text(10,10,'OBJECT CENTER','color',[0.5 1 0.5]);

[object.X,object.Y]=ginput(1);
ylim([object.Y-zoom object.Y+zoom])
xlim([object.X-zoom object.X+zoom])
[object.X,object.Y]=ginput(1);
ylim([0 size(bg,1)])
xlim([0 size(bg,2)])

imagesc(bg);
text(10,10,'JUVENILE CENTER','color',[0.5 1 0.5]);

[juv.X,juv.Y]=ginput(1);
ylim([juv.Y-zoom juv.Y+zoom])
xlim([juv.X-zoom juv.X+zoom])
[juv.X,juv.Y]=ginput(1);
ylim([0 size(bg,1)])
xlim([0 size(bg,2)])

imagesc(bg);
text(10,10,'OFT CORNERS','color',[0.5 1 0.5]);

for i=1:4
    [x(i),y(i)]=ginput(1);
end

for i=1:4
    ylim([y(i)-zoom y(i)+zoom])
    xlim([x(i)-zoom x(i)+zoom])
    [x(i),y(i)]=ginput(1);
end

[x,y]=reorderPointsClockwise(x,y);

ylim([0 size(bg,1)])
xlim([0 size(bg,2)])

boundaries.arena.x=x;
boundaries.arena.y=y;
boundaries.object.x=object.X;
boundaries.object.y=object.Y;
boundaries.juvenile.x=juv.X;
boundaries.juvenile.y=juv.Y;
save(boundaryFile,'boundaries');

else
    load(boundaryFile);
end

% imagesc(bg);
% circleDiam = 25;
% h = rectangle('Position',[boundaries.object.x-circleDiam/2 boundaries.object.y-circleDiam/2 circleDiam circleDiam],'Curvature',[1,1]);
% h = rectangle('Position',[boundaries.juvenile.x-circleDiam/2 boundaries.juvenile.y-circleDiam/2 circleDiam circleDiam],'Curvature',[1,1]);
% 
% for i=1:4
%     plot(boundaries.arena.x(i),boundaries.arena.y(i),'+')
%     text(boundaries.arena.x(i)+10,boundaries.arena.y(i)+10,num2str(i),'color',[0.5 1 0.5]);
% end




% 
% 
% pause(2);
% cmd=sprintf('%s.x=x;',matFileSuffix);eval(cmd);
% cmd=sprintf('%s.y=y;',matFileSuffix);eval(cmd);
% cmd=sprintf('save(matlabFile,''%s'');',matFileSuffix);eval(cmd);
% print(f1,figName,'-djpeg');
% close (f1);
% cmd=sprintf('corners=%s;',matFileSuffix);eval(cmd);
