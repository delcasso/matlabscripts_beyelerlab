function bonsai_output=getVideoTrackingData(params)

bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];

bonsai_output=[];
if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    bonsai_output.bodyX = bonsai_output.mouseX;
    bonsai_output.bodyY = bonsai_output.mouseY;
    rmfield(bonsai_output,'mouseX');rmfield(bonsai_output,'mouseY');

    
    mouseBody=getBodyPosition_v2(params,bonsai_output);
    bonsai_output.xF=[mouseBody(:).xF]';
    bonsai_output.yF=[mouseBody(:).yF]';
    bonsai_output.xB=[mouseBody(:).xB]';
    bonsai_output.yB=[mouseBody(:).yB]';
    bonsai_output.xL=[mouseBody(:).xL]';
    bonsai_output.yL=[mouseBody(:).yL]';
    bonsai_output.xR=[mouseBody(:).xR]';
    bonsai_output.yR=[mouseBody(:).yR]';
    
    switch params.MouseCoordinatesCentroid
        case 'Body'
            bonsai_output.mainX=bonsai_output.bodyX;
            bonsai_output.mainY=bonsai_output.bodyY;
        case 'Nose'
            bonsai_output.mainX=bonsai_output.xF;
            bonsai_output.mainY=bonsai_output.yF;
    end
    
        bonsai_output = getDistance(bonsai_output);    
        
end
end