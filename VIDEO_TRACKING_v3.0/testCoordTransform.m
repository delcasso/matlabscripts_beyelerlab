xL = vData.landmarks.x;
yL = vData.landmarks.y;

figure();

subplot(3,3,1)
hold on
axis equal
ylim([0 1024]);
xlim([0 1280]);
for i=1:4
    plot(xL(i),yL(i),'+');
    text(xL(i),yL(i),vData.landmarksStr{i});
end

subplot(3,3,2)
hold on
axis equal
ylim([0 1024]);
xlim([0 1280]);
plot(x,y);

p.apparatus.ref_pix = dX;
[xR,yR] = rotateXY(x,y,-rotAngle,xC,yC);
tmp = p.apparatus.side2_cm;
tmp = 2 * (tmp/sqrt(2));
p.apparatus.ref_cm = tmp;
 coef_pix2cm = p.apparatus.ref_cm/p.apparatus.ref_pix; 
xS = xR .* coef_pix2cm;yS = yR .* coef_pix2cm;

subplot(3,3,3)
hold on
axis equal
ylim([0 1024]);
xlim([0 1280]);
plot(xR,yR);

subplot(3,3,4)
hold on
axis equal
% ylim([-40 40]);
% xlim([-40 40]);
plot(xS,yS);

[xLR,yLR] = rotateXY(xL',yL',-rotAngle,xC,yC);

subplot(3,3,5)
hold on
axis equal
ylim([0 1024]);
xlim([0 1280]);
for i=1:4
    plot(xLR(i),yLR(i),'+');
    text(xLR(i),yLR(i),vData.landmarksStr{i});
end