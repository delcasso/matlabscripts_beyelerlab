
clear
clc
close all

d = 'Z:\PhotometryAndBehavior\01_DATA\20190107_ICa-ICp_G4\20190123_OFT-CONTROL';
l = dir([d filesep '*.avi']);
n = size(l,1);
figure();



for i=1:n
    subplot(4,4,i)
    hold on    
    axis equal
    xlim([0 1200])
    ylim([0 1200])
    f = l(i).name
    title(f)
    [filepath,name,ext] = fileparts(f)
    bonsaiPath=[d filesep name '-bonsai.txt'];
    if exist(bonsaiPath,'file')
        bonsai_output = getBonsaiDataForCheckBonsai(bonsaiPath);
        nSamples = size(bonsai_output.bodyX,1);
        plot(bonsai_output.mouseX,bonsai_output.mouseY);
    end
end

