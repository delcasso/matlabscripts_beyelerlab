
function vData=getCleanBonsaiData(params)

bonsaiClean=[params.dataRoot filesep params.dataFileTag '-bonsaiClean.mat'];
if ~exist(bonsaiClean,'file') || params.forceGetBodyParts
    plt = params.getVideoTrackingData_plot;
    
    if plt
        f=figure();
    end
    
    
    bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
    if exist(bonsaiPath,'file')
        vData = getBonsaiData(bonsaiPath);
    else
        %         bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
        %         if exist(bonsaiPath,'file')
        %         end
    end
    nSamples = size(vData.bodyX,1);
    flipNeeded = 0;
    
    if plt
        vidObj = VideoReader([params.dataRoot filesep params.dataFileTag '.' params.videoExtension]);
    end
    
    %     i1=1999;vidObj.CurrentTime=100.00;[x0,y0,maxL0,minL0,theta0,xF0,yF0,xB0,yB0,xL0,yL0,xR0,yR0]=depackBonsaiData(bonsai_output,i1-1,flipNeeded);
    
    i1=1;[x0,y0,maxL0,minL0,theta0,xF0,yF0,xB0,yB0,xL0,yL0,xR0,yR0]=depackBonsaiData(vData,1,flipNeeded);
    
    
    for i=i1:nSamples
        if plt
            clf(f);
            hold on
            vidFrame = readFrame(vidObj);image(vidFrame);
            xlim([0 size(vidFrame,2)]);ylim([0  size(vidFrame,1)]);
        end
        
        [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(vData,i,flipNeeded);
        
        dC=getDistanceBetweenObjects(x0,y0,x,y);
        
        if dC>10
            [centroid_thetaDiff,centroid_rhoDiff] = cart2pol(x-x0,y-y0);
            [F_thetaDiff,F_rhoDiff] = cart2pol(xF-x,yF-y);
            if (abs(F_thetaDiff-centroid_thetaDiff)>(pi/2))
                msg = 'Walking Bckward ?';
                xTmp = xF;xF = xB; xB=xTmp;
                yTmp = yF;yF = yB; yB=yTmp;
                flipNeeded = flipNeeded+pi;
            end
        end
        
        
        dF=getDistanceBetweenObjects(xF0,yF0,xF,yF);
        dB=getDistanceBetweenObjects(xB0,yB0,xB,yB);
        dFB=getDistanceBetweenObjects(xF,yF,xB,yB);
        dFB0=getDistanceBetweenObjects(xF0,yF0,xB0,yB0);
        
        if (dF > dFB0*4/5) && (dB>dFB0*4/5)
            msg = '!!!';
            xTmp = xF;xF = xB; xB=xTmp;
            yTmp = yF;yF = yB; yB=yTmp;
            flipNeeded = flipNeeded+pi;
        else
            msg = '';
        end
        
        if plt
            plot([x xF],[y yF],'m');
            plot([x xB],[y yB],'w');
            
            t=vidObj.CurrentTime;
            %     msg = [msg sprintf('t = %2.2f, fr = %d, dF = %2.2f, dB = %2.2f, dFB = %2.2f',t,i,dF,dB,dFB)];
            msg = [msg sprintf('t = %2.2f, fr = %d, dC=%2.2f, F[%2.2f,%2.2f], B[%2.2f,%2.2f]',t,i,dC,xF,yF,xB,yB)];
            %             fprintf('%s\n',msg);
            text(10,1000,msg,'color',[1 1 1])
            plot(x,y,'gx');
            pause(.001)
        end
        
        x0=x;y0=y;
        xF0=xF;yF0=yF;
        xB0=xB;yB0=yB;
        
        vData.xF(i) = xF;
        vData.yF(i) = yF;
        vData.xB(i) = xB;
        vData.yB(i) = yB;
    end
    
    vData.xF = vData.xF';
    vData.yF = vData.yF';
    vData.xB = vData.xB';
    vData.yB = vData.yB';
    
    save(bonsaiClean,'bonsai_output');
else
    load(bonsaiClean);
end

switch params.MouseCoordinatesCentroid
    case 'Body'
        vData.mainX=vData.bodyX;
        vData.mainY=vData.bodyY;
    case 'Nose'
        vData.mainX=vData.xF;
        vData.mainY=vData.yF;
end
vData = getDistance(vData);
vData.videoInfo = getVideoInfo(params);
vData.nSamples0 = size(vData.mouseX,1);
vData.num0 = 1:vData.nSamples0;
vData.num0 = vData.num0';
if ~isfield(vData.videoInfo,'FrameRate')
    vData.videoInfo=getVideoInfo(params);
    if ~isfield(vData.videoInfo,'FrameRate')
        framerate = 20;
    end
else
    framerate=vData.videoInfo.FrameRate;
end

vData.t0 = (1:vData.nSamples0) / framerate;
vData.t0  = vData.t0';
end

function [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(vData,i,flipNeeded)
x=vData.bodyX(i);
y=vData.bodyY(i);
maxL=vData.mouseMajorAxisLength(i);
minL=vData.mouseMinorAxisLength(i);
theta=vData.mouseAngle(i) + flipNeeded;
[xF,yF] = pol2cart(theta,maxL/2);xF=xF+x;yF=yF+y;
[xB,yB] = pol2cart(theta+pi,maxL/2);xB=xB+x;yB=yB+y;
[xL,yL] = pol2cart(theta+pi/2,minL/2);xL=xL+x;yL=yL+y;
[xR,yR] = pol2cart(theta-pi/2,minL/2);xR=xR+x;yR=yR+y;
end
function distance=getDistanceBetweenObjects(x1,y1,x2,y2)
dx = x2-x1;dy = y2-y1;distance = sqrt((dx.*dx)+(dy.*dy));
end

function vData = getBonsaiData(bonsaiPath)

%% get Bonsai Data
vData=[];
if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    vData={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    vData.bodyX = vData.mouseX;
    vData.bodyY = vData.mouseY;
    rmfield(vData,'mouseX');rmfield(vData,'mouseY');
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
end
end