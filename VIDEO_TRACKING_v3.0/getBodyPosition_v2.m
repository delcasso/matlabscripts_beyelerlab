function mouseBody=getBodyPosition_v2(params,vData)

% clear all;clc;close all
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180315_SI-preFC';
% params.dataFileTag = 'F002';
% bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
bodyPath=[params.dataRoot filesep params.dataFileTag '-body.mat'];

if ~exist(bodyPath,'file') || params.forceGetBodyParts
    flipNeeded = 0;
    nSamples = size(vData.bodyX,1);   
    
    for i=1:nSamples
        [mouseBody(i),flipNeeded]=getBodyParts(vData,i,flipNeeded);
    end
    save(bodyPath,'mouseBody');
else
    load(bodyPath);
end
end

    function distance=getDistanceBetweenObjects(obj1,obj2)
        x1 = obj1.x;y1 = obj1.y;x2 = obj2.x;y2 = obj2.y;
        dx = x2-x1;dy = y2-y1;distance = sqrt((dx.*dx)+(dy.*dy));
    end
    function [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai_output,i,flipNeeded)
        x=bonsai_output.bodyX(i);
        y=bonsai_output.bodyY(i);
        maxL=bonsai_output.mouseMajorAxisLength(i);
        minL=bonsai_output.mouseMinorAxisLength(i);
        theta=bonsai_output.mouseAngle(i);
        theta = theta + flipNeeded;
        [xF,yF] = pol2cart(theta,maxL/2);xF=xF+x;yF=yF+y;
        [xB,yB] = pol2cart(theta+pi,maxL/2);xB=xB+x;yB=yB+y;
        [xL,yL] = pol2cart(theta+pi/2,minL/2);xL=xL+x;yL=yL+y;
        [xR,yR] = pol2cart(theta-pi/2,minL/2);xR=xR+x;yR=yR+y;
    end
    function [mouseBody,flipNeeded]=getBodyParts(bonsai,i,flipNeeded)
        [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded);
        if i>1
            [x0,y0,maxL0,minL0,theta0,xF0,yF0,xB0,yB0,xL0,yL0,xR0,yR0]=depackBonsaiData(bonsai,i-1,flipNeeded);
            dx = x-x0;dy = y-y0;d = sqrt((dx.*dx)+(dy.*dy));
            if d>1
                [centroid_thetaDiff,centroid_rhoDiff] = cart2pol(x-x0,y-y0);
                [F_thetaDiff,F_rhoDiff] = cart2pol(xF-x,yF-y);
                if (abs(F_thetaDiff-centroid_thetaDiff)>(pi/2))
                    flipNeeded = flipNeeded+pi;
                    [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded);
                end
            end
            F.x=xF;F.y=yF;
            B0.x=xB0;    B0.y=yB0;
            F0.x=xF0;F0.y=yF0;
            dFF0=getDistanceBetweenObjects(F0,F);
            dFB0=getDistanceBetweenObjects(B0,F);
            if dFF0> dFB0
                flipNeeded = flipNeeded+pi;
                [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded);
            end
        end
        mouseBody.xF=xF;mouseBody.yF=yF;
        mouseBody.xB=xB;mouseBody.yB=yB;
        mouseBody.xL=xL;mouseBody.yL=yL;
        mouseBody.xR=xR;mouseBody.yR=yR;
    end