
clear all
clc
close all

f=figure();

params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180315_SI-preFC';
params.dataFileTag = 'F002';
bonsaiPath=[params.dataRoot filesep params.dataFileTag '-bonsai.txt'];
params.outputFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\SI' ;
params.figureFolder = [params.outputFolder filesep 'figures'];
params.videoExtension ='mkv';

interObj = 0; interJuv = 0;

flipNeeded = 0;

bonsai = getBonsaiData(bonsaiPath);
nSamples = size(bonsai.mouseX,1);
%% get video
vidObj = VideoReader([params.dataRoot filesep params.dataFileTag '.' params.videoExtension]);

%%get Boundaires
boundaries=getSocialZones(params);
xA_pix=boundaries.arena.x;
yA_pix=boundaries.arena.y;
dxA_pix = diff([xA_pix xA_pix(1)]);
dyA_pix = diff([yA_pix yA_pix(1)]);
dist_pix = sqrt((dxA_pix.*dxA_pix)+(dyA_pix.*dyA_pix));
distTOT_pix = sum(dist_pix);
distTOT_cm = 60+30+60+30;
cm2pix = distTOT_pix / distTOT_cm;
pix2cm = distTOT_cm / distTOT_pix;
Object = boundaries.object;
Juvenile = boundaries.juvenile;


vidObj.CurrentTime = 62.65;


i=1250;


[mouseBody,flipNeeded]=getBodyParts(bonsai,i,flipNeeded);

i1=i+1;
for i=i1:nSamples
    
%     [x,y,maxL,minL,theta]=depackBonsaiData(bonsai,i,flipNeeded);
    
    [mouseBody,flipNeeded]=getBodyParts(bonsai,i,flipNeeded);
    
    [distPixObject,distPixJuvenile]=getInteraction(mouseBody,Object,Juvenile);
    
    clf(f);
    hold on
    xlim([0 320]);ylim([0 240]);
    
    circleDiam_cm = 10;
    circleDiam_pix = circleDiam_cm * cm2pix;
    h = rectangle('Position',[boundaries.object.x-circleDiam_pix/2 boundaries.object.y-circleDiam_pix/2 circleDiam_pix circleDiam_pix],'Curvature',[1,1],'EdgeColor',[0 0 0],'FaceColor','none');
    h = rectangle('Position',[boundaries.juvenile.x-circleDiam_pix/2 boundaries.juvenile.y-circleDiam_pix/2 circleDiam_pix circleDiam_pix],'Curvature',[1,1],'EdgeColor',[0 0 0],'FaceColor','none');
    
    circleDiam_cm = 7;
    circleDiam_pix = circleDiam_cm * cm2pix;
    h = rectangle('Position',[boundaries.object.x-circleDiam_pix/2 boundaries.object.y-circleDiam_pix/2 circleDiam_pix circleDiam_pix],'Curvature',[1,1]);
    h = rectangle('Position',[boundaries.juvenile.x-circleDiam_pix/2 boundaries.juvenile.y-circleDiam_pix/2 circleDiam_pix circleDiam_pix],'Curvature',[1,1]);
    
    
    currAxes.Visible = 'off';
    if i>50,jj=i:i+50;plot(bonsai.mouseX(jj),bonsai.mouseY(jj),'Marker','o','MarkerEdgeColor','none','MarkerFaceColor','m','MarkerSize',2,'LineStyle','none');end
    
    patch([mouseBody.xL mouseBody.xF mouseBody.xR],[mouseBody.yL mouseBody.yF mouseBody.yR],'m');
    patch([mouseBody.xL mouseBody.xB mouseBody.xR],[mouseBody.yL mouseBody.yB mouseBody.yR],'k');
    
    if ((distPixObject*pix2cm)<5)
        h = rectangle('Position',[boundaries.object.x-circleDiam_pix/2 boundaries.object.y-circleDiam_pix/2 circleDiam_pix circleDiam_pix],'Curvature',[1,1],'EdgeColor',[0 0 0],'FaceColor',[1 0 1]);
        interObj = interObj + 1;
    end
    text(boundaries.object.x,150,sprintf('0=%d',interObj),'color',[0 0 0]);
    
    if ((distPixJuvenile*pix2cm)<5)
        h = rectangle('Position',[boundaries.juvenile.x-circleDiam_pix/2 boundaries.juvenile.y-circleDiam_pix/2 circleDiam_pix circleDiam_pix],'Curvature',[1,1],'EdgeColor',[0 0 0],'FaceColor',[1 0 1]);
        interJuv = interJuv + 1;
    end
    text(boundaries.juvenile.x,150,sprintf('J=%d',interJuv),'color',[0 0 0]);
    pause(.001)
    
end

function bonsai = getBonsaiData(bonsaiPath)

%% get Bonsai Data
bonsai=[];
if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    bonsai = getDistance(bonsai);
end
end

function [distPixObject,distPixJuvenile]=getInteraction(mouseBody,Object,Juvenile)

mouseFront.x = mouseBody.xF;
mouseFront.y = mouseBody.yF;
mouseBack.x = mouseBody.xB;
mouseBack.y = mouseBody.yB;
distPixObject     = min([dist(mouseFront,Object) dist(mouseBack,Object)]);
distPixJuvenile  = min([dist(mouseFront,Juvenile) dist(mouseBack,Juvenile)]);

end

function d=dist(obj1,obj2)

x1 = obj1.x;
y1 = obj1.y;
x2 = obj2.x;
y2 = obj2.y;

dx = x2-x1;
dy = y2-y1;
d = sqrt((dx.*dx)+(dy.*dy));
end
function [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded)
x=bonsai.mouseX(i);
y=bonsai.mouseY(i);
maxL=bonsai.mouseMajorAxisLength(i);
minL=bonsai.mouseMinorAxisLength(i);
theta=bonsai.mouseAngle(i);
theta = theta + flipNeeded;
[xF,yF] = pol2cart(theta,maxL/2);xF=xF+x;yF=yF+y;
[xB,yB] = pol2cart(theta+pi,maxL/2);xB=xB+x;yB=yB+y;
[xL,yL] = pol2cart(theta+pi/2,minL/2);xL=xL+x;yL=yL+y;
[xR,yR] = pol2cart(theta-pi/2,minL/2);xR=xR+x;yR=yR+y;
end
function [mouseBody,flipNeeded]=getBodyParts(bonsai,i,flipNeeded)

[x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded);

if i>1    
    [x0,y0,maxL0,minL0,theta0,xF0,yF0,xB0,yB0,xL0,yL0,xR0,yR0]=depackBonsaiData(bonsai,i-1,flipNeeded);
    

    
    dx = x-x0;
    dy = y-y0;
    d = sqrt((dx.*dx)+(dy.*dy))
    if d>1
        [centroid_thetaDiff,centroid_rhoDiff] = cart2pol(x-x0,y-y0);
        [F_thetaDiff,F_rhoDiff] = cart2pol(xF-x,yF-y);
        
        if (abs(F_thetaDiff-centroid_thetaDiff)>(pi/2))
            flipNeeded = flipNeeded+pi;
            [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded);
        end
    end
    
    
    F.x=xF;F.y=yF;
    B0.x=xB0;    B0.y=yB0;
    F0.x=xF0;F0.y=yF0;
    dFF0=dist(F0,F);
    dFB0=dist(B0,F);
    
    if dFF0> dFB0
        flipNeeded = flipNeeded+pi;
        [x,y,maxL,minL,theta,xF,yF,xB,yB,xL,yL,xR,yR]=depackBonsaiData(bonsai,i,flipNeeded);
    end
    
    
    
    
    
    
    
end


mouseBody.xF=xF;
mouseBody.yF=yF;
mouseBody.xB=xB;
mouseBody.yB=yB;
mouseBody.xL=xL;
mouseBody.yL=yL;
mouseBody.xR=xR;
mouseBody.yR=yR;



end







