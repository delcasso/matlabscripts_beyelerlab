function bonsai_output = getBonsaiDataForCheckBonsai(bonsaiPath)

%% get Bonsai Data
bonsai_output=[];
if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    bonsai_output.bodyX = bonsai_output.mouseX;
    bonsai_output.bodyY = bonsai_output.mouseY;
    rmfield(bonsai_output,'mouseX');rmfield(bonsai_output,'mouseY');
end
end