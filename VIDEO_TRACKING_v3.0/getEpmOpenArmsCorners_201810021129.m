function openArmsCorners=getEpmOpenArmsCorners_201810021129(params)


if ~exist([params.dataRoot filesep params.dataFileTag '-openArmsCorners.mat'])
    
    videoTrackingData = getVideoTrackingData(params);
    bg=getBackGroundQuick(params);
    
    f1=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    
    zoom = 75;
    
    figure(f1)
    hold on
    axis equal
    axis off
    set(gca,'Ydir','reverse')
    colormap(gray(1024));
    imagesc(bg);
    text(10,10,'mark the four corners (extrimities) of the open arms','color',[0.5 1 0.5])
    for i=1:4
        [x(i),y(i)]=ginput(1);
    end
            
    for i=1:4
        ylim([y(i)-zoom y(i)+zoom])
        xlim([x(i)-zoom x(i)+zoom])
        [x(i),y(i)]=ginput(1);
    end
    
    [x,y]=reorderPointsClockwise(x,y);
    
    ylim([0 size(bg,1)])
    xlim([0 size(bg,2)])
    
    
    
    for i=1:4
        text(x(i),y(i),num2str(i),'color',[0.5 1 0.5]);
    end
    
    
    
    pause(2);
    
    openArmsCorners.x=x;
    openArmsCorners.y=y;
    
    save([params.dataRoot filesep params.dataFileTag '-openArmsCorners.mat'],'openArmsCorners');
    close (f1);
else
    
    load([params.dataRoot filesep params.dataFileTag '-openArmsCorners.mat']);
    
end

end

