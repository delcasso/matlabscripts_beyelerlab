 function vData = getDeepLabCutData(params)

 dlcPath=[params.dataRoot filesep params.dataFileTag '-deeplabcut.csv'];
 tmpMatrix = csvread(dlcPath,3,1);
 vData.EarL.X = tmpMatrix(:,1);
 vData.EarL.Y = tmpMatrix(:,2);
 vData.EarL.C = tmpMatrix(:,3);
 vData.Nose.X = tmpMatrix(:,4);
 vData.Nose.Y = tmpMatrix(:,5);
 vData.Nose.C = tmpMatrix(:,6);
 vData.EarR.X = tmpMatrix(:,7);
 vData.EarR.Y = tmpMatrix(:,8);
 vData.EarR.C = tmpMatrix(:,9);
 vData.TailStart.X = tmpMatrix(:,10);
 vData.TailStart.Y = tmpMatrix(:,11);
 vData.TailStart.C = tmpMatrix(:,12);
 vData.OptoLED.X = tmpMatrix(:,13);
 vData.OptoLED.Y = tmpMatrix(:,14);
 vData.OptoLED.C = tmpMatrix(:,15);
 
 nSamples = size(tmpMatrix,1);
 
 vData.xF = vData.Nose.X;
 vData.yF = vData.Nose.Y;
 vData.xB = vData.TailStart.X;
 vData.yB = vData.TailStart.Y;
%  vData.mouseX = vData.bodyX;
%  vData.mouseY = vData.bodyY;

 
%  vData.xF = nan(nSamples,1);
%  vData.yF = nan(nSamples,1);
%  vData.xB = nan(nSamples,1);
%  vData.yB = nan(nSamples,1);
 vData.mouseX = nan(nSamples,1);
 vData.mouseY = nan(nSamples,1);
 vData.mouseAngle = nan(nSamples,1);
 vData.mouseMajorAxisLength = nan(nSamples,1);
 vData.mouseMinorAxisLength = nan(nSamples,1);
 vData.mouseArea = nan(nSamples,1);
 vData.optoPeriod = vData.OptoLED.C
 

 vData.bodyX =  (vData.xF +  vData.xB) ./2 ;
 vData.bodyY =  (vData.yF +  vData.yB) ./2 ;
 
 switch params.MouseCoordinatesCentroid
    case 'Body'
        vData.mainX=vData.bodyX;
        vData.mainY=vData.bodyY;
    case 'Nose'
        vData.mainX=vData.xF;
        vData.mainY=vData.yF;
 end
 
 vData = getDistance(vData);


 end
 
 function distance=getDistanceBetweenObjects(x1,y1,x2,y2)
dx = x2-x1;dy = y2-y1;distance = sqrt((dx.*dx)+(dy.*dy));
end
 
 
 

