function Ca=analyzeRawCaSignal(experiment)

% clear;close all
% LOAD TEST DATA
% load(['C:\__Softwares\MatlabScripts_BeyelerLab\PHOTOMETRY' filesep 'F255_EPM_experiment.mat']);

%% INITIALIZATION
removeFirstMinute=0;
params = experiment.p;
pData = experiment.pData;
sig = pData.sig;ref = pData.ref;
frameRate_Hz = params.HamamatsuFrameRate_Hz;
slidingWindowSize = params.deltaFF_slidingWindowWidth;

%% For Test Only
testPlot1=0;
testPlot2=0;
testPlot3=0;

suffix = 'transientDetectionPlot1';
figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end
figName1 = [figureSubFolder filesep params.dataFileTag '-' suffix '.fig'];
if ~exist(figName1,'file') || params.forceRedrawing
    testPlot1=1;
end


suffix = 'transientDetectionPlot2';
figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end
figName2 = [figureSubFolder filesep params.dataFileTag '-' suffix '.fig'];
if ~exist(figName2,'file') || params.forceRedrawing
    testPlot2=1;
end


suffix = 'transientDetectionPlot3';
figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end
figName3 = [figureSubFolder filesep params.dataFileTag '-' suffix '.fig'];
if ~exist(figName3,'file') || params.forceRedrawing
    testPlot3=1;
end



if testPlot1
    f1=figure();
    rPlot=4;cPlot=2;iPlot={1,3,5,7,[2 4 6 8]};
end

%%

nSamples = size(sig,1);

xLookMin=pData.T(1);
xLooxMax=pData.T(end);
% xLookMin=100;xLooxMax=200;
if xLookMin>0
    iMin = int64(find(pData.T>=xLookMin,1,'first'));iMax = int64(find(pData.T<=xLooxMax,1,'last'));
end
% iMin=1;
% iMax=nSamples;


%% PROCESS BULK SIGNAL
Ca = processBulkSignal(sig,ref,frameRate_Hz,slidingWindowSize,removeFirstMinute);

Ca.mainSig = Ca.slidingdiffSig_fit;

%% TEST PLOTS
if testPlot1
    subplot(rPlot,cPlot,iPlot{1});      hold on;      title('DFF.sig');               plot(Ca.T,Ca.DFF.sig);           xlim([xLookMin xLooxMax])
    subplot(rPlot,cPlot,iPlot{2});      hold on;      title('DFF.ref_fit');           plot(Ca.T,Ca.DFF.ref_fit);       xlim([xLookMin xLooxMax])
    subplot(rPlot,cPlot,iPlot{3});      hold on;      title('diffSig');                   plot(Ca.T,Ca.diffSig);              xlim([xLookMin xLooxMax])
    subplot(rPlot,cPlot,iPlot{4});      hold on;      title('slidingdiffSig_fit');     plot(Ca.T,Ca.slidingdiffSig_fit); xlim([xLookMin xLooxMax])
end

% Figure 1, for test only // Figure 2, for test only,  The code for creating both figures is located at the end fo the file
%% Processing Transients
%% Filtering
if testPlot1
    subplot(rPlot,cPlot,iPlot{5})
    hold on
    xlim([xLookMin xLooxMax])
    plot(Ca.T,Ca.mainSig);
    text(xLookMin+10,max(Ca.mainSig)/2,'slidingdiffSig_fit');
    gap = max(Ca.mainSig);
end

iPass=1;
lowPassTh_Hz=[];
for highPassTh_Hz= 0.05:0.05:0.5
    filtered_Sig_withNaN = filterWithNaN(Ca.mainSig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,'high');
    if testPlot1
        text(xLookMin+10,max(filtered_Sig_withNaN+iPass*gap),sprintf('%2.2fHz',highPassTh_Hz));
        plot(Ca.T,filtered_Sig_withNaN+iPass*gap)
    end
    iPass=iPass+1;
end

if testPlot1
    savefig(f1,figName1);
    close(f1);
end

lowPassTh_Hz= 0.05;
highPassTh_Hz= 2;
Ca.BandPass = filterWithNaN(Ca.mainSig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,'bandpass');

%% Define Two MADs threshold
Ca.madBP = mad(Ca.BandPass,1);
Ca.medBP=nanmedian(Ca.BandPass);
Ca.BPlowTH = Ca.medBP;Ca.BPhighTH = Ca.medBP+(2*Ca.madBP);

%% Extract transients (chunk of signal exceding the threshold)
Tr_BPTwoMAD = extractTransientsAboveThreshold(Ca.BandPass,Ca.BPhighTH,Ca.T);
% Tr_BPmedian = extractTransientsAboveThreshold(Ca.BandPass,Ca.medBP,Ca.T);
Tr_BPmedian = extractTransientsAboveThreshold(Ca.BandPass, Ca.medBP+Ca.madBP,Ca.T);
%% Show application of selection criterion, (one by one)
% FindShortTransients
transientsDurationThreshold_sec=0.1;
transientsDurationThreshold_idx=transientsDurationThreshold_sec * frameRate_Hz;
shortTransients_BPmedian = findShortTransients(Tr_BPmedian,transientsDurationThreshold_idx);
% FindLowTransients
ampThreshold = Ca.BPhighTH; %(2 MADS)
lowTransients_BPmedian = findLowTransients(Tr_BPmedian,ampThreshold);
% FindTooCloseTeansients
minGapThreshold_sec=0.2;
 tooCloseTransients = findTooCloseTransients(Tr_BPmedian,minGapThreshold_sec);
 
if testPlot2
    
    yMin = min(Ca.mainSig(iMin:iMax));
    yMax = max(Ca.mainSig(iMin:iMax));
    f2=figure();
    rPlot=4;cPlot=2;iPlot={1,3,5,7,2,4,6,8};
    subplot(rPlot,cPlot,iPlot{1})
    hold on
    title('mainSig (mS)')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    subplot(rPlot,cPlot,iPlot{2})
    hold on
    title('BandPass (BP)')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.BandPass,'k');
    plot([xLookMin xLooxMax],[Ca.BPhighTH Ca.BPhighTH],'k')
    plot([xLookMin xLooxMax],[Ca.BPlowTH Ca.BPlowTH],'k:')
    lgd=legend({'BP','2 MADs','median'},'Location','southwest');
    lgd.NumColumns = 3;
    
    subplot(rPlot,cPlot,iPlot{3})
    hold on
    title('Two MADs')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=[];
    textCriterion=[];
    colorCriterion = [];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPTwoMAD,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion)
    
    subplot(rPlot,cPlot,iPlot{4})
    hold on
    title('Median')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=[];
    textCriterion=[];
    colorCriterion = [];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    
    subplot(rPlot,cPlot,iPlot{5})
    hold on
    title('Short Transients < 200 ms')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=shortTransients_BPmedian;
    textCriterion='shortTransients_BPmedian';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);        
    
    subplot(rPlot,cPlot,iPlot{6})
    hold on
    title('Low Transients < 2 MADs')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=lowTransients_BPmedian;
    textCriterion='lowTransients_BPmedian';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    
    subplot(rPlot,cPlot,iPlot{7})
    hold on
    title('Too Close Transients (gap<1sec)')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=tooCloseTransients;
    textCriterion='tooCloseTransients';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
        
    subplot(rPlot,cPlot,iPlot{8})
    hold on
    title('GoodTransients')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=sum([shortTransients_BPmedian lowTransients_BPmedian  tooCloseTransients],2)>0;
    textCriterion='goodTransients';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    
    savefig(f2,figName2);
    close(f2);
                   
end

if testPlot3
    
    yMin = min(Ca.mainSig(iMin:iMax));
    yMax = max(Ca.mainSig(iMin:iMax));
    f3=figure();
    rPlot=4;cPlot=2;iPlot={1,3,5,7,2,4,6,8};
    subplot(rPlot,cPlot,iPlot{1})
    hold on
    title('mainSig (mS)')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    subplot(rPlot,cPlot,iPlot{2})
    hold on
    title('BandPass (BP)')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.BandPass,'k');
    plot([xLookMin xLooxMax],[Ca.BPhighTH Ca.BPhighTH],'k')
    plot([xLookMin xLooxMax],[Ca.BPlowTH Ca.BPlowTH],'k:')
    lgd=legend({'BP','2 MADs','median'},'Location','southwest');
    lgd.NumColumns = 3;
    
    subplot(rPlot,cPlot,iPlot{3})
    hold on
    title('Two MADs')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=[];
    textCriterion=[];
    colorCriterion = [];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPTwoMAD,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    
    subplot(rPlot,cPlot,iPlot{4})
    hold on
    title('Median')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=[];
    textCriterion=[];
    colorCriterion = [];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    
    subplot(rPlot,cPlot,iPlot{5})
    hold on
    title('Short Transients < 200 ms')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    % FindShortTransients
transientsDurationThreshold_sec=0.1;
transientsDurationThreshold_idx=transientsDurationThreshold_sec * frameRate_Hz;
    shortTransients_BPmedian = findShortTransients(Tr_BPmedian,transientsDurationThreshold_idx);
    selectionCriterion=shortTransients_BPmedian;
    textCriterion='shortTransients_BPmedian';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);       
    Tr_BPmedian(logical(shortTransients_BPmedian))=[];
    
    subplot(rPlot,cPlot,iPlot{6})
    hold on
    title('Low Transients < 2 MADs')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    ampThreshold = Ca.BPhighTH; %(2 MADS)
    lowTransients_BPmedian = findLowTransients(Tr_BPmedian,ampThreshold);
    selectionCriterion=lowTransients_BPmedian;
    textCriterion='lowTransients_BPmedian';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    Tr_BPmedian(logical(lowTransients_BPmedian))=[];
        
    subplot(rPlot,cPlot,iPlot{7})
    hold on
    title('Too Close Transients (gap<1sec)')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    minGapThreshold_sec=0.2;
    tooCloseTransients = findTooCloseTransients(Tr_BPmedian,minGapThreshold_sec);
    selectionCriterion=tooCloseTransients;
    textCriterion='tooCloseTransients';
    colorCriterion = [1 0 0];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
    Tr_BPmedian(logical(tooCloseTransients))=[];
        
    subplot(rPlot,cPlot,iPlot{8})
    hold on
    title('GoodTransients')
    xlim([xLookMin xLooxMax])
    ylim([yMin yMax])
    plot(Ca.T,Ca.mainSig,'k');
    selectionCriterion=[];
    textCriterion=[];
    colorCriterion = [];
    drawTransientsOnTop(Ca.T,Ca.mainSig,Tr_BPmedian,[0 1 0],iMin,iMax,selectionCriterion,textCriterion,colorCriterion);
            
    savefig(f3,figName3);
    close(f3);
    
end


%% Extract transients (chunk of signal exceding the threshold)
Tr_BPTwoMAD = extractTransientsAboveThreshold(Ca.BandPass,Ca.BPhighTH,Ca.T);
% Tr_BPmedian = extractTransientsAboveThreshold(Ca.BandPass,Ca.medBP,Ca.T);
Tr_BPmedian = extractTransientsAboveThreshold(Ca.BandPass, Ca.medBP+Ca.madBP,Ca.T);

Ca.rawTransients_BPmedian = Tr_BPmedian;
Ca.rawTransients_BPTwoMAD = Tr_BPTwoMAD;
%% Show application of selection criterion, (one by one)
% FindShortTransients
transientsDurationThreshold_sec=0.1;
transientsDurationThreshold_idx=transientsDurationThreshold_sec * frameRate_Hz;
shortTransients_BPmedian = findShortTransients(Tr_BPmedian,transientsDurationThreshold_idx);
Tr_BPmedian(logical(shortTransients_BPmedian))=[];
% FindLowTransients
ampThreshold = Ca.BPhighTH; %(2 MADS)
lowTransients_BPmedian = findLowTransients(Tr_BPmedian,ampThreshold);
Tr_BPmedian(logical(lowTransients_BPmedian))=[];
% FindTooCloseTransients
minGapThreshold_sec=.2;
 tooCloseTransients = findTooCloseTransients(Tr_BPmedian,minGapThreshold_sec);
 Tr_BPmedian(logical(tooCloseTransients))=[];
 
 Ca.transients = Tr_BPmedian;

end
 

%% Subfunctions for Bulk Signal Processing
function Ca = processBulkSignal(sig,ref,frameRate_Hz,slidingWindowSize,removeFirstMinute)
Ca=[];
%% If this has not been taken care off earlier, this has to be done to remove autobleaching period that corrupt analysis
if(removeFirstMinute), sig(1:20*60)=[];ref(1:20*60)=[];end
%% Processing Bulk Signal
Ca.nFrames = size(ref,1);
Ca.T = 1: Ca.nFrames;
Ca.T = Ca.T./ frameRate_Hz;
Ca.raw.sig = sig;
Ca.raw.ref = ref;
Ca.DFF.sig=deltaFF(sig);
Ca.DFF.ref =deltaFF(ref);


slidingDFF.sig=getSlidingDFF(sig,slidingWindowSize);
slidingDFF.ref=getSlidingDFF(ref,slidingWindowSize);
Ca.slidingDFF.sig = slidingDFF.sig;
Ca.slidingDFF.ref = slidingDFF.ref;
Ca.movmean.sig = movmean(sig,slidingWindowSize,'omitnan');
Ca.movmean.ref = movmean(ref,slidingWindowSize,'omitnan');

Ca.DFF.ref_fit = controlFit (Ca.DFF.sig,Ca.DFF.ref);
Ca.slidingDFF.ref_fit = controlFit (Ca.slidingDFF.sig,Ca.slidingDFF.ref);

Ca.diffSig = (Ca.DFF.sig - Ca.DFF.ref)*100;
Ca.diffSig_fit = (Ca.DFF.sig - Ca.DFF.ref_fit)*100;
Ca.slidingdiffSig_fit = (Ca.slidingDFF.sig - Ca.slidingDFF.ref_fit)*100;

Ca.divSig = (Ca.DFF.sig ./ Ca.DFF.ref)*100;
Ca.divSig_fit = (Ca.DFF.sig ./ Ca.DFF.ref_fit)*100;
Ca.slidingdivSig_fit = (Ca.slidingDFF.sig ./ Ca.slidingDFF.ref_fit)*100;

Ca.mainSig = Ca.slidingdiffSig_fit;
end

function fittedRef = controlFit (sig, ref)
iNanSig = isnan(sig);iNanRef = isnan(ref);
iNan = iNanSig | iNanRef;
noNanSig = sig;noNanRef = ref;
noNanSig(iNan)=[];noNanRef(iNan)=[];
reg = polyfit(noNanRef, noNanSig, 1);
a = reg(1);
b = reg(2);
fittedRef = a.*ref + b;
end
function v2=deltaFF(v)
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
m=nanmean(v);
v2 = v - m;
v2 = v2./m;
end
function v3=getSlidingDFF(v,slidingWindowSize)
movmean_v = movmean(v,slidingWindowSize,'omitnan');
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
v2 = v - movmean_v;
v3 = v2./movmean_v;
end

function s=norm01(s)
s = s-min(s);
s = s ./ max(s);
end

function filtered_Sig_withNaN = butterWorthFilterWithNaN(Sig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,filterType)
%% Remove NaN for filtering
sig_without_NaN=Sig;iNaN=isnan(Sig);sig_without_NaN(iNaN)=[];
%% Filtering
switch filterType
    case 'low'
        fc = lowPassTh_Hz;
    case 'high'
        fc = highPassTh_Hz;
    case 'bandpass'
        fc = [lowPassTh_Hz highPassTh_Hz];
end
fs = frameRate_Hz;
[b,c] = butter(4,fc/(fs/2), filterType);filtered_Sig_without_NaN = filtfilt(b, c, sig_without_NaN);
%% Reintroduce NaNs values into the filtered vector
j=1;
nSamples = max(size(Sig));
filtered_Sig_withNaN = nan(nSamples,1);
for i=1:nSamples
    if iNaN(i)
        filtered_Sig_withNaN(i)=nan;
    else
        filtered_Sig_withNaN(i)=filtered_Sig_without_NaN(j);
        j=j+1;
    end
    
end

end
function filtered_Sig_withNaN = matlabFilterWithNaN(Sig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,filterType)
%% Remove NaN for filtering
sig_without_NaN=Sig;iNaN=isnan(Sig);sig_without_NaN(iNaN)=[];
%% Filtering
switch filterType
    case 'low'
        filtered_Sig_without_NaN = lowpass(sig_without_NaN,lowPassTh_Hz,frameRate_Hz);
    case 'high'
        filtered_Sig_without_NaN = highpass(sig_without_NaN,highPassTh_Hz,frameRate_Hz);
    case 'bandpass'
        filtered_Sig_without_NaN = bandpass(sig_without_NaN,[lowPassTh_Hz highPassTh_Hz],frameRate_Hz);
end
%% Reintroduce NaNs values into the filtered vector
j=1;
nSamples = max(size(Sig));
filtered_Sig_withNaN = nan(nSamples,1);
for i=1:nSamples
    if iNaN(i)
        filtered_Sig_withNaN(i)=nan;
    else
        filtered_Sig_withNaN(i)=filtered_Sig_without_NaN(j);
        j=j+1;
    end
    
end

end
function  filtered_Sig_withNaN = filterWithNaN(Sig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,filterType)
filtered_Sig_withNaN = butterWorthFilterWithNaN(Sig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,filterType);
% filtered_Sig_withNaN = matlabFilterWithNaN(Sig,lowPassTh_Hz,highPassTh_Hz,frameRate_Hz,filterType);
end


function drawTransientsOnTop(T,Sig,Tr,TrColor,iMin,iMax,selectionCriterion,textCriterion,colorCriterion)

nTr=size(Tr,2);
nCrit = size(selectionCriterion,2);

 maxSig = max(Sig(iMin:iMax));
 minSig = min(Sig(iMin:iMax));
 ampSig = maxSig - minSig;
 tickSize = ampSig/20;
 tickUp = maxSig;
 tickDown = tickUp - tickSize;
 txtPos = tickUp + (tickSize*5);

for iTr=1:nTr
    i1Tr = Tr(iTr).start ;
    i2Tr = Tr(iTr).start+Tr(iTr).duration_idx;
    if (i1Tr>= iMin) &&  (i2Tr<= iMax)
        color_=TrColor;
        t = T(Tr(iTr).iMax);
        for iCrit = 1:nCrit
            if selectionCriterion(iTr,iCrit)
                color_=colorCriterion(iCrit,:);
            end
        end
        plot(T(i1Tr:i2Tr),Sig(i1Tr:i2Tr),'color',color_);
        plot([t t],[tickUp tickDown],'color',color_);
    end
end



end

function Tr = extractTransientsAboveThreshold(sig,threshold,T)

thSig = sig>threshold;
Tr=[];iTr=0;preVal=0;
nSamples = max(size(sig));
nSamples=int64(nSamples);
for i=1:nSamples-1
    if thSig(i)
        if ~preVal
            iTr=iTr+1;Tr(iTr).start=i;Tr(iTr).indices=[];
        end
        Tr(iTr).indices=[Tr(iTr).indices i];
    end
    preVal=thSig(i);
end

nTr=size(Tr,2);

for iTr=1:nTr
    idx=Tr(iTr).indices;    
    Tr(iTr).duration_idx = size(idx,2);
    [Tr(iTr).vMax,Tr(iTr).iMax] = max(sig(idx));
    Tr(iTr).ts=T(Tr(iTr).iMax);
    Tr(iTr).values = sig(idx);
    Tr(iTr).iMax = Tr(iTr).iMax + idx(1) - 1;
end


end

function shortTransients = findShortTransients(Tr,durationThreshold_idx)
nTr=size(Tr,2);
shortTransients=nan(nTr,1);
for iTr=1:nTr
    %% Duration of the transient is too short
    if Tr(iTr).duration_idx<durationThreshold_idx %too short
        shortTransients(iTr) = 1;
    else
        shortTransients(iTr) = 0;
    end
end
end

function lowTransients = findLowTransients(Tr,ampThreshold)
nTr=size(Tr,2);
lowTransients=nan(nTr,1);
for iTr=1:nTr
    %% Duration of the transient is too short
    if Tr(iTr).vMax<ampThreshold %too low
        lowTransients(iTr) = 1;
    else
        lowTransients(iTr) = 0;
    end
end
end

function tooCloseTransients = findTooCloseTransients(Tr,minGapThreshold_sec)
nTr=size(Tr,2);
tooCloseTransients=nan(nTr,1);
tooCloseTransients(1)=0;
dt = diff([Tr(:).ts]);

for iTr=2:nTr
    %% Duration of the transient is too short
  if dt(iTr-1)>=minGapThreshold_sec %too close
        tooCloseTransients(iTr) = 0;
    else
        tooCloseTransients(iTr) = 1;
    end
end
end



