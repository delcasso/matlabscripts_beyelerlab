clear

Fo='Z:\Tmp\19200FramesBufferedTest\Buffered';
load([Fo filesep '19200FramesTestBuffered.mat']);
sig(1)=[];
B=load([Fo filesep '19200FramesTestBuffered-Bonsai.txt']);
figure()
hold on
plot(mapminmax(B'))
plot(mapminmax(sig'))

Fo='Z:\Tmp\19200FramesBufferedTest\Streamed';
load([Fo filesep '19200FramesTestStreaming_000.mat']);
sig(1)=[];
B=load([Fo filesep '19200FramesTestStreaming-Bonsai.txt']);
figure()
hold on
plot(mapminmax(B'))
plot(mapminmax(sig'))