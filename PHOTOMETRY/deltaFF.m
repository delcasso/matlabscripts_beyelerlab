function photometryData=deltaFF(photometryData)

sig = photometryData.sig;
ref = photometryData.ref;

meanSig=mean(sig);
meanRef=mean(ref);
sig = sig - meanSig;
ref = ref - meanRef;

sig = sig./meanSig;
ref = ref./meanRef;

d=sig-ref;

photometryData.mainSig = d;

end