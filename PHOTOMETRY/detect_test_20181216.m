e = experiment;
pData = e.pData;

sig = pData.sig;
ref = pData.ref;
T = pData.T;

T1 = 200;
T2 = 300;
i1 = find(T>T1,1,'first');i2 = find(T<T2,1,'last');


s = sig(i1:i2);
r = ref(i1:i2);
t = T(i1:i2); 
mS = Ca.mainSig(i1:i2);
bp = Ca.BandPass(i1:i2);

f = figure();
subplot(2,1,1)
hold on


% iTransients = [pData.transients(:).iMax];
% tTransients = T(iTransients);
% i1=find(tTransients>T1,1,'first');
% i2=find(tTransients<T2,1,'last');
% idx = i1:i2;
% nT = size(idx,2);
% for iT=1:nT
%     i = idx(iT);
%     x = tTransients(i);
%     plot([x x],[-10 10],'m')
% end

plot(t,mS,'color',[0.7 0.7 0.7]);
plot(t,bp,'color','m');
plot(t,bp>Ca.BPhighTH,'color','r');


subplot(2,1,2)
hold on
d = diff(bp>Ca.BPhighTH)
plot(d)

i1 = find(d==1);
i2 = find(d==-1);


% [pks,loc]= findpeaks(bp,'MinPeakHeight',Ca.BPhighTH);
% plot(t(loc),mS(loc),'go');











