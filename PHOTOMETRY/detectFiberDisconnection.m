function [photometryData,videoTrackingData]= detectFiberDisconnection(params,photometryData,videoTrackingData)

sig0 = photometryData.sig;
zSig0 = (sig0 - mean(sig0))./std(sig0);
ref0 = photometryData.ref;
zRef0 = (ref0 - mean(ref0))./std(ref0);

% Looking for total fiber disconnection
iTotalDeconnection = find(zRef0<-5);
if ~isempty(iTotalDeconnection)
    iTotalDeconnectionStarts = iTotalDeconnection(1) - params.HamamatsuFrameRate_Hz*2;
    iTotalDeconnectionStarts = max([iTotalDeconnectionStarts 1]);
    iTotalDeconnectionEnds = iTotalDeconnection(end) + params.HamamatsuFrameRate_Hz*2;
    [videoTrackingData,photometryData]=replaceFramesByNan(videoTrackingData,photometryData,iTotalDeconnectionStarts:iTotalDeconnectionEnds);
end
% Looking for partial fiber disconnection
iEnd = min([ (params.HamamatsuFrameRate_Hz*120) (size(photometryData.sig,1))]);
s=photometryData.sig(1:iEnd);m=mean(s);std_=std(s);inTH_sig = m - 5*std_;
s=photometryData.ref(1:iEnd);m=mean(s);std_=std(s);inTH_ref = m - 5*std_;
iAbnormalSig  = find(photometryData.sig<inTH_sig);
iAbnormalRef  = find(photometryData.ref<inTH_ref);
iAbnormal = intersect(iAbnormalSig,iAbnormalRef);
[videoTrackingData,photometryData]=replaceFramesByNan(videoTrackingData,photometryData,iAbnormal);
  

suffix = 'photometryAbnormalRawSignals';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file')
    
    figureHandle=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    
    subplot(4,1,1)
    title('RAW')
    hold on
    plot(sig0,'color',[0 183 255]./255);
    plot(ref0,'color',[130 0 200]./255);
    
    subplot(4,1,2)
    hold on
    title('z-score')
    plot(zSig0,'color',[0 183 255]./255);
    plot(zRef0,'color',[130 0 200]./255);
    
    % Looking for total fiber disconnection
    if ~isempty(iTotalDeconnection)
        plot(iTotalDeconnectionStarts:iTotalDeconnectionEnds,zRef0(iTotalDeconnectionStarts:iTotalDeconnectionEnds),'color',[1 0 0]);
        plot(iTotalDeconnectionStarts:iTotalDeconnectionEnds,zSig0(iTotalDeconnectionStarts:iTotalDeconnectionEnds),'color',[1 0 0]);
        subplot(4,1,3)
        hold on
        title('total deconnection')
        plot(photometryData.sig,'color',[0 183 255]./255);
        plot(photometryData.ref,'color',[130 0 200]./255);
    end
    
    % Looking for partial fiber disconnection
    subplot(4,1,4)
    hold on
    title('partial disconnection')
    plot(sig0,'color',[1 0 0]);
    plot([1 size(sig0,1)],[inTH_sig inTH_sig],'color',[0 183 255]./255,'LineStyle',':'); 
    plot(ref0,'color',[1 0 0]);
    plot([1 size(ref0,1)],[inTH_ref inTH_ref],'color',[130 0 200]./255,'LineStyle',':');  
    plot(photometryData.sig,'color',[0 183 255]./255);
    plot(photometryData.ref,'color',[130 0 200]./255);
    print(figureHandle,figName,'-djpeg');
    close(figureHandle);
end

end