function quickSignalCheck_20190111(foldername)

if isempty(foldername)
    foldername=pwd;
    fprintf('Processing folder %s\n',pwd)
    x = input (sprintf('Do You Want To process this folder ?',pwd),'s');
else
    x = 'Y';
end

switch x
    
    case 'Y'
        fileList = dir([foldername filesep '*.mat']);
        nFiles = size(fileList,1);
        
        f1=figure();
                   
        for iFile=1:nFiles
            f = fileList(iFile).name;
            fprintf('\tprocessing file %s\n',f);
            
            load([foldername filesep f]);
            

            
            [filepath,name,ext] = fileparts(f);
            
            
           t = 1:size(sig,1);t= t/20; 
           
           ref(1)=[];sig(1)=[];t(1)=[];
            
            subplot(3,2,1)
            title('raw signals')
            hold on
            plot(t,sig,'color',[0 183 255]./255);
            plot(t,ref,'color',[130 0 200]./255,'LineStyle',':');   
            
            
            sig = sig - mean(sig);
            ref = ref - mean(ref);
            
            ref_fit = controlFit (sig,ref);        
            
            subplot(3,2,2)
            title('raw signals centered on zero')
            hold on
            plot(t,sig,'color',[0 183 255]./255);
            plot(t,ref_fit,'color',[130 0 200]./255);            
            plot(t,ref,'color',[130 0 200]./255,'LineStyle',':');   
            
            %remove first minute
            sig(1:20*60)=[];ref(1:20*60)=[];ref_fit(1:20*60)=[];t(1:20*60)=[];
                                    
            subplot(3,2,3)
            title('1st minute removed')
            hold on
            plot(t,sig,'color',[0 183 255]./255);
            plot(t,ref_fit,'color',[130 0 200]./255);            
            plot(t,ref,'color',[130 0 200]./255,'LineStyle',':');   
            
            subplot(3,2,4)
            title('Corrected Signal')
            hold on
            plot(t,sig,'color',[0 183 255]./255);
            plot(t,sig-ref_fit,'color',[0 0 0]./255);
            plot(t,sig-ref,'color',[0 0 0]./255,'LineStyle',':');            
            
            t = t(1:20*20);            sig = sig(1:20*20);            ref = ref(1:20*20);            ref_fit = ref_fit(1:20*20);            
                      
            subplot(3,2,5)
            title('Corrected Signal 30 sec')
            hold on
            plot(t,sig,'color',[0 183 255]./255);
            plot(t,sig-ref_fit,'color',[0 0 0]./255);
            plot(t,sig-ref,'color',[0 0 0]./255,'LineStyle',':');     
            
            subplot(3,2,6)
            title('Corrected Signal 30 sec DFF')
            hold on
            plot(t,DFF(sig-ref_fit),'color',[0 0 0]./255);   
            
            print(f1,[foldername filesep name '-signalCheck.jpeg'],'-djpeg');
            clf(f1);                                
        end
        
        close(f1);
        
    otherwise
        fprintf('quit\n');
end



end

function Y=DFF(X)
            mean_X = mean(X);
            Y = (X - mean_X) / abs(mean_X);
end

function fittedRef = controlFit (sig, ref)
iNanSig = isnan(sig);iNanRef = isnan(ref);
iNan = iNanSig | iNanRef;
noNanSig = sig;noNanRef = ref;
noNanSig(iNan)=[];noNanRef(iNan)=[];
reg = polyfit(noNanRef, noNanSig, 1);
a = reg(1);
b = reg(2);
fittedRef = a.*ref + b;
end


