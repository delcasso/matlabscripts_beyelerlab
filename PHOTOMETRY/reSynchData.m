function [videoTrackingData2,nSynchro,dFrame]=reSynchData(videoTrackingData,photometryData,params)

ledDetectionThreshold = params.ledDetectionThreshold;
sig = photometryData.sig;

o = videoTrackingData.optoPeriod;
o=o>max(o)*(ledDetectionThreshold/100);
dO=diff(o);
iUp = find(dO==1);
dFrame = median(diff(iUp));
x=1:dFrame+1;
xq = linspace(1,dFrame+1,100); %synchro LED lights upeach 100 Hamamatsuframes
nSynchro=size(iUp,1);
videoTrackingData2.mainX=[];
videoTrackingData2.mainY=[];
videoTrackingData2.mouseAngle=[];
videoTrackingData2.mouseMajorAxisLength=[];
videoTrackingData2.mouseMinorAxisLength=[];
videoTrackingData2.mouseArea=[];
videoTrackingData2.optoPeriod=[];
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(videoTrackingData.optoPeriod,1));
for i=1:nSynchro
    i1=iUp(i);i2=i1+dFrame;
    videoTrackingData2.mainX = [videoTrackingData2.mainX ; interp1(x,videoTrackingData.mainX(i1:i2),xq)'];
    videoTrackingData2.mainY = [videoTrackingData2.mainY ; interp1(x,videoTrackingData.mainY(i1:i2),xq)'];
    videoTrackingData2.mouseAngle = [videoTrackingData2.mouseAngle ; interp1(x,videoTrackingData.mouseAngle(i1:i2),xq)'];
    videoTrackingData2.mouseMajorAxisLength = [videoTrackingData2.mouseMajorAxisLength ; interp1(x,videoTrackingData.mouseMajorAxisLength(i1:i2),xq)'];
    videoTrackingData2.mouseMinorAxisLength = [videoTrackingData2.mouseMinorAxisLength ; interp1(x,videoTrackingData.mouseMinorAxisLength(i1:i2),xq)'];
    videoTrackingData2.mouseArea = [videoTrackingData2.mouseArea ; interp1(x,videoTrackingData.mouseArea(i1:i2),xq)'];
    videoTrackingData2.optoPeriod = [videoTrackingData2.optoPeriod ; interp1(x,videoTrackingData.optoPeriod(i1:i2),xq)'];
end
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(videoTrackingData2.optoPeriod,1));
% fprintf('resynch done\n');

end