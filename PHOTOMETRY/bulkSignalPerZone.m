function bulkSignalStats=bulkSignalPerZone(inZone,nZones,zoneTime_fr,mainSig)
bulkSignalStats.zonesMeanAmp=nan(1,nZones);
%    fprintf('\n');
for iZone=1:nZones
    idx=find(inZone==iZone);
    bulkSignalStats.zonesMeanAmp(iZone) = nanmean(mainSig(idx)); 
%     fprintf('\tZone %d, bulkSignalStats = %2.2f\n',iZone,bulkSignalStats.zonesMeanAmp(iZone));
end