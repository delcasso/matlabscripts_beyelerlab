function [photometryData,videoTrackingData]= detectFiberDisconnection_SlidingWindow(params,photometryData,videoTrackingData)

sig0 = photometryData.sig;
zSig0 = (sig0 - mean(sig0))./std(sig0);

ref0 = photometryData.ref;
zRef0 = (ref0 - mean(ref0))./std(ref0);


%1 sec bins
binSize_sec = 1;
binSize_samples =  floor(binSize_sec * params.HamamatsuFrameRate_Hz);
n = size(ref0,1);n = floor(n / binSize_samples);n = n * binSize_samples;tmp = ref0(1:n);
n = n/binSize_samples;
tmp = reshape(tmp,binSize_samples,n);
tmp = mean(tmp);

dTmp = diff(tmp);

dTmp = (dTmp - mean(dTmp))./std(dTmp);


dTmp = abs(dTmp);

ii=[];
ii = find(dTmp>6);


if ~isempty(ii)    
    % 20190731 :: correction of bug in fiberdisconnection function		
    % before bug was     iFirstDisconnection = ii(1);
    iFirstDisconnection = ii(1)*binSize_samples;      
    [videoTrackingData,photometryData]=replaceFramesByNan(videoTrackingData,photometryData,iFirstDisconnection:photometryData.nSamples);
end


suffix = 'photometryAbnormalRawSignals';

figureSubFolder = [params.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep params.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file')
    
    figureHandle=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    subplot(3,1,1)
    title('RAW')
    hold on
    plot(zRef0,'color',[130 0 200]./255);
    
    subplot(3,1,2)
    title('1sec binned')
    hold on
    plot(tmp,'color',[130 0 200]./255);
    
    subplot(3,1,3)
    title('diff')
    hold on
    plot(dTmp,'color',[130 0 200]./255);
    plot(ii,dTmp(ii),'rx');
    
    print(figureHandle,figName,'-djpeg');
    close(figureHandle);
end

end