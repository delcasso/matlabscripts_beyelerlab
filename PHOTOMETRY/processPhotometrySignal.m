%% Subfunctions for Bulk Signal Processing

% load('F251_000.mat')
% frameRate_Hz = 20
% removeFirstMinute = 1
% slidingWindowSize = 1200
% Ca = processPhotometrySignal(sig,ref,frameRate_Hz,slidingWindowSize,removeFirstMinute);


function Ca = processPhotometrySignal(sig,ref,frameRate_Hz,slidingWindowSize,removeFirstMinute)

load('F251_000.mat')
frameRate_Hz = 20
removeFirstMinute = 1
slidingWindowSize = 1200

Ca=[];
%% If this has not been taken care off earlier, this has to be done to remove autobleaching period that corrupt analysis
if(removeFirstMinute), sig(1:20*60)=[];ref(1:20*60)=[];end
%% Processing Bulk Signal
Ca.nFrames = size(ref,1);
Ca.T = 1: Ca.nFrames;
Ca.T = Ca.T./ frameRate_Hz;
Ca.raw.sig = sig;
Ca.raw.ref = ref;
Ca.DFF.sig=deltaFF(sig);
Ca.DFF.ref =deltaFF(ref);


slidingDFF.sig=getSlidingDFF(sig,slidingWindowSize);
slidingDFF.ref=getSlidingDFF(ref,slidingWindowSize);
Ca.slidingDFF.sig = slidingDFF.sig;
Ca.slidingDFF.ref = slidingDFF.ref;
Ca.movmean.sig = movmean(sig,slidingWindowSize,'omitnan');
Ca.movmean.ref = movmean(ref,slidingWindowSize,'omitnan');

Ca.DFF.ref_fit = controlFit (Ca.DFF.sig,Ca.DFF.ref);
Ca.slidingDFF.ref_fit = controlFit (Ca.slidingDFF.sig,Ca.slidingDFF.ref);

Ca.diffSig = (Ca.DFF.sig - Ca.DFF.ref)*100;
Ca.diffSig_fit = (Ca.DFF.sig - Ca.DFF.ref_fit)*100;
Ca.slidingdiffSig_fit = (Ca.slidingDFF.sig - Ca.slidingDFF.ref_fit)*100;

Ca.divSig = (Ca.DFF.sig ./ Ca.DFF.ref)*100;
Ca.divSig_fit = (Ca.DFF.sig ./ Ca.DFF.ref_fit)*100;
Ca.slidingdivSig_fit = (Ca.slidingDFF.sig ./ Ca.slidingDFF.ref_fit)*100;

Ca.mainSig = Ca.slidingdiffSig_fit;

end








function v2=deltaFF(v)
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
m=nanmean(v);
v2 = v - m;
v2 = v2./abs(m);
end


function v3=getSlidingDFF(v,slidingWindowSize)
movmean_v = movmean(v,slidingWindowSize,'omitnan');
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
v2 = v - movmean_v;
v3 = v2./abs(movmean_v);
end




function fittedRef = controlFit (sig, ref)
iNanSig = isnan(sig);iNanRef = isnan(ref);
iNan = iNanSig | iNanRef;
noNanSig = sig;noNanRef = ref;
noNanSig(iNan)=[];noNanRef(iNan)=[];
reg = polyfit(noNanRef, noNanSig, 1);
a = reg(1);
b = reg(2);
fittedRef = a.*ref + b;
end







