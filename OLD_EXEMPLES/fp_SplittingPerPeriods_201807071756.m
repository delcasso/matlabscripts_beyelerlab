% function fp_SplittingPerPeriods_201807041939(fod,dataRoot,PeriodDurations_min,zoneDiam_cm,BoxDim)

function fp_SplittingPerPeriods_201807041939()

positionRealignementNeeded=0;
centralZoneAnalysis=0;

% clear; clc; close all;

% dataRoot = 'S:\Fiber-Photometry\data\IC_fpG1\20180308_FoxUrine-preFC_fpG1';
% dataRoot ='S:\Fiber-Photometry\data\IC_fpG1\20180314_AppleOdor-preFc_fpG1';
dataRoot = 'S:\Fiber-Photometry\data\IC_fpG1\20180307_OFT_IC-CeM_fpG1';
PeriodDurations_min = [5 5 5 5];
zoneDiam_cm =10;
BoxDim =[30 30];
cameraMode = 'synchronous';
videoExtension = 'avi';% % videoExtension = 'mp4'

xBoxDim_cm =BoxDim(1);yBoxDim_cm =BoxDim(2);

if positionRealignementNeeded || centralZoneAnalysis
    load([dataRoot filesep 'posData.mat']);
    if centralZoneAnalysis
        fod = fopen('test.txt','w');
    end
end

frameRate_Hz= 20;
speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
PeriodDurations_Frames  = PeriodDurations_min .* 60 .* frameRate_Hz;
nPeriods = size(PeriodDurations_min,2);

fileList = dir([dataRoot filesep '*.avi']);nFiles = size(fileList,1);

% if selected video extension was wonrg
if ~ nFiles, fileList = dir([dataRoot filesep '*.mp4']);nFiles = size(fileList,1);end

nBinPerMap = 1000;
binningFactor = 2;

all_SigMaps = nan(nBinPerMap,nBinPerMap,nFiles,nPeriods);
all_OccMaps = nan(nBinPerMap,nBinPerMap,nFiles,nPeriods);
all_NormedSigMaps = nan(nBinPerMap,nBinPerMap,nFiles,nPeriods);
all_SmoothedOccMaps = nan(nBinPerMap,nBinPerMap,nFiles,nPeriods);
all_SmoothedNormedSigMaps = nan(nBinPerMap,nBinPerMap,nFiles,nPeriods);

iSubPlot=0;

f_Occ = figure();%set(0, 'DefaultFigureRenderer', 'painters');
f_SigNorm = figure();%set(0, 'DefaultFigureRenderer', 'painters');
f_SigNormZone = figure();%set(0, 'DefaultFigureRenderer', 'painters');

avgCageSignal = nan(nFiles,nPeriods);
avgZoneSignal = nan(nFiles,nPeriods);

if centralZoneAnalysis, fprintf(fod,'%s\n',dataRoot);end

occ_colormap =  colormap([0.9 0.9 0.9; jet(1024)]);
c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
load('myCustomColormap.mat');
sig_colormap = colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);

for iFile = 1:nFiles
    
    [fPath, dataFileTag, ext] = fileparts(fileList(iFile).name);% Parse Filename To Use Same Filename to save analysis results
    all_Names{iFile} = dataFileTag;% Keep The filename in memory
    if centralZoneAnalysis, fprintf(fod,'%s',dataFileTag);end
    fprintf('\t\tProcessing File %d/%d [%s]\n',iFile,nFiles,dataFileTag);
    
    load([dataRoot filesep dataFileTag '.mat']);sig(1)=[];ref(1)=[];% Load fiber-photometry DATA
    % first valueof sig and ref are systematically aberant; if we control hamamatsu from Arduino, we always get one additional value due to fip matlab program
    B=getVideoTrackingData([dataRoot filesep 'bonsai-' dataFileTag '.txt'],cameraMode);% Load Behavioral DATA
    if strcmp(cameraMode,'asynchronous')
        [B_synchronized,nSynchro,dFrame]=reSynchData(B,sig,ledDetectionThreshold);
        fprintf('\t> Synchro nSig=%d dt=%d frames\n',nSynchro,dFrame);
        draw_figure6(dataRoot,dataFileTag,B,B_synchronized);
        B = B_synchronized;
    end
    
    [B,ref,sig]=removeOneMinute(B,ref,sig); %remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
    x=B.mainX;y=B.mainY;
    
    if positionRealignementNeeded
        [x,y,posData(iFile).x,posData(iFile).y] = reScalePositionData(posData(iFile),x,y,xBoxDim_cm,yBoxDim_cm);
    end
    
    [ref,sig,d]=deltaFF(ref,sig); %procress deltaFF
    
    if size(x,1)>size(d,1), x = x(1:size(d,1)); y = y(1:size(d,1)); end
    if size(x,1)<size(d,1), d = d(1:size(x,1)); end
    
    nFrames = size(x,1);T = 1: nFrames;T = T./ frameRate_Hz; %recreate time vector based on fiber-photometry frame rate
    [x,y]=cleanPosBasedOnSpeedThreshold(x,y,speedThreshold); % remove all position when animal spped exceed 20 pixel per sec.
    
    i1 = 1;
    for iPeriod = 1:nPeriods
        i2 = i1 + PeriodDurations_Frames(iPeriod);
        if i2>size(x,1)
            i2 = size(x,1);
        end
        
        xTmp = x(i1:i2);yTmp = y(i1:i2);dTmp = d(i1:i2);
        i = find(xTmp<=0);xTmp(i) = [];yTmp(i) = [];dTmp(i) = [];
        i = find(yTmp<=0);xTmp(i) = [];yTmp(i) = [];dTmp(i) = [];
        
        all_OccMaps(:,:,iFile,iPeriod)=buildOccupancyMap(xTmp,yTmp,nBinPerMap,binningFactor); %build a spatial map of the time spend in each pixel for the entire exp.
        all_SigMaps(:,:,iFile,iPeriod)=buildSignalMap(xTmp,yTmp,dTmp,nBinPerMap,binningFactor); %build a spatial map of the time spend in each pixel for the entire exp.
        all_NormedSigMaps(:,:,iFile,iPeriod) = normSigMap(squeeze(all_SigMaps(:,:,iFile,iPeriod)),squeeze(all_OccMaps(:,:,iFile,iPeriod)));
        
        iSubPlot=iSubPlot+1;
        
        figure(f_Occ);
        subplot(nFiles,nPeriods,iSubPlot)
        OccupancyMapFrame=squeeze(all_OccMaps(:,:,iFile,iPeriod));
        OccupancyMapSec = OccupancyMapFrame/20;
        colormap([0.9 0.9 0.9; jet(1024)]);
        hold on
        title(sprintf('m%d-p%d',iFile,iPeriod));
        
        %         xlim([0 (yBoxDim_cm*(8/4))/binningFactor])
        %         ylim([0 (xBoxDim_cm*(8/4))/binningFactor])
        axis equal
        axis off
        ii = isnan(OccupancyMapSec);
        OccupancyMapSec(ii)=0;
        gaussFilt_OccupancyMapSec = imgaussfilt(OccupancyMapSec,5);
        gaussFilt_OccupancyMapSec(ii)=nan;
        OccupancyMapSec(ii)=nan;
        imagesc(gaussFilt_OccupancyMapSec)
        set(gca,'Ydir','reverse')
        xlim([0 150]);
        ylim([0 150]);
        colorbar();
        
        if centralZoneAnalysis
            xC = (posData(iFile).x(5)-(zoneDiam_cm/2))/binningFactor;
            yC = (posData(iFile).y(5)-(zoneDiam_cm/2))/binningFactor;
            dC = zoneDiam_cm/binningFactor;
            rectangle('Position',[xC yC dC dC],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
        end
        
        
        %% Draw Normilized Signal Map
        figure(f_SigNorm);
        subplot(nFiles,nPeriods,iSubPlot)
        hold on
        axis equal
        title(sprintf('m%d-p%d',iFile,iPeriod));
        
        AvgSigMap = squeeze(all_NormedSigMaps(:,:,iFile,iPeriod));
        ii = isnan(AvgSigMap);
        AvgSigMap(ii)=0;
        gaussFilt_AvgSigMap = imgaussfilt(AvgSigMap,5);
        gaussFilt_AvgSigMap(ii)=nan;
        AvgSigMap(ii)=nan;
        c = linspace(0,1,512);
        z = ones(512,1);
        c2 = linspace(1,0,512);
        load('myCustomColormap.mat');
        colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);
        colorbar();
        imagesc(gaussFilt_AvgSigMap);
        xlim([0 150]);
        ylim([0 150]);
        set(gca,'Ydir','reverse');
        
        %         xlim([0 (yBoxDim_cm*(8/4))/binningFactor])
        %         ylim([0 (xBoxDim_cm*(8/4))/binningFactor])
        
        
        
        if centralZoneAnalysis
            
            avgCageSignal(iFile,iPeriod) = nanmedian(nanmedian(AvgSigMap))
            
            fprintf(fod,'\t%f', avgCageSignal(iFile,iPeriod) );
            
            rectangle('Position',[xC yC dC dC],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
            x_ = x(i1:i2);  y_ = y(i1:i2); d_ = d(i1:i2);
            xCenter = posData(iFile).x(5);
            yCenter = posData(iFile).y(5);
            dx_ = x_ - xCenter;
            dy_ = y_ - yCenter;
            distance_ = sqrt((dx_.^2)+(dy_.^2));
            ii=[];
            ii = find(distance_<(zoneDiam_cm/2));
            x_ = x_(ii); y_ = y_(ii); d_ = d_(ii);
            o=buildOccupancyMap(x_,y_,nBinPerMap,binningFactor); %build a spatial map of the time spend in each pixel for the entire exp.
            s=buildSignalMap(x_,y_,d_,nBinPerMap,binningFactor); %build a spatial map of the time spend in each pixel for the entire exp.
            sNorm = normSigMap(s,o);
            avgZoneSignal(iFile,iPeriod) = nanmean(nanmean(sNorm))
            fprintf(fod,'\t%f', avgZoneSignal(iFile,iPeriod) );
            
            figure(f_SigNormZone);
            subplot(nFiles,nPeriods,iSubPlot)
            hold on
            axis equal
            title(sprintf('m%d-p%d',iFile,iPeriod));
            colormap(parula(128));
            colorbar();
            imagesc(sNorm,[-5 10]);
            rectangle('Position',[xC yC dC dC],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
            %          plot(x_/binningFactor,y_/binningFactor)
            xlim([0 (yBoxDim_cm*(8/4))/binningFactor])
            ylim([0 (xBoxDim_cm*(8/4))/binningFactor])
            
        end
        
        
        i1 = i2+1;
    end
    
    if centralZoneAnalysis
        fprintf(fod,'\n');
    end
    
    
end


% figname = [dataRoot filesep 'occupancy'];
% print(f_Occ,[figname '.jpeg'],'-djpeg');
% print(f_Occ,[figname '.pdf'],'-dpdf');
% close(f_Occ);
%
% figname = [dataRoot filesep 'sig'];
% print(f_SigNorm,[figname '.jpeg'],'-djpeg');
% print(f_SigNorm,[figname '.pdf'],'-dpdf');
% close(f_SigNorm);
%
% figname = [dataRoot filesep 'zone-sig'];
% print(f_SigNormZone,[figname '.jpeg'],'-djpeg');
% print(f_SigNormZone,[figname '.pdf'],'-dpdf');
% close(f_SigNormZone);
xC=0
yC=0
dC=0
drawMainFigure(all_OccMaps,[0 12],xBoxDim_cm,yBoxDim_cm,binningFactor,occ_colormap,xC,yC,dC,[dataRoot filesep 'avg_occupancy'])
%  drawMainFigure(all_SmoothedOccMaps,[0 10],xBoxDim_cm,yBoxDim_cm,binningFactor)
drawMainFigure(all_NormedSigMaps,[-3 3],xBoxDim_cm,yBoxDim_cm,binningFactor,sig_colormap,xC,yC,dC,[dataRoot filesep 'avg_signal'])
%  drawMainFigure(all_SmoothedNormedSigMaps,[-5 5],xBoxDim_cm,yBoxDim_cm,binningFactor)

fclose(fod);

end

% function drawMainFigure(m,mapLims,xBoxDim_cm,yBoxDim_cm,binningFactor,colormap_,xC,yC,dC,figname)
%
% [r,c,nFiles,nPeriods] = size(m);
%
% avgMap = squeeze(nanmean(m,3));
%
% f=figure();
% set(0, 'DefaultFigureRenderer', 'painters');
% for iPeriod = 1 : nPeriods
%     subplot(1,nPeriods,iPeriod);
%     hold on
%     axis equal
%     colormap(colormap_);
%     title(sprintf('p%d',iPeriod));
%     colorbar();
%     imagesc(avgMap(:,:,iPeriod),mapLims);
%     rectangle('Position',[xC yC dC dC],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
%     xlim([0 (yBoxDim_cm*(8/4))/binningFactor])
%     ylim([0 (xBoxDim_cm*(8/4))/binningFactor])
% end
%
%
% print(f,[figname '.jpeg'],'-djpeg');
% print(f,[figname '.pdf'],'-dpdf');
%
% close(f);
%
% end

function drawMainFigure(m,mapLims,xBoxDim_cm,yBoxDim_cm,binningFactor,colormap_,xC,yC,dC,figname)

[r,c,nFiles,nPeriods] = size(m);

avgMap = squeeze(nanmean(m,3));

f=figure();

%set(0, 'DefaultFigureRenderer', 'painters');

for iPeriod = 1 : nPeriods
    subplot(1,nPeriods,iPeriod);
    %     hold on
    %     axis equal
    %     colormap(colormap_);
    %     title(sprintf('p%d',iPeriod));
    %     colorbar();
    %     imagesc(avgMap(:,:,iPeriod),mapLims);
    %     rectangle('Position',[xC yC dC dC],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
    %     xlim([0 (yBoxDim_cm*(8/4))/binningFactor])
    %     ylim([0 (xBoxDim_cm*(8/4))/binningFactor])        
    
    colormap(colormap_);
    hold on
    title(sprintf('p%d',iPeriod));
    axis equal
    axis off
    avgMapTmp=squeeze(avgMap(:,:,iPeriod));
    ii = isnan(avgMapTmp);
    avgMapTmp(ii)=0;
    gaussFilt_Map = imgaussfilt(avgMapTmp,5);
    gaussFilt_Map(ii)=nan;
    avgMapTmp(ii)=nan;
    imagesc(gaussFilt_Map,mapLims)
    xlim([0 150]);
    ylim([0 150]);
    
    set(gca,'Ydir','reverse')
    colorbar();
    %     rectangle('Position',[xC yC dC dC],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
    %     xlim([0 (yBoxDim_cm*(8/4))/binningFactor])
    %     ylim([0 (xBoxDim_cm*(8/4))/binningFactor])    
    
end

%
% print(f,[figname '.jpeg'],'-djpeg');
% print(f,[figname '.pdf'],'-dpdf');
%
% close(f);

end






function bonsai_output=getVideoTrackingData(vt_bonsaiPath,cameraMode)
bonsai_output=[];
if exist(vt_bonsaiPath,'file')
    bonsai_data = dlmread(vt_bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(vt_bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
end
end
function [B2,nSynchro,dFrame]=reSynchData(B,sig,ledDetectionThreshold)
o = B.optoPeriod;
o=o>max(o)*(ledDetectionThreshold/100);
dO=diff(o);
iUp = find(dO==1);
dFrame = median(diff(iUp));
x=1:dFrame+1;
xq = linspace(1,dFrame+1,100); %synchro LED lights upeach 100 Hamamatsuframes
nSynchro=size(iUp,1);
B2.mainX=[];
B2.mainY=[];
B2.mouseAngle=[];
B2.mouseMajorAxisLength=[];
B2.mouseMinorAxisLength=[];
B2.mouseArea=[];
B2.optoPeriod=[];
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(B.optoPeriod,1));
for i=1:nSynchro
    i1=iUp(i);i2=i1+dFrame;
    B2.mainX = [B2.mainX ; interp1(x,B.mainX(i1:i2),xq)'];
    B2.mainY = [B2.mainY ; interp1(x,B.mainY(i1:i2),xq)'];
    B2.mouseAngle = [B2.mouseAngle ; interp1(x,B.mouseAngle(i1:i2),xq)'];
    B2.mouseMajorAxisLength = [B2.mouseMajorAxisLength ; interp1(x,B.mouseMajorAxisLength(i1:i2),xq)'];
    B2.mouseMinorAxisLength = [B2.mouseMinorAxisLength ; interp1(x,B.mouseMinorAxisLength(i1:i2),xq)'];
    B2.mouseArea = [B2.mouseArea ; interp1(x,B.mouseArea(i1:i2),xq)'];
    B2.optoPeriod = [B2.optoPeriod ; interp1(x,B.optoPeriod(i1:i2),xq)'];
end
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(B2.optoPeriod,1));
% fprintf('resynch done\n');

end
function [ref,sig,d]=deltaFF(ref,sig)
meanSig=mean(sig);meanRef=mean(ref);sig = sig - meanSig;ref = ref - meanRef;sig = sig./meanSig;ref = ref./meanRef;d=sig-ref;
end
function [B,ref,sig]=removeOneMinute(B,ref,sig)
B.mainX=B.mainX(20*60:end);B.mainY=B.mainY(20*60:end);sig=sig(20*60:end);ref=ref(20*60:end);
end
function [OccupancyMap]=buildOccupancyMap(x,y,nBinPerMap,binningFactor)
xMin=0;xMax=nBinPerMap;yMin=0;yMax=nBinPerMap;OccupancyMap = nan(xMax,yMax);
nPos=size(x,1);
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos)./binningFactor)+1;j=floor(x(iPos)./binningFactor)+1;
        OccupancyMap(i,j)=nansum([OccupancyMap(i,j) 1]);
    end
end
end
function [SigMap]=buildSignalMap(x,y,d,nBinPerMap,binningFactor)
xMin=0;xMax=nBinPerMap;yMin=0;yMax=nBinPerMap;SigMap = nan(xMax,yMax);
nPos=size(x,1);
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos)./binningFactor)+1;j=floor(x(iPos)./binningFactor)+1;
        SigMap(i,j)= nansum([SigMap(i,j) d(iPos)]);
    end
end
end
function [PercAvgSigMap]=normSigMap(SumSigMap,OccupancyMap)
AvgSigMap = SumSigMap ./ OccupancyMap;  % Average Signal (deltaFF) Map base don time spent in each pixel
PercAvgSigMap = AvgSigMap .*100;  % express averaged signal map in percent of deltaFF
end
function [x,y]=cleanPosBasedOnSpeedThreshold(x,y,speedThreshold)
distance =  vt_getDistance(x,y);
%cleaning position based on abnormal speed.
idx = find(distance>speedThreshold);x(idx)=nan;y(idx)=nan;
end
function bg=getBackGroundQuick(dataRoot,dataFileTag,videoExtension)
videoPath = [dataRoot filesep dataFileTag '.' videoExtension];
bgFilename = [dataRoot filesep 'bg-' dataFileTag '.png'];
if ~exist(bgFilename)
    videoInfo=vt_getVideoInfo(videoPath);
    v = VideoReader(videoPath);
    bg = readFrame(v);
    bg = rgb2gray(bg);
    imwrite(bg,bgFilename,'jpg');
else
    bg = imread(bgFilename);
end
end
function distance =  vt_getDistance(x,y)
dx = diff(x);dy=diff(y);
distance = sqrt((dx.*dx)+(dy.*dy));
end
function v=vt_getVideoInfo(videoPath)
v = VideoReader(videoPath);
end


function [x2,y2,posData2_x,posData2_y] = reScalePositionData(posData,x,y,xBoxDim_cm,yBoxDim_cm)

x2=[];
y2=[];

xMin =  ( posData.x(1) + posData.x(3) )/2;
xMax =  (posData.x(2) + posData.x(3))/2;
xBoxDim_pix = xMax - xMin;

yMin =  (posData.y(1) + posData.y(2))/2;
yMax =  (posData.y(4) + posData.y(3))/2;
yBoxDim_pix = yMax - yMin;

posData2_x = posData.x;
posData2_y = posData.y;

% figure()
% subplot(2,2,1)
% plot(x,y,'.')
% rectangle('Position',[posData2_x(5)-(zoneDiam_cm/2) posData2_y(5)-(zoneDiam_cm/2) zoneDiam_cm zoneDiam_cm],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);



x2 = x - min(posData.x);posData2_x = posData.x  - min(posData.x);
y2 = y - min(posData.y);posData2_y = posData.y -  min(posData.y);


% subplot(2,2,2)
% plot(x2,y2,'.')
% rectangle('Position',[posData2_x(5)-(zoneDiam_cm/2) posData2_y(5)-(zoneDiam_cm/2) zoneDiam_cm zoneDiam_cm],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);

x2 = x2 ./xBoxDim_pix;posData2_x = posData2_x ./xBoxDim_pix;
y2 = y2 ./yBoxDim_pix;posData2_y = posData2_y ./yBoxDim_pix;

% subplot(2,2,3)
% plot(x2,y2,'.')
% rectangle('Position',[posData2_x(5)-(zoneDiam_cm/2) posData2_y(5)-(zoneDiam_cm/2) zoneDiam_cm zoneDiam_cm],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);
x2 =x2 .* xBoxDim_cm;posData2_x =posData2_x .* xBoxDim_cm;
y2 =y2 .*yBoxDim_cm;posData2_y = posData2_y  .*yBoxDim_cm;

% subplot(2,2,4)
% plot(x2,y2,'.')
% rectangle('Position',[posData2_x(5)-(zoneDiam_cm/2) posData2_y(5)-(zoneDiam_cm/2) zoneDiam_cm zoneDiam_cm],'Curvature',[1 1],'FaceColor','none','EdgeColor',[1 0 0]);


end



















