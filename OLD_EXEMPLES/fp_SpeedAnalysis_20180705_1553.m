function fp_SpeedAnalysis_20180704_1207

close all;clear;clc

%% Params SD OFT IC fpG1
dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180316_EPM-preFC_IC_fpG1';
% dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180307_OFT_IC-CeM_fpG1';
cameraMode = 'synchronous';
videoExtension = 'mp4'
% videoExtension = 'avi';
% groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max

%% MAIN PROGRAM
cd(dataRoot);
binningFactor=0.5;
frameRate_Hz= 20;
speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed

fileList = dir([dataRoot filesep '*.' videoExtension]);nFiles = size(fileList,1);

mycmap = [];

% PROCESS EACH DATA FILE INDIVIDUALLY
for iFile=1:nFiles
    
    [fPath, dataFileTag, ext] = fileparts(fileList(iFile).name);% Parse Filename To Use Same Filename to save analysis results
    all_Names{iFile} = dataFileTag;% Keep The filename in memory
    fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,dataFileTag)
    load([dataRoot filesep dataFileTag '.mat']);sig(1)=[];ref(1)=[];% Load fiber-photometry DATA
    % first valueof sig and ref are systematically aberant; if we control hamamatsu from Arduino, we always get one additional value due to fip matlab program
    B=getVideoTrackingData([dataRoot filesep 'bonsai-' dataFileTag '.txt'],cameraMode);% Load Behavioral DATA
    if strcmp(cameraMode,'asynchronous')
        [B_synchronized,nSynchro,dFrame]=reSynchData(B,sig,ledDetectionThreshold);
        fprintf('\t> Synchro nSig=%d dt=%d frames\n',nSynchro,dFrame);
        draw_figure6(dataRoot,dataFileTag,B,B_synchronized);
        B = B_synchronized;
    end
    
    [B,ref,sig]=removeOneMinute(B,ref,sig); %remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
    x=B.mainX;y=B.mainY;
    [ref,sig,d]=deltaFF(ref,sig); %procress deltaFF
    
    if size(x,1)>size(ref,1);
        x = x(1:size(ref,1));y = y(1:size(ref,1));
    end
    nFrames = size(x,1);T = 1: nFrames;T = T./ frameRate_Hz; %recreate time vector based on fiber-photometry frame rate
    %     xMin=0;xMax=1500;yMin=0;yMax=1500;OccupancyMap = zeros(xMax,yMax);SigMap = zeros(xMax,yMax);
    [x,y]=cleanPosBasedOnSpeedThreshold(x,y,speedThreshold); % remove all position when animal spped exceed 20 pixel per sec.
    
    dx = max(x)-min(x);dy = max(y)-min(y);dMean = mean([dx dy]);pix2cm = 60/dMean;
    x = x .* pix2cm;y = y .* pix2cm;
    
    OccupancyMapFrame=buildOccupancyMap(x,y,binningFactor); %build a spatial map of the time spend in each pixel for the entire exp.
    SumSigMap=buildSignalMap(x,y,d,binningFactor); %build a spatial map of the sum of Ca++ signal recorded in each pixel for the entire exp.
    AvgSigMap=normSigMap(SumSigMap,OccupancyMapFrame);
    
    %% DRAW Occ. MAp
    
        OccupancyMapSec = OccupancyMapFrame/20;
        f1=figure('name',dataFileTag,'Position',[20 20 800 800]);
        set(0, 'DefaultFigureRenderer', 'painters');
        colormap([0.9 0.9 0.9; jet(1024)]);
        hold on
        axis equal
        %     axis off
        ii = isnan(OccupancyMapSec);
        OccupancyMapSec(ii)=0;
        gaussFilt_OccupancyMapSec = imgaussfilt(OccupancyMapSec,5);
        gaussFilt_OccupancyMapSec(ii)=nan;
        OccupancyMapSec(ii)=nan;
        im = imagesc(gaussFilt_OccupancyMapSec,[0 0.5]);
        imagesc(gaussFilt_OccupancyMapSec)
        xlim([25 160]);
        ylim([5 140]);
        set(gca,'Ydir','reverse')
        colorbar();
        print(f1,['AvgOccupancyMap-' dataFileTag '.jpeg'],'-djpeg');
        print(f1,['AvgOccupancyMap-' dataFileTag '.dpdf'],'-dpdf');
        savefig(['AvgOccupancyMap-' dataFileTag '.fig'])
        close(f1);
    
    
    %% Draw Normilized Signal Map
    
            ii = isnan(AvgSigMap);
            AvgSigMap(ii)=0;
            gaussFilt_AvgSigMap = imgaussfilt(AvgSigMap,5);
            gaussFilt_AvgSigMap(ii)=nan;
            AvgSigMap(ii)=nan;
            f1=figure('name',dataFileTag,'Position',[20 20 800 800]);
            set(0, 'DefaultFigureRenderer', 'painters');
            hold on
            axis equal
    
            c = linspace(0,1,512);
            z = ones(512,1);
            c2 = linspace(1,0,512);
    
            load('myCustomColormap.mat');
            colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);
    %         colormap([[0.9 0.9 0.9];brewermap(1024,'RdYlGn')]);
            colorbar();
            imagesc(gaussFilt_AvgSigMap,[-3 3]);
                     imagesc(gaussFilt_AvgSigMap,[-.75 .75]);
            xlim([25 160]);
            ylim([5 140]);
            set(gca,'Ydir','reverse');
    
            print(f1,['BlackGreenAvgSigMap-' dataFileTag '.jpeg'],'-djpeg');
            print(f1,['BlackGreenAvgSigMap-' dataFileTag '.dpdf'],'-dpdf');
            savefig(['BlackGreenAvgSigMap-' dataFileTag '.fig'])
            close(f1);
    
    
    
    %% Speed Calculation
    x2 = x;y2 = y;
    windowSize_sec = 1;
    windowSize_frame = windowSize_sec * frameRate_Hz;
    x2 = [nan(windowSize_frame,1) ;x(1:end -windowSize_frame)];
    y2 = [nan(windowSize_frame,1) ;y(1:end -windowSize_frame)];
    dx = x2-x;dy = y2-y;
    dist = sqrt(dx .^2 + dy .^2);
    dist(find(dist>30))=NaN;
    speed = dist;
    acceleration = [nan; diff(speed)];
    
    
    %% 2 Dim Map, Speed, Acc, GCaMP6
    v = speed;
    a = acceleration;
    vNorm = v - min(v);
    vNorm = vNorm ./ max(vNorm);
    vNorm = vNorm*99;
    vNorm = floor(vNorm)+1;
    
    aNorm = a - min(a);
    aNorm = aNorm ./ max(aNorm);
    aNorm = aNorm*99;
    aNorm = floor(aNorm)+1;
    
    occ = zeros(100,100);
    sig = nan(100,100);
    
    for i = 1:size(v,1)
        j=aNorm(i);
        k=vNorm(i);
        if ~isnan(j) & ~isnan(k)
            occ(j,k) =  nansum([occ(j,k)+1]);
            sig(j,k) = nansum([ sig(j,k)+(d(i)*100)]);
        end
    end
    
    normSig  = sig./occ;
    
    f1 = figure();
%     set(0, 'DefaultFigureRenderer', 'painters');
    subplot(3,1,1)
    hold on
    axis equal;
    colormap([[0 0 0];cool(1024)])
    colorbar();
    ii = isnan(normSig);
    normSig(ii)=0;
    gaussFilt_normSig =  imgaussfilt(normSig,1);
    gaussFilt_normSig(ii)=nan;
    normSig(ii)=nan;
    imagesc(linspace(min(v),max(v),100),linspace(min(a),max(a),100),gaussFilt_normSig)%,[-0.02 0.02]);
    xlim([min(v) max(v)])
    ylim([min(a),max(a)])

    
    
    subplot(3,1,2)
    hold on
    plot(linspace(min(v),max(v),100),nanmean(gaussFilt_normSig));
   ylim([-2 2]);
   xlim([0 22]);
    
    subplot(3,1,3)
    hold on
%     [~,i] = min(abs(linspace(min(a),max(a),100)));
%     plot(linspace(min(v),max(v),100),normSig(i,:));

    plot(linspace(min(v),max(v),100),nanmean(normSig));
    ylim([-2 2]);
    xlim([0 22]);
    
    print(f1,['SpeedSigRelationship-' dataFileTag '.jpeg'],'-djpeg');
    print(f1,['SpeedSigRelationship-' dataFileTag '.dpdf'],'-dpdf');
    savefig(['SpeedSigRelationship-' dataFileTag '.fig'])
    close(f1);
    
    
    
    
    %     f1 = figure();
    %     set(0, 'DefaultFigureRenderer', 'painters');
    %     hold on
    %     axis equal;
    %     set(gca, 'Colormap', mycmap)
    %     colorbar
    %     ii = isnan(normSig);
    %     normSig(ii)=0;
    %     normSig =  imgaussfilt(normSig,5);
    %     imagesc(normSig);
    %     print(f1,['SpeedAccSigMap-notscaled-' dataFileTag '.jpeg'],'-djpeg');
    %     print(f1,['SpeedAccAvgSigMap-notscaled-' dataFileTag '.dpdf'],'-dpdf');
    %     savefig(['SpeedAccAvgSigMap-notscaled-' dataFileTag '.fig'])
    %     close(f1);
    
    
    %% Speed on Trajectory
    % max(dist)
    %       c = colormap(jet(floor(max(dist))+1));
    %       f=figure()
    %       set(0, 'DefaultFigureRenderer', 'painters');
    %         hold on
    %         colormap(c);
    %         colorbar();
    %         axis equal
    %         nPos = size(x,1);
    %         for iPos=2:nPos
    %             if ~isnan(dist(iPos))
    %     %         plot([x(iPos-1) x(iPos)],[y(iPos-1) y(iPos)],'color',c(distNorm(iPos),:));
    %               plot([x(iPos-1) x(iPos)],[y(iPos-1) y(iPos)],'color',c(floor(dist(iPos))+1,:));
    %             end
    %         end
    %         print(f,['speed-' dataFileTag '.jpeg'],'-djpeg');
    %         print(f,['speed-' dataFileTag '.pdf'],'-dpdf');
    %         close(f);
    
    
    
    
    
end


end


% ESSENTIALS SUBFUNCTIONS
function bonsai_output=getVideoTrackingData(vt_bonsaiPath,cameraMode)
bonsai_output=[];
if exist(vt_bonsaiPath,'file')
    bonsai_data = dlmread(vt_bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(vt_bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
end
end
function [B2,nSynchro,dFrame]=reSynchData(B,sig,ledDetectionThreshold)
o = B.optoPeriod;
o=o>max(o)*(ledDetectionThreshold/100);
dO=diff(o);
iUp = find(dO==1);
dFrame = median(diff(iUp));
x=1:dFrame+1;
xq = linspace(1,dFrame+1,100); %synchro LED lights upeach 100 Hamamatsuframes
nSynchro=size(iUp,1);
B2.mainX=[];
B2.mainY=[];
B2.mouseAngle=[];
B2.mouseMajorAxisLength=[];
B2.mouseMinorAxisLength=[];
B2.mouseArea=[];
B2.optoPeriod=[];
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(B.optoPeriod,1));
for i=1:nSynchro
    i1=iUp(i);i2=i1+dFrame;
    B2.mainX = [B2.mainX ; interp1(x,B.mainX(i1:i2),xq)'];
    B2.mainY = [B2.mainY ; interp1(x,B.mainY(i1:i2),xq)'];
    B2.mouseAngle = [B2.mouseAngle ; interp1(x,B.mouseAngle(i1:i2),xq)'];
    B2.mouseMajorAxisLength = [B2.mouseMajorAxisLength ; interp1(x,B.mouseMajorAxisLength(i1:i2),xq)'];
    B2.mouseMinorAxisLength = [B2.mouseMinorAxisLength ; interp1(x,B.mouseMinorAxisLength(i1:i2),xq)'];
    B2.mouseArea = [B2.mouseArea ; interp1(x,B.mouseArea(i1:i2),xq)'];
    B2.optoPeriod = [B2.optoPeriod ; interp1(x,B.optoPeriod(i1:i2),xq)'];
end
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(B2.optoPeriod,1));
% fprintf('resynch done\n');

end
function [ref,sig,d]=deltaFF(ref,sig)
meanSig=mean(sig);meanRef=mean(ref);sig = sig - meanSig;ref = ref - meanRef;sig = sig./meanSig;ref = ref./meanRef;d=sig-ref;
end
function [B,ref,sig]=removeOneMinute(B,ref,sig)
B.mainX=B.mainX(20*60:end);B.mainY=B.mainY(20*60:end);sig=sig(20*60:end);ref=ref(20*60:end);
end
function [x,y]=cleanPosBasedOnSpeedThreshold(x,y,speedThreshold)
distance =  vt_getDistance(x,y);
%cleaning position based on abnormal speed.
idx = find(distance>speedThreshold);x(idx)=nan;y(idx)=nan;
end
function distance =  vt_getDistance(x,y)
dx = diff(x);dy=diff(y);
distance = sqrt((dx.*dx)+(dy.*dy));
end
function [OccupancyMap]=buildOccupancyMap(x,y,binningFactor)
xMin=0;xMax=1500;yMin=0;yMax=1500;OccupancyMap = nan(xMax,yMax);
nPos=size(x,1);
x = x ./ binningFactor;y = y ./ binningFactor;
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos))+1;j=floor(x(iPos))+1;
        OccupancyMap(i,j)=nansum([OccupancyMap(i,j) 1]);
    end
end
end
function [SigMap]=buildSignalMap(x,y,d,binningFactor)
xMin=0;xMax=1500;yMin=0;yMax=1500;SigMap = nan(xMax,yMax);
nPos=size(x,1);
x = x ./ binningFactor;y = y ./ binningFactor;
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos))+1;j=floor(x(iPos))+1;
        SigMap(i,j)= nansum([ SigMap(i,j) d(iPos)]);
    end
end
end
function [PercAvgSigMap]=normSigMap(SumSigMap,OccupancyMap)
AvgSigMap = SumSigMap ./ OccupancyMap;  % Average Signal (deltaFF) Map base don time spent in each pixel
PercAvgSigMap = AvgSigMap .*100;  % express averaged signal map in percent of deltaFF
end





