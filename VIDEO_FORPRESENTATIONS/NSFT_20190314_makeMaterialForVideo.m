close all
clc
clear

dataFolder = 'C:\Users\Delcasso\Desktop\tmp-nsft-video-F276\20180830_NSFT';
sideviewDataFolder = 'C:\Users\Delcasso\Desktop\tmp-nsft-video-F276\20180830_NSFT-sideview';
analysisDataFolder = 'C:\Users\Delcasso\Desktop\tmp-nsft-video-F276\20180830_NSFT-analysis';
sig=load([sideviewDataFolder filesep 'F276-synchroLED.txt']);
sideviewVideoInfo = VideoReader([sideviewDataFolder filesep 'F276-sideview.mp4']);
sideviewVideo_nSamples = size(sig,1);

sideviewFrameStart = 2842;
sideviewFrameStop = 3368;

audioCorrection_sec=-1.5;

% removing error from bonsai on frame 1
% sig(1) = 0;

sig =(sig>max(sig)/10);
dSig = diff(sig);
iSig = find(dSig==1);
n_iSig = size(iSig,1);
d_iSig = diff(iSig);
iSig_interval = median(d_iSig);

idxMiss = find(d_iSig > iSig_interval*1.5);

n = size(idxMiss,1);

iSigCorrected=iSig;

for j = 1: n
    
    k = idxMiss(j);
    
    gap = iSigCorrected(k+1) - iSigCorrected(k);
    
    missingSteps = (gap / iSig_interval)-1;
    
    p1 = iSigCorrected(1:k);
    
    p2 =  iSigCorrected(k)+iSig_interval;
    
    for l = 2 : missingSteps
        p2 = [p2 ; p2(end)+iSig_interval];
    end
    
    
    p3 =  iSigCorrected(k+1:end);
    
    iSigCorrected = [ p1 ;p2 ; p3];
    
    idxMiss = idxMiss +missingSteps;
end

n_iSigCorrected = size(iSigCorrected,1);


%% Check Correction on Missing TRiggers
% figure()
% hold on
% plot(sig);
% for i=1:n_iSig
%     plot(iSig(i),1,'v','MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none');
%     text(iSig(i),.95,sprintf('%d',i))
% end
% for i=1:n_iSigCorrected
%     plot(iSigCorrected(i),1.1,'v','MarkerFaceColor',[0 .5 0],'MarkerEdgeColor','none');
%     text(iSigCorrected(i),1.15,sprintf('%d',i),'color',[0 .5 0]);
%     plot([iSigCorrected(i) iSigCorrected(i)],[0 1.1],'color',[0 .5 0]);
% end


%% sideView : selection : start - stop
[~,sideview_iTrigStart] = min(abs(iSigCorrected-sideviewFrameStart));
[~,sideview_iTrigStop] = min(abs(iSigCorrected-sideviewFrameStop));

%% Load Audio File
audiotrack = audioread([sideviewDataFolder filesep 'F276-sideview.mp4']);
sideviewAudio_nSamples = size(audiotrack,1);
sideviewAudio_samplingRate = sideviewAudio_nSamples / sideviewVideoInfo.Duration;
audio_nSamplePerVideoFrame = floor( sideviewAudio_nSamples / sideviewVideo_nSamples);

%% Selection part of audio file
% idxAudioStart  = sideviewFrameStart * audio_nSamplePerVideoFrame;
% idxAudioStop  = (sideviewFrameStop+1) * audio_nSamplePerVideoFrame;

idxAudioStart  = (sideviewFrameStart-1) * audio_nSamplePerVideoFrame;
idxAudioStop  = (sideviewFrameStop) * audio_nSamplePerVideoFrame;

audioCorrection_idx = floor(audioCorrection_sec * sideviewAudio_samplingRate);
idxAudioStart = idxAudioStart + audioCorrection_idx;
idxAudioStop = idxAudioStop + audioCorrection_idx;

cropAudio = audiotrack(idxAudioStart:idxAudioStop);
n_cropAudio = size(cropAudio,1);
t_cropAudio = 1: n_cropAudio;
t_cropAudio = t_cropAudio/sideviewAudio_samplingRate;

%% Load analyzed data
load([analysisDataFolder filesep 'F276.mat']);
num0 = experiment.pData.num0;
mainSig = experiment.pData.mainSig;

photo_iTrigStart = ((sideview_iTrigStart-1)*100)+1;
photo_iTrigStop = (sideview_iTrigStop-1)*100;
photo_idxStart = find(num0 == photo_iTrigStart);
photo_idxStop = find(num0 == photo_iTrigStop);
cropPhoto = mainSig(photo_idxStart:photo_idxStop);
n_cropPhoto = size(cropPhoto,1);
t_cropPhoto = 1:n_cropPhoto;
t_cropPhoto = t_cropPhoto/20;


%% Keep Only Transients in crop windows and update indices and timestamps
transients = experiment.pData.transients;
nT = size(transients,2);
toKeep = [];
for iT = 1:nT
    if (transients(iT).iMax>=photo_idxStart) && (transients(iT).iMax<=photo_idxStop)
        toKeep = [toKeep iT];
        transients(iT).indices =  transients(iT).indices- (photo_idxStart-1);
        transients(iT).iMax =  transients(iT).iMax - (photo_idxStart-1);
        transients(iT).ts =  transients(iT).ts - ((photo_idxStart-1)/20);
    end
end
transients = transients(toKeep);


%% Load AudioEvents
load([sideviewDataFolder filesep 'F276-audio.mat']);

audioCut =  audio.n0 - size(audio.track,1) ;
audioCutLag_sec = (audioCut/sideviewAudio_samplingRate) ;%+ audioCorrection_sec;
audioEvt_sec = audio.evt_sec;
nA = size(audioEvt_sec,2);
for iA = 1:nA
    audioEvents(iA).AudioTS = audioEvt_sec(iA) + audioCutLag_sec;
    audioEvents(iA).cropAudioTs = audioEvents(iA).AudioTS - (idxAudioStart/sideviewAudio_samplingRate);
    [~,audioEvents(iA).cropAudioIdx] = min(abs(t_cropAudio-audioEvents(iA).cropAudioTs))
end

toKeep = [];
for iA = 1:nA
    if (audioEvents(iA).cropAudioTs>=0) && (audioEvents(iA).cropAudioTs<=t_cropAudio(end))
        toKeep = [toKeep iA];
    end
end

audioEvents = audioEvents(toKeep);
nA = size(audioEvents,2);



save('C:\Users\Delcasso\Desktop\tmp-nsft-video-F276\videoMaterial\material4fig.mat','t_cropAudio','cropAudio','t_cropPhoto','cropPhoto','transients','audioEvents','sideviewFrameStart','sideviewFrameStop')


