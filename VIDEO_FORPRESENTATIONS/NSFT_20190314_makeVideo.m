
clear all
clc
close all
load('C:\Users\Delcasso\Desktop\tmp-nsft-video-F276\videoMaterial\material4fig.mat');
cropAudio = (cropAudio*10)-10;




% sideviewFrameStart = 2843;
% sideviewFrameStop = 3369;

nFrames = sideviewFrameStop-sideviewFrameStart+1;

f1=figure();

t1=0;
t2=nFrames/15;

c = clock();
writerObj = VideoWriter(['C:\Users\Delcasso\Desktop\tmp-nsft-video-F276\videoMaterial\' sprintf('video_%d%02d%02d-%02d%02d.avi',c(1),c(2),c(3),c(4),c(5))]);
writerObj.FrameRate = 15;
open(writerObj);

for iFrame = 1:nFrames
 
    hold on
    xlim([t1 t2])
    plot(t_cropAudio,cropAudio,'m');
    plot(t_cropPhoto,cropPhoto,'color',[0 0 0]);
    
    nT = size(transients,2);
    for iT = 1:nT
        ts = transients(iT).ts;
        amp =  transients(iT).vMax;
        iMax = transients(iT).iMax;
        plot([ts ts],[cropPhoto(iMax) 12],'Color',[0 0 0],'LineStyle',':');
        plot(ts,12,'v','MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none');
    end
    
    nA = size(audioEvents,2);
    for iA = 1:nA
        ts = audioEvents(iA).cropAudioTs;
        plot(ts,13,'v','MarkerFaceColor','m','MarkerEdgeColor','none');
        plot([ts ts], [cropAudio(audioEvents(iA).cropAudioIdx) 13],'m:');
    end
    
    t = iFrame/15;
    tend = nFrames/15;
    w = [tend - t];
    rectangle('Position',[t,-15,w,30],'EdgeColor','none','FaceColor','w')
     
    writeVideo(writerObj,getframe(gcf));
   
    clf(f1);
    
end

close(writerObj);
  






