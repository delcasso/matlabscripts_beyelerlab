clear
close all
clc


bonsaiPath = 'Z:\Optogenetics & Behavior\DATA\20181126_ICa-ICp-G1\20181126_EPM\F332-bonsai.txt';
filename = strsplit(bonsaiPath,'\')
filename= filename {end}
filename = strsplit(filename,'-')
filename= filename {1}
%% get Bonsai Data
bonsai_output={};

if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    bonsai_output.bodyX = bonsai_output.mouseX;
    bonsai_output.bodyY = bonsai_output.mouseY;
    rmfield(bonsai_output,'mouseX');rmfield(bonsai_output,'mouseY');
   
    
    
    
    f=figure();
    y=bonsai_output.mouseY;
    x=bonsai_output.mouseX;
    plot(x,y,'r')
    title('F332')
    print(f,[filename '.pdf'])

   f2=figure();
    y=bonsai_output.mouseY;
    x=bonsai_output.mouseX;
    plot(x,y,'r')
    title('F332')
    print(f2,[filename '-opto.pdf'])
   
    
    
    
   


    
    
end


