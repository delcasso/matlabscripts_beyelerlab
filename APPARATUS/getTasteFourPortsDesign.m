function tasteDesign=getTasteFourPortsDesign(apparatus)

%% PARAMS
d1 = apparatus.side_cm + 4; %Outer Diameter = 18 cm  + buffer of 4 cm
d2 = apparatus.side2_cm;     %Distance Center to Port = 9 cm

%% CENTER
x0=0;y0=0;

%% PORTS
dX = d2 / sqrt(2);  % Explanation: dX^2 + dY^2 = 9^2, dX = dY, 2 DX^2 = 9^2, dX = 9 / sqrt(2)
dY = dX;
x(1) = x0 - dX;y(1) = x0 - dY;
x(2) = x0 + dX;y(2) = x0 - dY;
x(3) = x0 - dX;y(3) = x0 + dY;
x(4) = x0 + dX;y(4) = x0 + dY;

%% (NORTH, EAST, SOUTH, WEST)
dX = d1; dY = dX; 
x2(1) = x0;y2(1) =  dY;
x2(2) = dX;y2(2) = y0;
x2(3) = x0;y2(3) = -dY;
x2(4) = -dX;y2(4) = y0;

%% RETURN
x=[x x2];y=[y y2];
tasteDesign.x = x;
tasteDesign.y = y;
