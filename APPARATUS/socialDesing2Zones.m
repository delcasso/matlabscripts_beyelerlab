function zones=socialDesing2Zones(apparatusDesign,socialDistance_cm)

nPoints=64;

x0=apparatusDesign.x;x0 = [x0 x0(1)];
y0=apparatusDesign.y;y0 = [y0 y0(1)];
H=y0(3)-y0(1);
W=x0(3)-x0(1);

xObject = x0(5);yObject = y0(5);
xJuvenile = x0(6);yJuvenile = y0(6);

zones=[];

radius = socialDistance_cm;

xCenter = xObject;
yCenter = yObject;
[x,y]=circle2points(xCenter,yCenter,radius,nPoints);
zones(1).xV=x;zones(1).yV=y;
zones(1).type = 'object';

xCenter = xJuvenile;
yCenter = yJuvenile;
[x,y]=circle2points(xCenter,yCenter,radius,nPoints);
zones(2).xV=x;zones(2).yV=y;
zones(2).type = 'juvenile';

end

