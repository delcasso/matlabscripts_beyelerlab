function socialDesign=getSocialDesign(apparatus)

x0=0;y0=0; %EPM Center

d1 = apparatus.side_cm/2;
d2 = apparatus.side_cm/4;

x(1)=x0-d1;y(1)=y0-d2;
x(2)=x0+d1;y(2)=y(1);
x(3)=x(2);y(3)=y0+d2;
x(4)=x(1);y(4)=y(3);

x(5) = x0 - d2;
y(5) = y0;

x(6) = x0 + d2;
y(6) = y0;

socialDesign.x = x;
socialDesign.y = y;