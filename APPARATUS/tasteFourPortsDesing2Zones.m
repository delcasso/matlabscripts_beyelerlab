function zones=tasteFourPortsDesing2Zones(apparatusDesign,tasteDistance_cm)
nPoints=64;
zones=[];
radius = tasteDistance_cm;
zoneNames = {'Quinine','Sucrose','Nothing','Water'};
for i=1:4
    zones=buildCircularZone(apparatusDesign.x(i),apparatusDesign.y(i),radius,nPoints,zoneNames{i},zones);
end
end

function zones=buildCircularZone(xC,yC,radius,nPoints,zoneStr,zones)
n = size(zones,2);
[zones(n+1).xV,zones(n+1).yV]=circle2points(xC,yC,radius,nPoints);
zones(n+1).type=zoneStr;
end

