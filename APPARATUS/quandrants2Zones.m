function zones=quandrants2Zones(apparatusDesign)
nPoints=64;
zones=[];

Edge.x = apparatusDesign.x(5:8);
Edge.y = apparatusDesign.y(5:8);
xCenter = Edge.x(1);
yCenter = Edge.y(2);

radius = max(Edge.x) - min(Edge.x);

zoneNames = {'Quinine','Sucrose','Nothing','Water'};

nPortions = 4;

i1=2.5*pi;

for i=1:nPortions
    i2 = i1 + (2*pi)/nPortions;
    theta = linspace(i1,i2,nPoints);
    x = radius * cos(theta) + xCenter;
    y = radius * sin(theta) + yCenter;
    i1 = i2;
    zones(i).xV = [xCenter x xCenter];
    zones(i).yV = [yCenter y yCenter];
    zones(i).type=zoneNames{i};
end

end

