function [videoTrackingData,photometryData]=replaceFramesByNan(videoTrackingData,photometryData,ii)

    videoTrackingData.mainX(ii)=nan;
    videoTrackingData.mainY(ii)=nan;
    videoTrackingData.mouseAngle(ii)=nan;
    videoTrackingData.mouseMajorAxisLength(ii)=nan;
    videoTrackingData.mouseMinorAxisLength(ii)=nan;
    videoTrackingData.mouseArea(ii)=nan;
    videoTrackingData.optoPeriod(ii)=nan;
    videoTrackingData.distance(ii)=nan;
    photometryData.sig(ii)=nan;
    photometryData.ref(ii)=nan;
  
    
end