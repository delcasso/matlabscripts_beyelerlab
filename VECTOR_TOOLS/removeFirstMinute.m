function [videoTrackingData,photometryData]=removeFirstMinute(videoTrackingData,photometryData)

    videoTrackingData.mainX=videoTrackingData.mainX((20*60)+1:end);
    videoTrackingData.mainY=videoTrackingData.mainY((20*60)+1:end);
    videoTrackingData.mouseAngle = videoTrackingData.mouseAngle((20*60)+1:end);
    videoTrackingData.mouseMajorAxisLength = videoTrackingData.mouseMajorAxisLength((20*60)+1:end);
    videoTrackingData.mouseMinorAxisLength = videoTrackingData.mouseMinorAxisLength((20*60)+1:end);
    videoTrackingData.mouseArea = videoTrackingData.mouseArea((20*60)+1:end);
    videoTrackingData.optoPeriod = videoTrackingData.optoPeriod((20*60)+1:end);
    videoTrackingData.distance = videoTrackingData.distance((20*60)+1:end);    
    photometryData.sig=photometryData.sig((20*60)+1:end);
    photometryData.ref=photometryData.ref((20*60)+1:end);
    
end