function experiment=eventBasedAnalysis_SucroseQuinine_OneServoTwoPositions_PB(experiment)


%% All Licks
% mode = 0;

%% First of each session
% mode = 1;

%% First of each trial
% mode = 2;

analysis_mode=2;

licks_gap_sec = 0.5;
sfreq = experiment.p.HamamatsuFrameRate_Hz;
nFrames = experiment.pData.nFrames;
edges_msec = experiment.p.eventBasedAnalysisEdges_msec;

S = experiment.vData.Sucrose;Q = experiment.vData.Quinine;t0 = experiment.vData.t0;


figure();hold on;plot(mapminmax(S'),'g');plot(mapminmax(Q'),'r');

s_LickIndices = findLicks(S);
q_LickIndices = findLicks(Q);
s_LickIndices = cleanLicks(s_LickIndices,licks_gap_sec,sfreq);
q_LickIndices = cleanLicks(q_LickIndices,licks_gap_sec,sfreq);

s_LickIndice_ts = t0(s_LickIndices)
q_LickIndice_ts = t0(q_LickIndices);

%% First of each session
if analysis_mode ==1
    if ~ isempty(s_LickIndices),
        s_LickIndices = s_LickIndices(1);
    end
    if ~ isempty(q_LickIndices),
        q_LickIndices = q_LickIndices(1);
    end
end

if analysis_mode == 2 
    
    behavioralStart = getBehavioralStart(experiment.p.journal,experiment.p.dataFileTag);
%     int wellPos[] = {S,S,S,S,Q,Q,S,S,Q,S,S,Q,Q,S,S,Q,Q,S,S,Q,S,Q,Q,S,Q,Q,S,Q,S,S};
% sucrose = 0, quinine = 1
    wellPos = [0,0,0,0,1,1,0,0,1,0,0,1,1,0,0,1,1,0,0,1,0,1,1,0,1,1,0,1,0,0];
    nTrials = size(wellPos,2);        
    
    sColor = [153 217 234]./255;
    qColor = [255 201 14]./255;
    
    figure()
    hold on
    x = s_LickIndice_ts;
    n = size(x,1);
    y = zeros(1,n);
    y = y + (1:n);    
    plot(x,y,'Marker','o','MarkerFaceColor',sColor,'MarkerEdgeColor','g','LineStyle','none')
    
    x = q_LickIndice_ts;
    n = size(x,1);
    y = zeros(1,n);
    y = y + (1:n);    
   plot(x,y,'Marker','o','MarkerFaceColor',qColor,'MarkerEdgeColor','g','LineStyle','none')
   
   s_LickIndice_ts_firstOfTrial = [];
   q_LickIndice_ts_firstOfTrial = [];
    
    for iTrial=1:nTrials  
        
        if wellPos(iTrial)
            colorTmp = qColor;                 
        else
            colorTmp = sColor;
        end
        
        t1 = behavioralStart + (30*(iTrial-1))
        plot([t1 t1],[0 50],'Color',colorTmp,'LineStyle',':')       
        t2 = behavioralStart + (30*(iTrial+1-1))
        
        ii1 = find(s_LickIndice_ts>t1);
        ii2 = find(s_LickIndice_ts<t2);
        iiS = intersect(ii1,ii2);
        
        ii1 = find(q_LickIndice_ts>t1);
        ii2 = find(q_LickIndice_ts<t2);
        iiQ = intersect(ii1,ii2);   
        
        if wellPos(iTrial)
            if ~isempty(iiS)
                plot(x(iiS),iiS,'Marker','o','MarkerFaceColor',sColor,'MarkerEdgeColor','r','LineStyle','none')
            end
            if ~isempty(iiQ)
                q_LickIndice_ts_firstOfTrial = [q_LickIndice_ts_firstOfTrial q_LickIndice_ts(iiQ(1))];
            end
        else
            if ~isempty(iiQ)
                plot(x(iiQ),iiQ,'Marker','o','MarkerFaceColor',qColor,'MarkerEdgeColor','r','LineStyle','none')
            end
            if ~isempty(iiS)
                s_LickIndice_ts_firstOfTrial = [s_LickIndice_ts_firstOfTrial s_LickIndice_ts(iiS(1))];
            end            
        end
        
        
        
        
        
        
        
        
        
        
    end
    
    
    s_LickIndicesTmp = [];    
    n = size(s_LickIndice_ts_firstOfTrial,2);
    for i = 1:n
        s_LickIndicesTmp = [s_LickIndicesTmp find(s_LickIndice_ts==s_LickIndice_ts_firstOfTrial(i))];
    end
    
    q_LickIndicesTmp = [];
    n = size(q_LickIndice_ts_firstOfTrial,2);
    for i = 1:n
        q_LickIndicesTmp = [q_LickIndicesTmp find(q_LickIndice_ts==q_LickIndice_ts_firstOfTrial(i))];
    end
    
    

    x = s_LickIndice_ts_firstOfTrial;
    n = size(x,1);
    y = zeros(1,n);
    y = y + (1:n);    
    plot(x,s_LickIndicesTmp,'Marker','x','MarkerFaceColor','none','MarkerEdgeColor','k','LineStyle','none')
    
    x = q_LickIndice_ts_firstOfTrial;
    n = size(x,1);
    y = zeros(1,n);
    y = y + (1:n);    
   plot(x,q_LickIndicesTmp,'Marker','x','MarkerFaceColor','none','MarkerEdgeColor','k','LineStyle','none')
    
   
   s_LickIndices = s_LickIndices(s_LickIndicesTmp);
   q_LickIndices = q_LickIndices(q_LickIndicesTmp);
   
   
end



figure()
subplot(1,2,1)
hold on
title('Sucrose Licks')
xlabel('Time (s)')
plot(t0,S)
subplot(1,2,2)
hold on
title('Inter Licks Interval')
xlabel('Time (s)')
histogram(diff(s_LickIndice_ts),0:.5:60)

bulkSignal = experiment.pData.mainSig;idx_transients = [experiment.pData.transients(:).iMax];

 [experiment.pData.s_bulkMatrix,experiment.pData.s_transientsMatrix] = getPSTHData(s_LickIndices,bulkSignal,idx_transients,edges_msec,sfreq,nFrames);
 [experiment.pData.q_bulkMatrix,experiment.pData.q_transientsMatrix] = getPSTHData(q_LickIndices,bulkSignal,idx_transients,edges_msec,sfreq,nFrames);
 
 figure();hold on;plot(experiment.pData.s_bulkMatrix','g');plot(experiment.pData.q_bulkMatrix','r');
  
end


function idx_Licks = findLicks(lickSig)
lickSig = lickSig> (max(lickSig)/10);
diff_lickSig =diff(lickSig);
idx_Licks =find(diff_lickSig==1);
end

function idx_Licks = cleanLicks(idx_Licks,licks_gap_sec,sfreq)
licks_gap_idx = licks_gap_sec * sfreq;
d_licks_idx = diff(idx_Licks);
ii = find(d_licks_idx<licks_gap_idx);
if ~ isempty(idx_Licks)
    idx_Licks(ii+1)=[];
end
end

function [bulkMatrix,transientsMatrix] = getPSTHData(idx_synchro,bulkSignal,idx_transients,edges_msec,sfreq,nFrames)

edges_sec = edges_msec/1000;
zeroPosition = find(edges_sec==0);
zeroPositionPerc = (zeroPosition/size(edges_sec,2))*100;
windowSize_sec = edges_sec(end)-edges_sec(1);
nColumns = ceil(windowSize_sec*sfreq);
edges_idx = 1:nColumns;
zeroPosition = floor((size(edges_idx,2)*zeroPositionPerc)/100);
edges_idx = edges_idx - zeroPosition;

n_idx_synchro = size(idx_synchro,1)
bulkMatrix = nan(n_idx_synchro,nColumns);
transientsMatrix=[];
if n_idx_synchro   
    transientsMatrix=get_peth(idx_synchro/sfreq,idx_transients/sfreq,edges_sec);    
    for j=1:n_idx_synchro
        i = idx_synchro(j);
        idx = edges_idx + i;
        if idx(1)>0 && idx(end)<nFrames
            bulkMatrix(j,:)=bulkSignal(idx);
        end
    end
    
end


end









