function experiment=sucrose_eventBasedAnalysis_PB(experiment)

LickSig = experiment.vData.optoPeriod;
LickSig = LickSig> (max(LickSig)/10);
dLickSig =diff(LickSig);
licks_idx =find(dLickSig==1);

licks_gap_sec = 10;
sfreq = experiment.p.HamamatsuFrameRate_Hz;
licks_gap_idx = licks_gap_sec * sfreq;

d_licks_idx = diff(licks_idx);

ii = find(d_licks_idx<licks_gap_idx);

licks_idx(ii+1)=[];

idx_synchro = licks_idx;


nFrames = experiment.pData.nFrames;
edges_msec = experiment.p.eventBasedAnalysisEdges_msec;
edges_sec = edges_msec/1000;
zeroPosition = find(edges_sec==0);
zeroPositionPerc = (zeroPosition/size(edges_sec,2))*100;
windowSize_sec = edges_sec(end)-edges_sec(1);
nColumns = ceil(windowSize_sec*sfreq);
edges_idx = 1:nColumns;
zeroPosition = floor((size(edges_idx,2)*zeroPositionPerc)/100);
edges_idx = edges_idx - zeroPosition;

bulkSignal = experiment.pData.mainSig;
idx_transients = [experiment.pData.transients(:).iMax];

n_idx_synchro = size(idx_synchro,1)
% nMax = 2;
% if n_idx_synchro>nMax
%     idx_synchro = idx_synchro(1:nMax);
%     n_idx_synchro = size(idx_synchro,1);
% end

bulkMatrix = nan(n_idx_synchro,nColumns);

if n_idx_synchro
    
    tansientsMatrix=get_peth(idx_synchro/sfreq,idx_transients/sfreq,edges_sec);
    
    for j=1:n_idx_synchro
        i = idx_synchro(j);
        idx = edges_idx + i;
        if idx(1)>0 && idx(end)<nFrames
            bulkMatrix(j,:)=bulkSignal(idx);
        end
    end
    
end

m = nanmean(bulkMatrix);
sem = nanstd(bulkMatrix) / sqrt(size(bulkMatrix,1));
experiment.pData.avgBulkSignalAroundFood = m;

end

