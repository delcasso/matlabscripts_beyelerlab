function experiment  = resultsWriteTransientsFrequency(experiment)

[r,c] = size(experiment.p.results);

index = getJournalIndex(experiment.p.results,experiment.p.dataFileTag);


if ~ismember('transients_frequency', experiment.p.results.Properties.VariableNames)
    experiment.p.results.transients_frequency = nan(r,1);
end

nTransitens = size(experiment.pData.transients,2);
duration_sec = (experiment.pData.nFrames / experiment.p.HamamatsuFrameRate_Hz);

experiment.p.results.transients_frequency(index) = nTransitens / duration_sec;
experiment.pData.sessionaveraged_transients_frequency =  nTransitens / duration_sec;


