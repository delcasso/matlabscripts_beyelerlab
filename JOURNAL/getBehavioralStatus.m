function status = getBehavioralStatus(journal,dataFileTag)
ii = find(journal.MouseNum==str2double(dataFileTag(2:end)));
status = journal.BehavioralCode(ii);