function status = getSignalStatus(journal,dataFileTag)
ii = find(journal.MouseNum==str2double(dataFileTag(2:end)));
status = journal.SignalCode(ii);