function behavioralStart = getBehavioralStart(journal,dataFileTag)
ii = find(journal.MouseNum==str2double(dataFileTag(2:end)));
behavioralStart = journal.behavioralStart(ii);