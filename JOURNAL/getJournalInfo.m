function info = getJournalInfo(journal,dataFileTag)
ii = find(journal.MouseNum==str2double(dataFileTag(2:end)));
info.MouseNum = journal.MouseNum(ii);
info.Sex = journal.Sex(ii);
info.RegionCode = journal.RegionCode(ii);
info.Opsin = journal.Opsin(ii);
info.HistologyCode = journal.HistologyCode(ii);
info.BatchCode = journal.BatchCode(ii);