folder = 'Z:\Optogenetics & Behavior\DATA\20190204_ICa-ICp-G2\20190214_RTPP';

L = dir([folder filesep '*videoMean.mat']);

nF = size(L,1);

for iF=1:nF
    n = L(iF).name;
    source= [folder filesep n]
    sep = findstr(n,'-');    
    if isempty(sep)
        insertionpoint = findstr(n,'videoMean.mat');
        dest= [folder filesep n(1:insertionpoint(1)-1) '-videoMean.mat']        ;
        [status,msg,msgID] = movefile(source,dest);
    end       
end
