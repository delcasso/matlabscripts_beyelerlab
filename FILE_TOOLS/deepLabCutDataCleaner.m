% function deepLabCutDataCleaner(d)

d = pwd;

removeFiles_h5 = 1;
removeFiles_pickle = 1;
removeFiles_vtcorrect = 0;

if removeFiles_h5
    l = dir([d filesep '*.h5']);
    n=size(l,1);
    for i=1:n
        delete([d filesep l(i).name])
    end
end

if removeFiles_pickle
    l = dir([d filesep '*.pickle']);
    n=size(l,1);
    for i=1:n
        delete([d filesep l(i).name])
    end
end

if removeFiles_vtcorrect
    l = dir([d filesep '*-vtcorrect.txt']);
    n=size(l,1);
    for i=1:n
        delete([d filesep l(i).name]);
    end
end


l = dir([d filesep '*.csv']);
n=size(l,1);
for i=1:n
    oldName = l(i).name
    i = findstr('filtered',oldName);
    if ~isempty(i)
        suffix = '-deeplabcutFiltered';
    else
        suffix = '-deeplabcut';
    end
    newName = [oldName(1:4) suffix '.csv']
    oldName = [d filesep oldName]
    newName = [d filesep newName]
    if ~strcmp(oldName,newName)        
        movefile(oldName,newName);
    end
end







% end