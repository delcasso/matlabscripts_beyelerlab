function sd_format_filenames(foldername)

if isempty(foldername)
    foldername=pwd
    x = input (sprintf('Do You Want To process this folder ?',pwd),'s');
else
    x = 'Y';
end


switch x
    
    case 'Y'
        
        fileList = dir([foldername filesep '*.*']);
        nFiles = size(fileList,1);
        
        for iFile=1:nFiles
            
            f = fileList(iFile).name;
            
            fprintf('\tprocessing file %s\n',f);
            
            %step 1 locate 'M' or 'F'
            
            i1 = findstr('M',f);
            if ~isempty(i1)
                fprintf('\t\tMale M found at position %d\n',i1);
            else
                i1 = findstr('F',f);
                if ~isempty(i1)
                    fprintf('\t\tFemale F found at position %d\n',i1);
                end
            end
            
            if isempty(i1)
                fprintf('\t\terror no M or F, this file could not be processed\n');
            else
                % step to locate separator
                
                j  =  findstr('-',f(i1:end));
                k =  findstr('_',f(i1:end));
                l  =  findstr('.',f(i1:end));
                
                i2 = min([j k l]);
                
                mouseName = f(i1:i1+i2-2);
                
                dest = [];
                
                if ~isempty(findstr('calibration',f))
                    dest = sprintf('%s-calibration.jpg',mouseName);
                end
                if ~isempty(findstr('mat',f))
                    dest = sprintf('%s.mat',mouseName);
                end
                if ~isempty(findstr('avi',f))
                    dest = sprintf('%s.avi',mouseName);
                end
                if ~isempty(findstr('PNG',f))
                    dest = sprintf('%s-screenshot.png',mouseName);
                end
                
                if ~isempty(dest)
                    fprintf('\t\tmove %s to %s\n',f,dest);
                    fprintf('\t\tDo You Wat To Make This Move ?\n');
                    x2 = input ('','s');
                    switch x
                        case 'Y'
                            fprintf('\t\tmake it\n');
                            [status,msg]  = movefile([foldername filesep f],[foldername filesep dest]);
                             fprintf('\t\t DONE msg = %s, status = %s\n',msg,status);
                        otherwise
                            fprintf('\t\tpass\n');
                    end
                end
                
            end
        end
        
    otherwise
        fprintf('quit\n');
end


end