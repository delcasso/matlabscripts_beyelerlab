function newFilePath=format_filenames(filePath)

newFilePath = filePath;


% filePath = '/home/delcasso/tmp/F354-01172019105554.avi';



[filepath,name,ext] = fileparts(filePath);
logPath = 'format_filename.log';

switch ext
    
    case '.avi'
    
        sep = findstr('-',name);
        
        if ~isempty(sep)
            
            tag = name(sep(1):end);
            r = checkTag(tag);
            
            if ~r
                
                newName = name(1:sep(1)-1);
                
                if ~exist([filepath filesep newName ext],'file')            
                
                    fod = fopen([filepath filesep logPath],'a');
                    fprintf(fod,sprintf('%s\t%s\n',[name ext],[newName ext]));
                    fclose(fod);
                    
                    source = [filepath filesep name ext];
                    destination = [filepath filesep newName ext];
                    [status,msg,msgID] = movefile(source,destination);
                    newFilePath = [filepath filesep destination];
                    
                else
                    
                    fprintf([name ext '\t']);
                    fprintf('destination file already exists !!\n');
                    fprintf('danger program needs to know all extensions for safety !!\n');
                
                end
                
                
            end
            
        end
end

end


function r = checkTag(tag)
switch tag
    case '-sideview'
        r=1;
    otherwise
        r = 0;
end
end
