clc;
clear;
close all;

folder = 'Z:\Photometry&Behavior\01_DATA\20190107_ICa-ICp_G4\20190122_NSFT-CONTROL';

%% 01 Rename Bonsai Files
% L = dir([folder filesep 'bonsai*']);
% nF = size(L,1);
% for iF=1:nF
%     n = L(iF).name;
%     source= [folder filesep n]
%     sep = findstr(n,'-');
%     dest= [folder filesep n(1:sep(2)-1) '.txt']
% %     [status,msg,msgID] = movefile(source,dest);
% end

%% 02 Rename Video Files (*.mp4)
% L = dir([folder filesep '*.mp4']);
% nF = size(L,1);
% for iF=1:nF
%     n = L(iF).name;
%     source= [folder filesep n]
%     sep = findstr(n,'-');
%     dest= [folder filesep n(1:sep(1)-1) '.mp4']
%     [status,msg,msgID] = movefile(source,dest);
% end
% 


% 02 Rename Video Files (*avi)
% L = dir([folder filesep '*.avi']);
% nF = size(L,1);
% for iF=1:nF
%     n = L(iF).name;
%     source= [folder filesep n]
%     sep = findstr(n,'-');
%     dest= [folder filesep n(1:sep(1)-1) '.avi']
% %     [status,msg,msgID] = movefile(source,dest);
% end

% 
% % 03 Rename Calibration Files
% L = dir([folder filesep '*.jpg']);
% nF = size(L,1);
% for iF=1:nF
%     n = L(iF).name;
%     source= [folder filesep n]
%     sep = findstr(n,'_');
%     dest= [folder filesep n(1:sep(1)-1) '-calibration.jpg']
%     [status,msg,msgID] = movefile(source,dest);
% end

% % 04 Rename photometry mat Files
% L = dir([folder filesep '*.mat']);
% nF = size(L,1);
% for iF=1:nF
%     n = L(iF).name;
%     
% %     if isempty(findstr('videoMean',n))
%         source= [folder filesep n]
%         sep = findstr(n,'_');
%         dest= [folder filesep n(1:sep(1)-1) '.mat']
%             [status,msg,msgID] = movefile(source,dest);
% %     end
% end

%% 05 Rename NSFT-sideview files
% L = dir([folder filesep '*.mp4']);
% nF = size(L,1);
% for iF=1:nF
%     n = L(iF).name;
%     source= [folder filesep n];
%     [filepath,name,ext] = fileparts(n); 
%     sep = findstr(name,'_');
%     n2 = [name(sep(2)+1:end) '-sideview.mp4'];
%     dest = [folder filesep n2];
%     prompt = sprintf('renaming : %s ==> %s? Y/N [Y]: ',n,n2);
%     str = input(prompt,'s');
%     if isempty(str)
%         str = 'N';
%     end
%     if strcmp(str,'Y')
%         fprintf('move')
%         [status,msg,msgID] = movefile(source,dest);
%     end
% end
