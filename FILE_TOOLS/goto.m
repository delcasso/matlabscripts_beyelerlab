function goto(str)

if isempty(str)
    
    fprintf('BS : Z:\\__Softwares\\MatlabScripts_BeyelerLab\n');
    fprintf('PD : Z:\\PhotometryAndBehavior\\01_DATA\n');
    
else
    
    switch str
        
        case 'BS'
            BS_folder = 'C:\__Softwares\MatlabScripts_BeyelerLab';
            if ~exist(BS_folder,'dir')
                BS_folder = 'Z:\__Softwares\MatlabScripts_BeyelerLab';
            end
            if ~exist(BS_folder,'dir')
                BS_folder = '/media/delcasso/shares/nas1/__Softwares/MatlabScripts_BeyelerLab';
            end
            if exist(BS_folder,'dir')
                cd(BS_folder)
            else
                fprintf('The link doesn''t exsit %s',BS_folder)
            end
            
        case 'PD'
            PD_folder = 'C:\PhotometryAndBehavior\01_DATA';
                        if ~exist(PD_folder,'dir')
                PD_folder = 'Z:\PhotometryAndBehavior\01_DATA';
            end
            if ~exist(PD_folder,'dir')
                PD_folder = '/media/delcasso/shares/nas1/PhotometryAndBehavior/01_DATA';
            end
            if exist(PD_folder,'dir')
                cd(PD_folder)
            else
                fprintf('The link doesn''t exsit %s',PD_folder)
            end
            
        case 'PA'
            PD_folder = 'C:\PhotometryAndBehavior\03_ANALYSIS';
                        if ~exist(PD_folder,'dir')
                PD_folder = 'Z:\PhotometryAndBehavior\03_ANALYSIS';
            end
            if ~exist(PD_folder,'dir')
                PD_folder = '/media/delcasso/shares/nas1/PhotometryAndBehavior/03_ANALYSIS';
            end
            if exist(PD_folder,'dir')
                cd(PD_folder)
            else
                fprintf('The link doesn''t exsit %s',PD_folder)
            end            

        case 'OD'
            PD_folder = 'C:\OptogeneticsAndBehavior\DATA';
                        if ~exist(PD_folder,'dir')
                PD_folder = 'Z:\OptogeneticsAndBehavior\DATA';
            end
            if ~exist(PD_folder,'dir')
                PD_folder = '/media/delcasso/shares/nas1/OptogeneticsAndBehavior/DATA';
            end
            if exist(PD_folder,'dir')
                cd(PD_folder)
            else
                fprintf('The link doesn''t exsit %s',PD_folder)
            end          
            
        case 'OA'
            PD_folder = 'C:\OptogeneticsAndBehavior\ANALYSIS';
                        if ~exist(PD_folder,'dir')
                PD_folder = 'Z:\OptogeneticsAndBehavior\ANALYSIS';
            end
            if ~exist(PD_folder,'dir')
                PD_folder = '/media/delcasso/shares/nas1/OptogeneticsAndBehavior/ANALYSIS';
            end
            if exist(PD_folder,'dir')
                cd(PD_folder)
            else
                fprintf('The link doesn''t exsit %s',PD_folder)
            end                 
            
    end
    
end