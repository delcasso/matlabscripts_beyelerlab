function zones=getEpmZones_201810011507(params)

% warning('drawEpmBoundaries doesn''t work if open arms are located vertically');

% %% Params EPM IC-SweetBitter G1
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
% % params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
% params.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
% params.cameraMode = 'synchronous';
% params.ledDetectionThreshold = 50; %percent of led signal max
% params.videoExtension='avi';
% params.HamamatsuFrameRate_Hz= 20;
% params.behaviorCameraFrameRate_Hz=20;
% 
% params.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
% 
% params.savePDF = 0;
% params.saveFIG = 0;
% params.savePNG = 0;
% 
% params.forceBehavioralStart = 0;
% 
% params.OccupancyMap_sigmaGaussFilt=2;
% params.PhotometrySignalMap_sigmaGaussFilt=2;
% 
% params.calciumAnalysis_highPath_Detection_Hz = 1;
% params.calciumAnalysis_highPath_Trace_Hz = 0.075;
% params.deltaFF_slidingWindowWidth = 1200;
% 
% params.epmOpenArmsEnvergure = 80;
% params.epmClosedArmsEnvergure = 75;
% params.epmArmsWidth = 5;
% 
% params.lookingForMouse = '';
% 
% params.dataFileTag='F251';

epmBoundaries=drawEpmBoundaries_201809261446(params);

x=epmBoundaries.xMaze_pix;
y=epmBoundaries.yMaze_pix;

i=1;
zones(i).type = 'open';
zones(i).color=[0 0 1];
zones(i).X=x(1);
zones(i).Y=y(1);
zones(i).W=x(2)-x(1);
zones(i).H=y(12)-y(1);
zones(i).positionSTR='WEST';
zones(i).lineStyle='-';
zones(i).position = [zones(i).X zones(i).Y zones(i).W zones(i).H];

i=2;
zones(i).type = 'closed';
zones(i).color=[1 0 0];
zones(i).X=x(3);
zones(i).Y=y(3);
zones(i).W=x(4)-x(3);
zones(i).H=y(2)-y(3);
zones(i).positionSTR='NORTH';
zones(i).lineStyle='-';
zones(i).position = [zones(i).X zones(i).Y zones(i).W zones(i).H];

i=3;
zones(i).type = 'open';
zones(i).color=[0 0 1];
zones(i).X=x(5);
zones(i).Y=y(5);
zones(i).W=x(6)-x(5);
zones(i).H=y(8)-y(5);
zones(i).positionSTR='EAST';
zones(i).lineStyle='-.';
zones(i).position = [zones(i).X zones(i).Y zones(i).W zones(i).H];

i=4;
zones(i).type = 'closed';
zones(i).color=[1 0 0];
zones(i).X=x(11);
zones(i).Y=y(11);
zones(i).W=x(8)-x(11);
zones(i).H=y(10)-y(11);
zones(i).positionSTR='SOUTH';
zones(i).lineStyle='-.';
zones(i).position = [zones(i).X zones(i).Y zones(i).W zones(i).H];

i=5;
zones(i).type = 'center';
zones(i).color=[0 1 0];
zones(i).X=x(2);
zones(i).Y=y(2);
zones(i).W=x(5)-x(2);
zones(i).H=y(11)-y(2);
zones(i).positionSTR='CENTER';
zones(i).lineStyle='-';
zones(i).position = [zones(i).X zones(i).Y zones(i).W zones(i).H];


% f1=figure();
% hold on
% axis equal
% axis off
% % set(gca,'Ydir','reverse')
% colormap(gray(1024));
% bg=getBackGroundQuick(params);
% imagesc(bg);
% for iZone=1:5
%     iZone
%     zones(iZone).position
%     rectangle('Position',zones(iZone).position,'EdgeColor',zones(iZone).color,'FaceColor','none','LineStyle',zones(iZone).lineStyle)
% end















