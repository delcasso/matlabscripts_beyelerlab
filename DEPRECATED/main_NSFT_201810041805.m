close all;clear;clc

%% DATA LIBRARIES
dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180817_NSFT';
dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180830_NSFT';

nsft{1}.Model='Pierre Feugas';
nsft{1}.side_cm = 60; % Open Arms Envergure

nsft{2}.Model='Pierre Feugas';
nsft{2}.side_cm = 60; % Open Arms Envergure

videoExt{1}='avi';
videoExt{2}='avi';


%% PARAMETERS

p.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\NSFT' ;
p.cameraMode = 'synchronous';
p.ledDetectionThreshold = 50; %percent of led signal max
p.HamamatsuFrameRate_Hz= 20;
p.behaviorCameraFrameRate_Hz=20;
p.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
p.savePDF = 0;
p.saveFIG = 0;
p.savePNG = 0;
p.forceRedrawing = 0;
p.forceBehavioralStart = 0;
p.MapScale_cmPerBin = 0.5;
p.OccupancyMap_sigmaGaussFilt=5;
p.PhotometrySignalMap_sigmaGaussFilt=5;
p.calciumAnalysis_highPath_Detection_Hz = 1;
p.calciumAnalysis_highPath_Trace_Hz = 0.075;
p.deltaFF_slidingWindowWidth = 1200;
p.lookingForMouse = '';

%% TO PROCESS ALL FOLDERS
for iFolder=1:3
    p.dataRoot = dataRoot{iFolder};
    p.apparatus = nsft{iFolder};
    p.videoExtension=videoExt{iFolder};
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        if ~isempty(p.lookingForMouse)
            processThisFile=0;
            if strcmp(p.dataFileTag,p.lookingForMouse)
                processThisFile=1;
            end
        end
        %% MAIN PROCESSING
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        
        fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);
        
        experiment = loadExpData(p);
        
        experiment = processCaSignal(experiment);
        experiment = standardizeEpmData(experiment);      
        
        vData=experiment.vData;pData=experiment.pData;p=experiment.p;
        
        vData.zones_cmSP=epmDesing2Zones(experiment.apparatusDesign_cmSP);          
        vData.inZone=position2zone(vData.mainX_cmSP,vData.mouseY_cmSP,vData.zones_cmSP);
        vData.nZones = size(vData.zones_cmSP,2);   
        [vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone,vData.nZones);
        pData.transients=transientsPerZone(vData.inZone,vData.nZones,vData.zoneTime_fr,pData.transients);
       
        
        %% CREATING ACTIVITY MAPS
        map.Occ.IO=buildOccupancyMap_cmStandardPositive(vData.mainX_cmSP,vData.mouseY_cmSP,p.xMax,p.yMax,p.MapScale_cmPerBin); %build a spatial map of the time spend in each pixel for the entire exp.
        map.SumSig.IO=buildSignalMap_cmStandardPositive(vData.mainX_cmSP,vData.mouseY_cmSP,pData.mainSig,p.xMax,p.yMax,p.MapScale_cmPerBin); %build a spatial map of the sum of Ca++ signal recorded in each pixel for the entire exp.
        map.NormSig.IO=normSigMap(map.SumSig.IO,map.Occ.IO);
        map.SumTransients.IO=buildSignalMap_cmStandardPositive(vData.mainX_cmSP,vData.mouseY_cmSP,pData.transients.cont,p.xMax,p.yMax,p.MapScale_cmPerBin); %build a spatial map of the sum of Ca++ signal recorded in each pixel for the entire exp.
        map.NormTransients.IO=normSigMap(map.SumTransients.IO,map.Occ.IO);
               
        %% DRAW MAPS
        drawVideoTrackingTrajectory(p,vData.mainX,vData.mouseY); % Draw traceXXX.jpeg
        %         drawTrajectoryOverlayedWithSpeed(params,videoTrackingData.mainX_cmSP,videoTrackingData.mouseY_cmSP,xMax,yMax)
        drawOccupancyMap(p,map.Occ,p.xMax,p.yMax); % Draw OccupancyMapXXX.jpeg
        drawPhotometrySignalMap(p,map.NormSig,p.xMax,p.yMax); % Draw SignalMapXXX.jpeg
        drawPhotometryContinuousTransientsSignalMap(p,map.NormSig,p.xMax,p.yMax); % Draw SignalMapXXX.jpeg
        
        
        
        %% SAVING DATA
        save([p.mergedOutputsFolder filesep p.dataFileTag '.mat'],'map','vData','pData');
        
    end
end

