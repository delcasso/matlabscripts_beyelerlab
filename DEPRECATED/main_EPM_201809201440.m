close all;clear;clc

%% Params EPM IC-SweetBitter G1
params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
params.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
params.cameraMode = 'synchronous';
params.ledDetectionThreshold = 50; %percent of led signal max
params.videoExtension='avi';
params.HamamatsuFrameRate_Hz= 20;
params.behaviorCameraFrameRate_Hz=20;

params.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed

params.savePDF = 0;
params.saveFIG = 0;
params.savePNG = 0;

params.forceBehavioralStart = 0;

params.OccupancyMap_sigmaGaussFilt=2;
params.PhotometrySignalMap_sigmaGaussFilt=2;

params.calciumAnalysis_highPath_Detection_Hz = 1;
params.calciumAnalysis_highPath_Trace_Hz = 0.075;
params.deltaFF_slidingWindowWidth = 1200;

params.lookingForMouse = '';




%% MAIN PROGRAM
fileList = dir([params.dataRoot filesep '*.' params.videoExtension]);nFiles = size(fileList,1);
all_Names = {};all_LSZ = {};% all_synchroSig = {};

% PROCESS EACH DATA FILE INDIVIDUALLY
for iFile=1:nFiles
    
    params.filename = fileList(iFile).name;
    [params.fPath, params.dataFileTag, params.ext] = fileparts(params.filename);% Parse Filename To Use Same Filename to save analysis results
    
    processThisFile=1;
    if ~isempty(params.lookingForMouse)
        processThisFile=0;
        if strcmp(params.dataFileTag,params.lookingForMouse)
            processThisFile=1;
        end
    end
    
    
    if processThisFile
    

    fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,params.dataFileTag);
    
    % LOADING, SYNCHRONIZING AND CLEANING THE DATA
    photometryData = loadPhotometryData(params);
    videoTrackingData = getVideoTrackingData(params);
    videoTrackingData.bg=getBackGroundQuick(params);
    [photometryData,videoTrackingData] =  HamamatsuBlackFlyCorrectFrameNumbers(photometryData,videoTrackingData,params);
    if strcmp(params.cameraMode,'asynchronous')
        [videoTrackingData,nSynchro,dFrame]=reSynchData(videoTrackingData,photometryData,params)
        fprintf('\t> Synchro nSig=%d dt=%d frames\n',nSynchro,dFrame);
        draw_figure6(params.dataRoot,params.dataFileTag,videoTrackingData,B_synchronized);
    end
    
    iBehavioralStarts=detectBehavioralStart(params);
    nFramesToRemove = params.HamamatsuFrameRate_Hz*60; %remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
    if iBehavioralStarts>nFramesToRemove
        nFramesToRemove=iBehavioralStarts(1); % After the minute of bleaching, the animal was placed on the maze by the experimenter, iBehavioralStarts(1) represents the time the experimenter as left the camera field of view
    end
    [videoTrackingData,photometryData]=removeFrames(videoTrackingData,photometryData,nFramesToRemove,'Beginning');%remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
    
    
    % DETECT EXPERIMENTER INTERRUPTION OF BEHAVIOR
    nExperimenterIntervention = size(iBehavioralStarts,2);
    if nExperimenterIntervention>1, warning('Abnormal Intervention of Experimenter, did animal fall of EPM ?');end
    
    % DETECT ABNORMALITIES IN PHOTOMETRY SIGNAL
    [photometryData,videoTrackingData]= detectFiberDisconnection(params,photometryData,videoTrackingData);
    
    % DETECT ABNORMAL PHOTOMETRY SIGNAL WITH NEGATIVE RAW VALUES
    photometryData = detectAbnormalNegativeValuesInPhotometryRawData(photometryData);
    
    videoTrackingData=cleanPosBasedOnSpeedThreshold(videoTrackingData,params); % remove all position when animal spped exceed 20 pixel per sec.
    nFrames = size(videoTrackingData.mainX,1);T = 1: nFrames;T = T./ params.HamamatsuFrameRate_Hz;  photometryData.T = T; %recreate time vector based on fiber-photometry frame rate
    fprintf('\t#Hamamatsu %d',size(photometryData.sig,1));fprintf('\t\t#BlackFly %d\n',size(videoTrackingData.mainX,1));
    
    % FINDING ARMS, ZONES, POSITIONS IN VIDEO
    videoTrackingData.zones = getEpmZones(params,videoTrackingData);
       
    CaSignal = analyzeRawCaSignal_2018009251459(photometryData.sig,photometryData.ref,params.HamamatsuFrameRate_Hz,params.calciumAnalysis_highPath_Detection_Hz,params.calciumAnalysis_highPath_Trace_Hz,0,params.deltaFF_slidingWindowWidth);
    photometryData.mainSig = CaSignal.slidingdiffSig_fit;
    
    figName = [params.dataRoot filesep 'figures' filesep params.dataFileTag '-photometryProcessedSignal.jpeg'];
    if ~exist(figName,'file')        
        f1=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
        plot(photometryData.mainSig,'color',[0 255 23]./255);
        print(f1,figName,'-djpeg');
        close(f1);
    end
    
    % CREATING ACTIVITY MAPS
    
    activityMaps.OccupancyMap=buildOccupancyMap(videoTrackingData.mainX,videoTrackingData.mouseY); %build a spatial map of the time spend in each pixel for the entire exp.
    activityMaps.SumSignalMap=buildSignalMap(videoTrackingData.mainX,videoTrackingData.mouseY,photometryData.mainSig); %build a spatial map of the sum of Ca++ signal recorded in each pixel for the entire exp.
    activityMaps.PercAvgSigMap=normSigMap(activityMaps.SumSignalMap,activityMaps.OccupancyMap);
    
    drawVideoTrackingTrajectory(params,videoTrackingData.mainX,videoTrackingData.mouseY); % Draw traceXXX.jpeg
    drawTrajectoryOverlayedWithSpeed(params,videoTrackingData.mainX,videoTrackingData.mouseY)
    drawOccupancyMap(params,activityMaps.OccupancyMap); % Draw OccupancyMapXXX.jpeg
    drawPhotometrySignalMap(params,activityMaps.PercAvgSigMap); % Draw SignalMapXXX.jpeg
    
    [activityMaps.OccIn,activityMaps.OccOut,activityMaps.SumSigIn,activityMaps.SumSigOut]=getEpmWithinArmDirection(videoTrackingData.mainX,videoTrackingData.mouseY,photometryData.mainSig,videoTrackingData.zones);
    activityMaps.PercAvgSigMap_I=normSigMap(activityMaps.SumSigIn,activityMaps.OccIn);
    activityMaps.PercAvgSigMap_O=normSigMap(activityMaps.SumSigOut,activityMaps.OccOut);
    
    outputPrefix = 'IN-OUT';
    %ZS stands for Zoned Signal, LZS for linearized Zoned Signal
    [avgActivityMaps_IO.ZS, avgActivityMaps_IO.LZS, avgActivityMaps_IO.minLZS, avgActivityMaps_IO.maxLZS] = epm_alignSignalToArms(params,activityMaps.PercAvgSigMap,videoTrackingData.zones,outputPrefix);
    draw_linearizedActivityMaps(params,outputPrefix,activityMaps.PercAvgSigMap,avgActivityMaps_IO,videoTrackingData);
    
    %     %% for each arm, calculate a vector of signal value, from center to periphery
    %     draw_figure4(dataRoot,dataFileTag,outputPrefix,PercAvgSigMap,zones_IO,minLZD_IO,maxLZD_IO);
    %     % Draw ZonedSignalMapXXX.jpeg outputPrefix = 'IN';
    %     [zones_I,minLZD_I,maxLZD_I]=epm_alignSignalToArms(dataRoot,dataFileTag,PercAvgSigMap_I,zones,outputPrefix);
    %     %% for each arm, calculate a vector of signal value, from center to periphery
    %     draw_figure4(dataRoot,dataFileTag,outputPrefix,PercAvgSigMap_I,zones_I,minLZD_I,maxLZD_I);
    %     % Draw ZonedSignalMapXXX.jpeg outputPrefix = 'OUT';
    %     [zones_O,minLZD_O,maxLZD_O]=epm_alignSignalToArms(dataRoot,dataFileTag,PercAvgSigMap_O,zones,outputPrefix);
    %     %% for each arm, calculate a vector of signal value, from center to periphery
    %     draw_figure4(dataRoot,dataFileTag,outputPrefix,PercAvgSigMap_O,zones_O,minLZD_O,maxLZD_O);
    %     % Draw ZonedSignalMapXXX.jpeg
    %
    %     [all_LSZ{iFile}.Open_IO,all_LSZ{iFile}.Closed_IO]=epm_avgLSZ_sameArmType(zones_IO);
    %     [all_LSZ{iFile}.Open_I,all_LSZ{iFile}.Closed_I]=epm_avgLSZ_sameArmType(zones_I);
    %     [all_LSZ{iFile}.Open_O,all_LSZ{iFile}.Closed_O]=epm_avgLSZ_sameArmType(zones_O);
    %     all_Names{iFile} = dataFileTag;% Keep The filename in memory
    
        save([params.mergedOutputsFolder filesep params.dataFileTag '.mat'],'activityMaps','videoTrackingData','photometryData','avgActivityMaps_IO');
    else
         fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,params.dataFileTag);
    end
    
end

% % GL0BAL AVERAGE
% nMice = size(all_LSZ,2);
% maxArmSize = [];
% suffix1={'Open','Closed'};
% suffix2={'IO','I','O'};
% % Find maxArmSize
% for iMouse=1:nMice
%     for iSuffix1=1:2
%         for iSuffix2=1:3
%             for iArm=1:2
%                 cmd = sprintf('[s1,s2]=size(all_LSZ{%d}.%s_%s{%d});',iMouse,suffix1{iSuffix1},suffix2{iSuffix2},iArm);eval(cmd);
%                 maxArmSize=[maxArmSize max([s1 s2])];
%             end
%         end
%     end
% end
% maxArmSize=max(maxArmSize);
% % Average Open and Closed Arms per session
% for iSuffix1=1:2
%     for iSuffix2=1:3
%         cmd = sprintf('%s_%s=nan(nMice,maxArmSize);',suffix1{iSuffix1},suffix2{iSuffix2});eval(cmd);
%         for iMouse=1:nMice
%             v=nan(2,maxArmSize);
%             for iArm=1:2
%                 cmd = sprintf('tmp=all_LSZ{%d}.%s_%s{%d};',iMouse,suffix1{iSuffix1},suffix2{iSuffix2},iArm);eval(cmd);
%                 size_=size(tmp);[~,idxMax]=max(size_);
%                 if idxMax==1, tmp=tmp';end
%                 v(iArm,1:size_(idxMax))=tmp;
%             end
%             v=nanmean(v);
%             cmd = sprintf('%s_%s(%d,:)=v;',suffix1{iSuffix1},suffix2{iSuffix2},iMouse);eval(cmd);
%             endll
%         end
%     end
%
%     % save([dataRoot filesep 'results.mat'],'Open*','Close*','all_Names','all_Group');
%     save([dataRoot filesep 'results.mat'],'Open*','Close*','all_Names');
%     clear all

%% ESSENTIALS SUBFUNCTIONS



function [Open,Closed]=epm_avgLSZ_sameArmType(zones)
Open={};iOpen=0;Closed={};iClosed=0;
for iZone=1:5
    switch zones(iZone).type
        case 'closed'
            iClosed=iClosed+1;
            Closed{iClosed}=zones(iZone).LSZ;
        case 'open'
            iOpen=iOpen+1;
            Open{iOpen}=zones(iZone).LSZ;
    end
end

end
function group=loadGroupInfo(dataRoot,groupFilename)
% dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180316_EPM-preFC_IC_fpG1';
% groupFilename = 'stressGroup.txt';
global videoExtension
videoList = dir([dataRoot filesep '*.' videoExtension]);
nVideo = size(videoList,1);
group= nan(1,nVideo);
[mouseData,groupData] = textread([dataRoot filesep groupFilename],'%s\t%d');
nGroupData = size(groupData,1);
for iVideo =1:nVideo
    for iGroupData=1:nGroupData
        if strfind(videoList(iVideo).name,mouseData{iGroupData})
            group(iVideo)=groupData(iGroupData);
        end
    end
end
end





function draw_figure6(dataRoot,dataFileTag,B,B_synchronized)
figName = [dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    hold on
    plot(B.mainX,B.mouseY,'color',[.7 .7 .7],'LineStyle','-','Marker','o');
    plot(B_synchronized.mainX,B_synchronized.mouseY,'color',[.7 .2 .7],'LineStyle','-','Marker','+');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.dpdf'],'-dpdf');
    close(f6);
end
figName = [dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    subplot(2,2,[1 2])
    hold on
    title('All')
    plot(B.optoPeriod);
    subplot(2,2,3)
    hold on
    title('First Min')
    plot(B.optoPeriod(1:15*60));
    subplot(2,2,4)
    hold on
    title('Last Min')
    plot(B.optoPeriod(end-(15*60):end));
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.dpdf'],'-dpdf');
    close(f6);
end
end

%%%%%%%%%%%%%%%%%%%%%%%


% IC = [1 4 5]; meanOpen = nanmean(all_Open(IC,:)); semOpen =
% std(all_Open(IC,:))./sqrt(3); meanClose = nanmean(all_Close(IC,:));
% semClose = std(all_Close(IC,:))./sqrt(3); f6=figure() hold on
% plot(meanOpen,'g') plot(meanOpen+semOpen,'g') plot(meanOpen-semOpen,'g')
% plot(meanClose,'k') plot(meanClose+semClose,'k')
% plot(meanClose-semClose,'k') ylim([-0.06 0.06])
% print(f6,['fp_OpenVsClosedArms.jpeg'],'-djpeg');
% print(f6,['fp_OpenVsClosedArms.dpdf'],'-dpdf'); close(f6);
%
% IC = [1 4 5]; meanOpen = nanmean(all_Open_GoingInsideTheArm(IC,:));
% semOpen = std(all_Open_GoingInsideTheArm(IC,:))./sqrt(3); meanClose =
% nanmean(all_Close_GoingInsideTheArm(IC,:)); semClose =
% std(all_Close_GoingInsideTheArm(IC,:))./sqrt(3); f7=figure() hold on
% plot(meanOpen,'g') plot(meanOpen+semOpen,'g') plot(meanOpen-semOpen,'g')
% plot(meanClose,'k') plot(meanClose+semClose,'k')
% plot(meanClose-semClose,'k') ylim([-0.06 0.06])
% print(f7,['fp_OpenVsClosedArms_GoingInsideTheArm.jpeg'],'-djpeg');
% print(f7,['fp_OpenVsClosedArms_GoingInsideTheArm.dpdf'],'-dpdf');
% close(f7);
%
%
% IC = [1 4 5]; meanOpen = nanmean(all_Open_GoingOutsideTheArm(IC,:));
% semOpen = std(all_Open_GoingOutsideTheArm(IC,:))./sqrt(3); meanClose =
% nanmean(all_Close_GoingOutsideTheArm(IC,:)); semClose =
% std(all_Close_GoingOutsideTheArm(IC,:))./sqrt(3); f8=figure() hold on
% plot(meanOpen,'g') plot(meanOpen+semOpen,'g') plot(meanOpen-semOpen,'g')
% plot(meanClose,'k') plot(meanClose+semClose,'k')
% plot(meanClose-semClose,'k') ylim([-0.06 0.06])
% print(f8,['fp_OpenVsClosedArms_GoingOutsideTheArm.jpeg'],'-djpeg');
% print(f8,['fp_OpenVsClosedArms_GoingOutsideTheArm.dpdf'],'-dpdf');
% close(f8);
%
%
% %Histogram For Center Half Extremity hO=nan(3,3);hC=nan(3,3);
% hO(1:3,1)=nanmean(all_Open(IC,1:33),2);
% hC(1:3,1)=nanmean(all_Close(IC,1:33),2);
% hO(1:3,2)=nanmean(all_Open(IC,34:66),2);
% hC(1:3,2)=nanmean(all_Close(IC,34:66),2);
% hO(1:3,3)=nanmean(all_Open(IC,67:101),2);
% hC(1:3,3)=nanmean(all_Close(IC,67:101),2);
%
%
% hO_GoingOutsideTheArm=nan(3,3);hC_GoingOutsideTheArm=nan(3,3);
% hO_GoingOutsideTheArm(1:3,1)=nanmean(all_Open_GoingOutsideTheArm(IC,1:33),2);
% hC_GoingOutsideTheArm(1:3,1)=nanmean(all_Close_GoingOutsideTheArm(IC,1:33),2);
% hO_GoingOutsideTheArm(1:3,2)=nanmean(all_Open_GoingOutsideTheArm(IC,34:66),2);
% hC_GoingOutsideTheArm(1:3,2)=nanmean(all_Close_GoingOutsideTheArm(IC,34:66),2);
% hO_GoingOutsideTheArm(1:3,3)=nanmean(all_Open_GoingOutsideTheArm(IC,67:101),2);
% hC_GoingOutsideTheArm(1:3,3)=nanmean(all_Close_GoingOutsideTheArm(IC,67:101),2);
%
% hO_GoingInsideTheArm=nan(3,3);hC_GoingInsideTheArm=nan(3,3);
% hO_GoingInsideTheArm(1:3,1)=nanmean(all_Open_GoingInsideTheArm(IC,1:33),2);
% hC_GoingInsideTheArm(1:3,1)=nanmean(all_Close_GoingInsideTheArm(IC,1:33),2);
% hO_GoingInsideTheArm(1:3,2)=nanmean(all_Open_GoingInsideTheArm(IC,34:66),2);
% hC_GoingInsideTheArm(1:3,2)=nanmean(all_Close_GoingInsideTheArm(IC,34:66),2);
% hO_GoingInsideTheArm(1:3,3)=nanmean(all_Open_GoingInsideTheArm(IC,67:101),2);
% hC_GoingInsideTheArm(1:3,3)=nanmean(all_Close_GoingInsideTheArm(IC,67:101),2);
%
% % IC = [6]; % figure() % hold on % plot(all_Open(IC,:),'g') %
% plot(all_Close(IC,:),'k')





%% Params SD EPM IC fpG1
% dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180316_EPM-preFC_IC_fpG1';
% cameraMode = 'synchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max videoExtension =
% 'mp4i';
%% Params LS EPM IC-vHPC fpG1
% dataRoot ='Y:\Fiber-Photometry\data\IC-vHPC_fpG1\20180511_EPM_IC-vHPC_fpG1';
% cameraMode = 'asynchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max videoExtension =
% 'mp4';
%% Params YM EPM IC-BLA fpG1
% dataRoot =% 'Y:\Fiber-Photometry\data\IC-BLA_fpG1\20180524_EPM_IC-BLA_fpG1';
% cameraMode = 'asynchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 25; %percent of led signal max videoExtension =
% 'avi';




