close all;clear;clc

%% DATA LIBRARIES
dataRoot{1} =  'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
dataRoot{2} =  'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
dataRoot{3} =  'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180316_EPM-preFC_IC_fpG1';

apparatus{1}.type='EPM';
apparatus{1}.Model='Ugo Basile version 1';
apparatus{1}.OA_cm = 80; % Open Arms Envergure
apparatus{1}.CA_cm = 75; % Closed Arms Envergure
apparatus{1}.W_cm = 5;     % Arms Width

apparatus{2}.type='EPM';
apparatus{2}.Model='Ugo Basile version 1';
apparatus{2}.OA_cm = 80; % Open Arms Envergure
apparatus{2}.CA_cm = 75; % Closed Arms Envergure
apparatus{2}.W_cm = 5;     % Arms Width

apparatus{3}.type='EPM';
apparatus{3}.Model='Cajal School, Noldus, version 1';
apparatus{3}.OA_cm = 80; % Open Arms Envergure
apparatus{3}.CA_cm = 80; % Closed Arms Envergure
apparatus{3}.W_cm = 6;     % Arms Width

videoExt{1}='avi';
videoExt{2}='avi';
videoExt{3}='mkv';

%% PARAMETERS

p.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
p.cameraMode = 'synchronous';
p.ledDetectionThreshold = 50; %percent of led signal max
p.HamamatsuFrameRate_Hz= 20;
p.behaviorCameraFrameRate_Hz=20;
p.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
p.savePDF = 0;
p.saveFIG = 0;
p.savePNG = 0;
p.forceRedrawing = 0;
p.forceBehavioralStart = 0;
p.MapScale_cmPerBin = 0.5;
p.OccupancyMap_sigmaGaussFilt=5;
p.PhotometrySignalMap_sigmaGaussFilt=5;
p.calciumAnalysis_highPath_Detection_Hz = 1;
p.calciumAnalysis_highPath_Trace_Hz = 0.075;
p.deltaFF_slidingWindowWidth = 1200;
p.lookingForMouse = '';

%% TO PROCESS ALL FOLDERS
for iFolder=1:3
    p.dataRoot = dataRoot{iFolder};
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        if ~isempty(p.lookingForMouse)
            processThisFile=0;
            if strcmp(p.dataFileTag,p.lookingForMouse)
                processThisFile=1;
            end
        end
        %% MAIN PROCESSING
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        
        fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);
        
        experiment = loadExpData(p);
        
        experiment = processCaSignal(experiment);
        experiment = standardizePositionData(experiment);      
        
        vData=experiment.vData;pData=experiment.pData;p=experiment.p;
        
        vData.zones_cmSP=epmDesing2Zones(experiment.apparatusDesign_cmSP);          
        vData.inZone=position2zone(vData.mainX_cmSP,vData.mainY_cmSP,vData.zones_cmSP);
        vData.nZones = size(vData.zones_cmSP,2);   
        [vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone,vData.nZones);                
        pData.transients=transientsPerZone(vData.inZone,vData.nZones,vData.zoneTime_fr,pData.transients);
                
       experiment.vData=vData;experiment.pData=pData;
        

                
        experiment.map = map;        
        experiment=buildDirectionalMapsForEPM(experiment)
        experiment=buildLinearMapsForEPM(experiment);
        

                      
        %% SAVING DATA
        save([p.mergedOutputsFolder filesep p.dataFileTag '.mat'],'experiment');
        
    end
end

