function draw_linearizedActivityMaps(params,outputPrefix,PercAvgSigMap,avgActivityMaps, videoTrackingData)

ZS = avgActivityMaps.ZS;
LZS =avgActivityMaps.LZS;
minLZS =avgActivityMaps.minLZS;
maxLZS =avgActivityMaps.maxLZS;
zones = videoTrackingData.zones;

figName = [params.dataRoot filesep 'figures' filesep params.dataFileTag '-LZS_' outputPrefix '.jpeg'];
if ~exist(figName,'file')
    
    bg=getBackGroundQuick(params);    
    f=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    
    subplot(4,4,[1 2 5 6 9 10 13 14])    
    hold on
    axis equal
    axis off
    set(gca,'Ydir','reverse')
    c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
    load('Z:\__Softwares\MatlabScripts_BeyelerLab\PHOTOMETRY\myCustomColormap.mat');
    colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);
    ii = isnan(PercAvgSigMap);
    PercAvgSigMap(ii)=0;
    gaussFilt_PercAvgSigMap = imgaussfilt(PercAvgSigMap,params.PhotometrySignalMap_sigmaGaussFilt);
     gaussFilt_PercAvgSigMap  = PercAvgSigMap;
    gaussFilt_PercAvgSigMap(ii)=nan;
    im=imagesc(gaussFilt_PercAvgSigMap);
    bg=getBackGroundQuick(params);
    sMax = max(size(bg));
    xlim([0 sMax]);
    ylim([0 sMax]);
    colorbar();
    
    for iZones=1:5
        rectangle('Position',zones(iZones).position,'EdgeColor',zones(iZones).Color,'LineStyle',zones(iZones).lineStyle);
    end
    
    for iZones=1:4
        subplot(4,4,iZones*4-1)
        hold on
        axis off
        axis equal
        imagesc([ZS{iZones}(:,:)],[min(min(PercAvgSigMap)) max(max(PercAvgSigMap))+0.001]);
    end
    
    for iZones=1:4
        subplot(4,4,iZones*4)
        hold on
        xlim([0 100]);
        if ~isnan(minLZS) && ~isnan(maxLZS)
            ylim([minLZS maxLZS+0.001])
        end
        plot(LZS{iZones},'color',zones(iZones).Color,'LineStyle',zones(iZones).lineStyle);
    end
    
    print(f,figName,'-djpeg');
%     print(f,[figName '.pdf'],'-dpdf');
    close(f);
    
end
end