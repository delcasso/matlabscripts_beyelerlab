close all;clear;clc

global videoExtension;

%% Params EPM IC-SweetBitter G1
dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
figureFolder=[dataRoot filesep  'figures'];
cameraMode = 'synchronous';
% groupFilename= 'stressGroup.txt';
ledDetectionThreshold = 50; %percent of led signal max
videoExtension='avi';
% extList{1}='jpeg'

%% Params SD EPM IC fpG1
% dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180316_EPM-preFC_IC_fpG1';
% cameraMode = 'synchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max videoExtension =
% 'mp4i';
%% Params LS EPM IC-vHPC fpG1
% dataRoot =
% 'Y:\Fiber-Photometry\data\IC-vHPC_fpG1\20180511_EPM_IC-vHPC_fpG1';
% cameraMode = 'asynchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max videoExtension =
% 'mp4';
%% Params YM EPM IC-BLA fpG1
% dataRoot =
% 'Y:\Fiber-Photometry\data\IC-BLA_fpG1\20180524_EPM_IC-BLA_fpG1';
% cameraMode = 'asynchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 25; %percent of led signal max videoExtension =
% 'avi';



%% MAIN PROGRAM
cd(dataRoot);

if ~exist(figureFolder,'dir'),mkdir(figureFolder);end

frameRate_Hz= 20;
speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed

fileList = dir([dataRoot filesep '*.' videoExtension]);nFiles = size(fileList,1);

all_Names = {};
all_LSZ = {};
% all_synchroSig = {};

% 201809121409 COMMENT all_Group=loadGroupInfo(dataRoot,groupFilename);

% PROCESS EACH DATA FILE INDIVIDUALLY
for iFile=1:nFiles
    
    [fPath, dataFileTag, ext] = fileparts(fileList(iFile).name);% Parse Filename To Use Same Filename to save analysis results
    fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,dataFileTag)
    load([dataRoot filesep dataFileTag '.mat']);
    
    sig(1)=[];ref(1)=[];% Load fiber-photometry DATA
    % first valueof sig and ref are systematically aberant; if we control
    % hamamatsu from Arduino, we always get one additional value due to
    % fip matlab program (this has been tested three times at different
    % moment 3/2018, 09/2018
    B=getVideoTrackingData([dataRoot filesep dataFileTag '-bonsai.txt'],cameraMode);% Load Behavioral DATA
    
    %During Summer 2018, recordings with spinView (BlackFly, FLIR) we loose
    % the last second of recording to make the two video streams (Hamamatsu
    % and BlackFly even in size, I remove the last second from Hamamatsu
    % Camera
    sig = sig(1:end-frameRate_Hz);ref = ref(1:end-frameRate_Hz);
    nFramesBlackFly = size(B.mainX,1);nFramesHamamatsu = size(sig,1);
    if nFramesBlackFly>nFramesHamamatsu, B.mainX=B.mainX(1:nFramesHamamatsu);B.mouseY=B.mouseY(1:nFramesHamamatsu);end
    if nFramesBlackFly<nFramesHamamatsu, B.mainX=[B.mainX; nan(nFramesHamamatsu-nFramesBlackFly,1)];B.mouseY=[B.mouseY; nan(nFramesHamamatsu-nFramesBlackFly,1)];end
    
    if strcmp(cameraMode,'asynchronous')
        [B_synchronized,nSynchro,dFrame]=reSynchData(B,sig,ledDetectionThreshold);
        fprintf('\t> Synchro nSig=%d dt=%d frames\n',nSynchro,dFrame);
        draw_figure6(dataRoot,dataFileTag,B,B_synchronized);
        B = B_synchronized;
    end
    fprintf('\t#Hamamatsu %d',size(sig,1));fprintf('\t\t#BlackFly %d\n',size(B.mainX,1));
    [B,ref,sig]=removeOneMinute(B,ref,sig); %remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
    x=B.mainX;y=B.mouseY;
    
    [ref,sig,d]=deltaFF(ref,sig); %procress deltaFF
    nFrames = size(x,1);T = 1: nFrames;T = T./ frameRate_Hz; %recreate time vector based on fiber-photometry frame rate
    %     xMin=0;xMax=1500;yMin=0;yMax=1500;OccupancyMap =
    %     zeros(xMax,yMax);SigMap = zeros(xMax,yMax);
    [x,y]=cleanPosBasedOnSpeedThreshold(x,y,speedThreshold); % remove all position when animal spped exceed 20 pixel per sec.
    
    bg=getBackGroundQuick(dataRoot,dataFileTag);
    zones = epm_getZones(dataRoot,dataFileTag);
    [OccupancyMap]=buildOccupancyMap(x,y); %build a spatial map of the time spend in each pixel for the entire exp.
    [SumSigMap]=buildSignalMap(x,y,d); %build a spatial map of the sum of Ca++ signal recorded in each pixel for the entire exp.
    [PercAvgSigMap]=normSigMap(SumSigMap,OccupancyMap);
    [OccIn,SumSigIn,OccOut,SumSigOut]=epm_InAndOut(x,y,d,zones);
    [PercAvgSigMap_I]=normSigMap(SumSigIn,OccIn);
    [PercAvgSigMap_O]=normSigMap(SumSigOut,OccOut);
    
    draw_figure1(dataRoot,figureFolder,dataFileTag,OccupancyMap); % Draw OccupancyMapXXX.jpeg
    draw_figure2(dataRoot,figureFolder,dataFileTag,PercAvgSigMap); % Draw SignalMapXXX.jpeg
    draw_figure3(dataRoot,figureFolder,dataFileTag,x,y); % Draw traceXXX.jpeg
    
    
    
    %     outputPrefix = 'IN-OUT';
    %     [zones_IO,minLZD_IO,maxLZD_IO]=epm_alignSignalToArms(dataRoot,dataFileTag,PercAvgSigMap,zones,outputPrefix);
    %     %% for each arm, calculate a vector of signal value, from center to
    %     periphery
    %     draw_figure4(dataRoot,dataFileTag,outputPrefix,PercAvgSigMap,zones_IO,minLZD_IO,maxLZD_IO);
    %     % Draw ZonedSignalMapXXX.jpeg outputPrefix = 'IN';
    %     [zones_I,minLZD_I,maxLZD_I]=epm_alignSignalToArms(dataRoot,dataFileTag,PercAvgSigMap_I,zones,outputPrefix);
    %     %% for each arm, calculate a vector of signal value, from center to
    %     periphery
    %     draw_figure4(dataRoot,dataFileTag,outputPrefix,PercAvgSigMap_I,zones_I,minLZD_I,maxLZD_I);
    %     % Draw ZonedSignalMapXXX.jpeg outputPrefix = 'OUT';
    %     [zones_O,minLZD_O,maxLZD_O]=epm_alignSignalToArms(dataRoot,dataFileTag,PercAvgSigMap_O,zones,outputPrefix);
    %     %% for each arm, calculate a vector of signal value, from center to
    %     periphery
    %     draw_figure4(dataRoot,dataFileTag,outputPrefix,PercAvgSigMap_O,zones_O,minLZD_O,maxLZD_O);
    %     % Draw ZonedSignalMapXXX.jpeg
    %
    %     [all_LSZ{iFile}.Open_IO,all_LSZ{iFile}.Closed_IO]=epm_avgLSZ_sameArmType(zones_IO);
    %     [all_LSZ{iFile}.Open_I,all_LSZ{iFile}.Closed_I]=epm_avgLSZ_sameArmType(zones_I);
    %     [all_LSZ{iFile}.Open_O,all_LSZ{iFile}.Closed_O]=epm_avgLSZ_sameArmType(zones_O);
    %     all_Names{iFile} = dataFileTag;% Keep The filename in memory
end


%% GL0BAL AVERAGE
% nMice = size(all_LSZ,2);
% maxArmSize = [];
% suffix1={'Open','Closed'};
% suffix2={'IO','I','O'};
% % Find maxArmSize
% for iMouse=1:nMice
%     for iSuffix1=1:2
%         for iSuffix2=1:3
%             for iArm=1:2
%                 cmd = sprintf('[s1,s2]=size(all_LSZ{%d}.%s_%s{%d});',iMouse,suffix1{iSuffix1},suffix2{iSuffix2},iArm);eval(cmd);
%                 maxArmSize=[maxArmSize max([s1 s2])];
%             end
%         end
%     end
% end
% maxArmSize=max(maxArmSize);
% % Average Open and Closed Arms per session
% for iSuffix1=1:2
%     for iSuffix2=1:3
%         cmd = sprintf('%s_%s=nan(nMice,maxArmSize);',suffix1{iSuffix1},suffix2{iSuffix2});eval(cmd);
%         for iMouse=1:nMice
%             v=nan(2,maxArmSize);
%             for iArm=1:2
%                 cmd = sprintf('tmp=all_LSZ{%d}.%s_%s{%d};',iMouse,suffix1{iSuffix1},suffix2{iSuffix2},iArm);eval(cmd);
%                 size_=size(tmp);[~,idxMax]=max(size_);
%                 if idxMax==1, tmp=tmp';end
%                 v(iArm,1:size_(idxMax))=tmp;
%             end
%             v=nanmean(v);
%             cmd = sprintf('%s_%s(%d,:)=v;',suffix1{iSuffix1},suffix2{iSuffix2},iMouse);eval(cmd);
%             endll
%     end
% end

% save([dataRoot filesep 'results.mat'],'Open*','Close*','all_Names','all_Group');
% save([dataRoot filesep 'results.mat'],'Open*','Close*','all_Names');
% clear all




%% ESSENTIALS SUBFUNCTIONS
function v=vt_getVideoInfo(videoPath)
v = VideoReader(videoPath);
end
function bonsai_output=getVideoTrackingData(vt_bonsaiPath,cameraMode)
bonsai_output=[];
if exist(vt_bonsaiPath,'file')
    bonsai_data = dlmread(vt_bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(vt_bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
end
end
function distance =  vt_getDistance(x,y)
dx = diff(x);dy=diff(y);
distance = sqrt((dx.*dx)+(dy.*dy));
end
function [B2,nSynchro,dFrame]=reSynchData(B,sig,ledDetectionThreshold)
o = B.optoPeriod;
o=o>max(o)*(ledDetectionThreshold/100);
dO=diff(o);
iUp = find(dO==1);
dFrame = median(diff(iUp));
x=1:dFrame+1;
xq = linspace(1,dFrame+1,100); %synchro LED lights upeach 100 Hamamatsuframes
nSynchro=size(iUp,1);
B2.mainX=[];
B2.mouseY=[];
B2.mouseAngle=[];
B2.mouseMajorAxisLength=[];
B2.mouseMinorAxisLength=[];
B2.mouseArea=[];
B2.optoPeriod=[];
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(B.optoPeriod,1));
for i=1:nSynchro
    i1=iUp(i);i2=i1+dFrame;
    B2.mainX = [B2.mainX ; interp1(x,B.mainX(i1:i2),xq)'];
    B2.mouseY = [B2.mouseY ; interp1(x,B.mouseY(i1:i2),xq)'];
    B2.mouseAngle = [B2.mouseAngle ; interp1(x,B.mouseAngle(i1:i2),xq)'];
    B2.mouseMajorAxisLength = [B2.mouseMajorAxisLength ; interp1(x,B.mouseMajorAxisLength(i1:i2),xq)'];
    B2.mouseMinorAxisLength = [B2.mouseMinorAxisLength ; interp1(x,B.mouseMinorAxisLength(i1:i2),xq)'];
    B2.mouseArea = [B2.mouseArea ; interp1(x,B.mouseArea(i1:i2),xq)'];
    B2.optoPeriod = [B2.optoPeriod ; interp1(x,B.optoPeriod(i1:i2),xq)'];
end
% fprintf('sig[%d] x[%d]\n',size(sig,1),size(B2.optoPeriod,1));
% fprintf('resynch done\n');

end
function [ref,sig,d]=deltaFF(ref,sig)
meanSig=mean(sig);meanRef=mean(ref);sig = sig - meanSig;ref = ref - meanRef;sig = sig./meanSig;ref = ref./meanRef;d=sig-ref;
end
function [B,ref,sig]=removeOneMinute(B,ref,sig)
B.mainX=B.mainX((20*60)+1:end);B.mouseY=B.mouseY((20*60)+1:end);sig=sig((20*60)+1:end);ref=ref((20*60)+1:end);
end
function [OccupancyMap]=buildOccupancyMap(x,y)
xMin=0;xMax=1500;yMin=0;yMax=1500;OccupancyMap = zeros(xMax,yMax);
nPos=size(x,1);
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos))+1;j=floor(x(iPos))+1;
        OccupancyMap(i,j)=OccupancyMap(i,j)+1;
    end
end
end
function [SigMap]=buildSignalMap(x,y,d)
xMin=0;xMax=1500;yMin=0;yMax=1500;SigMap = zeros(xMax,yMax);
nPos=size(x,1);
for iPos=1:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        i=floor(y(iPos))+1;j=floor(x(iPos))+1;
        SigMap(i,j)= SigMap(i,j)+d(iPos);
    end
end
end
function [PercAvgSigMap]=normSigMap(SumSigMap,OccupancyMap)
AvgSigMap = SumSigMap ./ OccupancyMap;  % Average Signal (deltaFF) Map base don time spent in each pixel
PercAvgSigMap = AvgSigMap .*100;  % express averaged signal map in percent of deltaFF
end
function [zones,minLSZ,maxLSZ]=epm_alignSignalToArms(dataRoot,dataFileTag,PercAvgSigMap,zones,outputPrefix)
LSZ_MatFilename = [dataRoot filesep 'LSZ-' outputPrefix '-' dataFileTag '.mat'];
minLSZ=[];maxLSZ=[];

if ~exist(LSZ_MatFilename,'file')
    
    for iZone =1:5
        x1=zones(iZone).position(1);
        y1=zones(iZone).position(2)
        x2 = x1 + zones(iZone).position(3);
        y2 = y1 + zones(iZone).position(4);
        % LSZ : Linear-Signal Zoned
        switch zones(iZone).positionSTR
            case 'NORTH'
                zones(iZone).SZ = fliplr(PercAvgSigMap(y1:y2,x1:x2)');
                zones(iZone).LSZ = nanmean(zones(iZone).SZ);
            case 'EAST'
                zones(iZone).SZ = PercAvgSigMap(y1:y2,x1:x2);
                zones(iZone).LSZ = nanmean(zones(iZone).SZ);
            case 'SOUTH'
                zones(iZone).SZ = PercAvgSigMap(y1:y2,x1:x2)';
                zones(iZone).LSZ = nanmean(zones(iZone).SZ);
            case 'WEST'
                zones(iZone).SZ = fliplr(PercAvgSigMap(y1:y2,x1:x2));
                zones(iZone).LSZ = nanmean(zones(iZone).SZ);
            case 'CENTER'
                zones(iZone).LSZ = nanmedian(nanmedian(PercAvgSigMap(y1:y2,x1:x2)));
        end
    end
    minLSZ=[];maxLSZ=[];
    for iZone=1:5, minLSZ = [minLSZ min(zones(iZone).LSZ)];maxLSZ = [maxLSZ max(zones(iZone).LSZ)];end
    minLSZ = min(minLSZ);maxLSZ = max(maxLSZ);
    
    save(LSZ_MatFilename,'zones','minLSZ','maxLSZ');
else
    load(LSZ_MatFilename);
end
end
function [x,y]=cleanPosBasedOnSpeedThreshold(x,y,speedThreshold)
distance =  vt_getDistance(x,y);
%cleaning position based on abnormal speed.
idx = find(distance>speedThreshold);x(idx)=nan;y(idx)=nan;
end
function bg=getBackGroundQuick(dataRoot,dataFileTag)
global videoExtension;
videoPath = [dataRoot filesep dataFileTag '.' videoExtension];
bgFilename = [dataRoot filesep dataFileTag '-bg.png'];
if ~exist(bgFilename)
    videoInfo=vt_getVideoInfo(videoPath);
    v = VideoReader(videoPath);
    bg = readFrame(v);
    bg = rgb2gray(bg);
    imwrite(bg,bgFilename,'jpg');
else
    bg = imread(bgFilename);
end
end
function zones = epm_getZones(dataRoot,dataFileTag)
global videoExtension;
bg=getBackGroundQuick(dataRoot,dataFileTag);

zoneFilename = [dataRoot filesep dataFileTag '-zones.mat'];

if ~exist(zoneFilename,'file')
    
    f = dir([dataRoot filesep '*-zones.mat']);nF = size(f,1);if nF, load([dataRoot filesep f(nF).name]);end
    
    f1=figure();
    hold on;
    colormap(gray)
    imagesc(bg);
    axis off
    axis equal
    set(gca,'Ydir','reverse')
    
    openArmColor = [0 0 1];closedArmColor = [1 0 0];centerColor = [0 1 0];
    t=text(10,10,'#1 CLOSED','color',closedArmColor);
    t=text(10,20,'#2 OPEN','color',openArmColor);
    t=text(10,30,'#3 CLOSED','color',closedArmColor);
    t=text(10,40,'#4 OPEN','color',openArmColor);
    t=text(10,50,'#5 CENTER','color',centerColor);
    
    for i=1:5
        switch i
            case 1
                zones(i).type='closed';zones(i).Color=closedArmColor;
            case 2
                zones(i).type='open';zones(i).Color=openArmColor;
            case 3
                zones(i).type='closed';zones(i).Color=closedArmColor;
            case 4
                zones(i).type='open';zones(i).Color=openArmColor;
            case 5
                zones(i).type='center';zones(i).Color=centerColor;
        end
        fprintf(sprintf('Draw Zone %d [%s]\n',i,zones(i).type))
        if ~isempty(zones(i).position)
            h(i) = imrect(gca,zones(i).position);
        else
            h(i) = imrect();
        end
        h(i).setColor(zones(i).Color);
    end
    
    prompt = 'Press Any Key When You Are Done Drawing This ROI';
    str = input(prompt,'s');
    
    for i=1:5
        zones(i).position = h(i).getPosition;
        if zones(i).position(1)<1, zones(i).position(1)=1;end
        if zones(i).position(2)<1, zones(i).position(2)=1;end
        zX(i) = zones(i).position(1);zones(i).X=zX(i);
        zY(i) = zones(i).position(2);zones(i).Y=zY(i);
        zW(i) = zones(i).position(3);zones(i).W=zW(i);
        zH(i) = zones(i).position(4);zones(i).H=zH(i);
    end
    
    [~,ii] = min(zY);zones(ii).positionSTR = 'NORTH';% NORTH ARM
    [~,ii] = max(zX);zones(ii).positionSTR = 'EAST';% EAST ARM
    [~,ii] = max(zY);zones(ii).positionSTR = 'SOUTH';% SOUTH ARM
    [~,ii] = min(zX);zones(ii).positionSTR = 'WEST';% WEST ARM
    zones(5).positionSTR = 'CENTER';% CENTER
    
    zones(1).lineStyle = '-';
    zones(2).lineStyle = '-';
    zones(3).lineStyle = '-.';
    zones(4).lineStyle = '-.';
    zones(5).lineStyle = '-';
    
    % zones(1) is always a close arm
    
    save(zoneFilename,'zones');
    print(f1,[dataFileTag '-zones.jpeg'],'-djpeg');
    close(f1);
    
    
else
    load(zoneFilename);
end

for i=1:5
    zones(i).position = floor(zones(i).position);
end
end
function [OccIn,SumSigIn,OccOut,SumSigOut]=epm_InAndOut(x,y,d,zones)

nPos=size(x,1);

xMin=0;xMax=1500;yMin=0;yMax=1500;

%OccIn = going inside the arm
OccIn=zeros(xMax,yMax);OccOut=zeros(xMax,yMax);
SumSigIn=zeros(xMax,yMax);SumSigOut=zeros(xMax,yMax);

for iZone=1:5
    switch zones(iZone).positionSTR
        case 'NORTH'
            northLim = zones(iZone).Y + zones(iZone).H;
        case 'EAST'
            eastLim = zones(iZone).X;
        case 'SOUTH'
            southLim = zones(iZone).Y;
        case 'WEST'
            westLim = zones(iZone).X + zones(iZone).W;
            %             case 'CENTER'
    end
end

for iPos=2:nPos
    if ~isnan(x(iPos)) && ~isnan(y(iPos))
        iX=floor(x(iPos))+1;iY=floor(y(iPos))+1;
        cZone=0;
        if iY<northLim, cZone=1; end
        if iY>southLim, cZone=3; end
        if iX<westLim, cZone=4; end
        if iX>eastLim, cZone=2; end
        xDir=x(iPos)-x(iPos-1);yDir=y(iPos)-y(iPos-1);
        gDir = 0; %-1 in, 1 out
        switch cZone
            case 1, if yDir>0, gDir=1; end; if yDir<0, gDir=-1;end
            case 2, if xDir>0, gDir=-1;end; if xDir<0, gDir=1;end
            case 3, if yDir<0, gDir=1;end; if yDir>0, gDir=-1;end
            case 4, if xDir<0, gDir=-1;end; if xDir>0, gDir=1;end
        end
        if gDir==1, OccOut(iY,iX)=OccOut(iY,iX)+1;SumSigOut(iY,iX)= SumSigOut(iY,iX)+d(iPos);end
        if gDir==-1, OccIn(iY,iX)=OccIn(iY,iX)+1;SumSigIn(iY,iX)= SumSigIn(iY,iX)+d(iPos);end
    end
end

end
function [Open,Closed]=epm_avgLSZ_sameArmType(zones)
Open={};iOpen=0;Closed={};iClosed=0;
for iZone=1:5
    switch zones(iZone).type
        case 'closed'
            iClosed=iClosed+1;
            Closed{iClosed}=zones(iZone).LSZ;
        case 'open'
            iOpen=iOpen+1;
            Open{iOpen}=zones(iZone).LSZ;
    end
end

end
function group=loadGroupInfo(dataRoot,groupFilename)
% dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180316_EPM-preFC_IC_fpG1';
% groupFilename = 'stressGroup.txt';
global videoExtension
videoList = dir([dataRoot filesep '*.' videoExtension]);
nVideo = size(videoList,1);
group= nan(1,nVideo);
[mouseData,groupData] = textread([dataRoot filesep groupFilename],'%s\t%d');
nGroupData = size(groupData,1);
for iVideo =1:nVideo
    for iGroupData=1:nGroupData
        if strfind(videoList(iVideo).name,mouseData{iGroupData})
            group(iVideo)=groupData(iGroupData);
        end
    end
end
end
function outputFigure(figureHandle,figureFolder,dataFileTag,figNameSuffix)
    if ~exist([figureFolder filesep dataFileTag '-' figNameSuffix '.jpeg'])
        print(figureHandle,[figureFolder filesep dataFileTag '-' figNameSuffix '.jpeg'],'-djpeg');
    end
end

%% DRAWING FIGURES
function draw_figure1(dataRoot,figureFolder,dataFileTag,OccupancyMap)
figName = [dataRoot filesep dataFileTag '-occupancyMap.jpeg'];
if ~exist(figName,'file')
    f1=figure('name',dataFileTag,'Position',[20 20 800 800]);
    hold on
    colormap(hot);
    [r,c]=size(OccupancyMap);
    tmp = reshape(OccupancyMap,1,r*c);
    tmp(find(tmp==0))=[];
    imagesc(OccupancyMap,prctile(tmp,[10 90]));
    bg=getBackGroundQuick(dataRoot,dataFileTag);
    sMax = max(size(bg))
    xlim([0 sMax]);
    ylim([0 sMax]);
    axis off
    set(gca,'Ydir','reverse')
    colorbar();
    outputFigure(f1,figureFolder,dataFileTag,'occupancyMap');
    close(f1);
end
end
function draw_figure2(dataRoot,figureFolder,dataFileTag,PercAvgSigMap)
figName = [dataRoot filesep dataFileTag '-signalMap .jpeg'];
if ~exist(figName,'file')
    f2=figure('name',dataFileTag,'Position',[20 20 800 800]);
    hold on
    colormap(parula);
    imagesc(PercAvgSigMap)
    bg=getBackGroundQuick(dataRoot,dataFileTag);
    sMax = max(size(bg));
    xlim([0 sMax]);ylim([0 sMax]);
    axis off
    set(gca,'Ydir','reverse')
    colorbar();
    outputFigure(f2,figureFolder,dataFileTag,'signalMap');
    close(f2);
end
end
function draw_figure3(dataRoot,figureFolder,dataFileTag,x,y)
figName = [dataRoot filesep dataFileTag '-trace.jpeg'];
if ~exist(figName,'file')
    bg=getBackGroundQuick(dataRoot,dataFileTag);
    f3=figure('name',dataFileTag,'Position',[20 20 800 800]);
    hold on
    imagesc(bg);
    plot(x,y,'color',[1 0 0])
    axis off
    set(gca,'Ydir','reverse')
    sMax = max(size(bg))
    xlim([0 sMax]);ylim([0 sMax]);
    outputFigure(f3,figureFolder,dataFileTag,'trace');
    close(f3);
end
end
function draw_figure4(dataRoot,figureFolder,dataFileTag,outputPrefix,PercAvgSigMap,zones,minLZD,maxLZD)

figName = [dataRoot filesep 'LSZ-' outputPrefix '-' dataFileTag];
if ~exist([figName '.jpeg'],'file')
    
    bg=getBackGroundQuick(dataRoot,dataFileTag);
    
    f4=figure('name',dataFileTag,'Position',[20 20 800 800]);
    
    subplot(4,4,[1 2 5 6 9 10 13 14])
    hold on
    set(gca,'Ydir','reverse')
    colormap(parula);
    imagesc(PercAvgSigMap);
    sMax = max(size(bg));xlim([0 sMax]);ylim([0 sMax]);
    colorbar();
    
    for iZones=1:5
        rectangle('Position',zones(iZones).position,'EdgeColor',zones(iZones).Color,'LineStyle',zones(iZones).lineStyle);
    end
    
    for iZones=1:4
        subplot(4,4,iZones*4-1)
        hold on
        colormap(parula);
        axis off
        axis equal
        imagesc(zones(iZones).SZ,[min(min(PercAvgSigMap)) max(max(PercAvgSigMap))]);
    end
    
    for iZones=1:4
        subplot(4,4,iZones*4)
        hold on
        xlim([0 100]);ylim([minLZD maxLZD])
        plot(zones(iZones).LSZ,'color',zones(iZones).Color,'LineStyle',zones(iZones).lineStyle);
    end
    
    print(f4,[figName '.jpeg'],'-djpeg');
    print(f4,[figName '.pdf'],'-pdf');
    close(f4);
    
end
end
function draw_figure6(dataRoot,dataFileTag,B,B_synchronized)
figName = [dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    hold on
    plot(B.mainX,B.mouseY,'color',[.7 .7 .7],'LineStyle','-','Marker','o');
    plot(B_synchronized.mainX,B_synchronized.mouseY,'color',[.7 .2 .7],'LineStyle','-','Marker','+');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.pdf'],'-dpdf');
    close(f6);
end
figName = [dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    subplot(2,2,[1 2])
    hold on
    title('All')
    plot(B.optoPeriod);
    subplot(2,2,3)
    hold on
    title('First Min')
    plot(B.optoPeriod(1:15*60));
    subplot(2,2,4)
    hold on
    title('Last Min')
    plot(B.optoPeriod(end-(15*60):end));
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.pdf'],'-dpdf');
    close(f6);
end
end

%%%%%%%%%%%%%%%%%%%%%%%


% IC = [1 4 5]; meanOpen = nanmean(all_Open(IC,:)); semOpen =
% std(all_Open(IC,:))./sqrt(3); meanClose = nanmean(all_Close(IC,:));
% semClose = std(all_Close(IC,:))./sqrt(3); f6=figure() hold on
% plot(meanOpen,'g') plot(meanOpen+semOpen,'g') plot(meanOpen-semOpen,'g')
% plot(meanClose,'k') plot(meanClose+semClose,'k')
% plot(meanClose-semClose,'k') ylim([-0.06 0.06])
% print(f6,['fp_OpenVsClosedArms.jpeg'],'-djpeg');
% print(f6,['fp_OpenVsClosedArms.pdf'],'-dpdf'); close(f6);
%
% IC = [1 4 5]; meanOpen = nanmean(all_Open_GoingInsideTheArm(IC,:));
% semOpen = std(all_Open_GoingInsideTheArm(IC,:))./sqrt(3); meanClose =
% nanmean(all_Close_GoingInsideTheArm(IC,:)); semClose =
% std(all_Close_GoingInsideTheArm(IC,:))./sqrt(3); f7=figure() hold on
% plot(meanOpen,'g') plot(meanOpen+semOpen,'g') plot(meanOpen-semOpen,'g')
% plot(meanClose,'k') plot(meanClose+semClose,'k')
% plot(meanClose-semClose,'k') ylim([-0.06 0.06])
% print(f7,['fp_OpenVsClosedArms_GoingInsideTheArm.jpeg'],'-djpeg');
% print(f7,['fp_OpenVsClosedArms_GoingInsideTheArm.pdf'],'-dpdf');
% close(f7);
%
%
% IC = [1 4 5]; meanOpen = nanmean(all_Open_GoingOutsideTheArm(IC,:));
% semOpen = std(all_Open_GoingOutsideTheArm(IC,:))./sqrt(3); meanClose =
% nanmean(all_Close_GoingOutsideTheArm(IC,:)); semClose =
% std(all_Close_GoingOutsideTheArm(IC,:))./sqrt(3); f8=figure() hold on
% plot(meanOpen,'g') plot(meanOpen+semOpen,'g') plot(meanOpen-semOpen,'g')
% plot(meanClose,'k') plot(meanClose+semClose,'k')
% plot(meanClose-semClose,'k') ylim([-0.06 0.06])
% print(f8,['fp_OpenVsClosedArms_GoingOutsideTheArm.jpeg'],'-djpeg');
% print(f8,['fp_OpenVsClosedArms_GoingOutsideTheArm.pdf'],'-dpdf');
% close(f8);
%
%
% %Histogram For Center Half Extremity hO=nan(3,3);hC=nan(3,3);
% hO(1:3,1)=nanmean(all_Open(IC,1:33),2);
% hC(1:3,1)=nanmean(all_Close(IC,1:33),2);
% hO(1:3,2)=nanmean(all_Open(IC,34:66),2);
% hC(1:3,2)=nanmean(all_Close(IC,34:66),2);
% hO(1:3,3)=nanmean(all_Open(IC,67:101),2);
% hC(1:3,3)=nanmean(all_Close(IC,67:101),2);
%
%
% hO_GoingOutsideTheArm=nan(3,3);hC_GoingOutsideTheArm=nan(3,3);
% hO_GoingOutsideTheArm(1:3,1)=nanmean(all_Open_GoingOutsideTheArm(IC,1:33),2);
% hC_GoingOutsideTheArm(1:3,1)=nanmean(all_Close_GoingOutsideTheArm(IC,1:33),2);
% hO_GoingOutsideTheArm(1:3,2)=nanmean(all_Open_GoingOutsideTheArm(IC,34:66),2);
% hC_GoingOutsideTheArm(1:3,2)=nanmean(all_Close_GoingOutsideTheArm(IC,34:66),2);
% hO_GoingOutsideTheArm(1:3,3)=nanmean(all_Open_GoingOutsideTheArm(IC,67:101),2);
% hC_GoingOutsideTheArm(1:3,3)=nanmean(all_Close_GoingOutsideTheArm(IC,67:101),2);
%
% hO_GoingInsideTheArm=nan(3,3);hC_GoingInsideTheArm=nan(3,3);
% hO_GoingInsideTheArm(1:3,1)=nanmean(all_Open_GoingInsideTheArm(IC,1:33),2);
% hC_GoingInsideTheArm(1:3,1)=nanmean(all_Close_GoingInsideTheArm(IC,1:33),2);
% hO_GoingInsideTheArm(1:3,2)=nanmean(all_Open_GoingInsideTheArm(IC,34:66),2);
% hC_GoingInsideTheArm(1:3,2)=nanmean(all_Close_GoingInsideTheArm(IC,34:66),2);
% hO_GoingInsideTheArm(1:3,3)=nanmean(all_Open_GoingInsideTheArm(IC,67:101),2);
% hC_GoingInsideTheArm(1:3,3)=nanmean(all_Close_GoingInsideTheArm(IC,67:101),2);
%
% % IC = [6]; % figure() % hold on % plot(all_Open(IC,:),'g') %
% plot(all_Close(IC,:),'k')









