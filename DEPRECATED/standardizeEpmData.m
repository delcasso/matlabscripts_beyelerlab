function experiment = standardizeEpmData(experiment)

p=experiment.p;
vData=experiment.vData;

%% standardized Experiments  (transform pix to cm, get epm design)
% standardize EPM position,size and rotation to camera
orderSTR='mark the four corners (extrimities) of the open arms';
switch p.apparatus.type
    case 'EPM'
        vData.corners=getRectangularZone_201810041830(p,'openArmsCorners',orderSTR);
        apparatusDesign_cm=getEpmDesign(p.apparatus);
end

[vData,p]=transformMouseCoordinates_pix2cmStandard(vData,p);
% From cmStandard to cmStandardPositive, to match positive indices of matlab arrays used in following map constructions
[vData,apparatusDesign_cmSP]=transformCoordinates_cmStandard2cmStandardPositive(vData,apparatusDesign_cm); % cmSP : centimeter STANDARD POSITIVE
p.xMax = max(apparatusDesign_cmSP.x);
p.yMax = max(apparatusDesign_cmSP.y);

experiment.p = p;
experiment.vData = vData;
experiment.apparatusDesign_cm = apparatusDesign_cm;
experiment.apparatusDesign_cmSP = apparatusDesign_cmSP;
end