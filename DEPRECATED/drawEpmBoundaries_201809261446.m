function epmBoundaries=drawEpmBoundaries_201809261446(params)

warning('drawEpmBoundaries doesn''t work if open arms are located vertically');

if ~exist([params.dataRoot filesep params.dataFileTag '-epmBoundaries.mat'])    

videoTrackingData = getVideoTrackingData(params);
bg=getBackGroundQuick(params);

f1=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
f2=figure('name',params.dataFileTag,'Position',[20 20 800 800]);

zoom = 50;

figure(f1)
hold on
axis equal
axis off
set(gca,'Ydir','reverse')
colormap(gray(1024));
imagesc(bg);
    
    for i=1:3
        
        figure(f1)
        [x(i),y(i)]=ginput(1);
        
        figure(f2)
        clf(f2)
        hold on
        axis equal
        axis off
        set(gca,'Ydir','reverse')
        colormap(gray(1024));
        imagesc(bg(y(i)-zoom:y(i)+zoom,x(i)-zoom:x(i)+zoom));
        [x2(i),y2(i)]=ginput(1);
        x(i)=x2(i) + (x(i)-zoom);
        y(i)=y2(i) + (y(i)-zoom);
        
    end
    
    xOpenArms=x;yOpenArms=y;
    epmParams.OpenArmsEnvergure =  params.epmOpenArmsEnvergure;
    epmParams.ClosedArmsEnvergure =params.epmClosedArmsEnvergure;
    epmParams.ArmsWidth =params.epmArmsWidth;
        
    dX=x(2)-x(1);
    dY=y(3)-y(2);
    
    if dX>dY
        epmParams.openArmsOrientation = 'horizontal';
    else
        epmParams.openArmsOrientation = 'vertical';
    end
                            
    figure(f1)
    plot(xOpenArms,yOpenArms,'r');
    
    % In realty the envergure of the EPM is 80 cm, and we want them to be oriented horizontaly
    % so we calculcate the rotAngle by the experimenter to correct it
    
    dX = diff(xOpenArms(1:2));
    dY = diff(yOpenArms(1:2));
    rotAngle = rad2deg(asin(dY/dX));
    
    xCenter = sum(xOpenArms([1 3]))/2;
    yCenter = sum(yOpenArms([1 3]))/2;
    
    x0=0;y0=0;
    
    d1 = params.epmOpenArmsEnvergure/2;
    d2 = params.epmClosedArmsEnvergure/2;
    d3 = params.epmArmsWidth;
    d4 = d3/2;
    
    xMaze_cm(1)=x0-d1;xMaze_cm(12)=xMaze_cm(1);
    yMaze_cm(1)=y0-d4;yMaze_cm(2)=yMaze_cm(1);yMaze_cm(5)=yMaze_cm(1);yMaze_cm(6)=yMaze_cm(1);
    xMaze_cm(2)=x0-d4;xMaze_cm(3)=xMaze_cm(2);xMaze_cm(10)=xMaze_cm(2);xMaze_cm(11)=xMaze_cm(2);
    yMaze_cm(3)=y0-d2;yMaze_cm(4)=yMaze_cm(3);
    xMaze_cm(4)=xMaze_cm(3)+d3;xMaze_cm(5)=xMaze_cm(4);xMaze_cm(8)=xMaze_cm(4);xMaze_cm(9)=xMaze_cm(4);
    xMaze_cm(6)=x0+d1;xMaze_cm(7)=xMaze_cm(6);
    yMaze_cm(7)=yMaze_cm(6)+d3;yMaze_cm(8)=yMaze_cm(7);yMaze_cm(11)=yMaze_cm(7);yMaze_cm(12)=yMaze_cm(7);
    yMaze_cm(9)=y0+d2;yMaze_cm(10)=yMaze_cm(9);
    
    coef_cm2pix = dX/80;
    coef_pix2cm = 80/dX;
    
    xMaze_pix = xMaze_cm * coef_cm2pix;
    yMaze_pix = yMaze_cm * coef_cm2pix;
    xMaze_pix = xMaze_pix + xCenter;
    yMaze_pix = yMaze_pix + yCenter;
    
    plot(x0,y0,'rx');
    
    for i=1:11
        plot(xMaze_pix([i i+1]),yMaze_pix([i i+1]),'b:')
    end
     plot(xMaze_pix([12 1]),yMaze_pix([12 1]),'b:')
    
    
    for i=1:12
        [xMaze_pix_rotated(i),yMaze_pix_rotated(i)] = rotateXY(xMaze_pix(i),yMaze_pix(i),rotAngle,xCenter,yCenter);
    end
    
    for i=1:11
        plot(xMaze_pix_rotated([i i+1]),yMaze_pix_rotated([i i+1]),'g:');
    end
    plot(xMaze_pix_rotated([12 1]),yMaze_pix_rotated([12 1]),'g:');    
    
    epmBoundaries.xOpenArms=xOpenArms;
    epmBoundaries.yOpenArms=yOpenArms;
    epmBoundaries.rotAngle=rotAngle;
    epmBoundaries.xCenter=xCenter;
    epmBoundaries.yCenter=yCenter;
    epmBoundaries.epmParams=epmParams;
    epmBoundaries.xMaze_pix=xMaze_pix;
    epmBoundaries.yMaze_pix=yMaze_pix;
    epmBoundaries.xMaze_cm=xMaze_cm;
    epmBoundaries.yMaze_cm=yMaze_cm;
    epmBoundaries.xMaze_pix_rotated=xMaze_pix_rotated;
    epmBoundaries.yMaze_pix_rotated=yMaze_pix_rotated;
    epmBoundaries.coef_cm2pix=coef_cm2pix;
    epmBoundaries.coef_pix2cm=coef_pix2cm;
        
    save([params.dataRoot filesep params.dataFileTag '-epmBoundaries.mat'],'epmBoundaries');    
    close (f1);  close (f2);
else
    
    load([params.dataRoot filesep params.dataFileTag '-epmBoundaries.mat']);
    
end
       
   
    
end





