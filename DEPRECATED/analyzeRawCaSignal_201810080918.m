function Ca=analyzeRawCaSignal_201810080918(sig,ref,frameRate_Hz,removefirstMinute,slidingWindowSize,ouputFig,Force)

% For Test Only
% function Ca=analyzeRawCaSignal_201810080918()
% clearvars
% clc
% close all
% load('Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM\F255.mat');
% frameRate_Hz= 20;
% highPassTh_Hz = .075;
% lowPassTh_Hz = 100;
% removefirstMinute=1;
% slidingWindowSize=1200;

%% If this has not been taken care off earlier, this has to be done to remove autobleaching period that corrupt analysis
if(removefirstMinute), sig(1:20*60)=[];ref(1:20*60)=[];end

%% Processing Bulk Signal
Ca.nFrames = size(ref,1);T = 1: Ca.nFrames;T = T./ frameRate_Hz;
Ca.raw.sig = sig;Ca.raw.ref = ref;Ca.raw.T = T;
deltaFF_sig=deltaFF(sig);deltaFF_ref=deltaFF(ref);
Ca.delta_ff.sig = deltaFF_sig;Ca.delta_ff.ref = deltaFF_ref;

slidingdeltaFF_sig=slidingDeltaFF(sig,slidingWindowSize);
slidingdeltaFF_ref=slidingDeltaFF(ref,slidingWindowSize);
Ca.slidingdelta_ff.sig = slidingdeltaFF_sig;
Ca.slidingdelta_ff.ref = slidingdeltaFF_ref;
Ca.movmean.sig = movmean(sig,slidingWindowSize);
Ca.movmean.ref = movmean(ref,slidingWindowSize);

Ca.delta_ff.ref_fit = controlFit (Ca.delta_ff.sig,Ca.delta_ff.ref);
Ca.slidingdelta_ff.ref_fit = controlFit (Ca.slidingdelta_ff.sig,Ca.slidingdelta_ff.ref);

Ca.diffSig = (Ca.delta_ff.sig - Ca.delta_ff.ref)*100;
Ca.diffSig_fit = (Ca.delta_ff.sig - Ca.delta_ff.ref_fit)*100;
Ca.slidingdiffSig_fit = (Ca.slidingdelta_ff.sig - Ca.slidingdelta_ff.ref_fit)*100;

Ca.divSig = (Ca.delta_ff.sig ./ Ca.delta_ff.ref)*100;
Ca.divSig_fit = (Ca.delta_ff.sig ./ Ca.delta_ff.ref_fit)*100;
Ca.slidingdivSig_fit = (Ca.slidingdelta_ff.sig ./ Ca.slidingdelta_ff.ref_fit)*100;

Ca.mainSig = Ca.slidingdiffSig_fit;

% Figure 1, for test only // Figure 2, for test only
% The code for creating both figures is located at the end fo the file


%% Processing Transients
%% Remove NaN for filtering
Ca.noNaN_mainSig=Ca.mainSig;
iNaN=isnan(Ca.mainSig);
Ca.noNaN_mainSig(iNaN)=[];
%% Filtering
highPassTh_Hz = .075;
fc = highPassTh_Hz;
fs = 20;
[b,c] = butter(4,fc/(fs/2), 'high');
Ca.mainSigHP = filtfilt(b, c, Ca.noNaN_mainSig);
%% Reintroduce NaNs values into the filtered vector
j=1;
Ca.mainSigHP_withNaN = nan(Ca.nFrames,1);
for i=1:Ca.nFrames
    if iNaN(i)
        Ca.mainSigHP_withNaN(i)=nan;
    else
        Ca.mainSigHP_withNaN(i)=Ca.mainSigHP(j);
        j=j+1;
    end
end
%% Define MAD threshold
Ca.mad_ = mad(Ca.mainSigHP_withNaN,1);
Ca.nanmedian_=nanmedian(Ca.mainSigHP_withNaN);
Ca.madTH_mad200 = Ca.nanmedian_+(2*Ca.mad_);
Ca.madTH_mad291 = Ca.nanmedian_+(2.91*Ca.mad_);
thSig = Ca.mainSigHP_withNaN>Ca.madTH_mad200;
%% Extract trnasients (chunk of signal exceding the threshold)
Tr=[];iTr=0;preVal=0;
for i=1:Ca.nFrames-1
    if thSig(i)
        if ~preVal
            iTr=iTr+1;Tr(iTr).start=i;Tr(iTr).indices=[];
        end
        Tr(iTr).indices=[Tr(iTr).indices i];
    end
    preVal=thSig(i);
end

nTr=size(Tr,2);

for iTr=1:nTr
    idx=Tr(iTr).indices;
    Tr(iTr).ts=T(Tr(iTr).start);
    Tr(iTr).duration = size(idx,2);
    [Tr(iTr).vMax,Tr(iTr).iMax] = max(Ca.mainSigHP_withNaN(idx));
    Tr(iTr).iMax = Tr(iTr).iMax + idx(1) - 1;
end

%% (1.1) First Excluision Procedure : Find Transients To Exclude (first Round, Amplitude>2.91 MAD, Duration > 0.2 sec)
for iTr=1:nTr
    %% Amplitude of the transient is to small
    if Tr(iTr).vMax<Ca.madTH_mad291 %too small
        iRemove=[iRemove iTr];
    end
    %% Duration of the transient is too short
    if (Tr(iTr).duration/frameRate_Hz)<0.2 %too short
        iRemove=[iRemove iTr];
    end
end
%% (1.2) Plot Figure to check first exclusion procedure  (first Round, Amplitude>2.91 MAD, Duration > 0.2 sec)
if ~exist(figName,'file') || params.forceRedrawing
f=figure();
hold on
ylim([-25 35])
plot(Ca.raw.T,Ca.mainSig-(max(Ca.mainSig)),'color',[5 69 126]./255);
plot(Ca.raw.T,Ca.mainSigHP_withNaN,'color',[198 8 27]./255);

HP_max = max(Ca.mainSigHP_withNaN);HP_amp = HP_max - min(Ca.mainSigHP_withNaN);tick_size = HP_amp/20;tickUp = HP_max+(tick_size*5);tickDown = tickUp - tick_size;durationTxtPos = tickUp + (tick_size*5);

plot([Ca.raw.T(1) Ca.raw.T(end)],[Ca.madTH_mad200 Ca.madTH_mad200],'k:');
plot([Ca.raw.T(1) Ca.raw.T(end)],[Ca.madTH_mad291 Ca.madTH_mad291],'k:');

col_=colormap(lines(10));
nTr=size(Tr,2);

for iTr=1:nTr
    idx=Tr(iTr).indices;
    plot(Ca.raw.T(idx),Ca.mainSigHP_withNaN(idx),'color',[26 160 70]./255);
    t = Ca.raw.T(Tr(iTr).iMax);
    iMod=mod(iTr,10)+1;
    
    if (Tr(iTr).vMax>=Ca.madTH_mad291) && ((Tr(iTr).duration/frameRate_Hz)>=0.2)
        plot(t,Ca.mainSigHP_withNaN(Tr(iTr).iMax),'MarkerEdgeColor',col_(iMod,:),'MarkerFaceColor','none','Marker','s');
        text(t,durationTxtPos+((HP_amp/10)*iMod),num2str(Tr(iTr).duration/frameRate_Hz),'FontSize',6,'color',col_(iMod,:));
        plot([t t],[tickUp tickDown],'color',col_(iMod,:));
        plot(t,Ca.mainSig(Tr(iTr).iMax)-(max(Ca.mainSig)),'Marker','o','MarkerEdgeColor',col_(iMod,:));
    end
    
    if Tr(iTr).vMax<Ca.madTH_mad291 %too small
        plot([t t],[tickUp tickDown],'color',[0.5 0.5 0.5]);
        text(t,durationTxtPos+((HP_amp/10)*iMod),num2str(Tr(iTr).duration/frameRate_Hz),'FontSize',6,'color',[0.5 0.5 0.5]);
        plot(t,Ca.mainSig(Tr(iTr).iMax)-(max(Ca.mainSig)),'Marker','o','MarkerEdgeColor',[0.5 0.5 0.5]);
    end
    
    if (Tr(iTr).duration/frameRate_Hz)<0.2 %too short
        plot([t t],[tickUp tickDown],'color',[1 .8 .8]);
        text(t,durationTxtPos+((HP_amp/10)*iMod),num2str(Tr(iTr).duration/frameRate_Hz),'FontSize',6,'color',[1 .8 .8]);
        plot(t,Ca.mainSig(Tr(iTr).iMax)-(max(Ca.mainSig)),'Marker','o','MarkerEdgeColor',[1 .8 .8]);
    end
    
end
%% (1.3) First Excluision Procedure : Remove the bad transients
Tr(iRemove)=[];

%% (2.1) Second Excluision Procedure : Find Transients To Exclude (Seocnd Round, Time Between Transients >= 1 sec)
nTr=size(Tr,2);
iRemove=[];
dt = diff([Tr(:).ts]);
for iTr=2:nTr
    if dt(iTr-1)<1
        iRemove=[iRemove iTr];
    end
end
%% (2.2) Plot Figure to check first exclusion procedure  (first Round, Amplitude>2.91 MAD, Duration > 0.2 sec)
f=figure();
hold on
ylim([-25 35])
plot(Ca.raw.T,Ca.mainSig-(max(Ca.mainSig)),'color',[5 69 126]./255);
plot(Ca.raw.T,Ca.mainSigHP_withNaN,'color',[198 8 27]./255);

HP_max = max(Ca.mainSigHP_withNaN);HP_amp = HP_max - min(Ca.mainSigHP_withNaN);tick_size = HP_amp/20;tickUp = HP_max+(tick_size*5);tickDown = tickUp - tick_size;durationTxtPos = tickUp + (tick_size*5);

plot([Ca.raw.T(1) Ca.raw.T(end)],[Ca.madTH_mad200 Ca.madTH_mad200],'k:');
plot([Ca.raw.T(1) Ca.raw.T(end)],[Ca.madTH_mad291 Ca.madTH_mad291],'k:');

col_=colormap(lines(10));
nTr=size(Tr,2);

dt = diff([Tr(:).ts]);

for iTr=2:nTr
    idx=Tr(iTr).indices;
    plot(Ca.raw.T(idx),Ca.mainSigHP_withNaN(idx),'color',[26 160 70]./255);
    t = Ca.raw.T(Tr(iTr).iMax);
    iMod=mod(iTr,10)+1;
    
    if dt(iTr-1)>=1
        plot(t,Ca.mainSigHP_withNaN(Tr(iTr).iMax),'MarkerEdgeColor',col_(iMod,:),'MarkerFaceColor','none','Marker','s');
        text(t,durationTxtPos+((HP_amp/10)*iMod),sprintf('%2.2f-%2.2f',Tr(iTr).duration/frameRate_Hz,dt(iTr-1)),'FontSize',6,'color',col_(iMod,:));
        plot([t t],[tickUp tickDown],'color',col_(iMod,:));
        plot(t,Ca.mainSig(Tr(iTr).iMax)-(max(Ca.mainSig)),'Marker','o','MarkerEdgeColor',col_(iMod,:));
    else
        plot(t,Ca.mainSigHP_withNaN(Tr(iTr).iMax),'MarkerEdgeColor',[0.5 0.5 0.5],'MarkerFaceColor','none','Marker','s');
        text(t,durationTxtPos+((HP_amp/10)*iMod),sprintf('%2.2f-%2.2f',Tr(iTr).duration/frameRate_Hz,dt(iTr-1)),'FontSize',6,'color',[0.5 0.5 0.5]);
        plot([t t],[tickUp tickDown],'color',[0.5 0.5 0.5]);
        plot(t,Ca.mainSig(Tr(iTr).iMax)-(max(Ca.mainSig)),'Marker','o','MarkerEdgeColor',[0.5 0.5 0.5]);
    end
    
    
end
%% (2.3) First Excluision Procedure : Remove the bad transients
Tr(iRemove)=[];

Ca.transients.idx=[Tr(:).iMax];
Ca.transients.ts=T(Ca.transients.idx);
Ca.transients.values=[Tr(:).vMax];
Ca.transientsStructure=Tr;

end


function [controlFit] = controlFit (dat1, dat2)

iNanDat1 = isnan(dat1);iNanDat2 = isnan(dat2);
iNan = iNanDat1 | iNanDat2;

noNanDat1 = dat1;noNanDat2 = dat2;
noNanDat1(iNan)=[];noNanDat2(iNan)=[];
reg = polyfit(noNanDat2, noNanDat1, 1);
a = reg(1);
b = reg(2);
controlFit = a.*dat2 + b;
end
function v2=deltaFF(v)
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
m=nanmean(v);
v2 = v - m;
v2 = v2./m;
end
function v3=slidingDeltaFF(v,slidingWindowSize)
movmean_v = movmean(v,slidingWindowSize);
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
v2 = v - movmean_v;
v3 = v2./movmean_v;
end





%% Figure 1, for test only
% f=figure();
% subplot(3,1,1)
% hold on
% plot(Ca.raw.T,Ca.raw.sig);
% movmean_sig = movmean(sig,slidingWindowSize);
% plot(Ca.raw.T,movmean_sig);
% legend({'Ca.raw.sig','movmean_sig'})
% subplot(3,1,2)
% hold on
% plot(Ca.raw.T,Ca.delta_ff.sig);
% plot(Ca.raw.T,Ca.slidingdelta_ff.sig);
% legend({'Ca.delta_ff.sig','Ca.slidingdelta_ff.sig'})
% subplot(3,1,3)
% hold on
% plot(Ca.raw.T,Ca.diffSig );
% plot(Ca.raw.T,Ca.diffSig_fit);
% plot(Ca.raw.T,Ca.slidingdiffSig_fit);
% legend({'Ca.diffSig.sig','Ca.diffSig_fit.sig','Ca.slidingdiffSig_fit'})
%% Figure 2, for test only
% f=figure();
% hold on
% plot(Ca.raw.T,Ca.delta_ff.sig,'b');
% plot(Ca.raw.T,Ca.delta_ff.ref ,'m');
% [controlFit] = controlFit (Ca.delta_ff.sig,Ca.delta_ff.ref)
% plot(Ca.raw.T,controlFit,'k');
% legend({'Ca.raw.sig','movmean_sig'})



