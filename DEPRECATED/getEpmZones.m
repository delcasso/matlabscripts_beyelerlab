function zones = getEpmZones(params,videoTrackingData)

zoneFilename = [params.dataRoot filesep params.dataFileTag '-zones.mat'];

if ~exist(zoneFilename,'file')
    
    f = dir([params.dataRoot filesep '*-zones.mat']);
    nF = size(f,1);
    if nF, load([params.dataRoot filesep f(nF).name]);end
    
    f1=figure();
    hold on;
    colormap(gray)
    imagesc(videoTrackingData.bg);
    axis off
    axis equal
    set(gca,'Ydir','reverse')
    
    openArmColor = [0 0 1];closedArmColor = [1 0 0];centerColor = [0 1 0];
    bgSize = size(videoTrackingData.bg);
    t=text(10,(bgSize(1)/100)*10,'CLOSED ARMS IN RED','color',closedArmColor);
    t=text(10,(bgSize(1)/100)*20,'OPEN ARMS IN BLUE','color',openArmColor);
    t=text(10,(bgSize(1)/100)*30,'CENTER IN GREEN','color',centerColor);
    
    for i=1:5
        switch i
            case 1
                zones(i).type='closed';zones(i).Color=closedArmColor;
            case 2
                zones(i).type='open';zones(i).Color=openArmColor;
            case 3
                zones(i).type='closed';zones(i).Color=closedArmColor;
            case 4
                zones(i).type='open';zones(i).Color=openArmColor;
            case 5
                zones(i).type='center';zones(i).Color=centerColor;
        end
        fprintf(sprintf('Draw Zone %d [%s]\n',i,zones(i).type))      
        
        if ~isfield(zones, 'position')
            zones(i).position=[];
        end
        if ~isempty(zones(i).position) 
            h(i) = imrect(gca,zones(i).position);
        else
            h(i) = imrect();
        end
        h(i).setColor(zones(i).Color);
    end
    
    prompt = 'Press Any Key When You Are Done Drawing This ROI';
    str = input(prompt,'s');
    
    for i=1:5
        zones(i).position = h(i).getPosition;
        if zones(i).position(1)<1, zones(i).position(1)=1;end
        if zones(i).position(2)<1, zones(i).position(2)=1;end
        zX(i) = zones(i).position(1);zones(i).X=zX(i);
        zY(i) = zones(i).position(2);zones(i).Y=zY(i);
        zW(i) = zones(i).position(3);zones(i).W=zW(i);
        zH(i) = zones(i).position(4);zones(i).H=zH(i);
    end
    
    [~,ii] = min(zY);zones(ii).positionSTR = 'NORTH';% NORTH ARM
    [~,ii] = max(zX);zones(ii).positionSTR = 'EAST';% EAST ARM
    [~,ii] = max(zY);zones(ii).positionSTR = 'SOUTH';% SOUTH ARM
    [~,ii] = min(zX);zones(ii).positionSTR = 'WEST';% WEST ARM
    zones(5).positionSTR = 'CENTER';% CENTER
    
    zones(1).lineStyle = '-';
    zones(2).lineStyle = '-';
    zones(3).lineStyle = '-.';
    zones(4).lineStyle = '-.';
    zones(5).lineStyle = '-';
    
    % zones(1) is always a close arm
    
    save(zoneFilename,'zones');
    print(f1,[params.dataFileTag '-zones.jpeg'],'-djpeg');
    close(f1);
    
    
else
    load(zoneFilename);
end

for i=1:5
    zones(i).position = floor(zones(i).position);
end


end