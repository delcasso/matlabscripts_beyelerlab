function Ca=analyzeRawCaSignal_201810041018(sig,ref,frameRate_Hz,highPath_Detection_Hz,highPath_Trace_Hz,removefirstMinute,slidingWindowSize)

%% For Test Only
% function Ca=analyzeRawCaSignal_201806131229()
% clear
% clc
% close all
% load('S:\Fiber-Photometry\data\roman-mito\20180706_thc_fullexp\m2-thc.mat');
% frameRate_Hz= 20;
% highPath_Detection_Hz = 1;
% highPath_Trace_Hz = 0.075;
% removefirstMinute=1;

if(removefirstMinute), sig(1:20*60)=[];ref(1:20*60)=[];end


%%
nFrames = size(ref,1);T = 1: nFrames;T = T./ frameRate_Hz;
Ca.raw.sig = sig;Ca.raw.ref = ref;Ca.raw.T = T;
deltaFF_sig=deltaFF(sig);deltaFF_ref=deltaFF(ref);
Ca.delta_ff.sig = deltaFF_sig;Ca.delta_ff.ref = deltaFF_ref;

slidingdeltaFF_sig=slidingDeltaFF(sig,slidingWindowSize);
slidingdeltaFF_ref=slidingDeltaFF(ref,slidingWindowSize);
Ca.slidingdelta_ff.sig = slidingdeltaFF_sig;
Ca.slidingdelta_ff.ref = slidingdeltaFF_ref;
Ca.movmean.sig = movmean(sig,slidingWindowSize);
Ca.movmean.ref = movmean(ref,slidingWindowSize);

Ca.delta_ff.ref_fit = controlFit (Ca.delta_ff.sig,Ca.delta_ff.ref);
Ca.slidingdelta_ff.ref_fit = controlFit (Ca.slidingdelta_ff.sig,Ca.slidingdelta_ff.ref);

Ca.diffSig = (Ca.delta_ff.sig - Ca.delta_ff.ref)*100;
Ca.diffSig_fit = (Ca.delta_ff.sig - Ca.delta_ff.ref_fit)*100;
Ca.slidingdiffSig_fit = (Ca.slidingdelta_ff.sig - Ca.slidingdelta_ff.ref_fit)*100;

Ca.divSig = (Ca.delta_ff.sig ./ Ca.delta_ff.ref)*100;
Ca.divSig_fit = (Ca.delta_ff.sig ./ Ca.delta_ff.ref_fit)*100;
Ca.slidingdivSig_fit = (Ca.slidingdelta_ff.sig ./ Ca.slidingdelta_ff.ref_fit)*100;

Ca.mainSig = Ca.slidingdiffSig_fit;



%%

Ca.mad_ = mad(Ca.mainSig,1);
Ca.nanmedian_=nanmedian(Ca.mainSig);
iMAD=2.91;
Ca.madTH = Ca.nanmedian_+(iMAD*Ca.mad_);
thSig = Ca.mainSig>Ca.madTH;



Tr=[];iTr=0;preVal=0;
for i=1:nFrames-1
    if thSig(i)
        if ~preVal
            iTr=iTr+1;Tr(iTr).start=i;Tr(iTr).indices=[];
        end
            Tr(iTr).indices=[Tr(iTr).indices i];
    end
    preVal=thSig(i);    
end

nTr=size(Tr,2);

for iTr=1:nTr
    idx=Tr(iTr).indices;
    Tr(iTr).ts=T(Tr(iTr).start);
    Tr(iTr).duration = size(idx,2);
    [Tr(iTr).vMax,Tr(iTr).iMax] = max(Ca.mainSig(idx));
    Tr(iTr).iMax = Tr(iTr).iMax + idx(1) - 1;
end

Ca.transients.idx=[Tr(:).iMax];
Ca.transients.ts=T(Ca.transients.idx);
Ca.transients.values=[Tr(:).vMax];

Ca.transientsStructure=Tr;



end


function [controlFit] = controlFit (dat1, dat2)

iNanDat1 = isnan(dat1);iNanDat2 = isnan(dat2);
iNan = iNanDat1 | iNanDat2;

noNanDat1 = dat1;noNanDat2 = dat2;
noNanDat1(iNan)=[];noNanDat2(iNan)=[];
reg = polyfit(noNanDat2, noNanDat1, 1);
a = reg(1);
b = reg(2);
controlFit = a.*dat2 + b;
end
function v2=deltaFF(v)
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
m=nanmean(v);
v2 = v - m;
v2 = v2./m;
end
function v3=slidingDeltaFF(v,slidingWindowSize)
movmean_v = movmean(v,slidingWindowSize);
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
v2 = v - movmean_v;
v3 = v2./movmean_v;
end








% f=figure();
% subplot(3,1,1)
% hold on
% plot(Ca.raw.T,Ca.raw.sig);
% movmean_sig = movmean(sig,slidingWindowSize);
% plot(Ca.raw.T,movmean_sig);
% subplot(3,1,2)
% hold on
% plot(Ca.raw.T,Ca.delta_ff.sig);
% plot(Ca.raw.T,Ca.slidingdelta_ff.sig);
% subplot(3,1,3)
% hold on
% plot(Ca.raw.T,Ca.diffSig );
% plot(Ca.raw.T,Ca.diffSig_fit);
% plot(Ca.raw.T,Ca.slidingdiffSig_fit);
%% For Test Only
% f=figure();
% hold on
% plot(Ca.raw.T,Ca.delta_ff.sig,'b');
% plot(Ca.raw.T,Ca.delta_ff.ref ,'m');
% [controlFit] = controlFit (Ca.delta_ff.sig,Ca.delta_ff.ref)
% plot(Ca.raw.T,controlFit,'m






