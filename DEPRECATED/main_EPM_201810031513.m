close all;clear;clc

%% Params EPM IC-SweetBitter G1
% params.dataRoot = 'C:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180307_IC_G1_newAnalysis\20180316_EPM-preFC_IC_fpG1';
% params.dataRoot = 'C:\Users\Delcasso\Desktop\20180316_EPM-preFC_IC_fpG1';
params.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
params.cameraMode = 'synchronous';
params.ledDetectionThreshold = 50; %percent of led signal max
params.videoExtension='avi';
params.HamamatsuFrameRate_Hz= 20;
params.behaviorCameraFrameRate_Hz=20;

params.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed

params.savePDF = 0;
params.saveFIG = 0;
params.savePNG = 0;
params.forceRedrawing = 0;

params.forceBehavioralStart = 0;

params.MapScale_cmPerBin = 0.5;
params.OccupancyMap_sigmaGaussFilt=5;
params.PhotometrySignalMap_sigmaGaussFilt=5;

params.calciumAnalysis_highPath_Detection_Hz = 1;
params.calciumAnalysis_highPath_Trace_Hz = 0.075;
params.deltaFF_slidingWindowWidth = 1200;

% Ugo Basile version 1
params.epmOpenArmsEnvergure_cm = 80;
params.epmClosedArmsEnvergure_cm = 75;
params.epmArmsWidth_cm = 5;

% Cajal Schoole, Noldus, version 1
% params.epmOpenArmsEnvergure_cm = 80;
% params.epmClosedArmsEnvergure_cm = 80;
% params.epmArmsWidth_cm = 6;

params.lookingForMouse = '';

%% MAIN PROGRAM
fileList = dir([params.dataRoot filesep '*.' params.videoExtension]);nFiles = size(fileList,1);
all_Names = {};all_LSZ = {};% all_synchroSig = {};

% PROCESS EACH DATA FILE INDIVIDUALLY
for iFile=1:nFiles
    
    params.filename = fileList(iFile).name;
    [params.fPath, params.dataFileTag, params.ext] = fileparts(params.filename);% Parse Filename To Use Same Filename to save analysis results
    
    processThisFile=1;
    if ~isempty(params.lookingForMouse)
        processThisFile=0;
        if strcmp(params.dataFileTag,params.lookingForMouse)
            processThisFile=1;
        end
    end
    
    if processThisFile
        
        fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,params.dataFileTag);
        
        % LOADING, SYNCHRONIZING AND CLEANING THE DATA
        photometryData = loadPhotometryData(params);
        videoTrackingData = getVideoTrackingData(params);
        videoTrackingData.bg=getBackGroundQuick(params);
        [photometryData,videoTrackingData] =  HamamatsuBlackFlyCorrectFrameNumbers(photometryData,videoTrackingData,params);
        if strcmp(params.cameraMode,'asynchronous')
            [videoTrackingData,nSynchro,dFrame]=reSynchData(videoTrackingData,photometryData,params)
            fprintf('\t> Synchro nSig=%d dt=%d frames\n',nSynchro,dFrame);
            draw_figure6(params.dataRoot,params.dataFileTag,videoTrackingData,B_synchronized);
        end
        
        iBehavioralStarts=detectBehavioralStart(params);
        nFramesToRemove = params.HamamatsuFrameRate_Hz*60; %remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
        if iBehavioralStarts>nFramesToRemove
            nFramesToRemove=iBehavioralStarts(1); % After the minute of bleaching, the animal was placed on the maze by the experimenter, iBehavioralStarts(1) represents the time the experimenter as left the camera field of view
        end
        [videoTrackingData,photometryData]=removeFrames(videoTrackingData,photometryData,nFramesToRemove,'Beginning');%remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording
        
        % DETECT EXPERIMENTER INTERRUPTION OF BEHAVIOR
        nExperimenterIntervention = size(iBehavioralStarts,2);
        if nExperimenterIntervention>1, warning('Abnormal Intervention of Experimenter, did animal fall of EPM ?');end
        
        % DETECT ABNORMALITIES IN PHOTOMETRY SIGNAL
        [photometryData,videoTrackingData]= detectFiberDisconnection(params,photometryData,videoTrackingData);
        
        % DETECT ABNORMAL PHOTOMETRY SIGNAL WITH NEGATIVE RAW VALUES
        photometryData = detectAbnormalNegativeValuesInPhotometryRawData(photometryData);
        
        videoTrackingData=cleanPosBasedOnSpeedThreshold(videoTrackingData,params); % remove all position when animal spped exceed 20 pixel per sec.
        nFrames = size(videoTrackingData.mainX,1);T = 1: nFrames;T = T./ params.HamamatsuFrameRate_Hz;  photometryData.T = T; %recreate time vector based on fiber-photometry frame rate
        fprintf('\t#Hamamatsu %d',size(photometryData.sig,1));fprintf('\t\t#BlackFly %d\n',size(videoTrackingData.mainX,1));
        
        % standardized Experiments for inter batch comparaison
        % standardize EPM position,size and rotation to camera
        videoTrackingData.openArmsCorners=getEpmOpenArmsCorners_201810021129(params);
        % videoTrackingData.orientationSTR = getEpmOrientation(videoTrackingData.openArmsCorners);
        [videoTrackingData,params]=transformMouseCoordinates_pix2cmStandard(videoTrackingData,params);
        epmDesign_cmStandard=getEpmDesign(params.epmOpenArmsEnvergure_cm,params.epmClosedArmsEnvergure_cm,params.epmArmsWidth_cm);
        
        % From cmStandard to cmStandardPositive, to match positive indices
        % of matlab arrays used in following map constructions
        [videoTrackingData,epmDesign_cmStandardPositive]=transformCoordinates_cmStandard2cmStandardPositive(videoTrackingData,epmDesign_cmStandard);
        videoTrackingData.zones_cmStandardPositive=getEpmZones_201810020928(epmDesign_cmStandardPositive);
        
        CaSignal = analyzeRawCaSignal_2018009251459(photometryData.sig,photometryData.ref,params.HamamatsuFrameRate_Hz,params.calciumAnalysis_highPath_Detection_Hz,params.calciumAnalysis_highPath_Trace_Hz,0,params.deltaFF_slidingWindowWidth);
        %         photometryData.mainSig = CaSignal.slidingdiffSig_fit;
        photometryData.mainSig = CaSignal.diffSig;
        photometryData.transients.ts = CaSignal.transients.ts;
        photometryData.transients.values = CaSignal.transients.values;
        photometryData.transients.idx = CaSignal.transients.idx;        
        photometryData.transients.continious =  CaSignal.transients.continious;
        
        
        % plot main photometry signal
        figName = [params.dataRoot filesep 'figures' filesep params.dataFileTag '-photometryProcessedSignal.jpeg'];
        if ~exist(figName,'file') || params.forceRedrawing
            f1=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
            plot(photometryData.mainSig,'color',[0 255 23]./255);
            print(f1,figName,'-djpeg');
            close(f1);
        end
        
        
        
        % CREATING ACTIVITY MAPS
        xMax = max(epmDesign_cmStandardPositive.x); yMax = max(epmDesign_cmStandardPositive.y);
        activityMaps.OccupancyMap=buildOccupancyMap_cmStandardPositive(videoTrackingData.mainX_cmStandardPositive,videoTrackingData.mouseY_cmStandardPositive,xMax,yMax,params.MapScale_cmPerBin); %build a spatial map of the time spend in each pixel for the entire exp.
        activityMaps.SumSignalMap=buildSignalMap_cmStandardPositive(videoTrackingData.mainX_cmStandardPositive,videoTrackingData.mouseY_cmStandardPositive,photometryData.mainSig,xMax,yMax,params.MapScale_cmPerBin); %build a spatial map of the sum of Ca++ signal recorded in each pixel for the entire exp.                
        activityMaps.PercAvgSigMap=normSigMap(activityMaps.SumSignalMap,activityMaps.OccupancyMap);
                
        drawVideoTrackingTrajectory(params,videoTrackingData.mainX,videoTrackingData.mouseY); % Draw traceXXX.jpeg
        %         drawTrajectoryOverlayedWithSpeed(params,videoTrackingData.mainX_cmStandardPositive,videoTrackingData.mouseY_cmStandardPositive,xMax,yMax)
        drawOccupancyMap(params,activityMaps.OccupancyMap,xMax,yMax); % Draw OccupancyMapXXX.jpeg
        drawPhotometrySignalMap(params,activityMaps.PercAvgSigMap,xMax,yMax); % Draw SignalMapXXX.jpeg
        
        
        
        
        % TAKING MOUSE DIRECTION (INSIDE/ Vs. OUSIDE) THE ARM
        [activityMaps.OccIn,activityMaps.OccOut,activityMaps.SumSigIn,activityMaps.SumSigOut]=getEpmWithinArmDirection_cmStandardPositive(videoTrackingData.mainX_cmStandardPositive,videoTrackingData.mouseY_cmStandardPositive,photometryData.mainSig,videoTrackingData.zones_cmStandardPositive,xMax,yMax,params.MapScale_cmPerBin);
        activityMaps.PercAvgSigMap_I=normSigMap(activityMaps.SumSigIn,activityMaps.OccIn);
        activityMaps.PercAvgSigMap_O=normSigMap(activityMaps.SumSigOut,activityMaps.OccOut);
        
        % CREATING LINEAR MAPS         %ZS stands for Zoned Signal, LZS for linearized Zoned Signal
        outputPrefix = 'IN-OUT';
        [avgActivityMaps_IO.ZS, avgActivityMaps_IO.LZS, avgActivityMaps_IO.minLZS, avgActivityMaps_IO.maxLZS] = epm_alignSignalToArms_cmStandardPositive(params,activityMaps.PercAvgSigMap,videoTrackingData.zones_cmStandardPositive,outputPrefix);
        draw_linearizedActivityMaps_cmStandardPositive(params,outputPrefix,activityMaps.PercAvgSigMap,avgActivityMaps_IO,videoTrackingData);
        outputPrefix = 'IN';
        [avgActivityMaps_I.ZS, avgActivityMaps_I.LZS, avgActivityMaps_I.minLZS, avgActivityMaps_I.maxLZS] = epm_alignSignalToArms_cmStandardPositive(params,activityMaps.PercAvgSigMap_I,videoTrackingData.zones_cmStandardPositive,outputPrefix);
        draw_linearizedActivityMaps_cmStandardPositive(params,outputPrefix,activityMaps.PercAvgSigMap_I,avgActivityMaps_I,videoTrackingData);
        outputPrefix = 'OUT';
        [avgActivityMaps_O.ZS, avgActivityMaps_O.LZS, avgActivityMaps_O.minLZS, avgActivityMaps_O.maxLZS] = epm_alignSignalToArms_cmStandardPositive(params,activityMaps.PercAvgSigMap_O,videoTrackingData.zones_cmStandardPositive,outputPrefix);
        draw_linearizedActivityMaps_cmStandardPositive(params,outputPrefix,activityMaps.PercAvgSigMap_O,avgActivityMaps_O,videoTrackingData);

        
        save([params.mergedOutputsFolder filesep params.dataFileTag '.mat'],'activityMaps','videoTrackingData','photometryData','avgActivityMaps_IO','avgActivityMaps_I','avgActivityMaps_O');
    else
        fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,params.dataFileTag);
    end
    
end





function draw_figure6(dataRoot,dataFileTag,B,B_synchronized)
figName = [dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    hold on
    plot(B.mainX,B.mouseY,'color',[.7 .7 .7],'LineStyle','-','Marker','o');
    plot(B_synchronized.mainX,B_synchronized.mouseY,'color',[.7 .2 .7],'LineStyle','-','Marker','+');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamPosition-' dataFileTag '.dpdf'],'-dpdf');
    close(f6);
end
figName = [dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'];
if ~exist(figName,'file')
    f6=figure();
    subplot(2,2,[1 2])
    hold on
    title('All')
    plot(B.optoPeriod);
    subplot(2,2,3)
    hold on
    title('First Min')
    plot(B.optoPeriod(1:15*60));
    subplot(2,2,4)
    hold on
    title('Last Min')
    plot(B.optoPeriod(end-(15*60):end));
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.jpeg'],'-djpeg');
    print(f6,[dataRoot filesep 'resynchWebCamLED-' dataFileTag '.dpdf'],'-dpdf');
    close(f6);
end
end





%% Params SD EPM IC fpG1
% dataRoot = 'Y:\Fiber-Photometry\data\IC_fpG1\20180316_EPM-preFC_IC_fpG1';
% cameraMode = 'synchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max videoExtension =
% 'mp4i';
%% Params LS EPM IC-vHPC fpG1
% dataRoot ='Y:\Fiber-Photometry\data\IC-vHPC_fpG1\20180511_EPM_IC-vHPC_fpG1';
% cameraMode = 'asynchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 50; %percent of led signal max videoExtension =
% 'mp4';
%% Params YM EPM IC-BLA fpG1
% dataRoot =% 'Y:\Fiber-Photometry\data\IC-BLA_fpG1\20180524_EPM_IC-BLA_fpG1';
% cameraMode = 'asynchronous'; groupFilename= 'stressGroup.txt';
% ledDetectionThreshold = 25; %percent of led signal max videoExtension =
% 'avi';




