% function drawEpmBoundaries(params)

clear


%% Params EPM IC-SweetBitter G1
params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
params.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
params.cameraMode = 'synchronous';
params.ledDetectionThreshold = 50; %percent of led signal max
params.videoExtension='avi';
params.HamamatsuFrameRate_Hz= 20;
params.behaviorCameraFrameRate_Hz=20;

params.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed

params.savePDF = 0;
params.saveFIG = 0;
params.savePNG = 0;

params.forceBehavioralStart = 0;

params.OccupancyMap_sigmaGaussFilt=2;
params.PhotometrySignalMap_sigmaGaussFilt=2;

params.calciumAnalysis_highPath_Detection_Hz = 1;
params.calciumAnalysis_highPath_Trace_Hz = 0.075;
params.deltaFF_slidingWindowWidth = 1200;

params.lookingForMouse = '';

params.filename = 'F251.avi';
[~, params.dataFileTag, ~] = fileparts(params.filename);% Parse Filename To Use Same Filename to save analysis results
    
videoTrackingData = getVideoTrackingData(params);
bg=getBackGroundQuick(params);

f3=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
subplot(3,4,[1 2 3 5 6 7 9 10 11])
hold on
colormap(gray(1024));
imagesc(bg);
axis equal
axis off
set(gca,'Ydir','reverse')
sMax = max(size(bg));
xlim([0 sMax]);ylim([0 sMax]);

% if ~exist([params.dataRoot filesep params.dataFileTag '-epmBoundaries.mat'])
%     fprintf('Draw Open Arms (3 points)')
%     [xOpenArms,yOpenArms]=ginput(3);
    
%     save([params.dataRoot filesep params.dataFileTag '-epmBoundaries.mat'],'xOpenArms','yOpenArms');
% else
%     load([params.dataRoot filesep params.dataFileTag '-epmBoundaries.mat']);
% end

xOpenArms
yOpenArms
plot(xOpenArms,yOpenArms,'r');
% In realty the envergure of the EPM is 80 cm, and we want them to be oriented horizontaly
% so we calculcate the rotAngle by the experimenter to correct it

dX = diff(xOpenArms(1:2));
dY = diff(yOpenArms(1:2));
rotAngle = rad2deg(asin(dY/dX));

xCenter = sum(xOpenArms([1 3]))/2
yCenter = sum(yOpenArms([1 3]))/2


% the diagonal of the open arms as an angle of  3.5833 deg 
% We use it to find the yCenter coor used  for coordinates rotation

plot(xCenter,yCenter,'rx');

for i=1:2
    [xOpenArms(i),yOpenArms(i)] = rotateXY(xOpenArms(i),yOpenArms(i),-rotAngle,xCenter,yCenter)
end


plot(xOpenArms,yOpenArms,'r:');



