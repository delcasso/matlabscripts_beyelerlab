function orientationSTR = getEpmOrientation(openArmsCorners)

x=openArmsCorners.x;
y=openArmsCorners.y;
dX=x(2)-x(1);
dY=y(3)-y(2);
if dX>dY
    orientationSTR='horizontal';
else
    orientationSTR='vertical';
end