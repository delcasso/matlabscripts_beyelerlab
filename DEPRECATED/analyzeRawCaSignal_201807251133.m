
function Ca=analyzeRawCaSignal_201809171332(sig,ref,frameRate_Hz,highPath_Detection_Hz,highPath_Trace_Hz,removefirstMinute)

%% For Test Only
% function Ca=analyzeRawCaSignal_201806131229()
% clear
% clc
% close all
% load('S:\Fiber-Photometry\data\roman-mito\20180706_thc_fullexp\m2-thc.mat');
% frameRate_Hz= 20;
% highPath_Detection_Hz = 1;
% highPath_Trace_Hz = 0.075;
% removefirstMinute=1;

if(removefirstMinute), sig(1:20*60)=[];ref(1:20*60)=[];end

%%
nFrames = size(ref,1);T = 1: nFrames;T = T./ frameRate_Hz;
Ca.raw.sig = sig;Ca.raw.ref = ref;Ca.raw.T = T;
deltaFF_sig=deltaFF(sig);deltaFF_ref=deltaFF(ref);
Ca.delta_ff.sig = deltaFF_sig;Ca.delta_ff.ref = deltaFF_ref;

slidingWindowSize = 1200;
slidingdeltaFF_sig=slidingDeltaFF(sig,slidingWindowSize);
slidingdeltaFF_ref=slidingDeltaFF(ref,slidingWindowSize);
Ca.slidingdelta_ff.sig = slidingdeltaFF_sig;
Ca.slidingdelta_ff.ref = slidingdeltaFF_ref;
Ca.movmean.sig = movmean(sig,slidingWindowSize);
Ca.movmean.ref = movmean(ref,slidingWindowSize);

Ca.delta_ff.ref_fit = controlFit (Ca.delta_ff.sig,Ca.delta_ff.ref);
Ca.slidingdelta_ff.ref_fit = controlFit (Ca.slidingdelta_ff.sig,Ca.slidingdelta_ff.ref);

Ca.diffSig = (Ca.delta_ff.sig - Ca.delta_ff.ref)*100;
Ca.diffSig_fit = (Ca.delta_ff.sig - Ca.delta_ff.ref_fit)*100;
Ca.slidingdiffSig_fit = (Ca.slidingdelta_ff.sig - Ca.slidingdelta_ff.ref_fit)*100;

a = Ca.slidingdiffSig_fit;ii = find(a>mean(a)+(3*std(a)));Ca.threestdpeaks.idx = ii; 



% f=figure();
% subplot(3,1,1)
% hold on
% plot(Ca.raw.T,Ca.raw.sig);
% movmean_sig = movmean(sig,slidingWindowSize);
% plot(Ca.raw.T,movmean_sig);
% subplot(3,1,2)
% hold on
% plot(Ca.raw.T,Ca.delta_ff.sig);
% plot(Ca.raw.T,Ca.slidingdelta_ff.sig);
% subplot(3,1,3)
% hold on
% plot(Ca.raw.T,Ca.diffSig );
% plot(Ca.raw.T,Ca.diffSig_fit);
% plot(Ca.raw.T,Ca.slidingdiffSig_fit);
%% For Test Only
% f=figure();
% hold on
% plot(Ca.raw.T,Ca.delta_ff.sig,'b');
% plot(Ca.raw.T,Ca.delta_ff.ref ,'m');
% [controlFit] = controlFit (Ca.delta_ff.sig,Ca.delta_ff.ref)
% plot(Ca.raw.T,controlFit,'m

%%
Ca.diffSig = (Ca.delta_ff.sig - Ca.delta_ff.ref)*100;
Ca.diffSig_fit = (Ca.delta_ff.sig - Ca.delta_ff.ref_fit)*100;
fc = highPath_Detection_Hz;
fs = 100;
[b,c] = butter(4,fc/(fs/2), 'high');
fd = filtfilt(b, c, Ca.slidingdiffSig_fit );

if exist('smooth','builtin')
    smoothedFdDetection = smooth(fd);
else
    smoothedFdDetection = fd;
end

Ca.smoothedFdDetection.values=smoothedFdDetection;

fc = highPath_Trace_Hz;
fs = 100;
[b,c] = butter(4,fc/(fs/2), 'high');
fd = filtfilt(b, c, Ca.diffSig_fit);


if exist('smooth','builtin')
    smoothedFd = smooth(fd);
else
    smoothedFd = fd;
end

% Ca.smoothedNormSig = smoothedFd;
Ca.smoothedTrace.values = smoothedFd;



mad_ = mad(smoothedFdDetection,1);
medFd=median(smoothedFdDetection);
iMAD=2.91;
madTH = medFd+iMAD*mad_;

Ca.smoothedNormSig.mad = mad_;
Ca.smoothedNormSig.median = medFd;
Ca.smoothedNormSig.madThreshold = madTH;

[pksTransients,locsTransients,w,p] = findpeaks(smoothedFdDetection);
locsTransients=locsTransients(find(pksTransients>madTH));
nTransients = size(locsTransients,1);
Ca.nTransients=nTransients;
dt_Transients = diff(T(locsTransients));
ii = find(dt_Transients<1);
ii=ii+1;

iTransients = 1: nTransients;
iTransients = setdiff(iTransients,ii);


Ca.peaks.ts = T(locsTransients);
Ca.peaks.values = smoothedFdDetection(locsTransients);
Ca.peaks.idx = locsTransients;
Ca.transients.ts = T(locsTransients(iTransients));
Ca.transients.values = smoothedFdDetection(locsTransients(iTransients));
Ca.transients.idx = locsTransients(iTransients);

end


function [controlFit] = controlFit (dat1, dat2)
reg = polyfit(dat2, dat1, 1);
a = reg(1);
b = reg(2);
controlFit = a.*dat2 + b;
end
function v2=deltaFF(v)
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
m=mean(v);
v2 = v - m;
v2 = v2./m;
end
function v2=slidingDeltaFF(v,slidingWindowSize)
movmean_v = movmean(v,slidingWindowSize);
% meanSig=mean(sig);sig = sig - meanSig;sig = sig./meanSig;
v2 = v - movmean_v;
v2 = v2./movmean_v;
end



