function openArmsCorners=getEpmOpenArmsCorners_201810011604(params)


if ~exist([params.dataRoot filesep params.dataFileTag '-openArmsCorners.mat'])
    
    videoTrackingData = getVideoTrackingData(params);
    bg=getBackGroundQuick(params);
    
    f1=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    f2=figure('name',params.dataFileTag,'Position',[20 20 800 800]);
    
    zoom = 75;
    
    figure(f1)
    ax1=subplot(1,2,1)
    hold on
    axis equal
    axis off
    set(gca,'Ydir','reverse')
    colormap(gray(1024));
    imagesc(bg);
    text(10,10,'mark the four corners (extrimities) of the open arms','color',[0.5 1 0.5])
    ax2=subplot(1,2,2)
    
    for i=1:4
        
        axes(ax1);
        [x(i),y(i)]=ginput(1);
        
        axes(ax2);
        hold on
        axis equal
        axis off
        set(gca,'Ydir','reverse')
        colormap(gray(1024));
        imagesc(bg(y(i)-zoom:y(i)+zoom,x(i)-zoom:x(i)+zoom));
        [x2(i),y2(i)]=ginput(1);
        x(i)=x2(i) + (x(i)-zoom);
        y(i)=y2(i) + (y(i)-zoom);
        
    end
                   
    [x,y]=reorderPointsClockwise(x,y);
    
    figure(f1)
    for i=1:4
        text(x(i),y(i),num2str(i),'color',[0.5 1 0.5]);
    end
    
    openArmsCorners.x=x;
    openArmsCorners.y=y;
    
    save([params.dataRoot filesep params.dataFileTag '-openArmsCorners.mat'],'openArmsCorners');
    close (f1);  close (f2);
else
    
    load([params.dataRoot filesep params.dataFileTag '-openArmsCorners.mat']);
    
end

end




% % %% Params EPM IC-SweetBitter G1
% params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180809_IC-SweetBitter_G1\20180813_EPM';
% % params.dataRoot = 'Z:\Fiber-Photometry\01_DATA\20180827_IC-SweetBitter_G2\20180827_EPM';
% params.mergedOutputsFolder = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM' ;
% params.cameraMode = 'synchronous';
% params.ledDetectionThreshold = 50; %percent of led signal max
% params.videoExtension='avi';
% params.HamamatsuFrameRate_Hz= 20;
% params.behaviorCameraFrameRate_Hz=20;
%
% params.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
%
% params.savePDF = 0;
% params.saveFIG = 0;
% params.savePNG = 0;
%
% params.forceBehavioralStart = 0;
%
% params.OccupancyMap_sigmaGaussFilt=2;
% params.PhotometrySignalMap_sigmaGaussFilt=2;
%
% params.calciumAnalysis_highPath_Detection_Hz = 1;
% params.calciumAnalysis_highPath_Trace_Hz = 0.075;
% params.deltaFF_slidingWindowWidth = 1200;
%
% params.epmOpenArmsEnvergure = 80;
% params.epmClosedArmsEnvergure = 75;
% params.epmArmsWidth = 5;
%
% params.lookingForMouse = '';
%
% params.dataFileTag='F251';




