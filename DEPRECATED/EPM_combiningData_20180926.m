% function EPM_combiningData_20180926()
close all;clear all;clc;tic;t0=toc;

d = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM';
miceList = load('Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\miceList.txt');
fileList=dir([d filesep '*.mat']);nFiles=size(fileList,1);


%Warning, here the reference is the center of the maze
% For the analysis the reference was the arm itself, going IN (inside) our
% OUT the arm, so directionSTR and variable names are inverted
directionSTR={'IN AND OUT','OUT','IN'};
nBins = 80/0.5;

signalHisto_edges = 2:0.1:20;% signalHisto_edges = -10:0.1:10;
signalHisto_binSize = mean(diff(signalHisto_edges))
signalHisto_nBins = size(signalHisto_edges,2)-1;
signalHisto_x = signalHisto_edges(1)+(signalHisto_binSize/2):signalHisto_binSize: signalHisto_edges(end)-(signalHisto_binSize/2);

for iDir = 1:3
        
    photometryActivityMaps=nan(nFiles,nBins,nBins);
    openArms=nan(nFiles,nBins/2);
    closedArms=nan(nFiles,nBins/2);           
    Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);    
    signalHisto = nan(nFiles,signalHisto_nBins);    
    intermediaire=0;anterior=0;
    
    for i=1:nFiles
        
        f = fileList(i).name;
        t2=toc;
        ii = find(miceList(:,1)==str2num(f(2:4)));
        Region(i)=miceList(ii,2);Histo(i)=miceList(ii,3);BatchNum(i)=miceList(ii,4);TaskComment(i)=miceList(ii,5);SignalComment(i)=miceList(ii,6);
                
        if Histo(i)>0.5 && ((TaskComment(i)==1) || (TaskComment(i)==1)) && (BatchNum(i)>1) && (SignalComment(i)==1) 
            if Region(i), intermediaire = intermediaire+1; else, anterior = anterior+1;end
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([d filesep f]);
            signalHisto(i,:) = histcounts(photometryData.mainSig,signalHisto_edges,'Normalization','probability');
            
            switch iDir
                case 1
                    tmp1 = activityMaps.PercAvgSigMap;tmp2 = avgActivityMaps_IO.LZS;
                case 2
                    tmp1 = activityMaps.PercAvgSigMap_I;tmp2 = avgActivityMaps_I.LZS;
                case 3
                    tmp1 = activityMaps.PercAvgSigMap_O;tmp2 = avgActivityMaps_O.LZS;
            end
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            photometryActivityMaps(i,:,:) = tmp1;
            L = tmp2(1);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;
            openArms(i,:) = nansum([openArms(i,:) ; L]);
            L = tmp2(3);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;
            openArms(i,:) = nansum([openArms(i,:) ; L]);
            L = tmp2(2);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;
            closedArms(i,:) = nansum([closedArms(i,:) ; L]);
            L =tmp2(4);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;
            closedArms(i,:) = nansum([closedArms(i,:) ; L]);
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,miceList(ii,2),miceList(ii,3),miceList(ii,4),miceList(ii,5))
        end
        
        
        
    end
    
    AnteriorSweetActivityMaps = squeeze(nanmean(photometryActivityMaps(~logical(Region),:,:)));
    IntermediateBitterActivityMaps = squeeze(nanmean(photometryActivityMaps(logical(Region),:,:)));
    
    ii = isnan(AnteriorSweetActivityMaps);
    AnteriorSweetActivityMaps(ii)=0;
    gaussFilt_AnteriorSweet = imgaussfilt(AnteriorSweetActivityMaps,2);
    gaussFilt_AnteriorSweet(ii)=nan;
    
    ii = isnan(IntermediateBitterActivityMaps);
    IntermediateBitterActivityMaps(ii)=0;
    gaussFilt_intermediateBitter = imgaussfilt(IntermediateBitterActivityMaps,2);
    gaussFilt_intermediateBitter(ii)=nan;
    
    %% AVG SIGNAL MAPS
    figure();
    colormap_ = load('Z:\__Softwares\MatlabScripts_BeyelerLab\COLORMAPS\dawn.txt');
    colormap_ = flipud(colormap_);
    % c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
    % load('Z:\__Softwares\MatlabScripts_BeyelerLab\PHOTOMETRY\myCustomColormap.mat');
    % colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);
    
    colormap(jet(1024))
    %     colormap([[0 0 0] ; colormap_./255]);
    
    subplot(3,2,1)
    hold on
    title([directionSTR{iDir} ' anterior'])
    axis equal;
    axis off
    %     colorbar();
    imagesc(gaussFilt_AnteriorSweet,[-1 1])
    subplot(3,2,2)
    hold on
    title([directionSTR{iDir} ' intermediate'])
    axis equal;
    axis off
    %     colorbar();
    imagesc(gaussFilt_intermediateBitter,[-1 1])
    
    
    
    
    
    
    meanAnteriorSweetOpenArms=nanmean(openArms(~logical(Region),:));
    meanAnteriorSweetClosedArms=nanmean(closedArms(~logical(Region),:));
    meanIntermediateBitterOpenArms=nanmean(openArms(logical(Region),:));
    meanIntermediateBitterClosedArms=nanmean(closedArms(logical(Region),:));
    
    semAnteriorSweetOpenArms=nanstd(openArms(~logical(Region),:))./sqrt(anterior);
    semAnteriorSweetClosedArms=nanstd(closedArms(~logical(Region),:))./sqrt(anterior);
    semIntermediateBitterOpenArms=nanstd(openArms(logical(Region),:))./sqrt(intermediaire);
    semIntermediateBitterClosedArms=nanstd(closedArms(logical(Region),:))./sqrt(intermediaire);
    
    
    
    subplot(3,2,3)
    hold on
    title([directionSTR{iDir} ' Anterior'])
    m=meanAnteriorSweetOpenArms;
    s=semAnteriorSweetOpenArms;
    plot(m,'g');plot(m+s,'g');plot(m-s,'g');
    m=meanAnteriorSweetClosedArms;
    s=semAnteriorSweetClosedArms;
    plot(m,'k');plot(m+s,'k');plot(m-s,'k');
    ylim([-4 8])
    subplot(3,2,4)
    hold on
    title([directionSTR{iDir} ' Intermediate'])
    m=meanIntermediateBitterOpenArms;
    s=semIntermediateBitterOpenArms;
    plot(m,'g');plot(m+s,'g');plot(m-s,'g');
    m=meanIntermediateBitterClosedArms;
    s=semIntermediateBitterClosedArms;
    plot(m,'k');plot(m+s,'k');plot(m-s,'k');
    ylim([-4 8])
    
    
    nBlocks = 3;
    blockSize = floor((nBins/2)/nBlocks);
    openArmsBlocks=nan(nFiles,nBlocks);
    closedArmsBlocks=nan(nFiles,nBlocks);
    
    
%     i1=1;
%     for iBlock=1:nBlocks
%         i2 = i1 + blockSize -1;
%         if i2>nBins/2, i2=nBins/2; end
%         openArmsBlocks(:,iBlock) = nanmean(openArms(:,i1:i2),2);
%         closedArmsBlocks(:,iBlock) = nanmean(closedArms(:,i1:i2),2);
%         i1=i2+1;
%     end 
openArmsBlocks(:,1) = nanmean(openArms(:,3:24),2);
openArmsBlocks(:,2) = nanmean(openArms(:,25:46),2);
openArmsBlocks(:,3) = nanmean(openArms(:,47:68),2);
closedArmsBlocks(:,1) = nanmean(closedArms(:,3:24),2);
closedArmsBlocks(:,2) = nanmean(closedArms(:,25:46),2);
closedArmsBlocks(:,3) = nanmean(closedArms(:,47:68),2);
    
    
    meanAnteriorSweetOpenArms=nanmean(openArmsBlocks(~logical(Region),:));
    meanAnteriorSweetClosedArms=nanmean(closedArmsBlocks(~logical(Region),:));
    meanIntermediateBitterOpenArms=nanmean(openArmsBlocks(logical(Region),:));
    meanIntermediateBitterClosedArms=nanmean(closedArmsBlocks(logical(Region),:));
    
    semAnteriorSweetOpenArms=nanstd(openArmsBlocks(~logical(Region),:))./sqrt(anterior);
    semAnteriorSweetClosedArms=nanstd(closedArmsBlocks(~logical(Region),:))./sqrt(anterior);
    semIntermediateBitterOpenArms=nanstd(openArmsBlocks(logical(Region),:))./sqrt(intermediaire);
    semIntermediateBitterClosedArms=nanstd(closedArmsBlocks(logical(Region),:))./sqrt(intermediaire);
    
    
    subplot(3,2,5)
    hold on
    title([directionSTR{iDir} ' Anterior'])
    m=meanAnteriorSweetOpenArms;
    s=semAnteriorSweetOpenArms;
    plot(m,'g');plot(m+s,'g');plot(m-s,'g');
    m=meanAnteriorSweetClosedArms;
    s=semAnteriorSweetClosedArms;
    plot(m,'k');plot(m+s,'k');plot(m-s,'k');
    ylim([-4 8])
    subplot(3,2,6)
    hold on
    title([directionSTR{iDir} ' Intermediate'])
    m=meanIntermediateBitterOpenArms;
    s=semIntermediateBitterOpenArms;
    plot(m,'g');plot(m+s,'g');plot(m-s,'g');
    m=meanIntermediateBitterClosedArms;
    s=semIntermediateBitterClosedArms;
    plot(m,'k');plot(m+s,'k');plot(m-s,'k');
    ylim([-4 8])
    
end

figure();
hold on
imagesc(signalHisto_x,1:nFiles,signalHisto)
for i=1:nFiles   
    f = fileList(i).name;
     [~, f, ~] = fileparts(f);
    text(signalHisto_x(end)/2,i,f,'color',[1 1 1])
end