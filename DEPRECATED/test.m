a = rand(100,1);

for i=1:100
    if a(i)<0.1
        a(i)=nan;
    end
end


b=a;

ii=isnan(a);b(ii)=[];

c=[];

iSource=1;
vSource=b;
vDestination = nan(100,1);
for i=1:100
    if ii(i)
        vDestination(i)=nan;
    else
        vDestination(i)=vSource(iSource);
        iSource=iSource+1;
    end
end

figure()
hold on
plot(a,'+-')
plot(b-1,'+-')
plot(vDestination-2,'+-')
