function [ZS, LZS, minLZS, maxLZS] = epm_alignSignalToArms(params,PercAvgSigMap,zones,outputPrefix)

ZS=[];LZS=[];     %ZS stands for Zoned Signal, LZS for linearized Zoned Signal
for iZone =1:5
    x1=zones(iZone).position(1);
    y1=zones(iZone).position(2);
    x2 = x1 + zones(iZone).position(3);
    y2 = y1 + zones(iZone).position(4);
    % LSZ : Linear-Signal Zoned
    switch zones(iZone).positionSTR
        case 'NORTH'
            ZS{iZone} = fliplr(PercAvgSigMap(y1:y2,x1:x2)');
            LZS{iZone} = nanmean([ZS{iZone}(:,:)]);
        case 'EAST'
            ZS{iZone} = PercAvgSigMap(y1:y2,x1:x2);
            LZS{iZone}= nanmean([ZS{iZone}(:,:)]);
        case 'SOUTH'
           ZS{iZone} = PercAvgSigMap(y1:y2,x1:x2)';
           LZS{iZone} = nanmean([ZS{iZone}(:,:)]);
        case 'WEST'
            ZS{iZone} = fliplr(PercAvgSigMap(y1:y2,x1:x2));
           LZS{iZone} = nanmean([ZS{iZone}(:,:)]);
        case 'CENTER'
           LZS{iZone} = nanmedian(nanmedian(PercAvgSigMap(y1:y2,x1:x2)));
    end
end

minLSZ=[];maxLSZ=[];

for iZone=1:5
    minLZS(iZone) = nanmin(LZS{iZone});
    maxLZS(iZone) = nanmax(LZS{iZone});
end
  minLZS = nanmin(minLZS);
 maxLZS = nanmax(maxLZS);


end