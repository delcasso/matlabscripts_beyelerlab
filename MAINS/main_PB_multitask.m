% initialization, close all figures, delete all variables, clear the command window
close all;clear;clc

logFilePath = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\MULTITASK\log.xlsx';


if ~exist(logFilePath,'file')
    tic;t0=toc;
    processDisconnection = 1;
    outputFolder = 'Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\MULTITASK';
    taskList = {'ICa-ICp_EPM','ICa-ICp_OFT','ICa-ICp_NSFT','ICa-ICp_SI','ICa-ICp_OFT-CONTROL','ICa-ICp_NSFT-CONTROL','ICa-ICp_SUCROSE'};
    nTasks = size(taskList,2);
    log = readtable('Z:\PhotometryAndBehavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\MULTITASK\Journal_Template.xlsx');
    for iTask = 1 : nTasks
        log=oneTask(taskList{iTask},processDisconnection,outputFolder,log);
    end
else
    T=readtable(logFilePath);
end

cNames = T.Properties.VariableNames;
[nR,nC]=size(T);
selectedCol=[];
for iC = 1:nC
    if strfind(cNames{iC},'avgTrFreq')
        selectedCol = [selectedCol iC]
    end
end

figure()
hold on
nCol = size(selectedCol,2);
for iMouse=1:nR   
    y=[];
    for iCol = 1:nCol
        tmp = T(iMouse,selectedCol(iCol));
        tmp = table2array(tmp);
        if ~tmp
            tmp = NaN;
        end
        y = [y tmp];
    end
    plot(y);
end












function log=oneTask(batchID,processDisconnection,outputFolder,log)

close all
p=[];allExp=[];
machine = 'SD_remote';
[dataRoot,~,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);

p.lookingForMouse = '';
p.processDisconnection = processDisconnection;

%%CREATES OUTPUT FOLDERS
% if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end

nFolders = size(dataRoot,2);
%% TO PROCESS ALL FOLDERS
for iFolder=1:nFolders
    
    p.dataRoot = dataRoot{iFolder};
    if ~exist(p.dataRoot,'dir'), fprintf('%s doesn''t exist',p.dataRoot);pause;end
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    nApparatus = size(apparatus,2);
    if nFolders < nApparatus
        % is there a model of the apparatus for normalizatoin
        if strcmp(apparatus{nFolders+1}.Model,'ForNormalization'),p.apparatusModelForNormalization = apparatus{nFolders+1};end
    end
    
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        
        % If you want to process only one file
        if ~isempty(p.lookingForMouse),processThisFile=0;if strcmp(p.dataFileTag,p.lookingForMouse),processThisFile=1;end;end
        
        % Depending of the histologfy status we skeep the analysis for the current file
        status = getHistologyStatus(p.journal,p.dataFileTag);
        if isempty(status) | status<1
            processThisFile=0;
        end
        
        %% MAIN PROCESSING
        
        %% MAIN PROCESSING
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        tProcessingStart=toc;
        fprintf('[%.1f sec.]\t%s\tProcessing File %d/%d [%s] ...',tProcessingStart,batchID,iFile,nFiles,p.dataFileTag);
        
        experiment = loadExpData_PB(p);
        experiment = processCaSignal(experiment);
        experiment = standardizePositionData(experiment);
        experiment=buildBasicMaps_PB(experiment);
        %         drawBasicMaps_PB(experiment);
        experiment = getApparatusZones(experiment);
        experiment = getZonesStatistics2_PB(experiment);
        
        %     experiment  = resultsWriteTransientsFrequency(experiment);
        
        %         experiment = generateEventList(experiment);
        %         experiment = eventBasedAnalysis_PB(experiment);
        
        %% SAVING DATA
        
        mouseFullName = p.dataFileTag;
        mouseNum = str2num(mouseFullName(2:end))
        iRow = find((log.MouseNum==mouseNum)==1)
        
        nTransitens = size(experiment.pData.transients,2);
        duration_sec = (experiment.pData.nFrames / experiment.p.HamamatsuFrameRate_Hz);
        avgTrFreq = nTransitens / duration_sec;
        
        sep = findstr('_',batchID)
        task = batchID(sep(1)+1:end)
        task = strrep(task,'-','')
        task = strrep(task,'_','')
        cmd = sprintf('log.%s_avgTrFreq(iRow)=%2.2f',task,avgTrFreq);
        eval(cmd);
        
        
        %% THIS PART IS SPECIFIC TO main_PB_MULTITASK
        switch batchID
            case 'ICa-ICp_EPM'
                timeInZone1_fr = experiment.vData.zoneTime_fr(1) + experiment.vData.zoneTime_fr(3);
                timeInZone2_fr = experiment.vData.zoneTime_fr(2) + experiment.vData.zoneTime_fr(4);
            otherwise
                timeInZone1_fr= experiment.vData.zoneTime_fr(1);
                timeInZone2_fr = experiment.vData.zoneTime_fr(2);
        end
        
        cmd = sprintf('log.%s_timeInZone1(iRow)=%2.2f',task,timeInZone1_fr);
        eval(cmd);
        cmd = sprintf('log.%s_timeInZone2(iRow)=%2.2f',task,timeInZone2_fr);
        eval(cmd);
        
        %         save([p.outputFolder filesep p.dataFileTag '_' batchID  '.mat'],'experiment');
        
        p = experiment.p;
        
        tProcessingStop=toc;
        fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
        
    end
    
end
writetable(log,logFilePath);
end




