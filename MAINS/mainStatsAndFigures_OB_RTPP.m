batchID = 'ICa-ICp_RTPP';

nMice = size(infoDB,2);
mouse = [];sex = {};region = {};batch = [];opsin = {};histo = [];
results = [];

for iMouse=1:nMice
    results(iMouse).mouse = infoDB{1,iMouse}.MouseNum;
    results(iMouse).sex = infoDB{1,iMouse}.Sex;
    results(iMouse).region = infoDB{1,iMouse}.RegionCode;
    results(iMouse).opsin =  infoDB{1,iMouse}.Opsin;
    results(iMouse).histo =  infoDB{1,iMouse}.HistologyCode;
    results(iMouse).batch = infoDB{1,iMouse}.BatchCode;
end

TimeInZone = nan(nMice,2);

%mice
for iMouse=1:nMice
    %zones (1 = stim, 2 = no stim)
    TimeInZone(iMouse,1) = expDB{1,iMouse}.TimeInNoStimZone_sec
    TimeInZone(iMouse,2) = expDB{1,iMouse}.TimeInStimZone_sec
end





sum_TimeInZone = sum(TimeInZone,2);
sum_TimeInZone = repmat(sum_TimeInZone,1,2);
TimeInZone_perc = (TimeInZone./sum_TimeInZone)*100;

results = struct2table(results);

results.mouse = categorical(results.mouse);
results.sex = categorical(results.sex);
results.region = categorical(results.region);
results.opsin = categorical(results.opsin);
results.batch = categorical(results.batch);

nRegions = 2;
nOpsins = 2;

regions = nan(nMice,1);
ii_ICa = results.region == 'ICa - BLA';
ii_ICp = results.region == 'ICp - CeM';
regions(ii_ICa)=0;regions(ii_ICp)=1;
regionsStr{1} = {'ICa-BLA'};
regionsStr{2} = {'ICp-CeM'};

opsins = nan(nMice,1);
ii_eYFP = results.opsin == 'eYFP';
ii_ChR2 = results.opsin == 'ChR2';
opsins(ii_eYFP)=0;opsins(ii_ChR2)=1;
opsinsStr{1} = {'eYFP'};
opsinsStr{2} = {'ChR2'};

ZoneStr{1} = {'No Stim'};
ZoneStr{2} = {'Stim'};


ii_ICa_eYFP = ii_ICa & ii_eYFP;
ii_ICp_eYFP = ii_ICp & ii_eYFP;
ii_ICa_ChR2 = ii_ICa & ii_ChR2;
ii_ICp_ChR2 = ii_ICp & ii_ChR2;

FaceGroupColors(1,1:3) = [101 65 153]./255;
EdgesGroupColors(1,1:3) = [101 65 153]./255;
LineGroupColors(1,1:3) = [0 0 0];
LineGroupStlye{1} = ':';

FaceGroupColors(2,1:3) = [0 165 77]./255;
EdgesGroupColors(2,1:3) = [0 165 77]./255;
LineGroupColors(2,1:3) = [0 0 0];
LineGroupStlye{2} = ':';


batch_colors = nan(5,3);
batch_colors(1,:)=[0 135 68];
batch_colors(2,:)=[0 87 231];
batch_colors(3,:)=[214 45 32];
batch_colors(4,:)=[255 167 0];
batch_colors(5,:)=[255 255 255];
batch_colors = batch_colors ./255;


figure()

rPlot = ceil(sqrt(nMice));
for iMouse=1:nMice
    subplot(rPlot,rPlot,iMouse)
    hold on
    m=results.mouse(iMouse);
    s=results.sex(iMouse);
    r=results.region(iMouse);
    o=results.opsin(iMouse);
    b=results.batch(iMouse);
    expID = sprintf('%s%s-%s-%s',s,m,r,o);
    title(expID)
    
    
    %% To remove 10 first min.
    fr = expDB{1,iMouse}.vData.videoInfo.FrameRate;
    idx1 = 10 * 60 * fr;
    sessionDuration = 20 * 60 * fr;
    nSamples = size(expDB{1,iMouse}.vData.mainX_cmSP,1);
    if nSamples > sessionDuration
        expDB{1,iMouse}.vData.mainX_cmSP = expDB{1,iMouse}.vData.mainX_cmSP(1:nSamples);
        expDB{1,iMouse}.vData.mainY_cmSP = expDB{1,iMouse}.vData.mainY_cmSP(1:nSamples);
    end
    expDB{1,iMouse}.vData.mainX_cmSP = expDB{1,iMouse}.vData.mainX_cmSP(idx1:end);
    expDB{1,iMouse}.vData.mainY_cmSP = expDB{1,iMouse}.vData.mainY_cmSP(idx1:end);
    % End to remove 10 first min.

    
    x =expDB{1,iMouse}.vData.mainX_cmSP;
    y =expDB{1,iMouse}.vData.mainY_cmSP;
    % x =expDB{1,iMouse}.vData.mouseX;
    % y =expDB{1,iMouse}.vData.mouseY;
    b = results.batch(iMouse);
    plot(x,y,'color',batch_colors(b,:));
    L_str = expDB{1,iMouse}.vData.zones_cmSP(1).stim;
    R_str = expDB{1,iMouse}.vData.zones_cmSP(2).stim;
    if strcmp(L_str,'no stim zone')
        L_val = TimeInZone_perc(iMouse,1);
        R_val = TimeInZone_perc(iMouse,2);
    else
        L_val = TimeInZone_perc(iMouse,2);
        R_val = TimeInZone_perc(iMouse,1);
    end
    t=text(15,60,sprintf('%s-%2.2f%%',L_str,L_val));
    t.FontSize=6;
    t=text(45,60,sprintf('%s-%2.2f%%',R_str,R_val));
    t.FontSize=6;
    
end


figure()
hold on

Ymax = max(max(100));Ymax1 = Ymax;Ymax2 = Ymax + (Ymax/4)
iX = 0;
ylim([0 Ymax2])

for iRegion=1:nRegions
    iX = iX +4;
    for iOpsin = 1:nOpsins
        iX = iX +2;
        tmp = [];
        for iZone=1:2
            iX = iX +1;
            ii1 = find(regions==(iRegion-1));ii2 = find(opsins==(iOpsin-1));ii3 = intersect(ii1,ii2);
            m_ = mean(TimeInZone_perc(ii3,iZone));sem_ = std(TimeInZone_perc(ii3,iZone))/sqrt(size(ii3,1));
            tmp = [tmp TimeInZone_perc(ii3,iZone)];
            batch_id = results.batch(ii3);
            if m_>0
                rectangle('Position',[iX 0 0.5 m_],'EdgeColor',EdgesGroupColors(iRegion,:),'FaceColor',FaceGroupColors(iRegion,:));
                plot([iX+0.25 iX+0.25],[m_ m_+sem_],'color',EdgesGroupColors(iRegion,:),'Linewidth',2);
            else
                rectangle('Position',[iX m_ 0.5 -m_],'EdgeColor',EdgesGroupColors(iRegion,:),'FaceColor',FaceGroupColors(iRegion,:));
                plot([iX+0.25 iX+0.25],[m_ m_+sem_],'color',EdgesGroupColors(iRegion,:),'Linewidth',2);
            end
            text(iX+0.25 ,Ymax1,sprintf('%s-%s-%s',regionsStr{iRegion}{:},opsinsStr{iOpsin}{:},ZoneStr{iZone}{:}),'Rotation',90,'VerticalAlignment','top')
        end
        [h,p,ci,stats] = ttest(tmp(:,1),tmp(:,2));
        text(iX-0.25,Ymax1,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'Rotation',90,'VerticalAlignment','top')
        n = size(tmp,1);
        for i=1:n
            plot([((iX-1)+0.25) (iX+0.25)],[tmp(i,1) tmp(i,2)],'color',batch_colors(batch_id(i),:))
        end
        for i=1:4
            rectangle('Position',[15 (50+10*i) 1 10],'Facecolor',batch_colors(i,:),'EdgeColor','none')
            text(16.1,(50+10*i)+5,sprintf('batch %d',i));
        end
    end
end

