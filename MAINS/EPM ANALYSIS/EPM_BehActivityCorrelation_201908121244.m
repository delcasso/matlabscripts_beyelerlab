
% save('timeInZone_backup.mat','Mice','Region','zoneStats','ZonesStr','zones');

% zoneStats=[meanOA meanCA];
% save('global_backup.mat','Mice','Region','zoneStats','ZonesStr','Maps');

% zoneStats = meanTransientsAmp;
% save('transientsAmp_backup.mat','Mice','Region','zoneStats','ZonesStr');

% zoneStats = meanTransientsProba;
% save('transientsFreq_backup.mat','Mice','Region','zoneStats','ZonesStr','Maps');

clear
load('timeInZone_backup.mat');
timeCA = zoneStats(:,1);
timeOA = zoneStats(:,2);
Mice = Mice';
masterTable = table(Mice,Region,timeOA,timeCA);
ii = find(isnan([masterTable.Mice{:}])==1)
masterTable(ii,:)=[];

nMice = size(masterTable,1);
globalOA=nan(nMice,1);
globalCA=nan(nMice,1);
globalMapAvg=nan(nMice,1);
load('global_backup.mat');
%% Check Mice Uniformity
nMice = size(Mice,2)
for iMouse=1:nMice
    if ~isnan(Mice{iMouse})
        fprintf('(%d/%d) Looking for %d ...',iMouse,nMice,Mice{iMouse});
        ii=find([masterTable.Mice{:}] == Mice{iMouse});
        fprintf('masterTable line %d, ',ii);
        fprintf('mouse = %d\n',masterTable.Mice{ii});
        globalOA(ii) = zoneStats(iMouse,1);
        globalCA(ii) = zoneStats(iMouse, 2);
        map_tmp = squeeze(Maps(ii,:,:));
        globalMapAvg(ii) = nanmean(nanmean(map_tmp));
    end
end
masterTable.globalOA = globalOA;
masterTable.globalCA= globalCA;
masterTable.globalMapAvg = globalMapAvg;



nMice = size(masterTable,1);
transAmpOA=nan(nMice,1);
transAmpCA=nan(nMice,1);
load('transientsAmp_backup.mat');
%% Check Mice Uniformity
nMice = size(Mice,2)
for iMouse=1:nMice
    if ~isnan(Mice{iMouse})
        fprintf('(%d/%d) Looking for %d ...',iMouse,nMice,Mice{iMouse});
        ii=find([masterTable.Mice{:}] == Mice{iMouse});
        fprintf('masterTable line %d, ',ii);
        fprintf('mouse = %d\n',masterTable.Mice{ii});
        transAmpOA(ii) = zoneStats(iMouse,1);
        transAmpCA(ii) = zoneStats(iMouse, 2);
    end
end
masterTable.transAmpOA = transAmpOA;
masterTable.transAmpCA= transAmpCA;


nMice = size(masterTable,1);
transFreqOA=nan(nMice,1);
transFreqCA=nan(nMice,1);
transFreqMapAvg=nan(nMice,1);
load('transientsFreq_backup.mat');
%% Check Mice Uniformity
nMice = size(Mice,2)
for iMouse=1:nMice
    if ~isnan(Mice{iMouse})
        fprintf('(%d/%d) Looking for %d ...',iMouse,nMice,Mice{iMouse});
        ii=find([masterTable.Mice{:}] == Mice{iMouse});
        fprintf('masterTable line %d, ',ii);
        fprintf('mouse = %d\n',masterTable.Mice{ii});
        transFreqOA(ii) = zoneStats(iMouse,1);
        transFreqCA(ii) = zoneStats(iMouse, 2);
        map_tmp = squeeze(Maps(ii,:,:));
        transFreqMapAvg(ii) = nanmean(nanmean(map_tmp));        
    end
end
masterTable.transFreqOA = transFreqOA;
masterTable.transFreqCA= transFreqCA;
masterTable.transFreqMapAvg = transFreqMapAvg;


writetable(masterTable,'correlation_database.txt') 
writetable(masterTable,'correlation_database.xlsx') 

ICa = masterTable.Region == 0;
ICp = masterTable.Region == 1;

masterTable.percTimeInOA = (masterTable.timeOA ./ (masterTable.timeOA + masterTable.timeCA))*100.0;

%------------------------------------------------------------------------------------------------------------
% tbl = table(masterTable.transFreqMapAvg(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')

tbl = table(masterTable.globalMapAvg(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
tbl = table(masterTable.globalMapAvg(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.globalOA(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.globalOA(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.globalCA(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.globalCA(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
% 
tbl = table(mean([masterTable.globalOA(ICa) masterTable.globalCA(ICa)],2),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
 tbl = table(mean([masterTable.globalOA(ICp) masterTable.globalCA(ICp)],2),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
% %------------------------------------------------------------------------------------------------------------
% tbl = table(masterTable.transAmpOA(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.transAmpOA(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.transAmpCA(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.transAmpCA(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
%------------------------------------------------------------------------------------------------------------
% tbl = table(masterTable.transFreqOA(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.transFreqOA(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.transFreqCA(ICa),masterTable.percTimeInOA(ICa));lm = fitlm(tbl,'linear')
% tbl = table(masterTable.transFreqCA(ICp),masterTable.percTimeInOA(ICp));lm = fitlm(tbl,'linear')








