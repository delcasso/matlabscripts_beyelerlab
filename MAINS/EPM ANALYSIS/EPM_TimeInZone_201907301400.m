close all;clearvars;clc;tic;t0=toc;

machine = 'SD_remote';
batchID = 'ICa-ICp_EPM';
p=[];
outputFolder=[];

[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);


Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

OA =  nan(nFiles,1);CA =  nan(nFiles,1);Ratio =  nan(nFiles,1);Z =  nan(nFiles,5);

curRegionNum = 0;

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    
    Mice{i}=str2double(f(2:4));
    
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);regionSTR = 'intermediate';
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);regionSTR = 'anterior';
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;vData = experiment.vData;pData = experiment.pData;
            
            zones = vData.zones_cmSP;
            nFrames(i) = sum(experiment.vData.zoneTime_fr);
            
            z = experiment.vData.zoneTime_fr; Z(i,:) = z;
            OA(i) = experiment.vData.zoneTime_fr(1) + experiment.vData.zoneTime_fr(3);
            CA(i) = experiment.vData.zoneTime_fr(2) + experiment.vData.zoneTime_fr(4);
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
            
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end


disp('here')

zoneStats = [CA OA];
Groups = Region+1;
ZonesStr = {'Closed' , 'Open'};
titleStr = 'Time In Zone';

plotZoneStatsTmp(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr);


dataSum = nansum(zoneStats,2);
dataSum = repmat(dataSum,1,2);
dataPerc = (zoneStats ./ dataSum) * 100;

plotZoneStatsTmp(dataPerc,Groups,Mice,ZonesStr,analysisFolder,titleStr);

function journal=plotZoneStatsTmp(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr)

nZones = size(zoneStats,2);
% 
% for iZone=1:nZones
%     cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
%     eval(cmd);
% end

uGroups= unique(Groups);
uGroups(isnan(uGroups))=[];
nGroups = size(uGroups,1);
 
FaceGroupColors(1,1:3) = [101 65 153]./255;
EdgesGroupColors(1,1:3) = [101 65 153]./255;
LineGroupColors(1,1:3) = [0 0 0];
LineGroupStyle{1} = ':';

FaceGroupColors(2,1:3) = [0 165 77]./255;
EdgesGroupColors(2,1:3) = [0 165 77]./255;
LineGroupColors(2,1:3) = [0 0 0];
LineGroupStyle{2} = ':';

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure()
hold on
title(titleStr)
set(gca,'xtick',[])

x=0;

for iGroup=1:nGroups    
    
     x=x+iGroup;
     
    for iZone=1:nZones                
        
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
       
    end
    
     [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
      text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
      text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
     
        for iZone=2:nZones
            plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',LineGroupColors(iGroup,:),'LineStyle',':');
        end   
        
end

print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[analysisFolder filesep titleStr '.fig'])

end
