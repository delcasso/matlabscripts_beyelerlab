%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
batchID = 'ICa-ICp_EPM';
p=[];

[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

stat = table();
fod = fopen([analysisFolder filesep 'TimeInZone_20190305.xls'],'w');

Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

OA =  nan(nFiles,1);CA =  nan(nFiles,1);Ratio =  nan(nFiles,1);Z =  nan(nFiles,5);

curRegionNum = 0;

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    
    Mice{i}=str2double(f(2:4));
    
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);regionSTR = 'intermediate';
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);regionSTR = 'anterior';
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            
            zones = vData.zones_cmSP;
            nFrames(i) = sum(experiment.vData.zoneTime_fr);
            
            z = experiment.vData.zoneTime_fr;
            Z(i,:) = z;
            OA(i) = experiment.vData.zoneTime_fr(1) + experiment.vData.zoneTime_fr(3);
            CA(i) = experiment.vData.zoneTime_fr(2) + experiment.vData.zoneTime_fr(4);
            Ratio(i) = (OA(i)-CA(i))/(OA(i)+CA(i));
            
            fprintf(fod,'%d\t%s\t%d\t%2.2f\t%2.2f\t%2.2f\t%2.2f\t%2.2f\n',Mice{i},regionSTR,Region(i),z(1),z(2),z(3),z(4),z(5));
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
            
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end

stat.Mice = Mice';
stat.Region = Region;
stat.Histo = Histo;
stat.BatchNum= BatchNum;
stat.TaskComment = TaskComment;
stat.SignalComment=SignalComment;
stat.OA = OA;
stat.CA = CA;
stat.Ratio = Ratio;
stat.Z1 = Z(:,1);
stat.Z2 = Z(:,2);
stat.Z3 = Z(:,3);
stat.Z4 = Z(:,4);
stat.Z5 = Z(:,5);

writetable(stat,[analysisFolder filesep 'stats_20190103.xls']);

fclose(fod);
