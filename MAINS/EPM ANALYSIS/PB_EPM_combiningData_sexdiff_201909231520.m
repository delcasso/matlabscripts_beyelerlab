%% INIT
close all;clearvars;clc;tic;t0=toc;
batchID = 'ICa-ICp_EPM';

%% DATA
machine = 'SD_remote';
p=[];
outputFolder = [];
[dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);

journal = readtable([outputFolder filesep 'Journal.xlsx']);
load([outputFolder filesep 'analysisParams.mat']);
params = p;
fileList=dir([outputFolder filesep '*.mat']);nFiles=size(fileList,1);
stat = table();

%% PARAMS
% analysisMode='ContinuousCaSignal';
analysisMode='Transients';

if params.apparatusNormalizationRequested
    fprintf('apparatus normalization requested\n');
    nApp = size(params.apparatus,2);
    normApparatus = params.apparatus{1,nApp};
    nBins = normApparatus.OA_cm / params.MapScale_cmPerBin;
else
    warning('nBins has been defined arbitrarly');
    nBins = 80/0.5;
end

cutoff_framenumber = 20*60*10;


transientsProba=nan(nFiles,5);
transientsAmp=nan(nFiles,5);

Maps=nan(nFiles,nBins,nBins);OA=nan(nFiles,nBins/2);CA=nan(nFiles,nBins/2);
OA_times =  nan(nFiles,1);CA_times =  nan(nFiles,1);nFrames =  nan(nFiles,1);
Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};Sex = nan(nFiles,1);
TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
females=0;males=0;maleMouseList={};femaleMouseList={};
rawGlobalSignalAverage = nan(nFiles,1);rawTransientAmplitude = nan(nFiles,1);rawTransientFrequency = nan(nFiles,1);
rawGlobalSignalAvg_timeRestrain = nan(nFiles,1);
nSamples = nan(nFiles,1);

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;ii = find(journal.MouseNum==str2double(f(2:4)));Mice{i}=str2double(f(2:4));
    
    if ~isempty(ii)
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        if strcmp(journal.Sex(ii),'M'), Sex(i)=0; end
        if strcmp(journal.Sex(ii),'F'), Sex(i)=1; end
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            if Sex(i)
                females = females+1; femaleMouseList{females}=Mice{i};fprintf('females = %d ',females);
            else
                males = males+1;maleMouseList{males}=Mice{i};fprintf('males = %d ',males);
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([outputFolder filesep f]);
            
            %% To measure Calcium Signals
            p = experiment.p; vData = experiment.vData; pData = experiment.pData;
            map = experiment.map;
            armAlignedSig=experiment.armAlignedSig;
            armAlignedTransients=experiment.armAlignedTransients;
            
            %% To measure the time spend in each zone
            zones = vData.zones_cmSP;
            nFrames(i) = sum(experiment.vData.zoneTime_fr);
            z = experiment.vData.zoneTime_fr; Z(i,:) = z;
            OA_times(i) = experiment.vData.zoneTime_fr(1) + experiment.vData.zoneTime_fr(3);
            CA_times(i) = experiment.vData.zoneTime_fr(2) + experiment.vData.zoneTime_fr(4);
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    cLim_=[-2 2];
                    tmp1 = map.NormSig.IO; tmp2 = armAlignedSig.IO.oneDim;
                    rawGlobalSignalAverage(i) = nanmean(experiment.pData.mainSig);
                    if experiment.pData.nFrames > cutoff_framenumber
                        rawGlobalSignalAvg_timeRestrain(i) = nanmean(experiment.pData.mainSig(1:cutoff_framenumber));
                    end
                case 'Transients'
                    cLim_=[0 0.2];
                    transientsProba(i,:)=pData.transientStats.zonesProba;
                    transientsAmp(i,:)=pData.transientStats.zonesMeanAmp;
                    tmp1 = map.NormTransients.IO; tmp2 = armAlignedTransients.IO.oneDim;
                    rawTransientAmplitude(i) = nanmean([experiment.pData.transients.vMax]);
                    rawTransientFrequency(i) = (size(experiment.pData.transients,2)/experiment.pData.nFrames)*1200;
            end
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            Maps(i,:,:) = tmp1;
            L = tmp2(1);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;OA(i,:) = nansum([OA(i,:) ; L]);
            L = tmp2(3);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;OA(i,:) = nansum([OA(i,:) ; L]);
            L = tmp2(2);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;CA(i,:) = nansum([CA(i,:) ; L]);
            L =tmp2(4);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;CA(i,:) = nansum([CA(i,:) ; L]);
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    else
        fprintf('!!! %s not in journal\n',f)
    end
end

stat.Mice = Mice';stat.Sex = Sex;stat.Region = Region;stat.Histo = Histo;stat.BatchNum= BatchNum;stat.TaskComment = TaskComment;stat.SignalComment=SignalComment;


iiMale = find(Sex==0);SweetMaps = squeeze(nanmean(Maps(iiMale,:,:)));
iiFemale   = find(Sex==1);BitterMaps = squeeze(nanmean(Maps(iiFemale,:,:)));

sigma = 0.5;
width = 2*ceil(2*sigma)+1;
imageFilter=fspecial('gaussian',width,sigma);
SweetMaps = imfilter(SweetMaps,imageFilter,'symmetric','conv');
BitterMaps = imfilter(BitterMaps,imageFilter,'symmetric','conv');


%% AVG SIGNAL MAPS
figure();
colormap([[1 1 1];jet(1024)])

subplot(1,2,1)
hold on;title(['males (n=' num2str(males) ')']);axis equal;axis off;colorbar();
x = experiment.apparatusDesign_cm.x;y = experiment.apparatusDesign_cm.y;
x = x ./ experiment.p.MapScale_cmPerBin;y = y ./ experiment.p.MapScale_cmPerBin;
x = x - min(x);y = y - min(y);
imagesc(SweetMaps,cLim_);
plot(x,y,'k')

subplot(1,2,2)
hold on;title(['females (n=' num2str(females) ')']);axis equal;axis off;colorbar();
imagesc(BitterMaps,cLim_)
plot(x,y,'k')

OA(OA==0)=nan; CA(CA==0)=nan;


%% All animals visited the open arm until 49 cm
iMax = 49;
meanOA=nanmean(OA(:,1:iMax),2);
meanCA=nanmean(CA(:,1:iMax),2);

GroupsIDs = Sex+1;
ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};

zoneStats = [CA_times OA_times];
Groups = Sex+1;
titleStr = 'TimeInZoneFrames';
journal=plotZoneStats(zoneStats,GroupsIDs,Mice,ZonesStr,outputFolder,titleStr,journal,'timeSpendFrameNumber');
timeInZoneSum = nansum(zoneStats,2);
timeInZoneSum = repmat(timeInZoneSum,1,2);
timeInZonePerc = (zoneStats ./ timeInZoneSum) * 100;
% timeInZonePerc = (zoneStats ./ nFrames) * 100;
titleStr = 'TimeInZonePercentage';
journal=plotZoneStats(timeInZonePerc,GroupsIDs,Mice,ZonesStr,outputFolder,titleStr,journal,'timeSpendPercentage');


switch analysisMode
    
    case 'ContinuousCaSignal'
        journal=plotZoneStats([meanOA meanCA],GroupsIDs,Mice,ZonesStr,outputFolder,'ContinuousCaSignal-zoneComparaison',journal,'bulk');
        
        meanGlobal = mean([meanOA meanCA],2);
        %         tmp = [Region meanGlobal timeInZonePerc(:,1)];
        %         tmp = [Region rawGlobalSignalAverage timeInZonePerc(:,1)];
        
        %         x= meanOA./meanCA;
        %         y = OA_times./CA_times;
        
        %         x = mean([meanOA meanCA],2);
        %         y = timeInZonePerc(:,1);
        %         x = meanOA - meanCA;
        %         y = timeInZonePerc(:,1)-timeInZonePerc(:,2);
        
        x=meanGlobal;
        %         x = rawGlobalSignalAverageFirstTenSec
        y = timeInZonePerc(:,1);
                
        tmp = [Sex x y];
        tmp(isnan(sum(tmp,2)),:)=[];
        
        figure();
        ii = find(tmp(:,1)==0);
        lm = fitlm(table(tmp(ii,2),tmp(ii,3)),'linear');
        subplot(1,2,1)
        plot(lm)
        %         xlim([60 100])
        %         ylim([-1.5 1.5])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1))
        text(nanmean(x),nanmean(y),msg)
        
        ii = find(tmp(:,1)==1);
        lm = fitlm(table(tmp(ii,2),tmp(ii,3)),'linear');
        subplot(1,2,2)
        plot(lm)
        %         xlim([60 100])
        %         ylim([-1.5 1.5])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1))
        text(nanmean(x),nanmean(y),msg)
        
    case 'Transients'
        
        meanTransientsProba = nan(size(transientsProba,1),2);
        meanTransientsAmp = nan(size(transientsAmp,1),2);
        meanTransientsProba(:,1) = nanmean(transientsProba(:,[1 3]),2);
        meanTransientsProba(:,2) = nanmean(transientsProba(:,[2 4]),2);
        meanTransientsAmp(:,1) = nanmean(transientsAmp(:,[1 3]),2);
        meanTransientsAmp(:,2) = nanmean(transientsAmp(:,[2 4]),2);
        journal=plotZoneStats(meanTransientsProba,GroupsIDs,Mice,ZonesStr,outputFolder,'TransientsFrequency-zoneComparaison',journal,'transientFrequency');
        journal=plotZoneStats(meanTransientsAmp,GroupsIDs,Mice,ZonesStr,outputFolder,'TransientsAmplitude-zoneComparaison',journal,'transientAmp');
        
        %         tmp = [Region mean(transientsProba,2)*1200 timeInZonePerc(:,1)];
        %         tmp = [Region rawTransientFrequency timeInZonePerc(:,1)];
        
        x= meanTransientsProba(:,1)./meanTransientsProba(:,2);
        y = OA_times./CA_times;
        %         x = mean([meanOA meanCA],2);
        %         y = timeInZonePerc(:,1);
        tmp = [Sex x y];
        tmp(isnan(sum(tmp,2)),:)=[];
        
        figure();
        ii = find(tmp(:,1)==0);
        lm = fitlm(table(tmp(ii,2),tmp(ii,3)),'linear');
        subplot(2,2,1)
        plot(lm)
        anova_lm = anova(lm)
        pValue = lm.Coefficients.pValue(2)
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(nanmean(x),nanmean(y),msg)
        
        ii = find(tmp(:,1)==1);
        lm = fitlm(table(tmp(ii,2),tmp(ii,3)),'linear');
        subplot(2,2,2)
        plot(lm)
        anova_lm = anova(lm)
        pValue = lm.Coefficients.pValue(2)
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(nanmean(x),nanmean(y),msg)
        
        %         tmp = [Region mean(meanTransientsAmp,2) timeInZonePerc(:,1)];
        %         tmp = [Region rawTransientAmplitude timeInZonePerc(:,1)];
        
        x= meanTransientsAmp(:,1)./meanTransientsAmp(:,2);
        y = OA_times./CA_times;
        %         x = mean([meanOA meanCA],2);
        %         y = timeInZonePerc(:,1);
        tmp = [Sex x y];
        tmp(isnan(sum(tmp,2)),:)=[];
        
        ii = find(tmp(:,1)==0);
        lm = fitlm(table(tmp(ii,2),tmp(ii,3)),'linear');
        subplot(2,2,3)
        plot(lm)
        anova_lm = anova(lm)
        pValue = lm.Coefficients.pValue(2)
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(nanmean(x),nanmean(y),msg)
        
        ii = find(tmp(:,1)==1);
        lm = fitlm(table(tmp(ii,2),tmp(ii,3)),'linear');
        subplot(2,2,4)
        plot(lm)
        anova_lm = anova(lm)
        pValue = lm.Coefficients.pValue(2)
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(nanmean(x),nanmean(y),msg)
        
        
end














