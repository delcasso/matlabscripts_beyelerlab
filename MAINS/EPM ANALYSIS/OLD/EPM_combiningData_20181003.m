% function EPM_combiningData_20180926()


%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
d = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM';
miceList = load('Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\miceList.txt');
fileList=dir([d filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS

analysisMode='ContinuousCaSignal';
% analysisMode='Transients';

%Warning, here the reference is the center of the maze
directionSTR={'ALL','IN','OUT'};
nBins = 80/0.5;

signalHisto_edges = 2:0.1:20;% signalHisto_edges = -10:0.1:10;
signalHisto_binSize = mean(diff(signalHisto_edges));
signalHisto_nBins = size(signalHisto_edges,2)-1;
signalHisto_x = signalHisto_edges(1)+(signalHisto_binSize/2):signalHisto_binSize: signalHisto_edges(end)-(signalHisto_binSize/2);

transientsProba=nan(nFiles,5);
transientsAmp=nan(nFiles,5);

%%
for iDir = 1:1
    
    Maps=nan(nFiles,nBins,nBins);
    OA=nan(nFiles,nBins/2);
    CA=nan(nFiles,nBins/2);
    Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);
    Mice={};
    signalHisto = nan(nFiles,signalHisto_nBins);
    intermediate=0;anterior=0;
    anteriorMouseList={};intermediateMouseList={};
    
    for i=1:nFiles
        
        f = fileList(i).name;t2=toc;ii = find(miceList(:,1)==str2double(f(2:4)));
        Mice{i}=str2double(f(2:4));
        Region(i)=miceList(ii,2);Histo(i)=miceList(ii,3);BatchNum(i)=miceList(ii,4);TaskComment(i)=miceList(ii,5);SignalComment(i)=miceList(ii,6);
        
        if Histo(i)>0.5 && (TaskComment(i)==1) && (BatchNum(i)>0)  && (SignalComment(i)==1)
            
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};
                fprintf('intermediate = %d ',intermediate);
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};
                fprintf('anterior = %d ',anterior);
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([d filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            armAlignedSig=experiment.armAlignedSig;
            armAlignedTransients=experiment.armAlignedTransients;
            
            signalHisto(i,:) = histcounts(pData.mainSig,signalHisto_edges,'Normalization','probability');
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    switch iDir
                        case 1
                            tmp1 = map.NormSig.IO;tmp2 = armAlignedSig.IO.oneDim;
                        case 2
                            tmp1 = map.NormSig.I;tmp2 = armAlignedSig.I.oneDim;
                        case 3
                            tmp1 = map.NormSig.O;tmp2 = armAlignedSig.O.oneDim;
                    end
                case 'Transients'
                    
                    transientsProba(i,:)=pData.transients.zonesProba;
                    transientsAmp(i,:)=pData.transients.zonesMeanAmp;
                    
                    switch iDir
                        case 1
                            tmp1 = map.NormTransients.IO;tmp2 = armAlignedTransients.IO.oneDim;
                        case 2
                            tmp1 = map.NormTransients.I;tmp2 = armAlignedTransients.I.oneDim;
                        case 3
                            tmp1 = map.NormTransients.O;tmp2 = armAlignedTransients.O.oneDim;
                    end
            end
            
            
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            Maps(i,:,:) = tmp1;
            L = tmp2(1);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;OA(i,:) = nansum([OA(i,:) ; L]);
            L = tmp2(3);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;OA(i,:) = nansum([OA(i,:) ; L]);
            L = tmp2(2);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;CA(i,:) = nansum([CA(i,:) ; L]);
            L =tmp2(4);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;CA(i,:) = nansum([CA(i,:) ; L]);
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
        
        
    end
    
    %     s=squeeze(nansum(Maps,3));
    %     s=squeeze(nansum(s,2));
    %     ii=isnan(s);
    %     Region(ii)=[];
    %     Maps(ii,:,:)=[];
    iiSweet = find(Region==0);
    iiBitter   = find(Region==1);
    
    
    SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));
    ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
    BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));
    ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;
    
    %% AVG SIGNAL MAPS
    figure();
    
    colormap([[1 1 1];jet(1024)])
    
    %     c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
    %     load('Z:\__Softwares\MatlabScripts_BeyelerLab\COLORMAPS\myCustomColormap.mat');
    %     colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);
    
    subplot(3,2,1)
    hold on;title([directionSTR{iDir} ' anterior (n=' num2str(anterior) ')']);axis equal;axis off;colorbar();
    imagesc(SweetMaps,[-0.05 0.05])
    
    subplot(3,2,2)
    hold on;title([directionSTR{iDir} ' intermediate (n=' num2str(intermediate) ')']);axis equal;axis off;colorbar();
    imagesc(BitterMaps,[-0.05 0.05])
    
    mSweetOA=nanmean(OA(~logical(Region),:));mSweetCA=nanmean(CA(~logical(Region),:));
    mBitterOA=nanmean(OA(logical(Region),:));mBitterCA=nanmean(CA(logical(Region),:));
    semSweetOA=nanstd(OA(~logical(Region),:))./sqrt(anterior);semSweetCA=nanstd(CA(~logical(Region),:))./sqrt(anterior);
    semBitterOA=nanstd(OA(logical(Region),:))./sqrt(intermediate);semBitterCA=nanstd(CA(logical(Region),:))./sqrt(intermediate);
    
    ax3=subplot(3,2,3);
    hold on;title([directionSTR{iDir} ' Anterior']);%ylim([-4 8])
    quickPLot(ax3,mSweetOA,semSweetOA,mSweetCA,semSweetCA)
    
    ax4=subplot(3,2,4);
    hold on;title([directionSTR{iDir} ' Intermediate']);%ylim([-4 8])
    quickPLot(ax4,mBitterOA,semBitterOA,mBitterCA,semBitterCA)
    
    nBlocks = 3;
    OA_Blocks=nan(nFiles,nBlocks);CA_Blocks=nan(nFiles,nBlocks);
    OA_Blocks(:,1) = nanmean(OA(:,3:24),2);OA_Blocks(:,2) = nanmean(OA(:,25:46),2);OA_Blocks(:,3) = nanmean(OA(:,47:68),2);
    CA_Blocks(:,1) = nanmean(CA(:,3:24),2);CA_Blocks(:,2) = nanmean(CA(:,25:46),2);CA_Blocks(:,3) = nanmean(CA(:,47:68),2);
    mSweetOA=nanmean(OA_Blocks(~logical(Region),:));mSweetCA=nanmean(CA_Blocks(~logical(Region),:));
    mBitterOA=nanmean(OA_Blocks(logical(Region),:));mBitterCA=nanmean(CA_Blocks(logical(Region),:));
    semSweetOA=nanstd(OA_Blocks(~logical(Region),:))./sqrt(anterior);semSweetCA=nanstd(CA_Blocks(~logical(Region),:))./sqrt(anterior);
    semBitterOA=nanstd(OA_Blocks(logical(Region),:))./sqrt(intermediate);semBitterCA=nanstd(CA_Blocks(logical(Region),:))./sqrt(intermediate);
    
    ax5=subplot(3,2,5);
    hold on;title([directionSTR{iDir} ' Anterior']);%ylim([-4 8])
    quickPLot(ax5,mSweetOA,semSweetOA,mSweetCA,semSweetCA)
    
    ax6=subplot(3,2,6);
    hold on;title([directionSTR{iDir} ' Intermediate']);%ylim([-4 8])
    quickPLot(ax6,mBitterOA,semBitterOA,mBitterCA,semBitterCA)
    
    
end

ii=isnan(transientsAmp(:,1));
transientsAmp(ii,:)=[];
transientsProba(ii,:)=[];
Region(ii)=[];

mAmp_OA = nanmean(transientsAmp(:,[1 3]),2);
mAmp_CA = nanmean(transientsAmp(:,[2 4]),2);
mProba_OA = nanmean(transientsProba(:,[1 3]),2);
mProba_CA = nanmean(transientsProba(:,[2 4]),2);

iiSweet = find(Region==0);
iiBitter   = find(Region==1);

meanSweetOA = nanmean(mAmp_OA(iiSweet));semSweetOA = nanstd(mAmp_OA(iiSweet),[],1)./sqrt(size(iiSweet,1));
meanSweetCA = nanmean(mAmp_CA(iiSweet));semSweetCA = nanstd(mAmp_CA(iiSweet),[],1)./sqrt(size(iiSweet,1));
meanBitterOA = nanmean(mAmp_OA(iiBitter));semBitterOA = nanstd(mAmp_OA(iiBitter),[],1)./sqrt(size(iiBitter,1));
meanBitterCA = nanmean(mAmp_CA(iiBitter));semBitterOA = nanstd(mAmp_CA(iiBitter),[],1)./sqrt(size(iiBitter,1));

figure()
hold on

rectangle('Position',[0.75 0 0.5 meanSweetOA],'EdgeColor',[0 0 1]);
plot([1 1],[meanSweetOA meanSweetOA+semSweetOA],'b');
rectangle('Position',[1.75 0 0.5 meanSweetCA],'EdgeColor',[0 0 1]);
plot([2 2],[meanSweetCA meanSweetCA+semSweetCA],'b');
plot([1 2],[mAmp_OA(iiSweet) mAmp_CA(iiSweet)],'b:')

rectangle('Position',[2.75 0 0.5 meanBitterOA],'EdgeColor',[1 0 0]);
plot([3 3],[meanBitterOA meanBitterOA+semBitterOA],'r');
rectangle('Position',[3.75 0 0.5 meanBitterCA],'EdgeColor',[1 0 0]);
plot([4 4],[meanBitterCA meanBitterCA+semBitterOA],'r');
plot([3 4],[mAmp_OA(iiBitter) mAmp_CA(iiBitter)],'r:')
legend({'Sweet','Bitter'})

meanSweetOA = nanmean(mProba_OA(iiSweet));semSweetOA = nanstd(mProba_OA(iiSweet),[],1)./sqrt(size(iiSweet,1));
meanSweetCA = nanmean(mProba_CA(iiSweet));semSweetCA = nanstd(mProba_CA(iiSweet),[],1)./sqrt(size(iiSweet,1));
meanBitterOA = nanmean(mProba_OA(iiBitter));semBitterOA = nanstd(mProba_OA(iiBitter),[],1)./sqrt(size(iiBitter,1));
meanBitterCA = nanmean(mProba_CA(iiBitter));semBitterOA = nanstd(mProba_CA(iiBitter),[],1)./sqrt(size(iiBitter,1));

figure()
hold on

rectangle('Position',[0.75 0 0.5 meanSweetOA],'EdgeColor',[0 0 1]);
plot([1 1],[meanSweetOA meanSweetOA+semSweetOA],'b');
rectangle('Position',[1.75 0 0.5 meanSweetCA],'EdgeColor',[0 0 1]);
plot([2 2],[meanSweetCA meanSweetCA+semSweetCA],'b');
plot([1 2],[mProba_OA(iiSweet) mProba_CA(iiSweet)],'b:')

rectangle('Position',[2.75 0 0.5 meanBitterOA],'EdgeColor',[1 0 0]);
plot([3 3],[meanBitterOA meanBitterOA+semBitterOA],'b');
rectangle('Position',[3.75 0 0.5 meanBitterCA],'EdgeColor',[1 0 0]);
plot([4 4],[meanBitterCA meanBitterCA+semBitterOA],'b');
plot([3 4],[mProba_OA(iiBitter) mProba_CA(iiBitter)],'r:')
legend({'Sweet','Bitter'})


figure();
hold on
imagesc(signalHisto_x,1:nFiles,signalHisto)
for i=1:nFiles
    f = fileList(i).name;
    [~, f, ~] = fileparts(f);
    text(signalHisto_x(end)/2,i,f,'color',[1 1 1])
end



function quickPLot(aH,m1,s1,m2,s2)
axes(aH);
plot(m1,'g');plot(m1+s1,'g');plot(m1-s1,'g');
plot(m2,'k');plot(m2+s2,'k');plot(m2-s2,'k');
end







% c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
% load('Z:\__Softwares\MatlabScripts_BeyelerLab\PHOTOMETRY\myCustomColormap.mat');
% colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);


%     colormap_ = load('Z:\__Softwares\MatlabScripts_BeyelerLab\COLORMAPS\dawn.txt');
%     colormap_ = flipud(colormap_);
%     colormap([[0 0 0] ; colormap_./255]);