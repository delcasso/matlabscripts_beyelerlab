% function EPM_combiningData_20180926()

%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
batchID= '20180926_iC-SweetBitter_AllTogether_EPM';

[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

stat = table();


%% PARAMS
analysisMode='ContinuousCaSignal';
% analysisMode='Transients';

%Warning, here the reference is the center of the maze
directionSTR={'ALL','IN','OUT'};

if params.apparatusNormalizationRequested
    fprintf('apparatus normalization requested\n');
    nApp = size(params.apparatus,2);
    normApparatus = params.apparatus{1,nApp};
    nBins = normApparatus.OA_cm / params.MapScale_cmPerBin;
else
    warning('nBins has been defined arbitrarly');
    nBins = 80/0.5;
end

signalHisto_edges = 2:0.1:20;% signalHisto_edges = -10:0.1:10;
signalHisto_binSize = mean(diff(signalHisto_edges));
signalHisto_nBins = size(signalHisto_edges,2)-1;
signalHisto_x = signalHisto_edges(1)+(signalHisto_binSize/2):signalHisto_binSize: signalHisto_edges(end)-(signalHisto_binSize/2);

transientsProba=nan(nFiles,5);transientsAmp=nan(nFiles,5);

%%
for iDir = 1:3
    
    Maps=nan(nFiles,nBins,nBins);OA=nan(nFiles,nBins/2);CA=nan(nFiles,nBins/2);
    Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
    TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
    signalHisto = nan(nFiles,signalHisto_nBins);
    intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};
    
    for i=1:nFiles
        
        f = fileList(i).name;t2=toc;
        ii = find(journal.MouseNum==str2double(f(2:4)));
        
        Mice{i}=str2double(f(2:4));
        
        
        if ~isempty(ii)
            
            iLine(i)=ii;
            Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
            
            if Histo(i)>0.5 && (TaskComment(i)==1) && (BatchNum(i)>0)  && (SignalComment(i)==1)
                %                 if Histo(i)==1 && ( (TaskComment(i)==1) || (TaskComment(i)==-1)) && (BatchNum(i)>1)  && (SignalComment(i)==1)
                
                if Region(i)
                    intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);
                else
                    anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);
                end
                
                fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
                
                p = experiment.p;vData = experiment.vData;pData = experiment.pData;
                map = experiment.map;armAlignedSig=experiment.armAlignedSig;armAlignedTransients=experiment.armAlignedTransients;
                
                signalHisto(i,:) = histcounts(pData.mainSig,signalHisto_edges,'Normalization','probability');
                
                switch analysisMode
                    
                    case 'ContinuousCaSignal'
                        cLim_=[-1 1];
                        quickPlot_minYlim=-2;quickPlot_maxYlim=6;
                        switch iDir
                            case 1
                                tmp1 = map.NormSig.IO;tmp2 = armAlignedSig.IO.oneDim;
                            case 2
                                tmp1 = map.NormSig.I;tmp2 = armAlignedSig.I.oneDim;
                            case 3
                                tmp1 = map.NormSig.O;tmp2 = armAlignedSig.O.oneDim;
                        end
                        
                    case 'Transients'
                        cLim_=[0 0.4];
                        quickPlot_minYlim=-1;quickPlot_maxYlim=1;
                        transientsProba(i,:)=pData.transientStats.zonesProba;transientsAmp(i,:)=pData.transientStats.zonesMeanAmp;
                        switch iDir
                            case 1
                                tmp1 = map.NormTransients.IO;tmp2 = armAlignedTransients.IO.oneDim;
                            case 2
                                tmp1 = map.NormTransients.I;tmp2 = armAlignedTransients.I.oneDim;
                            case 3
                                tmp1 = map.NormTransients.O;tmp2 = armAlignedTransients.O.oneDim;
                        end
                end
                
                [r1,c1]=size(tmp1);if r1< nBins, tmp1(r1+1:nBins,:)=nan;end;if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
                
                Maps(i,:,:) = tmp1;
                L = tmp2(1);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;OA(i,:) = nansum([OA(i,:) ; L]);
                L = tmp2(3);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;OA(i,:) = nansum([OA(i,:) ; L]);
                L = tmp2(2);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;CA(i,:) = nansum([CA(i,:) ; L]);
                L =tmp2(4);L=L{:};n=size(L,2);L(n+1:nBins/2)=nan;CA(i,:) = nansum([CA(i,:) ; L]);
                
                t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
            else
                fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
            end
            
        end
        
    end
    
    stat.Mice = Mice';
    stat.Region = Region;
    stat.Histo = Histo;
    stat.BatchNum= BatchNum;
    stat.TaskComment = TaskComment;
    stat.SignalComment=SignalComment;
    
    iiSweet = find(Region==0);iiBitter   = find(Region==1);
    
    SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));
    ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
    BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));
    ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;
    
    %% AVG SIGNAL MAPS
    figure();
    colormap([[1 1 1];jet(1024)])
    %     cmap=cmocean('balance');
    %     colormap(cmap)
    
    subplot(3,2,1)
    hold on;title([directionSTR{iDir} ' anterior (n=' num2str(anterior) ')']);axis equal;axis off;colorbar();
    x = experiment.apparatusDesign_cm.x;
    y = experiment.apparatusDesign_cm.y;
    x = x ./ experiment.p.MapScale_cmPerBin;
    y = y ./ experiment.p.MapScale_cmPerBin;
    x = x - min(x);
    y = y - min(y);
    imagesc(SweetMaps,cLim_);
    plot(x,y,'k')
    
    subplot(3,2,2)
    hold on;title([directionSTR{iDir} ' intermediate (n=' num2str(intermediate) ')']);axis equal;axis off;colorbar();
    imagesc(BitterMaps,cLim_)
    plot(x,y,'k')
    
    OA(OA==0)=nan; CA(CA==0)=nan;
    
    %         nanOA=sum(isnan(OA));
    %         figure()
    %         hold on
    %         title('Mice to Exclude from OA');
    %         xlabel('position in cm');
    %         ylabel('mice number');
    %         plot(nanOA-min(nanOA));
    %
    %         nanCA=sum(isnan(CA));
    %         figure()
    %         hold on
    %         title('Mice to Exclude from CA');
    %         xlabel('position in cm');
    %         ylabel('mice number');
    %         plot(nanCA-min(nanCA));
    %% Here we are looking for the last bin visited by all mice to clean the analysis
    
    iiSweet = find(Region==0); iiBitter = find(Region==1);
    
    mSweetOA=nanmean(OA(iiSweet,:));
    mSweetCA=nanmean(CA(iiSweet,:));
    mBitterOA=nanmean(OA(iiBitter,:));
    mBitterCA=nanmean(CA(iiBitter,:));
    semSweetOA=nanstd(OA(iiSweet,:))./sqrt(anterior);
    semSweetCA=nanstd(CA(iiSweet,:))./sqrt(anterior);
    semBitterOA=nanstd(OA(iiBitter,:))./sqrt(intermediate);
    semBitterCA=nanstd(CA(iiBitter,:))./sqrt(intermediate);
    
    
    
    ax3=subplot(3,2,3);
    hold on;title([directionSTR{iDir} ' Anterior']);%ylim([-4 8])
    quickPlot(ax3,mSweetOA,semSweetOA,mSweetCA,semSweetCA,quickPlot_minYlim,quickPlot_maxYlim)
    
    ax4=subplot(3,2,4);
    hold on;title([directionSTR{iDir} ' Intermediate']);%ylim([-4 8])
    quickPlot(ax4,mBitterOA,semBitterOA,mBitterCA,semBitterCA,quickPlot_minYlim,quickPlot_maxYlim)
    
    nBlocks = 3;
    block_idx(1,1) = 3;block_idx(1,2) = 24;
    block_idx(2,1) = 25;block_idx(2,2) = 46;
    block_idx(3,1) = 47;block_idx(3,2) = 64;
    OA_Blocks=nan(nFiles,nBlocks);CA_Blocks=nan(nFiles,nBlocks);
    for iB = 1:nBlocks
        OA_Blocks(:,1) = nanmean(OA(:,block_idx(iB,1):block_idx(iB,2)),2);
        cmd = sprintf('stat.bulk_%s_OA_B%d=OA_Blocks(:,1);',directionSTR{iDir},iB)
        eval(cmd);
        CA_Blocks(:,1) = nanmean(CA(:,block_idx(iB,1):block_idx(iB,2)),2);
        cmd = sprintf('stat.bulk_%s_CA_B%d=CA_Blocks(:,1);',directionSTR{iDir},iB)
        eval(cmd);
    end
    
    
    
    mSweetOA=nanmean(OA_Blocks(iiSweet,:));mSweetCA=nanmean(CA_Blocks(iiSweet,:));
    mBitterOA=nanmean(OA_Blocks(iiBitter,:));mBitterCA=nanmean(CA_Blocks(iiBitter,:));
    semSweetOA=nanstd(OA_Blocks(iiSweet,:))./sqrt(anterior);semSweetCA=nanstd(CA_Blocks(iiSweet,:))./sqrt(anterior);
    semBitterOA=nanstd(OA_Blocks(iiBitter,:))./sqrt(intermediate);semBitterCA=nanstd(CA_Blocks(iiBitter,:))./sqrt(intermediate);
    
    
    
    ax5=subplot(3,2,5);
    hold on;title([directionSTR{iDir} ' Anterior']);%ylim([-4 8])
    quickPlot(ax5,mSweetOA,semSweetOA,mSweetCA,semSweetCA,quickPlot_minYlim,quickPlot_maxYlim)
    
    ax6=subplot(3,2,6);
    hold on;title([directionSTR{iDir} ' Intermediate']);%ylim([-4 8])
    quickPlot(ax6,mBitterOA,semBitterOA,mBitterCA,semBitterCA,quickPlot_minYlim,quickPlot_maxYlim)
    
    
end



if strcmp(analysisMode,'Transients')
    
    ii=isnan(transientsAmp(:,1));
    transientsAmp(ii,:)=[];
    transientsProba(ii,:)=[];
    Region(ii)=[];
    
    mAmp_OA = nanmean(transientsAmp(:,[1 3]),2);
    mAmp_CA = nanmean(transientsAmp(:,[2 4]),2);
    mProba_OA = nanmean(transientsProba(:,[1 3]),2);
    mProba_CA = nanmean(transientsProba(:,[2 4]),2);
    
    iiSweet = find(Region==0);
    iiBitter   = find(Region==1);
    
    meanSweetOA = nanmean(mAmp_OA(iiSweet));semSweetOA = nanstd(mAmp_OA(iiSweet),[],1)./sqrt(size(iiSweet,1));
    meanSweetCA = nanmean(mAmp_CA(iiSweet));semSweetCA = nanstd(mAmp_CA(iiSweet),[],1)./sqrt(size(iiSweet,1));
    meanBitterOA = nanmean(mAmp_OA(iiBitter));semBitterOA = nanstd(mAmp_OA(iiBitter),[],1)./sqrt(size(iiBitter,1));
    meanBitterCA = nanmean(mAmp_CA(iiBitter));semBitterCA = nanstd(mAmp_CA(iiBitter),[],1)./sqrt(size(iiBitter,1));
    
    figure()
    hold on
    
    rectangle('Position',[0.75 0 0.5 meanSweetOA],'EdgeColor',[0 0 1]);
    plot([1 1],[meanSweetOA meanSweetOA+semSweetOA],'b');
    rectangle('Position',[1.75 0 0.5 meanSweetCA],'EdgeColor',[0 0 1]);
    plot([2 2],[meanSweetCA meanSweetCA+semSweetCA],'b');
    plot([1 2],[mAmp_OA(iiSweet) mAmp_CA(iiSweet)],'b:')
    
    rectangle('Position',[2.75 0 0.5 meanBitterOA],'EdgeColor',[1 0 0]);
    plot([3 3],[meanBitterOA meanBitterOA+semBitterOA],'r');
    rectangle('Position',[3.75 0 0.5 meanBitterCA],'EdgeColor',[1 0 0]);
    plot([4 4],[meanBitterCA meanBitterCA+semBitterCA],'r');
    plot([3 4],[mAmp_OA(iiBitter) mAmp_CA(iiBitter)],'r:')
    legend({'Sweet','Bitter'})
    
    meanSweetOA = nanmean(mProba_OA(iiSweet));semSweetOA = nanstd(mProba_OA(iiSweet),[],1)./sqrt(size(iiSweet,1));
    meanSweetCA = nanmean(mProba_CA(iiSweet));semSweetCA = nanstd(mProba_CA(iiSweet),[],1)./sqrt(size(iiSweet,1));
    meanBitterOA = nanmean(mProba_OA(iiBitter));semBitterOA = nanstd(mProba_OA(iiBitter),[],1)./sqrt(size(iiBitter,1));
    meanBitterCA = nanmean(mProba_CA(iiBitter));semBitterCA = nanstd(mProba_CA(iiBitter),[],1)./sqrt(size(iiBitter,1));
    
    figure()
    hold on
    
    rectangle('Position',[0.75 0 0.5 meanSweetOA],'EdgeColor',[0 0 1]);
    plot([1 1],[meanSweetOA meanSweetOA+semSweetOA],'b');
    rectangle('Position',[1.75 0 0.5 meanSweetCA],'EdgeColor',[0 0 1]);
    plot([2 2],[meanSweetCA meanSweetCA+semSweetCA],'b');
    plot([1 2],[mProba_OA(iiSweet) mProba_CA(iiSweet)],'b:')
    
    rectangle('Position',[2.75 0 0.5 meanBitterOA],'EdgeColor',[1 0 0]);
    plot([3 3],[meanBitterOA meanBitterOA+semBitterOA],'b');
    rectangle('Position',[3.75 0 0.5 meanBitterCA],'EdgeColor',[1 0 0]);
    plot([4 4],[meanBitterCA meanBitterCA+semBitterCA],'b');
    plot([3 4],[mProba_OA(iiBitter) mProba_CA(iiBitter)],'r:')
    legend({'Sweet','Bitter'})
    
end


writetable(stat,[analysisFolder filesep 'stats_20181218.xls']);



figure();
hold on
imagesc(signalHisto_x,1:nFiles,signalHisto)
for i=1:nFiles
    f = fileList(i).name;
    [~, f, ~] = fileparts(f);
    text(signalHisto_x(end)/2,i,f,'color',[1 1 1])
end



function quickPlot(aH,m1,s1,m2,s2,minYlim,maxYlim)
axes(aH);
plot(m1,'g');plot(m1+s1,'g');plot(m1-s1,'g');
plot(m2,'k');plot(m2+s2,'k');plot(m2-s2,'k');
ylim([minYlim maxYlim])
end






% c = linspace(0,1,512);z = ones(512,1);c2 = linspace(1,0,512);
% load('Z:\__Softwares\MatlabScripts_BeyelerLab\PHOTOMETRY\myCustomColormap.mat');
% colormap([ [1 .97 1] ; [c' c' c'] ; myCustomColormap]);


%     colormap_ = load('Z:\__Softwares\MatlabScripts_BeyelerLab\COLORMAPS\dawn.txt');
%     colormap_ = flipud(colormap_);
%     colormap([[0 0 0] ; colormap_./255]);