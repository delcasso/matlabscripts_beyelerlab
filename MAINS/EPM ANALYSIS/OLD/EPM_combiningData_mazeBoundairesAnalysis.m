% function EPM_combiningData_mazeBoundairesAnalysis

%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
d = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM';
miceList = load('Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\miceList.txt');
fileList=dir([d filesep '*.mat']);nFiles=size(fileList,1);

figure()
subplot(3,3,1)
hold on
color_ = colormap(lines(nFiles));
for i=1:nFiles      
    f = fileList(i).name;
            fprintf('Loading file %s [%d/%d]\n',f,i,nFiles);load([d filesep f]);      
            xA=experiment.apparatusDesign_cmSP.x;
            yA=experiment.apparatusDesign_cmSP.y;
            plot(xA,yA,'color',color_(i,:));
end

% xGap = 0:80:320;
% xGap = [xGap xGap xGap xGap xGap];
% yGap = [0 0 0 0 0];
% yGap = [yGap yGap+80 yGap+160 yGap+240 yGap+320];

xGap = 0:80:80*24;
yGap = zeros(25,1);

subplot(3,3,[2 3])
hold on
ylim([0 120]);
color_ = colormap(lines(nFiles));
for i=1:nFiles      
    f = fileList(i).name;
            fprintf('Loading file %s [%d/%d]\n',f,i,nFiles);load([d filesep f]);      
            xA=experiment.apparatusDesign_cmSP.x;
            yA=experiment.apparatusDesign_cmSP.y;
            plot(xA+xGap(i),yA+yGap(i),'color',color_(i,:));
            t=text(xGap(i),120-(mod(i,4)*3),sprintf('%s',f),'color',color_(i,:));
            t.FontSize = 6;
end

yGap = 0:80:80*24;
xGap = zeros(25,1);

subplot(3,3,[4 7])
hold on
color_ = colormap(lines(nFiles));
for i=1:nFiles      
    f = fileList(i).name;
            fprintf('Loading file %s [%d/%d]\n',f,i,nFiles);load([d filesep f]);      
            xA=experiment.apparatusDesign_cmSP.x;
            yA=experiment.apparatusDesign_cmSP.y;
            plot(xA+xGap(i),yA+yGap(i),'color',color_(i,:));
            t=text(-20,yGap(i),sprintf('%s',f),'color',color_(i,:));
            t.FontSize = 6;            
end
