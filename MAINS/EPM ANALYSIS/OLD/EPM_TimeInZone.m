% function EPM_combiningData_20180926()


%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
d = 'Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\EPM';
fod = fopen([d filesep 'TimeInZone_20190103.xls'],'w');
miceList = load('Z:\Fiber-Photometry\01_DATA\20180926_iC-SweetBitter_AllTogether\miceList.txt');
fileList=dir([d filesep '*.mat']);nFiles=size(fileList,1);

Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);
Mice=[];
intermediate=0;anterior=0;
anteriorMouseList={};intermediateMouseList={};
curRegionNum = 0;

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;ii = find(miceList(:,1)==str2double(f(2:4)));
    Mice(i)=str2double(f(2:4));
    Region(i)=miceList(ii,2);
    Histo(i)=miceList(ii,3);
    BatchNum(i)=miceList(ii,4);
    TaskComment(i)=miceList(ii,5);SignalComment(i)=miceList(ii,6);
    
    if Histo(i)>0.5 && (TaskComment(i)==1) && (BatchNum(i)>0)  && (SignalComment(i)==1)        
        
        if Region(i)
            intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice(i);
            curRegionNum = intermediate;       
            regionSTR = 'intermediate';
        else
            anterior = anterior+1;anteriorMouseList{anterior}=Mice(i);
             curRegionNum = anterior;     
            regionSTR = 'anterior';
        end
        
        fprintf('%2.2f Loading file %s [%d/%d] -- [%s #%d]',t2,f,i,nFiles,regionSTR,curRegionNum);load([d filesep f]);
        
        p = experiment.p;
        vData = experiment.vData;
        pData = experiment.pData;
        
        zones = vData.zones_cmSP;
        nFrames(i) = sum(experiment.vData.zoneTime_fr);        
        
        z = experiment.vData.zoneTime_fr;
        OA(i) = experiment.vData.zoneTime_fr(1) + experiment.vData.zoneTime_fr(3);
        CA(i) = experiment.vData.zoneTime_fr(2) + experiment.vData.zoneTime_fr(4);
        T(i) = (OA(i)-CA(i))/(OA(i)+CA(i));
       
        fprintf(fod,'%d\t%s\t%d\t%2.2f\t%2.2f\t%2.2f\t%2.2f\t%2.2f\n',Mice(i),regionSTR,Region(i),z(1),z(2),z(3),z(4),z(5));
        
        t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        
     
    else
        fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
    end
    
    
    
end


fclose(fod);
