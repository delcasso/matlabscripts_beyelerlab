nExp = size(expDB,2)
expDuration = nan(nExp,1);
OA_perc = nan(nExp,4);
OA_perc_centerIncluded = nan(nExp,4);

for iExp = 1:nExp
    T = expDB{1,iExp}.TimeInZone_sec(1:4,1:6);
    expDuration(iExp) = sum(sum(T,2));
    
    for iP=1:4
            tot = sum(T(iP,1:4));
             OA_perc(iExp,iP) = ((T(iP,1) + T(iP,3)) / tot ) * 100;             
            tot = sum(T(iP,1:5));
             OA_perc_centerIncluded(iExp,iP) = ((T(iP,1) + T(iP,3)) / tot ) * 100;
    end

end

