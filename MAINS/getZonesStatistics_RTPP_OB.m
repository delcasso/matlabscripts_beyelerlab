function experiment = getZonesStatistics_RTPP_OB(experiment)

vData=experiment.vData;
p=experiment.p;
nZones = size(vData.zones_cmSP,2);

fod = fopen(p.batch_ouputFile,'a');

if ~p.batch_ouputFile_headerWritten
    fprintf(fod,'be careful, the unit here is the number of frame not the time in seconde\n');
    fprintf(fod,'divide by your frame rate to have data in secondes\n\n');
    fprintf(fod,'mouse\tSimZone\tNoStimZone\tOut\n');
    p.batch_ouputFile_headerWritten = 1;
end



% 
% x = experiment.vData.mainX_cmSP;
% y = experiment.vData.mainY_cmSP;
% z = experiment.vData.inZone;
% nPos = size(x,1);

% ii1 = find(x<35);
% ii2 = find(x>25);
% ii3 = intersect(ii1,ii2);
% z(ii3)=0;


% zones_colors = nan(2,3);
% zones_colors(1,:) = [0 0.5 0];
% zones_colors(2,:) = [0.5 0.5 0];

% 
% figure()
% hold on
% plot(x,y,'color',[0.9 0.9 0.9])
% 
% for i=1:5000
%     if z(i)
%         plot(x(i),y(i),'Marker','.','MarkerFaceColor','none','MarkerEdgeColor',zones_colors(z(i),:));
%     end
% end


fprintf(fod,p.dataFileTag);


% vData.inZone=z;


 %% To remove 10 first min.
fr = experiment.vData.videoInfo.FrameRate;
idx1 = 10 * 60 * fr;
sessionDuration = 20 * 60 * fr;
nSamples = size(vData.inZone,1);
if nSamples > sessionDuration
  vData.inZone = vData.inZone(1:nSamples);  
  vData.processedOptoSig = vData.processedOptoSig(1:nSamples); 
end
vData.inZone = vData.inZone(idx1:end);
vData.processedOptoSig = vData.processedOptoSig(idx1:end);
% End to remove 10 first min.








[vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone,nZones);

idxLedOn = find(vData.processedOptoSig==1);
idxZone1 = find(vData.inZone==1);
idxZone2 = find(vData.inZone==2);

interLedZ1 = intersect(idxLedOn,idxZone1); s1 = size(interLedZ1,1);
interLedZ2 = intersect(idxLedOn,idxZone2); s2 = size(interLedZ2,1);
[~,stimZone] = max([s1 s2]);
[~,noStimZone] = min([s1 s2]);

% figure()
% hold on
% nSamples = 4000;%size(vData.bodyX,1);
% for i=2000:nSamples
%     if vData.processedOptoSig(i)
%         plot(vData.mainX_cmSP(i),vData.mainY_cmSP(i),'og')
%     else
%         plot(vData.mainX_cmSP(i),vData.mainY_cmSP(i),'or')
%     end
% end
% for iZ = 1:2
%     xMean = mean(vData.zones_cmSP(iZ).xV)
%     yMean = mean(vData.zones_cmSP(iZ).yV)
%     type_ = vData.zones_cmSP(iZ).type
%     text(xMean,yMean,type_);
% end
% disp('here')




fprintf(fod,sprintf('\t%d',vData.zoneTime_fr(stimZone)));
fprintf(fod,sprintf('\t%d',vData.zoneTime_fr(noStimZone)));
fprintf(fod,sprintf('\t%d',vData.outTime_fr));

vData.zones_cmSP(stimZone).stim = 'stim zone';
vData.zones_cmSP(noStimZone).stim = 'no stim zone';
experiment.TimeInStimZone_sec = vData.zoneTime_fr(stimZone)./vData.videoInfo.FrameRate;    
experiment.TimeInNoStimZone_sec = vData.zoneTime_fr(noStimZone)./vData.videoInfo.FrameRate;    


fprintf(fod,'\n');

experiment.vData=vData;
experiment.p = p;
fclose(fod);

end
