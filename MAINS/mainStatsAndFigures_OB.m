% batchID =  'ICa-ICp_CAV2Cre_EPM'
batchID = 'ICa-ICp_CAV2Cre_OFT';

nMice = size(infoDB,2);

mouse = [];sex = {};region = {};batch = [];opsin = {};histo = [];

results = [];

for iMouse=1:nMice
    results(iMouse).mouse = infoDB{1,iMouse}.MouseNum;
    results(iMouse).sex = infoDB{1,iMouse}.Sex;
    results(iMouse).region = infoDB{1,iMouse}.RegionCode;
    results(iMouse).opsin =  infoDB{1,iMouse}.Opsin;
    results(iMouse).histo =  infoDB{1,iMouse}.HistologyCode;
    results(iMouse).batch = infoDB{1,iMouse}.BatchCode;        
end

switch batchID
    case 'ICa-ICp_EPM'
        valToPlot = nan(nMice,4);
        %mice
        for iMouse=1:nMice
            %periods (ON1 OFF1 ON2 OFF2)
            for iPeriod=1:4
                %zones (1 = open, 2 = closed, 3= open, 4 = closed, 5 = center, 6 = OUT)
                valToPlot(iMouse,iPeriod) = expDB{1,iMouse}.TimeInZone_sec(iPeriod,1) + expDB{1,iMouse}.TimeInZone_sec(iPeriod,3);
            end
        end
        
    case 'ICa-ICp_CAV2Cre_EPM'
        valToPlot = nan(nMice,4);
        %mice
        for iMouse=1:nMice
            %periods (ON1 OFF1 ON2 OFF2)
            for iPeriod=1:4
                
                %zones (1 = open, 2 = closed, 3= open, 4 = closed, 5 = center, 6 = OUT)
                
               %Time In Open Arms 
               t_OA= expDB{1,iMouse}.TimeInZone_sec(iPeriod,1) + expDB{1,iMouse}.TimeInZone_sec(iPeriod,3);
               t_CA= expDB{1,iMouse}.TimeInZone_sec(iPeriod,2) + expDB{1,iMouse}.TimeInZone_sec(iPeriod,4);
               valToPlot(iMouse,iPeriod) = t_CA;
%             
% 
%                 eOA = expDB{1,iMouse}.eOA_perPeriod(iPeriod);
%                 eCA = expDB{1,iMouse}.eCA_perPeriod(iPeriod);
%                 valToPlot(iMouse,iPeriod) = (eOA / (eOA+eCA));
                 
            end
        end       
        
        
    case 'ICa-ICp_CAV2Cre_OFT'
        valToPlot = nan(nMice,4);
        %mice
        for iMouse=1:nMice
            %periods (ON1 OFF1 ON2 OFF2)
            for iPeriod=1:4
                %zones (1 = border, 2 = center, 3 = OUT)
                t_Border = expDB{1,iMouse}.TimeInZone_sec(iPeriod,1);
                t_Center = expDB{1,iMouse}.TimeInZone_sec(iPeriod,2);
                r = (t_Center / (t_Border+t_Center)) * 100;
                valToPlot(iMouse,iPeriod) = t_Center;
%                  eOA = expDB{1,iMouse}.eOA_perPeriod(iPeriod);
%                  eCA = expDB{1,iMouse}.eCA_perPeriod(iPeriod);
%                  valToPlot(iMouse,iPeriod) = (eOA / (eOA+eCA));                
            end
        end               
        
end

results = struct2table(results);

results.mouse = categorical(results.mouse);
results.sex = categorical(results.sex);
results.region = categorical(results.region);
results.opsin = categorical(results.opsin);
results.batch = categorical(results.batch);
results.Var=valToPlot;


nRegions = 2;nOpsins=2;

regions = nan(nMice,1);
% ii_ICa = results.region == 'ICa - BLA';
% ii_ICp = results.region == 'ICp - CeM';
ii_ICa = results.region == 'Anterior IC - BLA';
ii_ICp = results.region == 'Posterior IC - CeM';
regions(ii_ICa)=0;regions(ii_ICp)=1;
regionsStr{1} = {'ICa-BLA'};
regionsStr{2} = {'ICp-CeM'};

opsins = nan(nMice,1);
% ii_eYFP = results.opsin == 'eYFP';
% ii_ChR2 = results.opsin == 'ChR2';
ii_eYFP = results.opsin == 'eYFP';
ii_ChR2 = results.opsin == 'hChR2';
opsins(ii_eYFP)=0;opsins(ii_ChR2)=1;
opsinsStr{1} = {'eYFP'};
opsinsStr{2} = {'ChR2'};

periodsStr{1} = {'OFF1'};
periodsStr{2} = {'ON1'};
periodsStr{3} = {'OFF2'};
periodsStr{4} = {'ON2'};

ii_ICa_eYFP = ii_ICa & ii_eYFP;
ii_ICp_eYFP = ii_ICp & ii_eYFP;
ii_ICa_ChR2 = ii_ICa & ii_ChR2;
ii_ICp_ChR2 = ii_ICp & ii_ChR2;

FaceGroupColors(1,1:3) = [101 65 153]./255;
EdgesGroupColors(1,1:3) = [101 65 153]./255;
LineGroupColors(1,1:3) = [0 0 0];
LineGroupStlye{1} = ':';

FaceGroupColors(2,1:3) = [0 165 77]./255;
EdgesGroupColors(2,1:3) = [0 165 77]./255;
LineGroupColors(2,1:3) = [0 0 0];
LineGroupStlye{2} = ':';

figure()
hold on

Ymax = max(max(valToPlot));Ymax1 = Ymax;Ymax2 = Ymax + (Ymax/4)
iX = 0;
ylim([0 Ymax2+10])

for iRegion=1:nRegions
    iX = iX +4;
    
    for iOpsin = 1:nOpsins
        iX = iX +2;
        
        for iPeriod=1:4
             iX = iX +1;
             
            ii1 = find(regions==(iRegion-1));ii2 = find(opsins==(iOpsin-1));ii3 = intersect(ii1,ii2);            
            m_ = mean(valToPlot(ii3,iPeriod));sem_ = std(valToPlot(ii3,iPeriod))/sqrt(size(ii3,1));         
            
            if ~isnan(m_)
            
            if m_>0
                rectangle('Position',[iX 0 0.5 m_],'EdgeColor',EdgesGroupColors(iRegion,:),'FaceColor',FaceGroupColors(iRegion,:));
                plot([iX+0.25 iX+0.25],[m_ m_+sem_],'color',EdgesGroupColors(iRegion,:),'Linewidth',2);
            else
                rectangle('Position',[iX m_ 0.5 -m_],'EdgeColor',EdgesGroupColors(iRegion,:),'FaceColor',FaceGroupColors(iRegion,:));
                plot([iX+0.25 iX+0.25],[m_ m_+sem_],'color',EdgesGroupColors(iRegion,:),'Linewidth',2);
            end
            
            end
                                    
            text(iX+0.25 ,Ymax1,sprintf('%s-%s-%s',regionsStr{iRegion}{:},opsinsStr{iOpsin}{:},periodsStr{iPeriod}{:}),'Rotation',90,'VerticalAlignment','top')
            
        end
    end
end


valToPlot = [((valToPlot(:,1)+valToPlot(:,3))/2)  ((valToPlot(:,2)+valToPlot(:,4))/2)];

figure()
hold on

Ymax = max(max(valToPlot));Ymax1 = Ymax;Ymax2 = Ymax + (Ymax/4)
iX = 0;
ylim([0 Ymax2])

for iRegion=1:nRegions
    iX = iX +4;
    
    for iOpsin = 1:nOpsins
        iX = iX +2;
        
        tmp = [];
        for iPeriod=1:2
             iX = iX +1;
             
            ii1 = find(regions==(iRegion-1));ii2 = find(opsins==(iOpsin-1));ii3 = intersect(ii1,ii2);            
            m_ = mean(valToPlot(ii3,iPeriod));sem_ = std(valToPlot(ii3,iPeriod))/sqrt(size(ii3,1));        
            tmp = [tmp valToPlot(ii3,iPeriod)];
            
                        if ~isnan(m_)
            if m_>0
                rectangle('Position',[iX 0 0.5 m_],'EdgeColor',EdgesGroupColors(iRegion,:),'FaceColor',FaceGroupColors(iRegion,:));
                plot([iX+0.25 iX+0.25],[m_ m_+sem_],'color',EdgesGroupColors(iRegion,:),'Linewidth',2);
            else
                rectangle('Position',[iX m_ 0.5 -m_],'EdgeColor',EdgesGroupColors(iRegion,:),'FaceColor',FaceGroupColors(iRegion,:));
                plot([iX+0.25 iX+0.25],[m_ m_+sem_],'color',EdgesGroupColors(iRegion,:),'Linewidth',2);
            end
                   
                        end
            text(iX+0.25 ,Ymax1,sprintf('%s-%s-%s',regionsStr{iRegion}{:},opsinsStr{iOpsin}{:},periodsStr{iPeriod}{:}),'Rotation',90,'VerticalAlignment','top')
                                    
        end
        
        [h,p,ci,stats] = ttest(tmp(:,1),tmp(:,2));
        text(iX-0.25,Ymax1,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'Rotation',90,'VerticalAlignment','top')
        
        n = size(tmp,1)
        for i=1:n
            plot([((iX-1)+0.25) (iX+0.25)],[tmp(i,1) tmp(i,2)],'color',[0 0 0])
        end
        
        
        
    end
    
end




% 
% % function stat_plot(results,batchID)
% 
% nZones = size(zoneStats,2);
% 
% for iZone=1:nZones
%     cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
%     eval(cmd);
% end
% 
% uGroups= unique(Groups);
% uGroups(isnan(uGroups))=[];
% nGroups = size(uGroups,1);
%  
% FaceGroupColors(1,1:3) = [101 65 153]./255;
% EdgesGroupColors(1,1:3) = [101 65 153]./255;
% LineGroupColors(1,1:3) = [0 0 0];
% LineGroupStlye{1} = ':';
% 
% FaceGroupColors(2,1:3) = [0 165 77]./255;
% EdgesGroupColors(2,1:3) = [0 165 77]./255;
% LineGroupColors(2,1:3) = [0 0 0];
% LineGroupStlye{2} = ':';
% 
% ColorDiff = 0.1;
% GroupsStr = {'anterior','intermediate'};
% minY = min(min(zoneStats));
% maxY = max(max(zoneStats));
% 
% ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];
% 
% for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end
% 
% for iGroup=1:nGroups
%     for iZone=1:nZones
%         meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
%         semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
%     end
% end
% 
% f1=figure()
% hold on
% title(titleStr)
% set(gca,'xtick',[])
% 
% x=0;
% 
% for iGroup=1:nGroups    
%     
%      x=x+iGroup;
%      
%     for iZone=1:nZones                
%         
%         if meanGroupZone(iGroup,iZone)>0
%             rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
%             plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
%         else
%             rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
%             plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
%         end
%         
%         text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
%        
%     end
%     
%      [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
%       text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
%       text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
%      
%         for iZone=2:nZones
%             plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',LineGroupColors(iGroup,:),'LineStyle',':');
%         end   
%         
% end
% 
% print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
% savefig(f1,[analysisFolder filesep titleStr '.fig'])
% 
% % end
% 
% 
% 
% 
% 
% 
% 
