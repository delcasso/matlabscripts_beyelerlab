close all
if ~exist('all_experiments','var')
    
    
    % initialization, close all figures, delete all variables, clear the command window
    close all;clear;clc
    
    dbPath = 'Z:\Photometry&Behavior\03_ANALYSIS\20180926_iCa-ICp_allGroups\EPM\20190603.mat';
    
    if ~exist(dbPath,'file')
        
        %set up timer to time differnet analysis steps
        tic;t0=toc;
        
        % The is a unique identifier for szelecting the proper parameters, look
        % inside the getBatchAnalysisConfig_OB.m to understand better
        batchID = 'ICa-ICp_EPM';
        %batchID = '20181126_IC-SweetBitter-G2_NSFT'
        % batchID = 'ICa-ICp_OFT-CONTROL';
        % batchID = 'ICa-ICp_NSFT-CONTROL';
        % Where are the data, use machine flag
        % option 1 = 'SD_remote', data are on the nas 1 server mounted as Z: on my laptop
        % option 2 = 'SD_local', data have been copied on C:, the principal hard drive of my laptop
        machine = 'SD_remote';
        
        p=[];
        [dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p);
        
        nMice = size(p.journal,1);
        
        
        all_pData = [];
        
        %%CREATES OUTPUT FOLDERS
        % if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
        if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end
        
        nFolders = size(dataRoot,2);
        %% TO PROCESS ALL FOLDERS
        for iFolder=1:nFolders
            
            p.dataRoot = dataRoot{iFolder};
            if ~exist(p.dataRoot,'dir'), fprintf('%s doesn''t exist',p.dataRoot);pause;end
            p.apparatus = apparatus{iFolder};
            p.videoExtension=videoExt{iFolder};
            nApparatus = size(apparatus,2);
            if nFolders < nApparatus
                % is there a model of the apparatus for normalizatoin
                if strcmp(apparatus{nFolders+1}.Model,'ForNormalization'),p.apparatusModelForNormalization = apparatus{nFolders+1};end
            end
            
            %% MAIN PROGRAM
            fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
            %% PROCESS EACH DATA FILE INDIVIDUALLY
            for iFile=1:nFiles
                p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
                %% TO PROCESS ONE FILE IN PARTICUALR
                processThisFile=1;
                
                % If you want to process only one file
                if ~isempty(p.lookingForMouse),processThisFile=0;if strcmp(p.dataFileTag,p.lookingForMouse),processThisFile=1;end;end
                
                index = getJournalIndex(p.journal,p.dataFileTag)
                % Depending of the histologfy status we skeep the analysis for the current file
                status = getHistologyStatus(p.journal,p.dataFileTag);
                if isempty(status) | status<1
                    processThisFile=0;
                end
                
                %% MAIN PROCESSING
                
                %% MAIN PROCESSING
                if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
                tProcessingStart=toc;
                fprintf('[%.1f sec.]\tProcessing File %d/%d [%s] ...',tProcessingStart,iFile,nFiles,p.dataFileTag);
                
                experiment = [];
                
                dataPath = [p.outputFolder filesep p.dataFileTag '.mat'];
                if exist(dataPath,'file')
                    load(dataPath);
                end
                
                all_experiments{index} = experiment;
                
                
                tProcessingStop=toc;
                
                fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
                
                
                
            end
            
        end
        
        journal = experiment.p.journal;
        save(dbPath,'journal','all_experiments');
        
    else
        
        %set up timer to time differnet analysis steps
        tic;tProcessingStart=toc;
        fprintf('Loading the database...');
        load(dbPath)
        tProcessingStop=toc;
        fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
        
    end
    
else
    fprintf('all_experiments var already exists\n');
end


tic

nExp=size(all_experiments,2)
nFrames =nan(nExp,1)
for i=1:nExp
    nFrames(i) = all_experiments{i}.pData.nFrames;
end
max_nFrames = max(nFrames);
T_frames = 1:max_nFrames;
T_sec = T_frames./20;
T_min = T_sec ./ 60;

all_sig = nan(nExp,max_nFrames,5);

signalsNamesStr = {'473','405','473 dff','405 dff','diff','dif fit','sliding diff fit'};

for i=1:nExp
    pData = [];
    pData =  all_experiments{i}.pData;
    nFrames = pData.nFrames;
    all_sig(i,1:nFrames,1) = pData.raw.sig;
    all_sig(i,1:nFrames,2) = pData.raw.ref;
    all_sig(i,1:nFrames,3) = pData.DFF.sig;
    all_sig(i,1:nFrames,4) = pData.DFF.ref;    
    all_sig(i,1:nFrames,5) = pData.diffSig;
    all_sig(i,1:nFrames,6) = pData.diffSig_fit;
    all_sig(i,1:nFrames,7) = pData.slidingdiffSig_fit;
end

toc

figure()

plotPos = [1 3 5 7 9 11 13];

cLims_ = nan(7,2);

cLims_(1,:) = [0 20000];
cLims_(2,:) = [0 20000];

cLims_(3,:) = [-0.05 0.05];
cLims_(4,:) = [-0.05 0.05];

cLims_(5,:) = [-5 5];
cLims_(6,:) = [-5 5];
cLims_(7,:) = [-5 5];

for i=1:7
    
    subplot(7,2,plotPos(i))
    hold on
    title(signalsNamesStr{i})
    imagesc(T_min,1:nExp,all_sig(:,:,i),cLims_(i,:))
    colorbar()
    
    subplot(7,2,plotPos(i)+1)
    hold on
    title(['minmax' signalsNamesStr{i}])
    imagesc(T_min,1:nExp,mapminmax(all_sig(:,:,i)))
    colorbar()
end

colors_ = colormap(jet(35));

figure()
for i=1:7    
    
    subplot(7,2,plotPos(i))
    
    hold on
    title(signalsNamesStr{i})
    
    for j=1:35
        plot(T_min,all_sig(j,:,i),'color',colors_(j,:))
    end
    
    subplot(7,2,plotPos(i)+1)    
    hold on
    title(signalsNamesStr{i})
    m = mean(all_sig(:,:,i));
    plot(T_min,m,'Linewidth',2,'color',[0 0 0])
%     std_ = std(all_sig(:,:,i));
%     plot(T_min,m + std_)
%     plot(T_min,m - std_)
end



