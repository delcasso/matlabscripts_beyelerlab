% initialization, close all figures, delete all variables, clear the command window
close all;clear;clc
%set up timer to time differnet analysis steps
tic;t0=toc;

% The is a unique identifier for selecting the proper parameters, look
% inside the getBatchAnalysisConfig_OB.m to understand better
% batchID = 'ICa-ICp_OFT';
% batchID = 'ICa-ICp_OFT-CONTROL';
% batchID = 'ICa-ICp_CAV2Cre_EPM';
% batchID = 'ICa-ICp_CAV2Cre_OFT';
batchID = 'ICa-ICp_CAV2Cre_RTPP';
%batchID = '20181126_IC-SweetBitter-G2_NSFT'

% Where are the data, use machine flag
% option 1 = 'SD_remote', data are on the nas 1 server mounted as Z: on my laptop
% option 2 = 'SD_local', data have been copied on C:, the principal hard drive of my laptop
machine = 'SD_remote';

[dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_OB(batchID,machine);
p.lookingForMouse = '';

allDataMatPath=[outputFolder filesep 'allData.mat'];

if 1 %~exist(allDataMatPath,'file')

results = p.journal;

expDB = {};infoDB = {};idxDB = 0;


%%CREATES OUTPUT FOLDERS
% if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end

nFolders = size(dataRoot,2);
%% TO PROCESS ALL FOLDERS
for iFolder=1:nFolders
    
    p.dataRoot = dataRoot{iFolder};
    if ~exist(p.dataRoot,'dir'), fprintf('%s doesn''t exist',p.dataRoot);pause;end
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    nApparatus = size(apparatus,2);
    if nFolders < nApparatus
        % is there a model of the apparatus for normalizatoin
        if strcmp(apparatus{nFolders+1}.Model,'ForNormalization'),p.apparatusModelForNormalization = apparatus{nFolders+1};end
    end
    
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        
        % Depending of the histologfy status we skeep the analysis for the current file
        status = getHistologyStatus(p.journal,p.dataFileTag);
        if isempty(status) | status<1
            processThisFile=0;
        end
        
        status = [];
        status = getBehavioralStatus(p.journal,p.dataFileTag);
        if status<1
            processThisFile=0;
        end
        
        % If you want to process only one file
        if ~isempty(p.lookingForMouse),processThisFile=0;if strcmp(p.dataFileTag,p.lookingForMouse),processThisFile=1;end;end
        
        %Skeeping file already processed
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
               
        outputFile = [p.outputFolder filesep p.dataFileTag '.mat'];
        
        if ~exist(outputFile)                        
            
            %% MAIN PROCESSING
                        
            tProcessingStart=toc;
            fprintf('[%.1f sec.]\tProcessing File %d/%d [%s]\n',tProcessingStart,iFile,nFiles,p.dataFileTag);
            
            fprintf('\t\t\tloadExpData_OB ');
            experiment = loadExpData_OB(p);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\tstandardizePositionData ');
            experiment = standardizePositionData(experiment);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\tbuildBasicMaps_OB ');
            experiment=buildBasicMaps_OB(experiment);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\tbuildBasicMaps_OB ');
            drawBasicMaps_OB(experiment);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\tgetApparatusZones ');
            experiment = getApparatusZones(experiment);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart);
            
            fprintf('\t\t\ttransformPositionsInZones_OB ');
            experiment = transformPositionsInZones_OB(experiment);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\ttransformPositionsInZones_OB ');
            experiment= extractArmEntries(experiment)
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\textractLaserStatus_OB ');
            experiment = extractLaserStatus_OB(experiment);
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
            
            fprintf('\t\t\tgetZonesStatistics_OB ');
            if strcmp(p.apparatus.type,'RTPP')
                experiment = getZonesStatistics_RTPP_OB(experiment);
                p = experiment.p;
            else
                experiment = getZonesStatistics_OB(experiment);
                p = experiment.p;
            end
            
%             ii = find(p.journal.MouseNum==str2double(p.dataFileTag(2:end)));
%             switch p.apparatus.type
%                 case 'OFT'
%                     results.TimeBorder_sec(ii)=experiment.vData.zoneTime_fr(1)/p.behaviorCameraFrameRate_Hz;
%                     results.TimeCenter_sec(ii)=experiment.vData.zoneTime_fr(2)/p.behaviorCameraFrameRate_Hz;
%             end
            
            fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
                        
            %% SAVING DATA
            info = getJournalInfo(p.journal,p.dataFileTag);
            save(outputFile,'experiment','info');
            
            tProcessingStop=toc;
            fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
            
        else
            fprintf('output File already exist\n');
            clear experiment
            clear info
            load(outputFile);                        
        end
        
        idxDB = idxDB + 1;
        expDB{idxDB} = experiment;
        infoDB{idxDB} = info;  
        
    
        
    end
end
    save(allDataMatPath,'expDB','infoDB','p')
else
    load(allDataMatPath);
end
