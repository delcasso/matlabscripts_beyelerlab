%% INIT
close all;clearvars;clc;tic;t0=toc;

%% PARAMS
analysisMode='ContinuousCaSignal';
% analysisMode='Transients';
machine = 'SD_remote';
% batchID = 'ICa-ICp_OFT';
batchID = 'ICa-ICp_EPM';

p=[];

[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p);

journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

stat = table();

switch batchID
    
    case 'ICa-ICp_EPM'
        
        if params.apparatusNormalizationRequested
            fprintf('apparatus normalization requested\n');
            nApp = size(params.apparatus,2);
            normApparatus = params.apparatus{1,nApp};
            nBins = normApparatus.OA_cm / params.MapScale_cmPerBin;
        else
            warning('nBins has been defined arbitrarly');
            nBins = 80/0.5;
        end
  
        
    case 'ICa-ICp_OFT'
        
        if params.apparatusNormalizationRequested
            fprintf('apparatus normalization requested');
            nApp = size(params.apparatus,2);
            normApparatus = params.apparatus{1,nApp};
            nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
        else
            warning('nBins has been defined arbitrarly');
            nBins = 60/0.5;
        end
        
    
        
end


journalSize = size(journal,1);

bulkAmp=nan(journalSize,2);
transientsProba=nan(journalSize,2);
transientsAmp=nan(journalSize,2);

Maps=nan(journalSize,nBins,nBins);
Region = nan(journalSize,1);
Histo = nan(journalSize,1);
Task = nan(journalSize,1);
BatchNum = nan(journalSize,1);
Mice={};
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};




for iFile=1:nFiles
    
    f = fileList(iFile).name;t2=toc;
    i = find(journal.MouseNum==str2double(f(2:4)));
    
    if ~isempty(i)
        
        iLine(iFile)=i;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(i);Histo(i)=journal.HistologyCode(i);BatchNum(i)=journal.BatchCode(i);TaskComment(i)=journal.BehaviorCode(i);SignalComment(i)=journal.SignalCode(i);
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            if Region(i), intermediate = intermediate+1; else, anterior = anterior+1;end
            
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    tmp1 = map.NormSig.IO;
                    cMap_=[-0.5 0.5]; %OFT
                    %                     cMap_=[-2 2]; %NSFT
                    bulkAmp(i,:)=experiment.pData.bulkSignalStats.zonesMeanAmp;
                case 'Transients'
                    cMap_=[0 0.5]; %OFT
                    %                     cMap_=[0 0.5]; %NSFT
                    tmp1 = map.NormTransients.IO;
                    transientsProba(i,:)=pData.transientStats.zonesProba;
                    transientsAmp(i,:)=pData.transientStats.zonesMeanAmp;
            end
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            Maps(i,:,:) = tmp1;
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end % for i=1:nFiles

iiSweet = find(Region==0);iiBitter   = find(Region==1);
SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;





%% AVG SIGNAL MAPS
figure();
colormap([[1 1 1] ; jet(1024)])
s1=subplot(1,2,1)
hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
imagesc(SweetMaps,cMap_)
s2=subplot(1,2,2)
hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
imagesc(BitterMaps,cMap_)


switch batchID
    
    case 'ICa-ICp_EPM'
        x = experiment.apparatusDesign_cm.x;
        y = experiment.apparatusDesign_cm.y;
        x = x ./ experiment.p.MapScale_cmPerBin;
        y = y ./ experiment.p.MapScale_cmPerBin;
        x = x - min(x);
        y = y - min(y);
 
        
    case 'ICa-ICp_OFT'
  
        
end

subplot(s1);
plot(x,y,'k')

subplot(s2)
plot(x,y,'k')

GroupsIDs = Region+1;
ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};

switch analysisMode
    case 'ContinuousCaSignal'
        journal=plotZoneStats(bulkAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'ContinuousCaSignal-zoneComparaison',journal,'bulk');
    case 'Transients'
        journal=plotZoneStats(transientsProba,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsFrequency-zoneComparaison',journal,'transientFrequency');
        journal=plotZoneStats(transientsAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsAmplitude-zoneComparaison',journal,'transientAmp');
end


