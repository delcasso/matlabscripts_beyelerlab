% function OFT_combiningData_20181108()

%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
% batchID = '20180926_iC-SweetBitter_AllTogether_OFT';
batchID = '20180926_iC-SweetBitter_AllTogether_NSFT';
[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS
analysisMode='ContinuousCaSignal';
% analysisMode='Transients';

if params.apparatusNormalizationRequested
    fprintf('apparatus normalization requested');
    nApp = size(params.apparatus,2);
    normApparatus = params.apparatus{1,nApp};
    nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
else
    warning('nBins has been defined arbitrarly');
    nBins = 60/0.5;
end

signalHisto_edges = 2:0.1:20;% signalHisto_edges = -10:0.1:10;
signalHisto_binSize = mean(diff(signalHisto_edges))
signalHisto_nBins = size(signalHisto_edges,2)-1;
signalHisto_x = signalHisto_edges(1)+(signalHisto_binSize/2):signalHisto_binSize: signalHisto_edges(end)-(signalHisto_binSize/2);

transientsProba=nan(nFiles,2);transientsAmp=nan(nFiles,2);

Maps=nan(nFiles,nBins,nBins);
Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
signalHisto = nan(nFiles,signalHisto_nBins);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        
        
        if Histo(i)==1 && (BatchNum(i)>0) && (SignalComment(i)==1)
            
            if Region(i), intermediate = intermediate+1; else, anterior = anterior+1;end
            
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            
            signalHisto(i,:) = histcounts(pData.mainSig,signalHisto_edges,'Normalization','probability');
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    tmp1 = map.NormSig.IO;
%                     cMap_=[-0.5 0.5]; %OFT
                    cMap_=[-2 2]; %NSFT
                case 'Transients'
%                     cMap_=[0 0.5]; %OFT
                    cMap_=[0 0.5]; %NSFT
                    %                 transientsProba(i,:)=pData.transients.zonesProba;
                    %                 transientsAmp(i,:)=pData.transients.zonesMeanAmp;
                    tmp1 = map.NormTransients.IO;
                   transientsProba(i,:)=pData.transients.zonesProba;
                   transientsAmp(i,:)=pData.transients.zonesMeanAmp;
            end
            
            
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            Maps(i,:,:) = tmp1;
            
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end % for i=1:nFiles

iiSweet = find(Region==0);iiBitter   = find(Region==1);
SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;


%% AVG SIGNAL MAPS
figure();

colormap([[1 1 1] ; jet(1024)])

subplot(1,2,1)
hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
imagesc(SweetMaps,cMap_)

subplot(1,2,2)
hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
imagesc(BitterMaps,cMap_)


i1 = nBins/3;i2 = nBins-i1;
centerMask = nan(nBins,nBins);centerMask (i1:i2,i1:i2)=ones(i2-i1+1,i2-i1+1);
borderMask = ones(nBins,nBins);borderMask (i1:i2,i1:i2)=nan(i2-i1+1,i2-i1+1);
centerMaps = nan(nFiles,nBins,nBins);
borderMaps = nan(nFiles,nBins,nBins);
i=1;

for i=1:nFiles
    mapTMP = squeeze(Maps(i,:,:));
    centerMaps(i,:,:) = mapTMP .* centerMask;
    borderMaps(i,:,:) = mapTMP .* borderMask;    
end

centerActivity = squeeze(nanmean(centerMaps,2));
centerActivity = squeeze(nanmean(centerActivity,2));
borderActivity = squeeze(nanmean(borderMaps,2));
borderActivity = squeeze(nanmean(borderActivity,2));

meanSweetCenter = nanmean(centerActivity(iiSweet));semSweetCenter = nanstd(centerActivity(iiSweet),[],1)./sqrt(size(iiSweet,1));
meanSweetBorder = nanmean(borderActivity(iiSweet));semSweetBorder = nanstd(borderActivity(iiSweet),[],1)./sqrt(size(iiSweet,1));
meanBitterCenter = nanmean(centerActivity(iiBitter));semBitterCenter = nanstd(centerActivity(iiBitter),[],1)./sqrt(size(iiBitter,1));
meanBitterBorder = nanmean(borderActivity(iiBitter));semBitterBorder = nanstd(borderActivity(iiBitter),[],1)./sqrt(size(iiBitter,1));

iiSweet = find(Region==0);iiBitter   = find(Region==1);

figure()
hold on
ylim(cMap_)
if meanSweetCenter>0
    rectangle('Position',[0.75 0 0.5 meanSweetCenter],'EdgeColor',[0 0 1]);
    plot([1 1],[meanSweetCenter meanSweetCenter+semSweetCenter],'b');
else
    rectangle('Position',[0.75 meanSweetCenter 0.5 -meanSweetCenter],'EdgeColor',[0 0 1]);
    plot([1 1],[meanSweetCenter meanSweetCenter-semSweetCenter],'b');
end
if meanSweetBorder>0
    rectangle('Position',[1.75 0 0.5 meanSweetBorder],'EdgeColor',[0 0 1]);
     plot([2 2],[meanSweetBorder meanSweetBorder+semSweetBorder],'b');
else
    rectangle('Position',[1.75 meanSweetBorder 0.5 -meanSweetBorder],'EdgeColor',[0 0 1]);
    plot([2 2],[meanSweetBorder meanSweetBorder-semSweetBorder],'b');
end
plot([1 2],[centerActivity(iiSweet) borderActivity(iiSweet)],'b:');


figure()
hold on
ylim(cMap_)
if meanBitterCenter>0
    rectangle('Position',[0.75 0 0.5 meanBitterCenter],'EdgeColor',[0 0 1]);
    plot([1 1],[meanBitterCenter meanBitterCenter+semBitterCenter],'b');
else
    rectangle('Position',[0.75 meanBitterCenter 0.5 -meanBitterCenter],'EdgeColor',[0 0 1]);
    plot([1 1],[meanBitterCenter meanBitterCenter-semBitterCenter],'b');
end
if meanBitterBorder>0
    rectangle('Position',[1.75 0 0.5 meanBitterBorder],'EdgeColor',[0 0 1]);
     plot([2 2],[meanBitterBorder meanBitterBorder+semBitterBorder],'b');
else
    rectangle('Position',[1.75 meanBitterBorder 0.5 -meanBitterBorder],'EdgeColor',[0 0 1]);
    plot([2 2],[meanBitterBorder meanBitterBorder-semBitterBorder],'b');
end
plot([1 2],[centerActivity(iiBitter) borderActivity(iiBitter)],'b:');

% zones(1).type = 'border';
% zones(2).type = 'center';


    if strcmp(analysisMode,'Transients')
        
        ii=isnan(transientsAmp(:,1));
        transientsAmp(ii,:)=[];
        transientsProba(ii,:)=[];
        Region(ii)=[];
        
        mAmp_Center = nanmean(transientsAmp(:,2),2);
        mAmp_Border = nanmean(transientsAmp(:,1),2);
        mProba_Center = nanmean(transientsProba(:,2),2);
        mProba_Border = nanmean(transientsProba(:,1),2);
        
        iiSweet = find(Region==0);
        iiBitter   = find(Region==1);
        
        meanSweetCenter = nanmean(mAmp_Center(iiSweet));semSweetCenter = nanstd(mAmp_Center(iiSweet),[],1)./sqrt(size(iiSweet,1));
        meanSweetBorder = nanmean(mAmp_Border(iiSweet));semSweetBorder = nanstd(mAmp_Border(iiSweet),[],1)./sqrt(size(iiSweet,1));
        meanBitterCenter = nanmean(mAmp_Center(iiBitter));semBitterCenter = nanstd(mAmp_Center(iiBitter),[],1)./sqrt(size(iiBitter,1));
        meanBitterBorder = nanmean(mAmp_Border(iiBitter));semBitterBorder = nanstd(mAmp_Border(iiBitter),[],1)./sqrt(size(iiBitter,1));
        
        figure()
        hold on
        
        rectangle('Position',[0.75 0 0.5 meanSweetCenter],'EdgeColor',[0 0 1]);
        plot([1 1],[meanSweetCenter meanSweetCenter+semSweetCenter],'b');
        rectangle('Position',[1.75 0 0.5 meanSweetBorder],'EdgeColor',[0 0 1]);
        plot([2 2],[meanSweetBorder meanSweetBorder+semSweetBorder],'b');
        plot([1 2],[mAmp_Center(iiSweet) mAmp_Border(iiSweet)],'b:')
        
        rectangle('Position',[2.75 0 0.5 meanBitterCenter],'EdgeColor',[1 0 0]);
        plot([3 3],[meanBitterCenter meanBitterCenter+semBitterCenter],'r');
        rectangle('Position',[3.75 0 0.5 meanBitterBorder],'EdgeColor',[1 0 0]);
        plot([4 4],[meanBitterBorder meanBitterBorder+semBitterBorder],'r');
        plot([3 4],[mAmp_Center(iiBitter) mAmp_Border(iiBitter)],'r:')
        legend({'Sweet','Bitter'})
        
        meanSweetCenter = nanmean(mProba_Center(iiSweet));semSweetCenter = nanstd(mProba_Center(iiSweet),[],1)./sqrt(size(iiSweet,1));
        meanSweetBorder = nanmean(mProba_Border(iiSweet));semSweetBorder = nanstd(mProba_Border(iiSweet),[],1)./sqrt(size(iiSweet,1));
        meanBitterCenter = nanmean(mProba_Center(iiBitter));semBitterCenter = nanstd(mProba_Center(iiBitter),[],1)./sqrt(size(iiBitter,1));
        meanBitterBorder = nanmean(mProba_Border(iiBitter));semBitterBorder = nanstd(mProba_Border(iiBitter),[],1)./sqrt(size(iiBitter,1));
        
        figure()
        hold on
        
        rectangle('Position',[0.75 0 0.5 meanSweetCenter],'EdgeColor',[0 0 1]);
        plot([1 1],[meanSweetCenter meanSweetCenter+semSweetCenter],'b');
        rectangle('Position',[1.75 0 0.5 meanSweetBorder],'EdgeColor',[0 0 1]);
        plot([2 2],[meanSweetBorder meanSweetBorder+semSweetBorder],'b');
        plot([1 2],[mProba_Center(iiSweet) mProba_Border(iiSweet)],'b:')
        
        rectangle('Position',[2.75 0 0.5 meanBitterCenter],'EdgeColor',[1 0 0]);
        plot([3 3],[meanBitterCenter meanBitterCenter+semBitterCenter],'b');
        rectangle('Position',[3.75 0 0.5 meanBitterBorder],'EdgeColor',[1 0 0]);
        plot([4 4],[meanBitterBorder meanBitterBorder+semBitterBorder],'b');
        plot([3 4],[mProba_Center(iiBitter) mProba_Border(iiBitter)],'r:')
        legend({'Sweet','Bitter'})
        
    end


