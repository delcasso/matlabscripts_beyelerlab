%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
% batchID = 'ICa-ICp_Sucrose';
% batchID = 'ICa-ICp_NSFT-CONTROL';
batchID = 'ICa-ICp_OFT';
% batchID = 'ICa-ICp_OFT-CONTROL';
% batchID = 'ICa-ICp_NSFT';

p=[];

outputFolder = [];
[dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);

journal = readtable([outputFolder filesep 'Journal.xlsx']);
load([outputFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([outputFolder filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS
analysisMode='ContinuousCaSignal';
% analysisMode='Transients';

if params.apparatusNormalizationRequested
    fprintf('apparatus normalization requested');
    nApp = size(params.apparatus,2);
    normApparatus = params.apparatus{1,nApp};
    nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
else
    warning('nBins has been defined arbitrarly');
    nBins = 60/0.5;
end

journalSize = size(journal,1);

bulkAmp=nan(journalSize,2);transientsProba=nan(journalSize,2);transientsAmp=nan(journalSize,2);

Maps=nan(journalSize,nBins,nBins);
z1_times =  nan(journalSize,1);z2_times =  nan(journalSize,1);nFrames =  nan(journalSize,1);
rawGlobalSignalAverage = nan(journalSize,1);
rawTransientAmplitude = nan(journalSize,1);
rawTransientFrequency = nan(journalSize,1);


Region = nan(journalSize,1);
Histo = nan(journalSize,1);
Task = nan(journalSize,1);
BatchNum = nan(journalSize,1);
Mice={};Sex = nan(nFiles,1);
female=0;male=0;maleMouseList={};femaleMouseList={};

for iFile=1:nFiles
    
    f = fileList(iFile).name;t2=toc;
    i = find(journal.MouseNum==str2double(f(2:4)));
    
    if ~isempty(i)
        
        iLine(iFile)=i;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(i);Histo(i)=journal.HistologyCode(i);BatchNum(i)=journal.BatchCode(i);TaskComment(i)=journal.BehaviorCode(i);SignalComment(i)=journal.SignalCode(i);
        if strcmp(journal.Sex(i),'M'), Sex(i)=0; end
        if strcmp(journal.Sex(i),'F'), Sex(i)=1; end
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            %         if Histo(i)==1 && (BatchNum(i)>0) && (SignalComment(i)==1)
            
            if Sex(i), female = female+1; else, male = male+1;end
            
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);
            load([outputFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            
            %% To measure the time spend in each zone
            zones = vData.zones_cmSP;
            nFrames(i) = sum(experiment.vData.zoneTime_fr);
            z = experiment.vData.zoneTime_fr; Z(i,:) = z;
            z1_times(i) = experiment.vData.zoneTime_fr(1);
            z2_times(i) = experiment.vData.zoneTime_fr(2);
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    tmp1 = map.NormSig.IO;
                    cMap_=[-1 1]; %OFT
                    %                     cMap_=[-2 2]; %NSFT
                    bulkAmp(i,:)=experiment.pData.bulkSignalStats.zonesMeanAmp;
                    rawGlobalSignalAverage(i) = nanmean(experiment.pData.mainSig);
                case 'Transients'
                    cMap_=[0 0.5]; %OFT
                    %                     cMap_=[0 0.5]; %NSFT
                    tmp1 = map.NormTransients.IO;
                    transientsProba(i,:)=pData.transientStats.zonesProba;
                    transientsAmp(i,:)=pData.transientStats.zonesMeanAmp;
                    rawTransientAmplitude(i) = nanmean([experiment.pData.transients.vMax]);
                    rawTransientFrequency(i) = (size(experiment.pData.transients,2)/experiment.pData.nFrames)*1200;
            end
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            Maps(i,:,:) = tmp1;
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end % for i=1:nFiles

iiMale = find(Sex==0);
iiFemale = find(Sex==1);

maleMaps = squeeze(nanmean(Maps(iiMale,:,:)));
ii = isnan(maleMaps);
maleMaps(ii)=0;
maleMaps = imgaussfilt(maleMaps,2);
maleMaps(ii)=nan;

femaleMaps = squeeze(nanmean(Maps(iiFemale,:,:)));
ii = isnan(femaleMaps);
femaleMaps(ii)=0;
femaleMaps = imgaussfilt(femaleMaps,2);
femaleMaps(ii)=nan;

% SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));
% % ii = isnan(SweetMaps);
% % SweetMaps(ii)=0;
% % SweetMaps = imgaussfilt(SweetMaps,2);
% % SweetMaps(ii)=nan;
%
% BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));
% % ii = isnan(BitterMaps);
% % BitterMaps(ii)=0;
% % BitterMaps = imgaussfilt(BitterMaps,2);
% % BitterMaps(ii)=nan;
%
% sigma = 0.05;
% width = 2*ceil(2*sigma)+1;
% imageFilter=fspecial('gaussian',width,sigma);
% SweetMaps = imfilter(SweetMaps,imageFilter,'symmetric','conv');
% BitterMaps = imfilter(BitterMaps,imageFilter,'symmetric','conv');



%% AVG SIGNAL MAPS
figure();colormap([[1 1 1] ; jet(1024)]);
subplot(1,2,1);hold on;title(sprintf('male (n=%d)',male));axis equal;axis off;colorbar();imagesc(maleMaps,cMap_);
subplot(1,2,2);hold on;title(sprintf('female (n=%d)',female));axis equal;axis off;colorbar();imagesc(femaleMaps,cMap_);

GroupsIDs = Region+1;
ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};

zoneStats = [z1_times z2_times];
Groups = Region+1;
titleStr = 'TimeInZoneFrames';
journal=plotZoneStats(zoneStats,GroupsIDs,Mice,ZonesStr,outputFolder,titleStr,journal,'timeSpendFrameNumber');
timeInZoneSum = nansum(zoneStats,2);
timeInZoneSum = repmat(timeInZoneSum,1,2);
timeInZonePerc = (zoneStats ./ timeInZoneSum) * 100;
% timeInZonePerc = (zoneStats ./ nFrames) * 100;
titleStr = 'TimeInZonePercentage';
journal=plotZoneStats(timeInZonePerc,GroupsIDs,Mice,ZonesStr,outputFolder,titleStr,journal,'timeSpendPercentage');


switch analysisMode
    
    case 'ContinuousCaSignal'
        
        journal=plotZoneStats(bulkAmp,GroupsIDs,Mice,ZonesStr,outputFolder,'ContinuousCaSignal-zoneComparaison',journal,'bulk');
        
        %         tmp = [Region rawGlobalSignalAverage timeInZonePerc(:,1)];
        iZone = 1;
        meanGlobal = mean(bulkAmp,2);
        tmp = [Region meanGlobal timeInZonePerc(:,iZone)];
        
        tmp = [Region bulkAmp(:,2) timeInZonePerc(:,iZone)];        
        
        yMean = nanmean(rawGlobalSignalAverage);
        
        tmp(isnan(sum(tmp,2)),:)=[];
        
        figure();
        ii = find(tmp(:,1)==0);
        lm = fitlm(table(tmp(ii,3),tmp(ii,2)),'linear');
        subplot(1,2,1)
        plot(lm)
        xlim([60 100])
        %         ylim([-1.5 1.5])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1))
        text(60,yMean,msg)
        xlabel(sprintf('%% time in %s',ZonesStr{iZone}))
                
        ii = find(tmp(:,1)==1);
        lm = fitlm(table(tmp(ii,3),tmp(ii,2)),'linear');
        subplot(1,2,2)
        plot(lm)
        xlim([60 100])
        %         ylim([-1.5 1.5])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1))
        text(60,yMean,msg)
        xlabel(sprintf('%% time in %s',ZonesStr{iZone}))       
        
    case 'Transients'
        
        journal=plotZoneStats(transientsProba*1200,GroupsIDs,Mice,ZonesStr,outputFolder,'TransientsFrequency-zoneComparaison',journal,'transientFrequency');
        journal=plotZoneStats(transientsAmp,GroupsIDs,Mice,ZonesStr,outputFolder,'TransientsAmplitude-zoneComparaison',journal,'transientAmp');
        
        iZone = 1;
        tmp = [Region nanmean(transientsProba,2)*1200' timeInZonePerc(:,iZone)];
        %         tmp = [Region nanmean(transientsProba(:,[2 4]),2)*1200 timeInZonePerc(:,1)];
        %         tmp = [Region rawTransientFrequency timeInZonePerc(:,1)];
        tmp(isnan(sum(tmp,2)),:)=[];
        yMean = nanmean(nanmean(transientsProba)*1200);
        
        figure();
        
        ii = find(tmp(:,1)==0);
        lm = fitlm(table(tmp(ii,3),tmp(ii,2)),'linear');
        subplot(2,2,1)
        plot(lm)
        %         xlim([60 100])
        %         ylim([0 40])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(60,yMean,msg)
        xlabel(sprintf('%% time in %s',ZonesStr{iZone}))
        
        ii = find(tmp(:,1)==1);
        lm = fitlm(table(tmp(ii,3),tmp(ii,2)),'linear');
        subplot(2,2,2)
        plot(lm)
        %         xlim([60 100])
        %         ylim([0 40])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(60,yMean,msg)
        xlabel(sprintf('%% time in %s',ZonesStr{iZone}))

        tmp = [Region nanmean(transientsAmp,2) timeInZonePerc(:,iZone)];
        %         %          tmp = [Region nanmean(transientsAmp(:,[2 4]),2) timeInZonePerc(:,1)];
        %         tmp = [Region rawTransientAmplitude timeInZonePerc(:,1)];
        yMean = nanmean(nanmean(transientsAmp,2));
        
        tmp(isnan(sum(tmp,2)),:)=[];
        
        ii = find(tmp(:,1)==0);
        lm = fitlm(table(tmp(ii,3),tmp(ii,2)),'linear');
        subplot(2,2,3)
        plot(lm)
        %         xlim([60 100])
        %         ylim([-1 3])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(60,yMean,msg)
        xlabel(sprintf('%% time in %s',ZonesStr{iZone}))
        
        ii = find(tmp(:,1)==1);
        lm = fitlm(table(tmp(ii,3),tmp(ii,2)),'linear');
        subplot(2,2,4)
        plot(lm)
        %         xlim([60 100])
        %         ylim([-1 3])
        anova_lm = anova(lm);
        pValue = lm.Coefficients.pValue(2);
        msg = sprintf('R2 = %2.4f, F(1,%d)=%2.2f, p=%2.4f',lm.Rsquared.Adjusted,anova_lm.DF(2),anova_lm.F(1),anova_lm.pValue(1));
        text(60,yMean,msg)
        xlabel(sprintf('%% time in %s',ZonesStr{iZone}))
        
        

        
        
        
end

% zones(1).type = 'border'; zones(2).type = 'center';
function journal=plotZoneStats(zoneStats,Groups,Mice,ZonesStr,outputFolder,titleStr,journal,statName)

nZones = size(zoneStats,2);

for iZone=1:nZones
    cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
    eval(cmd);
end

nMice = size(zoneStats,1)
iNaN = isnan(zoneStats);
iNaN = logical(sum (iNaN,2));
zoneStats(iNaN,:)=[];
Mice(iNaN) = [];
Groups(iNaN) = [];

uGroups= unique(Groups);
uGroups(isnan(uGroups))=[];
nGroups = size(uGroups,1);

FaceGroupColors(1,1:3) = [101 65 153]./255;
EdgesGroupColors(1,1:3) = [101 65 153]./255;
LineGroupColors(1,1:3) = [0 0 0];
LineGroupStlye{1} = ':';

FaceGroupColors(2,1:3) = [0 165 77]./255;
EdgesGroupColors(2,1:3) = [0 165 77]./255;
LineGroupColors(2,1:3) = [0 0 0];
LineGroupStlye{2} = ':';

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

indices=isnan(zoneStats(:,1));zoneStats(indices,:)=[];Groups(indices)=[];Mice(indices)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure()
hold on
title(titleStr)
set(gca,'xtick',[])

x=0;

for iGroup=1:nGroups
    
    x=x+iGroup;
    
    for iZone=1:nZones
        
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
        
    end
    
    [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
    text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
    text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
    
    for iZone=2:nZones
        
        indices = iiGroup{iGroup};
        nLines = size(indices,1);
        
        lineColors = colormap(lines(nLines));
        
        for k=1:nLines
            idx = indices(k)
            x1 = (x+iZone-1);
            x2 = (x+iZone);
            y1 = zoneStats(idx,iZone-1);
            y2 = zoneStats(idx,iZone);
            
            % tan(alpha) = oppose / adjacent
            
            tan_alpha = (y2-y1)/(x2-x1);
            alpha_radian = atan(tan_alpha);
            alpha_deg = rad2deg(alpha_radian);
            
            xMid = (x2 + x1)/2;
            yMid = (y2 + y1)/2;
            h = text(xMid,yMid,num2str(Mice{idx}),'Rotation',alpha_deg,'HorizontalAlignment','center','color', lineColors(k,:));
            plot([x1 x2],[y1 y2],'color',lineColors(k,:),'LineStyle','-');
        end
        
    end
    
end

print(f1,[outputFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[outputFolder filesep titleStr '.fig'])

end












