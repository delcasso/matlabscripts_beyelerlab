%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
batchID = 'ICa-ICp_OFT';
% batchID = 'ICa-ICp_OFT-CONTROL';
% batchID = 'ICa-ICp_NSFT-CONTROL'
% batchID = 'ICa-ICp_NSFT';

p=[];
outputFolder=[];
[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);

journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

stat = table();

Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

Border =  nan(nFiles,1);Center =  nan(nFiles,1);Ratio =  nan(nFiles,1);Z =  nan(nFiles,2);

curRegionNum = 0;

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    
    Mice{i}=str2double(f(2:4));
    
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            %         if Histo(i)>0.5 && (TaskComment(i)==1) && (BatchNum(i)>0)  && (SignalComment(i)==1)
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);regionSTR = 'intermediate';
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);regionSTR = 'anterior';
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            
            zones = vData.zones_cmSP;
            nFrames(i) = sum(experiment.vData.zoneTime_fr);
            
            
            % 201907822 SD update, time in %
            Border(i) = vData.zoneTime_fr(1);
            Center(i) = vData.zoneTime_fr(2);
            % totDuration =  vData.zoneTime_fr(1) + vData.zoneTime_fr(2);
            %
            % Border(i) = (vData.zoneTime_fr(1) / totDuration) * 100;
            % Center(i) =  (vData.zoneTime_fr(2) / totDuration) * 100;
            
            
            Ratio(i) = (Center(i)-Border(i))/(Center(i)+Border(i));
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
            
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end

stat.Mice = Mice';
stat.Region = Region;
stat.Histo = Histo;
stat.BatchNum= BatchNum;
stat.TaskComment = TaskComment;
stat.SignalComment=SignalComment;
stat.Border = Border;
stat.Center = Center;
stat.Ratio = Ratio;

GroupsIDs = Region+1;
ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};
data = [stat.Border(:) stat.Center(:)]

dataSum = nansum(data,2);
dataSum = repmat(dataSum,1,2);
dataPerc = (data ./ dataSum) * 100;

yLim_ = [];
journal=plotZoneStats(data,GroupsIDs,Mice,ZonesStr,analysisFolder,'time in zone',journal,'timeInZone',yLim_);
writetable(stat,[analysisFolder filesep 'timeInZone_NSFT_20190103_02.xls']);
yLim_ = [0 100];
journal=plotZoneStats(dataPerc,GroupsIDs,Mice,ZonesStr,analysisFolder,'time in zone',journal,'timeInZone',yLim_);


function journal=plotZoneStats(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr,journal,statName,yLim_)

nZones = size(zoneStats,2);
%
% for iZone=1:nZones
%     cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
%     eval(cmd);
% end

uGroups= unique(Groups);
uGroups(isnan(uGroups))=[];
nGroups = size(uGroups,1);

FaceGroupColors(1,1:3) = [101 65 153]./255;
EdgesGroupColors(1,1:3) = [101 65 153]./255;
LineGroupColors(1,1:3) = [0 0 0];
LineGroupStlye{1} = ':';

FaceGroupColors(2,1:3) = [0 165 77]./255;
EdgesGroupColors(2,1:3) = [0 165 77]./255;
LineGroupColors(2,1:3) = [0 0 0];
LineGroupStlye{2} = ':';

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure()
hold on
title(titleStr)
set(gca,'xtick',[])
if ~isempty(yLim_)
    ylim([yLim_]);
end


x=0;

for iGroup=1:nGroups
    
    x=x+iGroup;
    
    for iZone=1:nZones
        
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
        
    end
    
    [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
    text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
    text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
    
            for iZone=2:nZones
                plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',LineGroupColors(iGroup,:),'LineStyle',':');
            end
    
%     for iZone=2:nZones
%         
%         indices = iiGroup{iGroup};
%         nLines = size(indices,1);
%         
%         lineColors = colormap(lines(nLines));
%         
%         for k=1:nLines
%             idx = indices(k)
%             x1 = (x+iZone-1);
%             x2 = (x+iZone);
%             y1 = zoneStats(idx,iZone-1);
%             y2 = zoneStats(idx,iZone);
%             
%             % tan(alpha) = oppose / adjacent
%             
%             tan_alpha = (y2-y1)/(x2-x1);
%             alpha_radian = atan(tan_alpha);
%             alpha_deg = rad2deg(alpha_radian);
%             
%             xMid = (x2 + x1)/2;
%             yMid = (y2 + y1)/2;
%             h = text(x2,y2,num2str(Mice{idx}),'Rotation',0,'HorizontalAlignment','center','color', lineColors(k,:));
%             plot([x1 x2],[y1 y2],'color',lineColors(k,:),'LineStyle','-');
%         end
%         
%     end
    
    
    
    
end

print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[analysisFolder filesep titleStr '.fig'])

end










