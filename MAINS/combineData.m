%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
% batchID = '20180926_iC-SweetBitter_AllTogether_OFT';
% batchID = '20180926_iC-SweetBitter_AllTogether_NSFT';
batchID= '20180926_iC-SweetBitter_AllTogether_EPM';
[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS
% analysisMode='ContinuousCaSignal';
analysisMode='Transients';

if params.apparatusNormalizationRequested
    fprintf('apparatus normalization requested');
    nApp = size(params.apparatus,2);
    normApparatus = params.apparatus{1,nApp};
    switch params.apparatus{1}.type
        case 'EPM'
            nBins = normApparatus.OA_cm / params.MapScale_cmPerBin;
        case 'OFT'
            nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
        case 'SI'
            nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
        case 'NSFT'
            nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
    end
else
    warning('nBins has been defined arbitrarly');
    nBins = 60/0.5;
end

journalSize = size(journal,1);

bulkAmp=nan(journalSize,2);transientsProba=nan(journalSize,2);transientsAmp=nan(journalSize,2);


Maps=nan(journalSize,nBins,nBins);
Region = nan(journalSize,1);
Histo = nan(journalSize,1);
Task = nan(journalSize,1);
BatchNum = nan(journalSize,1);
Mice={};
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

for iFile=1:nFiles
    
    f = fileList(iFile).name;t2=toc;
    i = find(journal.MouseNum==str2double(f(2:4)));
    
    if ~isempty(i)
        
        iLine(iFile)=i;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(i);Histo(i)=journal.HistologyCode(i);BatchNum(i)=journal.BatchCode(i);TaskComment(i)=journal.BehaviorCode(i);SignalComment(i)=journal.SignalCode(i);
        
        
        if Histo(i)>0.5 && (TaskComment(i)==1) && (BatchNum(i)>0)  && (SignalComment(i)==1)
            
            if Region(i), intermediate = intermediate+1; else, anterior = anterior+1;end
            
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            
            switch analysisMode
                
                case 'ContinuousCaSignal'
                    tmp1 = map.NormSig.IO;
                    bulkAmp(i,1)=mean(experiment.pData.bulkSignalStats.zonesMeanAmp([1 3]));
                    bulkAmp(i,2)=mean(experiment.pData.bulkSignalStats.zonesMeanAmp([2 4]));
                    switch params.apparatus{1}.type
                        case 'EPM'
                             cMap_=[-1 1];
                        case 'OFT'
                             cMap_=[-0.5 0.5];
                        case 'SI'
                             cMap_=[-0.5 0.5];
                        case 'NSFT'
                            cMap_=[-2 2];
                    end

                case 'Transients'
                    tmp1 = map.NormTransients.IO;                    
                    transientsProba(i,1)=mean(pData.transientStats.zonesProba([2 4]));
                    transientsProba(i,2)=mean(pData.transientStats.zonesProba([1 3]));
                    transientsAmp(i,1)=mean(pData.transientStats.zonesMeanAmp([1 3]));
                    transientsAmp(i,2)=mean(pData.transientStats.zonesMeanAmp([2 4]));
                    switch params.apparatus{1}.type
                        case 'EPM'
                             cMap_=[0 0.4];
                        case 'OFT'
                             cMap_=[0 0.5];
                        case 'SI'
                             cMap_=[0 0.5]; 
                        case 'NSFT'
                            cMap_=[0 0.4];
                    end                    
                    
            end
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            Maps(i,:,:) = tmp1;
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end % for i=1:nFiles

iiSweet = find(Region==0);iiBitter   = find(Region==1);
SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;


%% AVG SIGNAL MAPS
figure();
colormap([[1 1 1] ; jet(1024)])
subplot(1,2,1)
hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
imagesc(SweetMaps,cMap_)
subplot(1,2,2)
hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
imagesc(BitterMaps,cMap_)

GroupsIDs = Region+1;
ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};

switch analysisMode
    case 'ContinuousCaSignal'
        journal=plotZoneStats(bulkAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'ContinuousCaSignal-zoneComparaison',journal,'bulk');
    case 'Transients'
        journal=plotZoneStats(transientsProba,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsProbability-zoneComparaison',journal,'transientProba');
        journal=plotZoneStats(transientsAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsAmplitude-zoneComparaison',journal,'transientAmp');
end


% zones(1).type = 'border';
% zones(2).type = 'center';

function journal=plotZoneStats(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr,journal,statName)

nZones = size(zoneStats,2);

for iZone=1:nZones
    cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
    eval(cmd);
end

uGroups= unique(Groups);
uGroups(isnan(uGroups))=[];
nGroups = size(uGroups,1);

FaceGroupColors(1,1:3) = [163 135 179]./255;
EdgesGroupColors(1,1:3) = [85 44 136]./255;

FaceGroupColors(2,1:3) = [152 194 122]./255;
EdgesGroupColors(2,1:3) = [49 157 71]./255;

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure()
hold on
title(titleStr)
set(gca,'xtick',[])

x=0;

for iGroup=1:nGroups
    
    x=x+iGroup;
    
    for iZone=1:nZones
        
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:));
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:));
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
        
    end
    
    [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
    text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
    text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
    
    for iZone=2:nZones
        plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',EdgesGroupColors(iGroup,:));
    end
    
end

print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[analysisFolder filesep titleStr '.fig'])

end












