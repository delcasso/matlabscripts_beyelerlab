function experiment = getZonesStatistics(experiment)

vData=experiment.vData;pData=experiment.pData;p=experiment.p;

X_cmSP = vData.mainX_cmSP;Y_cmSP = vData.mainY_cmSP;

vData.inZone=position2zone(X_cmSP,Y_cmSP,vData.zones_cmSP);
vData.nZones = size(vData.zones_cmSP,2);
[vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone,vData.nZones);
pData.transientStats=transientsPerZone(vData.inZone,vData.nZones,vData.zoneTime_fr,pData.transients);
pData.bulkSignalStats=bulkSignalPerZone(vData.inZone,vData.nZones,vData.zoneTime_fr,pData.mainSig);

experiment.vData=vData;experiment.pData=pData;

end
