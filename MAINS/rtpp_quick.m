clear
% data_root = 'Z:\OptogeneticsAndBehavior\DATA\20190715_ICp-CeM-G1\20190722_RTPPA';
data_root = 'Z:\OptogeneticsAndBehavior\DATA\20190715_ICp-CeM-G2\20190722_RTPP';
l = dir([data_root filesep '*deeplabcutFiltered.csv']);
n = size(l,1);
pTOT = nan(n,1);
pPeriod = nan(n,4);

for i=1:n
    
    dlcPath = [data_root filesep l(i).name];
    tmpMatrix = csvread(dlcPath,3,1);
    optoSigRaw = tmpMatrix(:,15);
    videoInfo = VideoReader([data_root filesep l(i).name(1:4) '.avi']);
    nSamples = size(optoSigRaw,1);
    t=1:nSamples;
    t=t/videoInfo.FrameRate;
    
    figure()
    
    subplot(2,1,1)
    plot(t,optoSigRaw)
    optoSig=optoSigRaw>max(optoSigRaw)/10;
    
    subplot(2,1,2)
    plot(t,optoSig)
    disp('here');
       
%     ii1 = find(t>60*5,1,'first');
%     pPeriod(i,1) = ( sum( optoSig(1:ii1) ) / ii1 ) * 100;
%     ii2 = find(t>60*10,1,'first');
%     pPeriod(i,2) = ( sum( optoSig(ii1:ii2)) /(ii2-ii1) )*100;
%     ii3 = find(t>60*15,1,'first');
%     pPeriod(i,3) = ( sum( optoSig(ii2:ii3)) / (ii3-ii2) )*100;
%     ii4 = find(t>60*20,1,'first');
%     if isempty(ii4)
%         ii4 = nSamples
%     end
%     pPeriod(i,4) = ( sum( optoSig(ii3:ii4))/(ii4-ii2) )*100;
%     pTOT(i)      = ( sum( optoSig(1:ii4)) /ii4) *100;
%     
    
    ii1 = find(t>60*5,1,'first');
    pPeriod(i,1) = sum(optoSig(1:ii1))/videoInfo.FrameRate;
    ii2 = find(t>60*10,1,'first');
    pPeriod(i,2) = sum(optoSig(ii1:ii2))/videoInfo.FrameRate;
    ii3 = find(t>60*15,1,'first');
    pPeriod(i,3) = sum(optoSig(ii2:ii3))/videoInfo.FrameRate;
    ii4 = find(t>60*20,1,'first');
    if isempty(ii4)
        ii4 = nSamples
    end
    pPeriod(i,4) = sum(optoSig(ii3:ii4))/videoInfo.FrameRate;
    pTOT(i)      = ( sum( optoSig(1:ii4)) /ii4) *100;    
     
end

