% clear
% close all
% clc
dataRoot = 'Z:\Optogenetics & Behavior\ANALYSIS\20181126_iCa-ICp_allGroups\RTPP';
% journal = readtable([dataRoot filesep 'Journal.xlsx']);
nMice = size(journal,1);

conditions(1).name = 'ICa_eYFP'
conditions(2).name = 'ICa_ChR2'
conditions(3).name = 'ICp_eYFP'
conditions(4).name = 'ICp_ChR2'
conditions(1).indices = []
conditions(2).indices = []
conditions(3).indices = []
conditions(4).indices = []
conditions(1).nMice =  0
conditions(2).nMice = 0;
conditions(3).nMice =  0;
conditions(4).nMice =  0;
conditions(1).color =  [101 65 153]/255
conditions(2).color =  [101 65 153]/255
conditions(3).color =  [0 165 77]/255
conditions(4).color =  [0 165 77]/255
conditions(1).color2 =  [82 255 0]/255
conditions(2).color2 =  [0 183 255]/255
conditions(3).color2 =  [82 255 0]/255
conditions(4).color2 =  [0 183 255]/255

tic();
t(1) = toc();

max_nSamples = 13501;

allMice_optoSig = zeros(nMice,max_nSamples);
allMice_x = zeros(nMice,max_nSamples);
allMice_y = zeros(nMice,max_nSamples);

for iMouse=1:nMice
    
    fprintf('Processing file %d/%d...',iMouse,nMice)
    
    s = journal.Sex(iMouse);
    matFilename =[dataRoot filesep s{1}  num2str(journal.MouseNum(iMouse)) '.mat'];
    
    if ~exist(matFilename,'file')
        warning(sprintf('%s doesn''t exist',matFilename))
    else
        clear experiment
        load(matFilename)
        vData = experiment.vData;
        binnedOptoSig = vData.optoPeriod;
        binnedOptoSig = binnedOptoSig>(max(binnedOptoSig)/2);
        nSamples = size(binnedOptoSig,1);
        i2 = nSamples;
        if nSamples>max_nSamples
            i2 = max_nSamples;
        end
        allMice_optoSig(iMouse,1:i2)=binnedOptoSig(1:i2);
        allMice_x(iMouse,1:i2) =  vData.mouseX(1:i2);
        allMice_y(iMouse,1:i2) =  vData.mouseY(1:i2);
        
        switch journal.RegionCode(iMouse)
            case 0
                regionStr = 'ICa';
            case 1
                regionStr = 'ICp';
        end
        
        idx = [];
        v =  journal.virus(iMouse);
        for iC=1:4
            if strcmp(conditions(iC).name,[regionStr '_' v{:}])
                idx = iC;
            end
        end
        
        conditions(idx).indices = [conditions(idx).indices iMouse];
        conditions(idx).nMice = conditions(idx).nMice + 1;
                
        t(iMouse+1) = toc();
        fprintf('done in %2.2f sec\n',t(iMouse+1) - t(iMouse))
        
    end
    
end    
   

dx = diff(allMice_x,1,2);
dx2 = dx .* dx;
dy = diff(allMice_y,1,2);
dy2 = dy .* dy;
allMice_distance = sqrt(dx2 + dy2);
allMice_distance(:,end+1) = nan;

allMice_optoonly_distance = allMice_distance;
allMice_optoonly_distance(~logical(allMice_optoSig)) = nan;

disp('here')

nBlocks = 3;
blockSize = floor(max_nSamples / nBlocks);

binnedOptoSig = nan(nMice,nBlocks);
binnedTraveledDistance = nan(nMice,nBlocks);

i1 = 1;
for iB = 1:nBlocks
    i2 = i1 + (blockSize-1);
    binnedOptoSig(:,iB) = nanmean(allMice_optoSig(:,i1:i2),2);
%     binnedTraveledDistance(:,iB) = nansum(allMice_optoonly_distance(:,i1:i2),2)./nansum(allMice_distance(:,i1:i2),2);    
        binnedTraveledDistance(:,iB) = nansum(allMice_optoonly_distance(:,i1:i2),2);
    i1 = i2+1;
end

figure()
hold on
for iC=1:4
    n = conditions(iC).nMice;
    for i=1:n
        plot(binnedOptoSig(conditions(iC).indices(i),:),'color',conditions(iC).color)
    end
end

figure()
hold on
for iC=1:4
    n = conditions(iC).nMice;
    for i=1:n
        plot(binnedTraveledDistance(conditions(iC).indices(i),:),'color',conditions(iC).color)
    end
end
    
    % figure()
    % subplot(2,2,1)
    % title('ICa eYFP')
    % ii = intersect(find(region==0),find(virus==0));
    % imagesc(all_opto(ii,:));
    % subplot(2,2,2)
    % title('ICp eYFP')
    % ii = intersect(find(region==1),find(virus==0));
    % imagesc(all_opto(ii,:));
    % subplot(2,2,3)
    % title('ICa ChR2')
    % ii = intersect(find(region==0),find(virus==1));
    % imagesc(all_opto(ii,:));
    % subplot(2,2,4)
    % title('ICp ChR2')
    % ii = intersect(find(region==1),find(virus==1));
    % imagesc(all_opto(ii,:));
    
    %
    % figure()
    % hold on
    % ii = intersect(find(region==0),find(virus==0));
    % plot(smooth(mean(optoMatrix(ii,:))));
    % ii = intersect(find(region==1),find(virus==0));
    % plot(smooth(mean(optoMatrix(ii,:))));
    % ii = intersect(find(region==0),find(virus==1));
    % plot(smooth(mean(optoMatrix(ii,:))));
    % ii = intersect(find(region==1),find(virus==1));
    % plot(smooth(mean(optoMatrix(ii,:))));
    % legend({'ICa eYFP','ICp eYFP','ICa ChR2','ICp ChR2'})
    
