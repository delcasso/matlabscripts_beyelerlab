%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
batchID= '20180926_iC-SweetBitter_AllTogether_EPM';

[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS

analysisMode='ContinuousCaSignal';
% analysisMode='Transients';

%Warning, here the reference is the center of the maze
nBins = 60/0.5;

signalHisto_edges = 2:0.1:20;% signalHisto_edges = -10:0.1:10;
signalHisto_binSize = mean(diff(signalHisto_edges))
signalHisto_nBins = size(signalHisto_edges,2)-1;
signalHisto_x = signalHisto_edges(1)+(signalHisto_binSize/2):signalHisto_binSize: signalHisto_edges(end)-(signalHisto_binSize/2);

transientsProba=nan(nFiles,5);transientsAmp=nan(nFiles,5);

%%
for iDir = 1%:3
    
    Maps=nan(nFiles,nBins,nBins);OA=nan(nFiles,nBins/2);CA=nan(nFiles,nBins/2);
    Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
    signalHisto = nan(nFiles,signalHisto_nBins);
    intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};
    
    for i=1:nFiles
        
        f = fileList(i).name;t2=toc;
        ii = find(journal.MouseNum==str2double(f(2:4)));
        
        if ~isempty(ii)
            
            iLine(i)=ii;
            Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.EPM_BehaviorCode(ii);SignalComment(i)=journal.EPM_SignalCode(ii);
            
            %             if Histo(i)>0.5 && (TaskComment(i)==1) && (BatchNum(i)>0)  && (SignalComment(i)==1)
            %                 if Histo(i)==1 && ( (TaskComment(i)==1) || (TaskComment(i)==-1)) && (BatchNum(i)>0)  && (SignalComment(i)==1)
            
            if Histo(i)>0.5 && (BatchNum(i)>1) %&& (SignalComment(i)==1)
                
                if Region(i), intermediate = intermediate+1; else, anterior = anterior+1;end
                
                fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([d filesep f]);
                
                p = experiment.p;
                vData = experiment.vData;
                pData = experiment.pData;
                map = experiment.map;
                
                signalHisto(i,:) = histcounts(pData.mainSig,signalHisto_edges,'Normalization','probability');
                
                switch analysisMode
                    case 'ContinuousCaSignal'
                        tmp1 = map.NormSig.IO;
                        cMap_=[-0.5 0.5];
                    case 'Transients'
                        cMap_=[0 0.5];
                        %                 transientsProba(i,:)=pData.transients.zonesProba;
                        %                 transientsAmp(i,:)=pData.transients.zonesMeanAmp;
                        tmp1 = map.NormTransients.IO;
                end
                
                
                
                [r1,c1]=size(tmp1);
                if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
                if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
                
                Maps(i,:,:) = tmp1;
                
                
                t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
            else
                fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,miceList(ii,2),miceList(ii,3),miceList(ii,4),miceList(ii,5))
            end
            
            
            
        end
        
        SweetMaps = squeeze(nanmean(Maps(~logical(Region),:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
        BitterMaps = squeeze(nanmean(Maps(logical(Region),:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;
        
        %% AVG SIGNAL MAPS
        figure();
        
        colormap([[1 1 1] ; jet(1024)])
        
        subplot(1,2,1)
        hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
        imagesc(SweetMaps,cMap_)
        
        subplot(1,2,2)
        hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
        imagesc(BitterMaps,cMap_)
        
        
