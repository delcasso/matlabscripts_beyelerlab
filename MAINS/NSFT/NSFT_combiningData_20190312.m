%% INIT
close all;clearvars;clc;tic;t0=toc;
batchID = 'ICa-ICp_NSFT';

%% DATA
machine = 'SD_remote';
p=[];outputFolder = [];
[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;
fileList=dir([analysisFolder filesep '*.mat']);
nFiles=size(fileList,1);
stat = table();

%% PARAMS
% analysisMode='ContinuousCaSignal';
% analysisMode='Transients';
analysisMode='BulkAudio';

%Warning, here the reference is the center of the maze
nBins = 60/0.5;

journalSize = size(journal,1);

nZones=2;

bulkAmp=nan(journalSize,nZones);
transientsProba=nan(nFiles,nZones);transientsAmp=nan(nFiles,nZones);
PETH_nBins = size(p.eventBasedAnalysisEdges_msec,2)-1;
bulkAudioSync=nan(journalSize,PETH_nBins);
nAudioSync=nan(journalSize,1);

Maps=nan(nFiles,nBins,nBins);OA=nan(nFiles,nBins/2);CA=nan(nFiles,nBins/2);
Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
Sex =  nan(nFiles,1);TaskComment =  nan(nFiles,1);SignalComment =  nan(nFiles,1);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};



for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    Mice{i}=str2double(f(2:4));
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        if strcmp(journal.Sex(ii),'M')
            Sex(i)=0;
        end
        if strcmp(journal.Sex(ii),'F')
            Sex(i)=1;
        end
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    cLim_=[-0.5 0.5];
                    tmp1 = map.NormSig.IO;                    
                    [r1,c1]=size(tmp1);
                    if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
                    if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
                    Maps(i,:,:) = tmp1;
                    bulkAmp(i,:)=experiment.pData.bulkSignalStats.zonesMeanAmp;
                case 'Transients'
                    cLim_=[0 0.5];                    
                    tmp1 = map.NormTransients.IO;
                    [r1,c1]=size(tmp1);
                    if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
                    if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
                    Maps(i,:,:) = tmp1;
                    transientsProba(i,:)=pData.transientStats.zonesProba;
                    transientsAmp(i,:)=pData.transientStats.zonesMeanAmp;
                    
                case 'BulkAudio'
                    bulkAudioSync(i,:) = pData.avgBulkSignalAroundFood;
                    nAudioSync(i) = size(experiment.audio_evts_idx_cleaned,2);
            end
                                   
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
          fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    else
        fprintf('!!! %s not in journal\n',f)
    end
end

stat.Mice = Mice';
% stat.Sex = Sex;
% stat.Region = Region;
% stat.Histo = Histo;
% stat.BatchNum= BatchNum;
% stat.TaskComment = TaskComment;
% stat.SignalComment=SignalComment;

iiSweet = find(Region==0);iiBitter   = find(Region==1);



%% AVG SIGNAL MAPS

GroupsIDs = Region+1;
ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};

switch analysisMode
    
    case 'ContinuousCaSignal'
        
        SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));
        ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
        BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));
        ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;
        
        figure();
        
        colormap([[1 1 1] ; jet(1024)])
        
        subplot(1,2,1)
        hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
        imagesc(SweetMaps,cLim_)
        
        subplot(1,2,2)
        hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
        imagesc(BitterMaps,cLim_)
        
        
        journal=plotZoneStats(bulkAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'ContinuousCaSignal-zoneComparaison',journal,'bulk');
        
    case 'Transients'
        
        SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));
        ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
        BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));
        ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;
        
        figure();
        
        colormap([[1 1 1] ; jet(1024)])
        
        subplot(1,2,1)
        hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
        imagesc(SweetMaps,cLim_)
        
        subplot(1,2,2)
        hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
        imagesc(BitterMaps,cLim_)
        
        
        journal=plotZoneStats(transientsProba,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsFrequency-zoneComparaison',journal,'transientFrequency');
        journal=plotZoneStats(transientsAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsAmplitude-zoneComparaison',journal,'transientAmp');
        
        
    case 'BulkAudio'
        
        nSweet = size(iiSweet,1);
        SweetTraces = bulkAudioSync(iiSweet,:);
        mSweet = nanmean(SweetTraces);
        semSweet = nanstd(SweetTraces,0,1)./sqrt(nSweet);
        
        nBitter = size(iiBitter,1);
        BitterTraces = bulkAudioSync(iiBitter,:);
        mBitter = nanmean(BitterTraces);      
        semBitter = nanstd(BitterTraces,0,1)./sqrt(nBitter);
        
        
        sweetColors(1:3) = [101 65 153]./255;
        bitterColors(1:3) = [0 165 77]./255;
        
       x =experiment.p.eventBasedAnalysisEdges_msec;
       x(end)=[];
        
        figure()
        hold on
        plot(x,mSweet,'color',sweetColors);
        plot(x,mSweet+semSweet,'color',sweetColors,'Linestyle',':');
        plot(x,mSweet-semSweet,'color',sweetColors,'Linestyle',':');        
        plot(x,mBitter,'color',bitterColors);
        plot(x,mBitter+semBitter,'color',bitterColors,'Linestyle',':');
        plot(x,mBitter-semBitter,'color',bitterColors,'Linestyle',':');
        
        figure()
        hold on
        for i = 1:nSweet
            plot(SweetTraces(i,:)+(i*3),'color',sweetColors);            
        end
        for i = 1:nBitter
            plot(BitterTraces(i,:)+((i+nSweet)*3),'color',bitterColors);
        end 
        
        plot([600 600],[0 100],'k')
        
        figure()
        hold on
        subplot(2,2,1)
        imagesc(SweetTraces)
        subplot(2,2,2)
        imagesc(BitterTraces)
        
%         meanBaseline = mean(bulkAudioSync(:,1:20),2);
%         meanReponse = mean(bulkAudioSync(:,21:40),2);
        
        
        meanBaseline = mean(bulkAudioSync(:,560:580),2);
        meanReponse = mean(bulkAudioSync(:,580:600),2);
        
        
          
        journal=plotZoneStats([meanBaseline meanReponse],GroupsIDs,Mice,ZonesStr,analysisFolder,'AudioSync',journal,'audioSync');

               
end





function journal=plotZoneStats(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr,journal,statName)

nZones = size(zoneStats,2);

% for iZone=1:nZones
%     cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
%     eval(cmd);
% end

uGroups= unique(Groups);
uGroups(isnan(uGroups))=[];
nGroups = size(uGroups,1);

FaceGroupColors(1,1:3) = [101 65 153]./255;
EdgesGroupColors(1,1:3) = [101 65 153]./255;
LineGroupColors(1,1:3) = [0 0 0];
LineGroupStlye{1} = ':';

FaceGroupColors(2,1:3) = [0 165 77]./255;
EdgesGroupColors(2,1:3) = [0 165 77]./255;
LineGroupColors(2,1:3) = [0 0 0];
LineGroupStlye{2} = ':';

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure()
hold on
title(titleStr)
set(gca,'xtick',[])

x=0;

for iGroup=1:nGroups
    
    x=x+iGroup;
    
    for iZone=1:nZones
        
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:),'Linewidth',2);
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
        
    end
    
    [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
    text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
    text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
    
    for iZone=2:nZones
        plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',LineGroupColors(iGroup,:),'LineStyle',':');
    end
    
end

print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[analysisFolder filesep titleStr '.fig'])

end



















