% function SI_combiningData()

%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
batchID =  '20180926_iC-SweetBitter_AllTogether_TASTE';
[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS
% analysisMode='ContinuousCaSignal';
analysisMode='Transients';

goto PD
warning('nBins has been defined arbitrarly');
% nBins = 60/0.5;
nBins =176;

bulkAmp=nan(nFiles,4);transientsProba=nan(nFiles,4);transientsAmp=nan(nFiles,4);

Maps=nan(nFiles,nBins,nBins);
Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    
    if isempty(ii)
        ii = find(journal.AdrienWrongMouseNum==str2double(f(2:4)));
    end
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        
        if Histo(i)==1 && (BatchNum(i)>0) && ( (SignalComment(i)==1) || (SignalComment(i)==2) )
            
            if Region(i), intermediate = intermediate+1; else, anterior = anterior+1;end
            
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    tmp1 = map.NormSig.IO;
                    %                     cMap_=[-0.5 0.5]; %OFT
                    cMap_=[-1 1]; %NSFT
                    bulkAmp(i,:)=experiment.pData.bulkSignalStats.zonesMeanAmp;
                case 'Transients'
                    %                     cMap_=[0 0.5]; %OFT
                    cMap_=[0 0.5]; %NSFT
                    transientsProba(i,:)=pData.transients.zonesProba;
                    transientsAmp(i,:)=pData.transients.zonesMeanAmp;
                    tmp1 = map.NormTransients.IO;
                    
            end
            
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            %% For Map Alignement, we keep object on the left Side
            landmarksStr=experiment.vData.landmarksStr;
            landmarks=experiment.vData.landmarks;
            iObject =  strfind(landmarksStr, 'Object Center');
            iObject = find(not(cellfun('isempty',iObject)));
            xObject = landmarks.x(iObject);
            iJuvenile =  strfind(landmarksStr, 'Juvenile Center');
            iJuvenile = find(not(cellfun('isempty',iJuvenile)));
            xJuvenile = landmarks.x(iJuvenile);
            
            if xObject>xJuvenile
                tmp1 = fliplr(tmp1);
            end
            
            Maps(i,:,:) = tmp1;
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end % for i=1:nFiles

iiSweet = find(Region==0);iiBitter   = find(Region==1);
SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;


%% AVG SIGNAL MAPS
figure();
colormap([[1 1 1] ; jet(1024)])
subplot(1,2,1)
hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
imagesc(SweetMaps,cMap_)
subplot(1,2,2)
hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
imagesc(BitterMaps,cMap_)


GroupsIDs = Region+1;

ZonesStr = {experiment.vData.zones_cmSP(:).type}

% ZonesStr = {experiment.vData.zones_cmSP(1).type,experiment.vData.zones_cmSP(2).type};

switch analysisMode
    case 'ContinuousCaSignal'
        plotZoneStats(bulkAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'ContinuousCaSignal-zoneComapraison');
    case 'Transients'
        plotZoneStats(transientsProba,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsProbability-zoneComapraison');
        plotZoneStats(transientsAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsAmplitude-zoneComapraison');
end


function plotZoneStats(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr)

nZones = size(zoneStats,2);
uGroups= unique(Groups); uGroups(isnan(uGroups))=[];nGroups = size(uGroups,1);

FaceGroupColors(1,1:3) = [163 135 179]./255;
EdgesGroupColors(1,1:3) = [85 44 136]./255;

FaceGroupColors(2,1:3) = [152 194 122]./255;
EdgesGroupColors(2,1:3) = [49 157 71]./255;

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure()
hold on
title(titleStr)
set(gca,'xtick',[])

x=0;

for iGroup=1:nGroups
    
    x=x+iGroup+nZones;
    
    for iZone=1:nZones
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:));
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:));
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
        
    end
    
    [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
    text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
    text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
    
    for iZone=2:nZones
        plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',EdgesGroupColors(iGroup,:));
    end
    
end

print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[analysisFolder filesep titleStr '.fig'])

end












