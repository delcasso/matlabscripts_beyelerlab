%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
% batchID = '20180926_iC-SweetBitter_AllTogether_OFT';
% batchID = '20180926_iC-SweetBitter_AllTogether_NSFT';
batchID =  '20180926_iC-SweetBitter_AllTogether_TASTE';
[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

nBinsMaps=176;
nBinsbulkMatrix=200;
nBinsTansientsMatrix=200;

nEvents=4;nGroups=2;

eventBased_bulkMatrix = nan(nFiles,nBinsbulkMatrix,nEvents,nGroups);
eventBased_tansientsMatrix = nan(nFiles,nBinsbulkMatrix,nEvents,nGroups);

journalSize = size(journal,1);


for iAnalysis=1:2
    
    Maps=nan(journalSize,nBinsMaps,nBinsMaps);
    Region = nan(journalSize,1);Histo = nan(journalSize,1);Task = nan(journalSize,1);
    BatchNum = nan(journalSize,1);
    Mice={};
    intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};
    
    switch iAnalysis
        case 1
            analysisMode='ContinuousCaSignal';
        case 2
            analysisMode='Transients';
    end
    
    for iFile=1:nFiles
        
        f = fileList(iFile).name;t2=toc;
        i = find(journal.MouseNum==str2double(f(2:4)));
        
        if isempty(i)
            i = find(journal.AdrienWrongMouseNum==str2double(f(2:4)));
        end
        
        if ~isempty(i)
            
            iLine(iFile)=i;
            Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(i);Histo(i)=journal.HistologyCode(i);BatchNum(i)=journal.BatchCode(i);TaskComment(i)=journal.BehaviorCode(i);SignalComment(i)=journal.SignalCode(i);
            
            
            if Histo(i)==1 && (BatchNum(i)>0)  && (SignalComment(i)==1)  %( (SignalComment(i)==1) || (SignalComment(i)==2) )
                
                if Region(i)
                    intermediate = intermediate+1;
                    regionStr = 'intermediate';
                    curRegionNum = intermediate;
                else
                    anterior = anterior+1;
                    regionStr = 'anterior';
                    curRegionNum = anterior;
                end
                
                fprintf('%2.2f Loading file %s [%d/%d] %s-%d ...',t2,f,i,nFiles,regionStr,curRegionNum);load([analysisFolder filesep f]);
                
                p = experiment.p;
                vData = experiment.vData;
                pData = experiment.pData;
                map = experiment.map;
                
                switch analysisMode
                    case 'ContinuousCaSignal'
                        tmp1 = map.NormSig.IO;
                        cMap_=[-0.75 0.75]; %OFT
                        bulkAmp(i,:)=experiment.pData.bulkSignalStats.zonesMeanAmp;
                        if isfield(experiment,'eventCategory')
                            if isfield(experiment.eventCategory,'bulkMatrix')
                                tmp =  {experiment.eventCategory(:).bulkMatrix};
                                for iEvent=1:nEvents
                                    tmp2 = tmp{iEvent};
                                    if ~isempty(tmp2)
                                        if size(tmp2,1)>1, tmp2 = nanmean(tmp2);end;eventBased_bulkMatrix(i,:,iEvent,Region(i)+1) = tmp2;
                                    end
                                end
                            end
                        end
                    case 'Transients'
                        cMap_=[0 0.5]; %OFT
                        tmp1 = map.NormTransients.IO;
                        transientsProba(i,:)=pData.transientStats.zonesProba;
                        transientsAmp(i,:)=pData.transientStats.zonesMeanAmp;
                        if isfield(experiment,'eventCategory')
                            if isfield(experiment.eventCategory,'tansientsMatrix')
                                tmp =  {experiment.eventCategory(:).tansientsMatrix};
                                for iEvent=1:nEvents
                                    tmp2 = tmp{iEvent};
                                    if ~isempty(tmp2)
                                        if size(tmp2,1)>1,tmp2 = nanmean(tmp2);end;eventBased_bulkMatrix(i,:,iEvent,Region(i)+1) = tmp2;
                                    end
                                end
                            end
                        end
                end
                
                [r1,c1]=size(tmp1);
                if r1< nBinsMaps, tmp1(r1+1:nBinsMaps,:)=nan;end
                if c1< nBinsMaps, tmp1(:,c1+1:nBinsMaps)=nan;end
                
                Maps(i,:,:) = tmp1;
                
                t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
            else
                fprintf('DISCARD %s Region[%d]-Histology[%d]-Behavior[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
            end
            
        end
        
    end % for i=1:nFiles
    
    iiSweet = find(Region==0);iiBitter   = find(Region==1);
    SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
    BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;
    
    %% AVG SIGNAL MAPS
    f1=figure('name',analysisMode);
    colormap([[1 1 1] ; jet(1024)])
    subplot(1,2,1)
    hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
    imagesc(SweetMaps,cMap_)
    subplot(1,2,2)
    hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
    imagesc(BitterMaps,cMap_);
    print(f1,[analysisFolder filesep analysisMode '.pdf'],'-dpdf');
    savefig(f1,[analysisFolder filesep analysisMode '.fig']);
    
    GroupsIDs = Region+1;
    ZonesStr = {experiment.vData.zones_cmSP(:).type};
    EventStr = {experiment.eventCategory(:).name};
    GroupsStr = {'anterior','intermediate'};
    
    disp('BLOCK STARTS')
        
    % aIC
    FaceGroupColors(1,1:3) = [163 135 179]./255;
    EdgesGroupColors(1,1:3) = [85 44 136]./255;
    % iIC
    FaceGroupColors(2,1:3) = [152 194 122]./255;
    EdgesGroupColors(2,1:3) = [49 157 71]./255;
    
    quadantColors(1,1:3) = [1 0.8 0.9]; %Quinine
    quadantColors(2,1:3) = [0.9 0.8 1]; %Sucrose
    quadantColors(3,1:3) = [0.8 1 0.9]; % Nothing
    quadantColors(4,1:3) = [0.9 0.9 0.8]; %Water
    
    
    iPlot{1}=[1 2 7 8];
    iPlot{2}= iPlot{1}+2;
    iPlot{3}=iPlot{2}+2;
    iPlot{4}=[13:15 19:21];
    iPlot{5}=iPlot{4}+3;
    
    evtToPlot = [1 2 4];
    
    
    figure()
    for i=1:3
        iEvt = evtToPlot(i);
        subplot(4,6, iPlot{i})
        hold on       
        for iG=1:2
            m_ = nanmean(eventBased_bulkMatrix(:,:,iEvt,iG));
            effectif = sum(~isnan(eventBased_bulkMatrix(:,1,iEvt,iG)));
            std_ = nanstd(eventBased_bulkMatrix(:,:,iEvt,iG),[],1);
            sem_ = std_ ./sqrt(effectif);
            plot(1:nBinsbulkMatrix,m_,'color',EdgesGroupColors(iG,:));
            plot(1:nBinsbulkMatrix,m_+sem_,'color',EdgesGroupColors(iG,:),'LineStyle',':');
            plot(1:nBinsbulkMatrix,m_-sem_,'color',EdgesGroupColors(iG,:),'LineStyle',':');            
        end
    end
    
    for iG=1:2
        subplot(4,6, iPlot{3+iG})
        hold on
        for i=1:3
            iEvt = evtToPlot(i);
            m_ = nanmean(eventBased_bulkMatrix(:,:,iEvt,iG));
            effectif = sum(~isnan(eventBased_bulkMatrix(:,1,iEvt,iG)));
            std_ = nanstd(eventBased_bulkMatrix(:,:,iEvt,iG),[],1);
            sem_ = std_ ./sqrt(effectif);
            plot(1:nBinsbulkMatrix,m_,'color',quadantColors(iEvt,:));
            plot(1:nBinsbulkMatrix,m_+sem_,'color',quadantColors(iEvt,:),'LineStyle',':');
            plot(1:nBinsbulkMatrix,m_-sem_,'color',quadantColors(iEvt,:),'LineStyle',':');
        end
    end
    
    
    figure()
    for i=1:3
        iEvt = evtToPlot(i);
        subplot(4,6, iPlot{i})
        hold on       
        for iG=1:2
            notnan = ~isnan(eventBased_bulkMatrix(:,1,iEvt,iG));
             ii = find(notnan==1);
            effectif = sum(~isnan(eventBased_bulkMatrix(:,1,iEvt,iG)));
            for iM=1:effectif
                plot(1:nBinsbulkMatrix,eventBased_bulkMatrix(ii(iM),:,iEvt,iG),'color',EdgesGroupColors(iG,:));
            end               
        end
    end
    
    
    
    
    disp('BLOCK ENDS')
    
    

    
    
    switch analysisMode
        case 'ContinuousCaSignal'
            journal=plotZoneStats(bulkAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'ContinuousCaSignal-zoneComparaison',journal,'bulk');
        case 'Transients'
            journal=plotZoneStats(transientsProba,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsProbability-zoneComparaison',journal,'transientProba');
            journal=plotZoneStats(transientsAmp,GroupsIDs,Mice,ZonesStr,analysisFolder,'TransientsAmplitude-zoneComparaison',journal,'transientAmp');
    end
    

    
    savefig(f1,[analysisFolder filesep analysisMode '.fig']);
    
    writetable(journal,[analysisFolder filesep 'stats.xlsx']);
    
end
% zones(1).type = 'border';
% zones(2).type = 'center';

function journal=plotZoneStats(zoneStats,Groups,Mice,ZonesStr,analysisFolder,titleStr,journal,statName)

nZones = size(zoneStats,2);

for iZone=1:nZones
    cmd = sprintf('journal.%s_%s=zoneStats(:,iZone);',statName,ZonesStr{iZone});
    eval(cmd);
end

uGroups= unique(Groups);
uGroups(isnan(uGroups))=[];
nGroups = size(uGroups,1);

FaceGroupColors(1,1:3) = [163 135 179]./255;
EdgesGroupColors(1,1:3) = [85 44 136]./255;

FaceGroupColors(2,1:3) = [152 194 122]./255;
EdgesGroupColors(2,1:3) = [49 157 71]./255;

ColorDiff = 0.1;
GroupsStr = {'anterior','intermediate'};
minY = min(min(zoneStats));
maxY = max(max(zoneStats));

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups, iiGroup{iGroup} = find(Groups==iGroup);end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

f1=figure();
hold on
title(titleStr)
set(gca,'xtick',[])

x=0;

for iGroup=1:nGroups
    
    x=x+iGroup+nZones;
    
    for iZone=1:nZones
        
        if meanGroupZone(iGroup,iZone)>0
            rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:));
        else
            rectangle('Position',[x+iZone-0.25 meanGroupZone(iGroup,iZone) 0.5 -meanGroupZone(iGroup,iZone)],'EdgeColor',EdgesGroupColors(iGroup,:),'FaceColor',FaceGroupColors(iGroup,:));
            plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)-semGroupZone(iGroup,iZone)],'color',EdgesGroupColors(iGroup,:));
        end
        
        text(x+iZone,minY-abs(minY/2),ZonesStr{iZone},'Rotation',90,'VerticalAlignment','top')
        
    end
    
    [h,p,ci,stats] = ttest(zoneStats(iiGroup{iGroup},1),zoneStats(iiGroup{iGroup},2));
    text(x+1,maxY+abs((maxY-minY)/20),GroupsStr{iGroup},'color',EdgesGroupColors(iGroup,:))
    text(x+1,maxY,sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',EdgesGroupColors(iGroup,:))
    
    for iZone=2:nZones
        plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',EdgesGroupColors(iGroup,:));
    end
    
end

print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
savefig(f1,[analysisFolder filesep titleStr '.fig'])

end













