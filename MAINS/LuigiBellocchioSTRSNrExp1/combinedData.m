dataFolder = 'Z:\Fiber-Photometry\01_DATA\OTHER\Luigi Bellocchio STR-SNr Exp1\20181012_resultsAllCombined';

salineFile = dir([dataFolder filesep '*-4.mat']);
skfFile = dir([dataFolder filesep '*-6.mat']);

    f1=figure();

    
    nTransients=nan(3,2);
    meanTransientsValue=nan(3,2);
    
    
for iMouse=1:2
    load([dataFolder filesep salineFile(iMouse).name]);
    pSaline = experiment.pData;
    load([dataFolder filesep skfFile(iMouse).name]);
    pSKF = experiment.pData; 
    
    figure(f1);
    subplot(3,1,1)
    hold on
    nSaline = size(pSaline.mainSig,1);
    tSaline = (1:nSaline) * (1/20);
    plot(tSaline,pSaline.mainSig,'b');
    subplot(3,1,2)
    hold on
    nSKF = size(pSKF.mainSig,1);
    tSKF = (1:nSKF) * (1/20);
    plot(tSKF,pSKF.mainSig,'r');
    subplot(3,1,3)
    h1 = histogram(pSaline.mainSig,-2:0.01:2,'FaceColor','b','Normalization','probability');
    hold on
    h2 = histogram(pSKF.mainSig,-2:0.01:2,'FaceColor','r','Normalization','probability');
    print(f1,'-djpeg',[salineFile(iMouse).name([1 2]) '-bulk2.jpeg']);
    print(f1,'-dpdf',[salineFile(iMouse).name([1 2]) '-bulk2.pdf']);
    
    nTransients(iMouse,1)=size(pSaline.transients.ts,2);
     nTransients(iMouse,2)=size(pSKF.transients.ts,2);
    meanTransientsValue(iMouse,1)=nanmean(pSaline.transients.values);
    meanTransientsValue(iMouse,2)=nanmean(pSKF.transients.values);    
    
    clf(f1);
    
 
    

end
