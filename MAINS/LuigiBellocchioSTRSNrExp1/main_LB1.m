close all;clear;clc

    machine = 'SD_remote';
    batchID= 'Luigi Bellocchio STR-SNr Exp1';

[dataRoot,outputFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);

%% PARAMETERS
p.outputFolder=outputFolder;
p.cameraMode = 'synchronous';
p.ledDetectionThreshold = 50; %percent of led signal max
p.HamamatsuFrameRate_Hz= 20;
p.behaviorCameraFrameRate_Hz=20;
p.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
p.savePDF = 0;
p.saveFIG = 0;
p.savePNG = 0;
p.forceRedrawing = 1;
p.forceBehavioralStart = 0;
p.MapScale_cmPerBin = 0.5;
p.OccupancyMap_sigmaGaussFilt=5;
p.PhotometrySignalMap_sigmaGaussFilt=5;
p.deltaFF_slidingWindowWidth = 1200;
p.lookingForMouse = '';
p.bonzaiDone=0;


if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
p.figureFolder = [p.outputFolder filesep 'figures'];
if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end

%% TO PROCESS ALL FOLDERS
for iFolder=6
    p.dataRoot = dataRoot{iFolder};
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        if ~isempty(p.lookingForMouse)
            processThisFile=0;
            if strcmp(p.dataFileTag,p.lookingForMouse)
                processThisFile=1;
            end
        end
        %% MAIN PROCESSING
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        
        fprintf('Processing File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);
        
        experiment = loadExpData(p);
        experiment = processCaSignal(experiment);
%         experiment = standardizePositionData(experiment);
        
%         experiment=buildBasicMaps(experiment);
%         drawBasicMaps(experiment);
%         
%         experiment = getApparatusZones(experiment);
%         experiment = getZonesStatistics(experiment);
%         
%         switch p.apparatus.type
%             case 'EPM'
%                 experiment=buildDirectionalMapsForEPM(experiment);
%                 experiment=buildLinearMapsForEPM(experiment);
%         end
               
        
        %% SAVING DATA
        save([p.outputFolder filesep p.dataFileTag '-' num2str(iFolder)   '.mat'],'experiment');
        
    end
end



