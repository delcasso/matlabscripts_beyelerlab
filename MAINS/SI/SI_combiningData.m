% function SI_combiningData()

%% INIT
close all;clearvars;clc;tic;t0=toc;

%% DATA
machine = 'SD_remote';
batchID = '20180926_iC-SweetBitter_AllTogether_SI';
[dataRoot,analysisFolder,apparatus,videoExt]=getBatchAnalysisConfig(batchID,machine);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;

fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);

%% PARAMS
% analysisMode='ContinuousCaSignal';
% analysisMode='Transients';

if params.apparatusNormalizationRequested
    fprintf('apparatus normalization requested');
    nApp = size(params.apparatus,2);
    normApparatus = params.apparatus{1,nApp};
    nBins = normApparatus.side_cm / params.MapScale_cmPerBin;
else
    warning('nBins has been defined arbitrarly');
    nBins = 60/0.5;
end


bulkAmp=nan(nFiles,2);transientsProba=nan(nFiles,2);transientsAmp=nan(nFiles,2);

Maps=nan(nFiles,nBins,nBins);
Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        
        
        if Histo(i)==1 && (BatchNum(i)>0) && (SignalComment(i)==1)
            
            if Region(i), intermediate = intermediate+1; else, anterior = anterior+1;end
            
            fprintf('%2.2f Loading file %s [%d/%d] ...',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            map = experiment.map;            
            
            switch analysisMode
                case 'ContinuousCaSignal'
                    tmp1 = map.NormSig.IO;
%                     cMap_=[-0.5 0.5]; %OFT
                    cMap_=[-1 1]; %NSFT
                    bulkAmp(i,:)=experiment.pData.bulkSignalStats.zonesMeanAmp;
                case 'Transients'
%                     cMap_=[0 0.5]; %OFT
                    cMap_=[0 0.5]; %NSFT
                    %                 transientsProba(i,:)=pData.transients.zonesProba;
                    %                 transientsAmp(i,:)=pData.transients.zonesMeanAmp;
                    tmp1 = map.NormTransients.IO;
                   transientsProba(i,:)=pData.transients.zonesProba;
                   transientsAmp(i,:)=pData.transients.zonesMeanAmp;
            end
                                    
            [r1,c1]=size(tmp1);
            if r1< nBins, tmp1(r1+1:nBins,:)=nan;end
            if c1< nBins, tmp1(:,c1+1:nBins)=nan;end
            
            %% For Map Alignement, we keep object on the left Side            
            landmarksStr=experiment.vData.landmarksStr;
            landmarks=experiment.vData.landmarks;
            iObject =  strfind(landmarksStr, 'Object Center');
            iObject = find(not(cellfun('isempty',iObject)));
            xObject = landmarks.x(iObject);
            iJuvenile =  strfind(landmarksStr, 'Juvenile Center');
            iJuvenile = find(not(cellfun('isempty',iJuvenile)));        
            xJuvenile = landmarks.x(iJuvenile);
            
            if xObject>xJuvenile
                tmp1 = fliplr(tmp1);
            end
            
            Maps(i,:,:) = tmp1;
                        
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    end
    
end % for i=1:nFiles

iiSweet = find(Region==0);iiBitter   = find(Region==1);
SweetMaps = squeeze(nanmean(Maps(iiSweet,:,:)));ii = isnan(SweetMaps);SweetMaps(ii)=0;SweetMaps = imgaussfilt(SweetMaps,2);SweetMaps(ii)=nan;
BitterMaps = squeeze(nanmean(Maps(iiBitter,:,:)));ii = isnan(BitterMaps);BitterMaps(ii)=0;BitterMaps = imgaussfilt(BitterMaps,2);BitterMaps(ii)=nan;


%% AVG SIGNAL MAPS
figure();
colormap([[1 1 1] ; jet(1024)])
subplot(1,2,1)
hold on;title(sprintf('anterior (n=%d)',anterior));axis equal;axis off;colorbar();
imagesc(SweetMaps,cMap_)
subplot(1,2,2)
hold on;title(sprintf('intermediate (n=%d)',intermediate));axis equal;axis off;colorbar();
imagesc(BitterMaps,cMap_)

switch analysisMode
    case 'ContinuousCaSignal'
        plotZoneStats(bulkAmp,Region+1,Mice);
    case 'Transients'
        plotZoneStats(transientsProba,Region+1,Mice);
        plotZoneStats(transientsAmp,Region+1,Mice);
end
            
function plotZoneStats(zoneStats,Groups,Mice)

% zoneStats = bulkAmp;
% Groups = Region+1;

nZones = size(zoneStats,2);
 uGroups= unique(Groups); uGroups(isnan(uGroups))=[];nGroups = size(uGroups,1);
GoupColors(1,1:3) = [1 0 0];GoupColors(2,1:3) = [0 0 1];

ii=isnan(zoneStats(:,1));zoneStats(ii,:)=[];Groups(ii)=[];

for iGroup=1:nGroups
    iiGroup{iGroup} = find(Groups==iGroup);
end

for iGroup=1:nGroups
    for iZone=1:nZones
        meanGroupZone(iGroup,iZone)=nanmean(zoneStats(iiGroup{iGroup},iZone));
        semGroupZone(iGroup,iZone)=nanstd(zoneStats(iiGroup{iGroup},iZone),[],1)./sqrt(size(iiGroup{iGroup},1));
    end
end

figure()
hold on
x=0;
for iGroup=1:nGroups    
     x=x+iGroup;
    for iZone=1:nZones      
        rectangle('Position',[x+iZone-0.25 0 0.5 meanGroupZone(iGroup,iZone)],'EdgeColor',GoupColors(iGroup,:));
        plot([x+iZone x+iZone],[meanGroupZone(iGroup,iZone) meanGroupZone(iGroup,iZone)+semGroupZone(iGroup,iZone)],'color',GoupColors(iGroup,:));
    end
        for iZone=2:nZones
            plot([(x+iZone-1) (x+iZone)],[zoneStats(iiGroup{iGroup},iZone-1) zoneStats(iiGroup{iGroup},iZone)],'color',GoupColors(iGroup,:));
        end        
end



end