
%% INIT
close all;clearvars;clc;tic;t0=toc;


batchID = 'ICa-ICp_QUININE-SUCROSE';

GroupColors(1,1:3) = [101 65 153]./255;
GroupColors(2,1:3) = [0 165 77]./255;


%% DATA
machine = 'SD_remote';
p=[];
[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;
fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);
stat = table();

Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};Sex = nan(nFiles,1);
TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

nCol = 80;

s_bulk_PSTH_DB = nan(11,nCol);
s_transients_PSTH_DB = nan(11,nCol);
q_bulk_PSTH_DB = nan(11,nCol);
q_transients_PSTH_DB = nan(11,nCol);

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    Mice{i}=str2double(f(2:4));
    
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        if strcmp(journal.Sex(ii),'M'),Sex(i)=0;end
        if strcmp(journal.Sex(ii),'F'),Sex(i)=1;end
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            
            
            [r,c] = size(pData.s_bulkMatrix);
            if r>1, s_bulk_PSTH_DB(i,:)=nanmean(pData.s_bulkMatrix);end
            if r==1,s_bulk_PSTH_DB(i,:)=pData.s_bulkMatrix;end      
            
            [r,c] = size(pData.q_bulkMatrix);
            if r>1, q_bulk_PSTH_DB(i,:)=nanmean(pData.q_bulkMatrix);end
            if r==1,q_bulk_PSTH_DB(i,:)=pData.q_bulkMatrix;end
            
            [r,c] = size(pData.s_transientsMatrix);
            if r>1, s_transients_PSTH_DB(i,:)=nanmean(pData.s_transientsMatrix);end
            if r==1,s_transients_PSTH_DB(i,:)=pData.s_transientsMatrix;end
            
            [r,c] = size(pData.q_transientsMatrix);
            if r>1, q_transients_PSTH_DB(i,:)=nanmean(pData.q_transientsMatrix);end
            if r==1,q_transients_PSTH_DB(i,:)=pData.q_transientsMatrix;end            

            
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    else
        fprintf('!!! %s not in journal\n',f)
    end
end

stat.Mice = Mice';
stat.Sex = Sex;
stat.Region = Region;
stat.Histo = Histo;
stat.BatchNum= BatchNum;
stat.TaskComment = TaskComment;
stat.SignalComment=SignalComment;

iiSweet = find(Region==0);iiBitter   = find(Region==1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% step = 5;
%
% avg_s_bulk_PSTH_DB = nan(11,nCol/step);
% avg_q_bulk_PSTH_DB =  nan(11,nCol/step);
% avg_s_transients_PSTH_DB =  nan(11,nCol/step);
% avg_q_transients_PSTH_DB = nan(11,nCol/step);
%
%
% for i=1:step:nCol
%     k = ((i-1)/step)+1
%     l = i:i+step-1
%     avg_s_bulk_PSTH_DB(:,k) = mean(s_bulk_PSTH_DB(:,l),2);
%     avg_q_bulk_PSTH_DB(:,k) = mean(q_bulk_PSTH_DB(:,l),2);
%     avg_s_transients_PSTH_DB(:,k) = mean(s_transients_PSTH_DB(:,l),2);
%     avg_q_transients_PSTH_DB(:,k) = mean(q_transients_PSTH_DB(:,l),2);
% end

% s_bulk_PSTH_DB = avg_s_bulk_PSTH_DB;
% q_bulk_PSTH_DB =  avg_q_bulk_PSTH_DB;
% s_transients_PSTH_DB =  avg_s_transients_PSTH_DB;
% q_transients_PSTH_DB = avg_q_transients_PSTH_DB;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TEMPORARY FIX
s_bulk_PSTH_DB(9,:)=nan;
q_bulk_PSTH_DB(1,:)=nan;
q_bulk_PSTH_DB(9,:)=nan;
q_bulk_PSTH_DB(11,:)=nan;

figure()
subplot(2,3,1)
hold on;title('S Bulk')
j = iiSweet;n = size(j,1);
for i=1:n
    plot(s_bulk_PSTH_DB(j(i),:),'color',GroupColors(1,:));
end

mean_s_b_ICa = nanmean(s_bulk_PSTH_DB(j,:));
sem_s_b_ICa = nanstd(s_bulk_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));

tmp = s_bulk_PSTH_DB(j,:);
s_b_ICa_avg = nan(size(tmp,1),2);
s_b_ICa_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
s_b_ICa_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);

j = iiBitter;n = size(j,1);for i=1:n, plot(s_bulk_PSTH_DB(j(i),:),'color',GroupColors(2,:));end
mean_s_b_ICp = nanmean(s_bulk_PSTH_DB(j,:));
sem_s_b_ICp = nanstd(s_bulk_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));

tmp = s_bulk_PSTH_DB(j,:);
s_b_ICp_avg = nan(size(tmp,1),2);
s_b_ICp_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
s_b_ICp_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);

% ylim([-10 10])

subplot(2,3,2)

hold on;title('Q Bulk')

j = iiSweet;n = size(j,1);for i=1:n;plot(q_bulk_PSTH_DB(j(i),:),'color',GroupColors(1,:));end
mean_q_b_ICa = nanmean(q_bulk_PSTH_DB(j,:));
sem_q_b_ICa = nanstd(q_bulk_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));

tmp = q_bulk_PSTH_DB(j,:);
q_b_ICa_avg = nan(size(tmp,1),2);
q_b_ICa_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
q_b_ICa_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);

j = iiBitter;n = size(j,1);for i=1:n, plot(q_bulk_PSTH_DB(j(i),:),'color',GroupColors(2,:));end
mean_q_b_ICp = nanmean(q_bulk_PSTH_DB(j,:));
sem_q_b_ICp = nanstd(q_bulk_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));

tmp = q_bulk_PSTH_DB(j,:);
q_b_ICp_avg = nan(size(tmp,1),2);
q_b_ICp_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
q_b_ICp_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);

% ylim([-10 10])




sucroseColor = [154 190 224]./255;
quinineColor = [255 195 188]./255;
ICaColor =  [101 65 153]./255;
ICpColor = [0 165 77]./255;

subplot(2,3,3)
hold on

% plot(1:80,s_b_ICa,'color',sucroseColor,'LineWidth',4)
% plot(1:80,s_b_ICa,'color',ICaColor,'LineStyle',':','LineWidth',2)
% 
% plot(1:80,s_b_ICp,'color',sucroseColor,'LineWidth',4)
% plot(1:80,s_b_ICp,'color',ICpColor,'LineStyle',':','LineWidth',2)
% 
% plot(1:80,q_b_ICa,'color',quinineColor,'LineWidth',2)
% plot(1:80,q_b_ICa,'color',ICaColor,'LineStyle',':','LineWidth',2)
% 
% plot(1:80,q_b_ICp,'color',quinineColor,'LineWidth',2)
% plot(1:80,q_b_ICp,'color',ICpColor,'LineStyle',':','LineWidth',2)

c = nan(4,3);
c(1,:) = [3 155 208];
c(2,:) = [254 63 93];
c(3,:) = [10 92 91];
c(4,:) = [255 214 4];

c = c./255;

x=1:nCol;

y = mean_s_b_ICa;
errBar = sem_s_b_ICa;
shadedErrorBar(x,y,errBar,'lineprops','-r','transparent',1);

y = mean_s_b_ICp;
errBar = sem_s_b_ICp;
shadedErrorBar(x,y,errBar,'lineprops','-g','transparent',1);

y = mean_q_b_ICa;
errBar = sem_q_b_ICa;
shadedErrorBar(x,y,errBar,'lineprops','-b','transparent',1);

y = mean_q_b_ICp;
errBar = sem_q_b_ICp;
shadedErrorBar(x,y,errBar,'lineprops','-k','transparent',1);





legend({'s ICa','s ICp','q ICa','q ICp'})
% ylim([-10 10])



subplot(2,3,4)
hold on;title('S Transients')
j = iiSweet;n = size(j,1);for i=1:n, plot(s_transients_PSTH_DB(j(i),:),'color',GroupColors(1,:));end
s_t_ICa = nanmean(s_transients_PSTH_DB(j,:));
j = iiBitter;n = size(j,1);for i=1:n, plot(s_transients_PSTH_DB(j(i),:),'color',GroupColors(2,:));end
s_t_ICp = nanmean(s_transients_PSTH_DB(j,:));
subplot(2,3,5)
hold on;title('Q Transients')
j = iiSweet;n = size(j,1);for i=1:n;plot(q_transients_PSTH_DB(j(i),:),'color',GroupColors(1,:));end
q_t_ICa = nanmean(q_transients_PSTH_DB(j,:));
j = iiBitter;n = size(j,1);for i=1:n,plot(q_transients_PSTH_DB(j(i),:),'color',GroupColors(2,:));end
q_t_ICp = nanmean(q_transients_PSTH_DB(j,:));
subplot(2,3,6)
hold on
plot(s_t_ICa,'color',GroupColors(1,:))
plot(s_t_ICp,'color',GroupColors(2,:))
plot(q_t_ICa,'color',GroupColors(1,:),'LineStyle',':')
plot(q_t_ICp,'color',GroupColors(2,:),'LineStyle',':')
legend({'s ICa','s ICp','q ICa','q ICp'})


