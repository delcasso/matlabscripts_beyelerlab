%% INIT
close all;clearvars;clc;tic;t0=toc;

batchID = 'ICa-ICp_QUININE-SUCROSE';

% signalType = 'BULK';
signalType = 'TRANSIENTS';


GroupColors(1,1:3) = [101 65 153]./255;
GroupColors(2,1:3) = [0 165 77]./255;

%% DATA
machine = 'SD_remote';
p=[];
[dataRoot,analysisFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p);
journal = readtable([analysisFolder filesep 'Journal.xlsx']);
load([analysisFolder filesep 'analysisParams.mat']);
params = p;
fileList=dir([analysisFolder filesep '*.mat']);nFiles=size(fileList,1);
stat = table();

Region = nan(nFiles,1);Histo = nan(nFiles,1);Task = nan(nFiles,1);BatchNum = nan(nFiles,1);Mice={};Sex = nan(nFiles,1);
TaskComment = nan(nFiles,1); SignalComment = nan(nFiles,1);
intermediate=0;anterior=0;anteriorMouseList={};intermediateMouseList={};

nCol = 80;

s_bulk_PSTH_DB = nan(11,nCol);
s_transients_PSTH_DB = nan(11,nCol);
q_bulk_PSTH_DB = nan(11,nCol);
q_transients_PSTH_DB = nan(11,nCol);

for i=1:nFiles
    
    f = fileList(i).name;t2=toc;
    ii = find(journal.MouseNum==str2double(f(2:4)));
    Mice{i}=str2double(f(2:4));
    
    
    if ~isempty(ii)
        
        iLine(i)=ii;
        Mice{i}=str2double(f(2:4));Region(i)=journal.RegionCode(ii);Histo(i)=journal.HistologyCode(ii);BatchNum(i)=journal.BatchCode(ii);TaskComment(i)=journal.BehaviorCode(ii);SignalComment(i)=journal.SignalCode(ii);
        if strcmp(journal.Sex(ii),'M'),Sex(i)=0;end
        if strcmp(journal.Sex(ii),'F'),Sex(i)=1;end
        
        if Histo(i)==1 && (TaskComment(i)>0) && (BatchNum(i)>0)  && (SignalComment(i)>0)
            
            
            if Region(i)
                intermediate = intermediate+1; intermediateMouseList{intermediate}=Mice{i};fprintf('intermediate = %d ',intermediate);
            else
                anterior = anterior+1;anteriorMouseList{anterior}=Mice{i};fprintf('anterior = %d ',anterior);
            end
            
            fprintf('%2.2f Loading file %s [%d/%d] ',t2,f,i,nFiles);load([analysisFolder filesep f]);
            
            p = experiment.p;
            vData = experiment.vData;
            pData = experiment.pData;
            
            
            [r,c] = size(pData.s_bulkMatrix);
            if r>1, s_bulk_PSTH_DB(i,:)=nanmean(pData.s_bulkMatrix);end
            if r==1,s_bulk_PSTH_DB(i,:)=pData.s_bulkMatrix;end
            
            [r,c] = size(pData.q_bulkMatrix);
            if r>1, q_bulk_PSTH_DB(i,:)=nanmean(pData.q_bulkMatrix);end
            if r==1,q_bulk_PSTH_DB(i,:)=pData.q_bulkMatrix;end
            
            [r,c] = size(pData.s_transientsMatrix);
            if r>1, s_transients_PSTH_DB(i,:)=nanmean(pData.s_transientsMatrix);end
            if r==1,s_transients_PSTH_DB(i,:)=pData.s_transientsMatrix;end
            
            [r,c] = size(pData.q_transientsMatrix);
            if r>1, q_transients_PSTH_DB(i,:)=nanmean(pData.q_transientsMatrix);end
            if r==1,q_transients_PSTH_DB(i,:)=pData.q_transientsMatrix;end
            
            t3=toc;fprintf('done in %2.2f sec\n',t3-t2);
        else
            fprintf('DISCARD %s Region[%d]-Histology[%d]-EPM[%d]-Batch[%d]\n',f,Region(i),Histo(i),TaskComment(i),BatchNum(i));
        end
        
    else
        fprintf('!!! %s not in journal\n',f)
    end
end

stat.Mice = Mice';
stat.Sex = Sex;
stat.Region = Region;
stat.Histo = Histo;
stat.BatchNum= BatchNum;
stat.TaskComment = TaskComment;
stat.SignalComment=SignalComment;

ii_ICa = find(Region==0);
ii_ICp   = find(Region==1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


switch signalType
    
    case 'BULK'
        s_PSTH_DB = s_bulk_PSTH_DB
        q_PSTH_DB = q_bulk_PSTH_DB
    case 'TRANSIENTS'
        s_PSTH_DB = s_transients_PSTH_DB
        q_PSTH_DB = q_transients_PSTH_DB
end

% s_PSTH_DB(9,:)=nan;
% q_PSTH_DB(1,:)=nan;
% q_PSTH_DB(9,:)=nan;
% q_PSTH_DB(11,:)=nan;

figure()

subplot(2,2,1)
hold on;title('Response To Sucrose')
j = ii_ICa;n = size(j,1);
for i=1:n, plot(s_PSTH_DB(j(i),:),'color',GroupColors(1,:));end
mean_s_ICa = nanmean(s_PSTH_DB(j,:));
sem_s_ICa = nanstd(s_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));
tmp = s_PSTH_DB(j,:);
s_ICa_avg = nan(size(tmp,1),2);
s_ICa_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
s_ICa_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);
j = ii_ICp;n = size(j,1);for i=1:n, plot(s_PSTH_DB(j(i),:),'color',GroupColors(2,:));end
mean_s_ICp = nanmean(s_PSTH_DB(j,:));
sem_s_ICp = nanstd(s_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));
tmp = s_PSTH_DB(j,:);
s_ICp_avg = nan(size(tmp,1),2);
s_ICp_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
s_ICp_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);

% ylim([-10 10])

subplot(2,2,2)
hold on;title('Reponse To Quinine')
j = ii_ICa;n = size(j,1);for i=1:n;plot(q_PSTH_DB(j(i),:),'color',GroupColors(1,:));end
mean_q_ICa = nanmean(q_PSTH_DB(j,:));
sem_q_ICa = nanstd(q_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));
tmp = q_PSTH_DB(j,:);
q_ICa_avg = nan(size(tmp,1),2);
q_ICa_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
q_ICa_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);

j = ii_ICp;n = size(j,1);for i=1:n, plot(q_PSTH_DB(j(i),:),'color',GroupColors(2,:));end
mean_q_ICp = nanmean(q_PSTH_DB(j,:));
sem_q_ICp = nanstd(q_PSTH_DB(j,:),0,1) ./sqrt(size(j,1));
tmp = q_PSTH_DB(j,:);
q_ICp_avg = nan(size(tmp,1),2);
q_ICp_avg(:,1) = nanmean(tmp(:,1:nCol/2),2);
q_ICp_avg(:,2) = nanmean(tmp(:,nCol/2+1:nCol),2);


subplot(2,2,3)
hold on
x=1:nCol;

y = mean_s_ICa;
errBar = sem_s_ICa;
shadedErrorBar(x,y,errBar,'lineprops','-r','transparent',1);

y = mean_s_ICp;
errBar = sem_s_ICp;
shadedErrorBar(x,y,errBar,'lineprops','-g','transparent',1);

y = mean_q_ICa;
errBar = sem_q_ICa;
shadedErrorBar(x,y,errBar,'lineprops','-b','transparent',1);

y = mean_q_ICp;
errBar = sem_q_ICp;
shadedErrorBar(x,y,errBar,'lineprops','-k','transparent',1);

legend({'s ICa','s ICp','q ICa','q ICp'})

subplot(2,2,4)
[data, groupID, groupSTR]=mergeData(s_ICa_avg,s_ICp_avg,q_ICa_avg,q_ICp_avg);


customBarPlot(data, groupID, groupSTR)


function customBarPlot(data, groupID, groupSTR)

periodSTR = {'before', 'after'};

barSize = 1;
barGap = 0.5;
groupGap = 2;
xBar = 0;


hold on

% xlim([0 (xBar +1)])
ylim([-10 10])


colors = nan(4,3);
colors(1,:) = [1 0 0];
colors(2,:) = [0 1 0];
colors(3,:) = [0 0 1];
colors(4,:) = [0 0 0];



for iG=1:4
    
    ii = find(groupID==iG);
    n = size(ii,1)
    dataTmp = data(ii,:)
    groupSTR_tmp = groupSTR{iG}
    
    m_= mean(dataTmp);
    sem_= std(dataTmp)./sqrt(size(ii,1));
    
    xBar = xBar + groupGap;
    
    for iP=1:2
        
        xCenter = (xBar+(barSize/2)) ;
        
        if m_(iP)>0
            rectangle('Position',[xBar 0 barSize m_(iP)],'EdgeColor','none','FaceColor',[colors(iG,:) 0.2]);
            plot([xCenter xCenter],[m_(iP) (m_(iP)+sem_(iP))],'color',[colors(iG,:) 0.2],'Linewidth',2);
        else
            rectangle('Position',[xBar m_(iP) barSize -m_(iP)],'EdgeColor','none','FaceColor',[colors(iG,:) 0.2]);
            plot([xCenter xCenter],[m_(iP) (m_(iP)-sem_(iP))],'color',[colors(iG,:) 0.2],'Linewidth',2);
        end
        
        t1 = text(xCenter,-8,[groupSTR{iG} ' ' periodSTR{iP}],'Rotation',90,'VerticalAlignment','top','HorizontalAlignment','left')
        xBar = xBar + barSize + barGap;
        
    end
    
    [h,p,ci,stats] = ttest(dataTmp(:,1),dataTmp(:,2));
    t2 = text(4,(6+iG),sprintf('t(%d)=%2.4f, p=%2.4f',stats.df,stats.tstat,p),'color',colors(iG,:))

    
    for i=1:n
        plot([(xCenter - barSize) xCenter],[dataTmp(i,1) dataTmp(i,2)],'color',[0.7 0.7 0.7],'LineStyle','-');
    end
    %
    % end
    %
    % print(f1,[analysisFolder filesep titleStr '.pdf'],'-dpdf');
    % savefig(f1,[analysisFolder filesep titleStr '.fig'])
    
    
    
    
end
end









function [data, groupID, groupSTR]=mergeData(s_ICa_avg,s_ICp_avg,q_ICa_avg,q_ICp_avg)

n1 = size(s_ICa_avg,1);
n2 = size(s_ICp_avg,1);
n3 = size(q_ICa_avg,1);
n4 = size(q_ICp_avg,1);

data = [s_ICa_avg ; s_ICp_avg ; q_ICa_avg ; q_ICp_avg];
groupID = [ ones(n1,1) ; ones(n2,1)*2 ; ones(n3,1)*3 ; ones(n4,1)*4 ];

s = sum(isnan(data),2);
ii_toRemove = find(s==2);
data(ii_toRemove,:)=[];
groupID(ii_toRemove)=[];

groupSTR = {'sucr. ICa' , 'sucr. ICp' , 'quin. ICa' , 'quin. ICp'};

end