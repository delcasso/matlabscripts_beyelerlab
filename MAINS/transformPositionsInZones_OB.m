function experiment = transformPositionsInZones_OB(experiment)
vData=experiment.vData;
p=experiment.p;
X_cmSP = vData.mainX_cmSP;Y_cmSP = vData.mainY_cmSP;
vData.inZone=position2zone(X_cmSP,Y_cmSP,vData.zones_cmSP);
experiment.vData=vData;
end
