clear
clc
close all

d = 'Z:\Optogenetics & Behavior\DATA\20190318_ICa-ICp-G4\20190326_RTPA';
l = dir([d filesep '*.avi']);
n = size(l,1);

journal  = readtable([d filesep  'Journal.xlsx']);

figure();

L = [];
R = [];

for i=1:n
    
    f = l(i).name;
    [filepath,name,ext] = fileparts(f);    
    
    subplot(3,4,i)
    hold on
    
    jName = journal.NAME(i);
    jName= jName{1};
    jGroup = journal.Group(i);
    jGroup= jGroup{1};
    jOpsin = journal.Opsin(i);
    jOpsin= jOpsin{1};
    jStimSide = journal.StimSide(i);
    jStimSide= jStimSide{1};
    
    if ~strcmp(name,jName)
        name
        jName
        warning('error')
    end
    
    titleStr  =sprintf('%s-%s-%s-%s',jName,jGroup,jOpsin,jStimSide);
    title(titleStr)
    
    bonsaiPath=[d filesep name '-bonsai.txt'];
    bonsai_output = getBonsaiData(bonsaiPath);
    nSamples = size(bonsai_output.bodyX,1);
    x = bonsai_output.mouseX;
    y = bonsai_output.mouseY;
    ii = find(y<50);
    x(ii)=[];
    y(ii)=[];
    

    
    plot(x,y);
    ii = find(x<370);
    L(i) = size(find(x<370),1);
    R(i) = size(find(x>370),1);
    
    
    text(300,50,sprintf('%2.2f%',(L(i)/(L(i)+R(i)))*100))
    text(450,50,sprintf('%2.2f%',(R(i)/(L(i)+R(i)))*100))
end

pL = L ./(L+R);
pR = R ./(L+R);

journal.pL = pL';
journal.pR = pR';

writetable(journal,[d filesep 'resutls.xlsx']);



function bonsai_output = getBonsaiData(bonsaiPath)

%% get Bonsai Data
bonsai_output=[];
if exist(bonsaiPath,'file')
    bonsai_data = dlmread(bonsaiPath,' ',1,0);
    bonsai_data(:,end)=[];
    bonsai_output={};
    fid = fopen(bonsaiPath, 'r');
    tline = fgets(fid);
    tline(end)=[];tline(end)=[];
    remain = tline;
    fields=[];
    nFields=0;
    while ~isempty(remain)
        [token,remain] = strtok(remain, ' ');
        nFields=nFields+1;
        fields{nFields} = token;
        cmd = sprintf('bonsai_output.%s=bonsai_data(:,nFields);', fields{nFields} );
        eval(cmd);
    end
    fclose(fid);
    bonsai_output.bodyX = bonsai_output.mouseX;
    bonsai_output.bodyY = bonsai_output.mouseY;
    rmfield(bonsai_output,'mouseX');rmfield(bonsai_output,'mouseY');
end
end