function experiment = getZonesStatistics_OB(experiment)

vData=experiment.vData;
p=experiment.p;
nZones = size(vData.zones_cmSP,2);

periods = vData.anlayzedOptoPeriods.Single;
periods_id = unique(periods);
periods_id(isnan(periods_id))=[];
nPeriods = size(periods_id,1);

% fod = fopen(p.batch_ouputFile,'a');
% if ~p.batch_ouputFile_headerWritten
%    fprintf(fod,'Time in Zone (sec)\n');
%     for iP = 1: nPeriods
%         for iZ = 1:nZones
%                 fprintf(fod,sprintf('\tP%d',iP));
%         end
%         fprintf(fod,sprintf('\tP%d',iP));
%     end
%     fprintf(fod,'\n');    
%     fprintf(fod,'mouse');
%     for iP = 1: nPeriods
%         for iZ = 1:nZones
%             fprintf(fod,sprintf('\tZ%d',iZ));
%         end
%         fprintf(fod,sprintf('\tOUT'));
%     end
%     fprintf(fod,'\n');    
%     p.batch_ouputFile_headerWritten = 1;
% end
% 
% fprintf(fod,p.dataFileTag);
% 
% for i = 1: nPeriods
%     ii = find(periods==periods_id(i));
%     [vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone(ii),nZones);
%     for iZ = 1:nZones
%         fprintf(fod,sprintf('\t%d',vData.zoneTime_fr(iZ)./vData.videoInfo.FrameRate));
%     end
%     fprintf(fod,sprintf('\t%d',vData.outTime_fr./vData.videoInfo.FrameRate));
%     
% end
% fprintf(fod,'\n');

TimeInZone_sec = nan(nPeriods,nZones+1);

eOA_perPeriod = nan(nPeriods,1);
eCA_perPeriod = nan(nPeriods,1);
eOA = experiment.vData.eOA.idx;
eCA = experiment.vData.eCA.idx;


for i = 1: nPeriods
    
    ii = find(periods==periods_id(i));
    [vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone(ii),nZones);        
    for iZ = 1:nZones
        TimeInZone_sec(i,iZ) = vData.zoneTime_fr(iZ)./vData.videoInfo.FrameRate;    
    end
    TimeInZone_sec(i,nZones+1) = vData.outTime_fr./vData.videoInfo.FrameRate;  
    
    eOA_perPeriod(i) = size(intersect(ii,eOA),1);
    eCA_perPeriod(i) = size(intersect(ii,eCA),1);
    
end

experiment.TimeInZone_sec = TimeInZone_sec;
experiment.vData=vData;
experiment.p = p;
experiment.eOA_perPeriod = eOA_perPeriod;
experiment.eCA_perPeriod = eCA_perPeriod;
% fclose(fod);




end
