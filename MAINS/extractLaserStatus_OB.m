function experiment = extractLaserStatus_OB(experiment)

vData=experiment.vData;
p = experiment.p;

optoSigRaw=vData.optoPeriod; % bonsai_output.optoPeriod for Opto mice, bonsai_output.startSignal for photometry mice

fr = vData.videoInfo.FrameRate;
n = size(optoSigRaw,1);
t = 1:n;
t = t/fr;
% plot(t,optoSigRaw);
if optoSigRaw(1)>0.5
    optoSigRaw(1)=0;
end


suffix = 'optoPeriod';

figureSubFolder = [p.figureFolder filesep suffix];
if ~exist(figureSubFolder,'dir'),mkdir(figureSubFolder);end

figName = [figureSubFolder filesep p.dataFileTag '-' suffix '.jpeg'];

if ~exist(figName,'file') || p.forceRedrawing
    f=figure('name',p.dataFileTag,'Position',[20 20 800 800]);
    hold on
    plot(optoSigRaw);
    print(f,figName,'-djpeg');
    close(f);
end

nFrames=size(optoSigRaw,1);
optoSig=optoSigRaw>max(optoSigRaw)/10;
dRed=diff(optoSig);
raisingRed=find(dRed==1);
falingRed=find(dRed==-1);


% Previous quick patch replaced by this 20190312_1223
% if synchroLed was use properly nRaising == nFaling
% we clean bad signals otherwise
nRaise = size(raisingRed,1);
nFall = size(falingRed,1);
if (nRaise ~=nFall)
    fprintf('\terror with led synchro [nRaise ~=nFall]')
    if nRaise>nFall
        raisingRed(end) = [];
    end
    if nFall>nRaise
        falingRed(1) = [];
    end
    nRaise = size(raisingRed,1);
    nFall = size(falingRed,1);
end

redPeriods=[raisingRed falingRed];

switch p.apparatus.type
    case 'EPM'
        if (nRaise ~=nFall)
            warning('\t !!!!program could not correct error !');
        else
            period_durations_frame = diff(redPeriods,1,2);
            period_durations_sec = period_durations_frame ./ vData.videoInfo.FrameRate;
            nPeriods = size(period_durations_frame,1);
            % check if first sec LED ON synchro signal was hidden
            if nPeriods == 2 && (min(period_durations_sec)>175)
                redPeriods = [ [0 0] ; redPeriods];
                fprintf('\t error has been corrected');
            end                    
        end
    case 'OFT'
        [r,c]=size(redPeriods);
        if r==2 % one period is missing
            if raisingRed(1)>1200 % the first period happen more than one minute after video starts
                redPeriods=[1 2; redPeriods];
            end
        end
end


laserOnPeriods=redPeriods(2:end,:);
laserOffPeriods=[redPeriods(1,2)+1 laserOnPeriods(1,1)-1];
laserOffPeriods=[laserOffPeriods; laserOnPeriods(1,2)+1 laserOnPeriods(2,1)-1];
laserOffPeriods=[laserOffPeriods; laserOnPeriods(2,2)+1 nFrames];
optoPeriods.Idx=[laserOffPeriods(1,:) 0 ; laserOnPeriods(1,:) 1 ;laserOffPeriods(2,:) 0 ;laserOnPeriods(2,:) 1 ;laserOffPeriods(3,:) 0];

if  ~strcmp(p.apparatus.type,'RTPP')    
    optoPeriods.Single=nan(nFrames,1);
    optoPeriods.Merged=nan(nFrames,1);
    Sing=0:3;
    Merged=[0 1 0 1];
    for iP=1:4
        i1=optoPeriods.Idx(iP,1);i2=optoPeriods.Idx(iP,2);
        optoPeriods.Single(i1:i2)=Sing(iP);
        optoPeriods.Merged(i1:i2)=Merged(iP);
    end
end

vData.anlayzedOptoPeriods = optoPeriods;
vData.processedOptoSig = optoSig;

experiment.vData = vData;
