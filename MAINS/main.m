close all;clear;clc

tic;
t0=toc;

machine = 'SD_remote';
batchID= '20180926_iC-SweetBitter_AllTogether_EPM';
% batchID = '20180926_iC-SweetBitter_AllTogether_NSFT';
% batchID = '20180926_iC-SweetBitter_AllTogether_OFT';
% batchID = '20180926_iC-SweetBitter_AllTogether_SI';
% batchID = '20180926_iC-SweetBitter_AllTogether_TASTE';
p=[];
[dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig(batchID,machine,p);

%% PARAMETERS
p.outputFolder=outputFolder;
p.figureFolder = [p.outputFolder filesep 'figures'];
p.apparatus=apparatus;
p.videoExt=videoExt;
p.cameraMode = 'synchronous';
p.ledDetectionThreshold = 50; %percent of led signal max
p.HamamatsuFrameRate_Hz= 20;
p.behaviorCameraFrameRate_Hz=20;
p.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
p.body2licko_distanceMax_cm = 6;
p.savePDF = 0;
p.saveFIG = 0;
p.savePNG = 0;
p.forceRedrawing = 0;
p.forceBehavioralStart = 0;
p.getVideoTrackingData_plot=0;
p.getVideoTrackingData_force=1;
p.forceGetBodyParts=1;
p.OccupancyMap_sigmaGaussFilt=5;
p.PhotometrySignalMap_sigmaGaussFilt=5;
p.deltaFF_slidingWindowWidth = 1200;
p.lookingForMouse = '';
p.bonzaiDone = 1;
p.eventBasedAnalysisEdges_msec = [-5000:50:5000];
p.firstLickTh_msec = 500;

save([p.outputFolder filesep 'analysisParams.mat'],'p');

%%CREATES OUTPUT FOLDERS
% if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end

nFolders = size(dataRoot,2);
%% TO PROCESS ALL FOLDERS
for iFolder=1:nFolders
    
    p.dataRoot = dataRoot{iFolder};
    if ~exist(p.dataRoot,'dir')
        fprintf('%s doesn''t exist',p.dataRoot)
        pause
    end
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    
    nApparatus = size(apparatus,2);
    
    if nFolders < nApparatus
        % is there a model of the apparatus for normalizatoin
        if strcmp(apparatus{nFolders+1}.Model,'ForNormalization')
            p.apparatusModelForNormalization = apparatus{nFolders+1};
        end
    end
    
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        if ~isempty(p.lookingForMouse)
            processThisFile=0;
            if strcmp(p.dataFileTag,p.lookingForMouse)
                processThisFile=1;
            end
        end
        %% MAIN PROCESSING
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        tProcessingStart=toc;
        fprintf('[%.1f sec.]\tProcessing File %d/%d [%s] ...',tProcessingStart,iFile,nFiles,p.dataFileTag);
        
        experiment = loadExpData(p);
        experiment = processCaSignal(experiment);
        experiment = standardizePositionData(experiment);
        
        experiment=buildBasicMaps(experiment);
        drawBasicMaps(experiment);
        
        experiment = getApparatusZones(experiment);
        
        experiment = getZonesStatistics(experiment);
        
        switch p.apparatus.type
            case 'EPM'
                experiment=buildDirectionalMapsForEPM(experiment);
                experiment=buildLinearMapsForEPM(experiment);     
        end
        
        experiment = generateEventList(experiment);
        experiment = eventBasedAnalysis(experiment);
        
        
        %% SAVING DATA
        save([p.outputFolder filesep p.dataFileTag '.mat'],'experiment');
        
        
        tProcessingStop=toc;
        fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
        
    end
end



