function experiment = getZonesStatistics2_PB(experiment)

vData=experiment.vData;
pData=experiment.pData;
p=experiment.p;

X_cmSP = vData.mainX_cmSP;Y_cmSP = vData.mainY_cmSP;
vData.inZone=position2zone(X_cmSP,Y_cmSP,vData.zones_cmSP);
vData.nZones = size(vData.zones_cmSP,2);
[vData.zoneTime_fr,vData.outTime_fr]=framesInZone(vData.inZone,vData.nZones);

pData.transientStats=transientsPerZone(vData.inZone,vData.nZones,vData.zoneTime_fr,pData.transients);
pData.bulkSignalStats=bulkSignalPerZone(vData.inZone,vData.nZones,vData.zoneTime_fr,pData.mainSig);

%% Time in Zone in frames
filename = [p.batch_ouputFile(1:end-4) '-inZoneTime.txt']; %define filename
fod = fopen(filename,'a'); % open file
%% Writting Header
if ~p.batch_ouputFile_headerWritten
    fprintf(fod,'Time in Zone (sec)\n');
    for iZ = 1:vData.nZones, fprintf(fod,sprintf('\tZ%d',iZ));end;fprintf(fod,'\n');
    fprintf(fod,'mouse');
    for iZ = 1:vData.nZones, fprintf(fod,sprintf('\tZ%d',iZ));end;fprintf(fod,sprintf('\tOUT'));fprintf(fod,'\n');
    p.batch_ouputFile_headerWritten = 1;
end
%% Writtgin Values
if ~isfield(vData.videoInfo,'FrameRate')
    vData.videoInfo=getVideoInfo(p);
    if ~isfield(vData.videoInfo,'FrameRate')
        framerate = 20;
    end
else
    framerate=vData.videoInfo.FrameRate;
end
fprintf(fod,p.dataFileTag);for iZ = 1:vData.nZones, fprintf(fod,sprintf('\t%d',vData.zoneTime_fr(iZ)./framerate));end;fprintf(fod,sprintf('\t%d',vData.outTime_fr./framerate));fprintf(fod,'\n');   
fclose(fod); % close file
filename = [p.batch_ouputFile(1:end-4) '-inZoneBulk.txt']; %define filename
fod = fopen(filename,'a'); %open file

%% BulkSignal Amp in Zone
%% Writtgin Header
if ~p.batch_ouputFile_headerWritten
    fprintf(fod,'Bulk in Zone (sec)\n');
    for iZ = 1:vData.nZones, fprintf(fod,sprintf('\tZ%d',iZ));end;fprintf(fod,'\n');
    fprintf(fod,'mouse');
    for iZ = 1:vData.nZones, fprintf(fod,sprintf('\tZ%d',iZ));end;fprintf(fod,'\n');
    p.batch_ouputFile_headerWritten = 1;
end
%% Writting values
fprintf(fod,p.dataFileTag);
for iZ = 1:vData.nZones, fprintf(fod,sprintf('\t%d',pData.bulkSignalStats.zonesMeanAmp(iZ)./framerate));end;fprintf(fod,'\n');
fclose(fod); %close file

experiment.pData=pData;
experiment.vData=vData;
experiment.p = p;

end
