function experiment = getZonesStatistics_TimeBins_PB(experiment)

vData=experiment.vData;
pData=experiment.pData;
p=experiment.p;

X_cmSP = vData.mainX_cmSP;
Y_cmSP = vData.mainY_cmSP;
inZone=position2zone(X_cmSP,Y_cmSP,vData.zones_cmSP);

i1=1;

for iP=1:4
    
    i2=i1+(20*60*5);
    if i2>pData.nFrames
        i2 = pData.nFrames;
    end
    X_cmSP_tmp = vData.mainX_cmSP(i1:i2);
    Y_cmSP_tmp = vData.mainY_cmSP(i1:i2);
    inZone_tmp=position2zone(X_cmSP_tmp,Y_cmSP_tmp,vData.zones_cmSP);
    nZones = size(vData.zones_cmSP,2);
    [zoneTime_fr_tmp,outTime_fr_tmp]=framesInZone(inZone_tmp,nZones);
    
    nT = size(pData.transients,2);
    tmp = pData.transients;
    
    toKeep = [];
    for iT = 1:nT
        ii1 = find(tmp(iT).indices<i1);
        ii2 = find(tmp(iT).indices>i2);
        ii3 = [ii1 ii2];
        if isempty(ii3)
            toKeep = [toKeep iT];
        end
    end
    
    tmp = tmp(toKeep);
    pData.transientPeriodStats{iP}=transientsPerZone(inZone,nZones,zoneTime_fr_tmp,tmp);
    pData.bulkPeriodStats{iP} = bulkSignalPerZone(inZone_tmp,nZones,zoneTime_fr_tmp,pData.mainSig(i1:i2));           
    
    
    i1=i2+1;
    
end




experiment.vData=vData;
experiment.pData=pData;




end
