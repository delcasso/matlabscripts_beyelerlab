% initialization, close all figures, delete all variables, clear the command window
close all;clear;clc
%set up timer to time differnet analysis steps
tic;t0=toc;

% The is a unique identifier for szelecting the proper parameters, look
% inside the getBatchAnalysisConfig_OB.m to understand better
% batchID = 'ICa-ICp_EPM';
%batchID = '20181126_IC-SweetBitter-G2_NSFT'
% batchID = 'ICa-ICp_OFT-CONTROL';
% batchID = 'ICa-ICp_NSFT-CONTROL';
batchID = 'ICa-ICp_NSFT';
% batchID = 'ICa-ICp_QUININE-SUCROSE';
% batchID = 'ICa-ICp_OFT';median(movstd(audioDown_values,440));
% Where are the data, use machine flag
% option 1 = 'SD_remote', data are on the nas 1 server mounted as Z: on my laptop
% option 2 = 'SD_local', data have been copied on C:, the principal hard drive of my laptop
machine = 'SD_remote';

p = [];
outputFolder = [];
[dataRoot,outputFolder,apparatus,videoExt,p]=getBatchAnalysisConfig_PB(batchID,machine,p,outputFolder);

p.lookingForMouse = '';
p.processDisconnection = 1;


%%CREATES OUTPUT FOLDERS
% if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end

nFolders = size(dataRoot,2);
%% TO PROCESS ALL FOLDERS
for iFolder=1:nFolders
    
    p.dataRoot = dataRoot{iFolder};
    if ~exist(p.dataRoot,'dir'), fprintf('%s doesn''t exist',p.dataRoot);pause;end
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    nApparatus = size(apparatus,2);
    if nFolders < nApparatus
        % is there a model of the apparatus for normalizatoin
        if strcmp(apparatus{nFolders+1}.Model,'ForNormalization'),p.apparatusModelForNormalization = apparatus{nFolders+1};end
    end
    
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        
        % If you want to process only one file
        if ~isempty(p.lookingForMouse),processThisFile=0;if strcmp(p.dataFileTag,p.lookingForMouse),processThisFile=1;end;end
        
        % Depending of the histologfy status we skeep the analysis for the current file
        status = getHistologyStatus(p.journal,p.dataFileTag);
        if isempty(status) | status<1
            processThisFile=0;
        end
        
        %% MAIN PROCESSING
        
        %% MAIN PROCESSING
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        tProcessingStart=toc;
        fprintf('[%.1f sec.]\tProcessing File %d/%d [%s] ...',tProcessingStart,iFile,nFiles,p.dataFileTag);
        
        experiment = loadExpData_PB(p);
        experiment = processCaSignal(experiment);
        experiment = standardizePositionData(experiment);
        
        experiment=buildBasicMaps_PB(experiment);
        drawBasicMaps_PB(experiment);
        
        experiment = getApparatusZones(experiment);
        
        experiment = getZonesStatistics2_PB(experiment);
        
        %         experiment = getZonesStatistics_TimeBins_PB(experiment);
        
        switch batchID
            case 'ICa-ICp_EPM'
                experiment=buildDirectionalMapsForEPM(experiment);
                experiment=buildLinearMapsForEPM(experiment);
                experiment=mergeZoneForEPM(experiment);
            case 'ICa-ICp_NSFT'
                p = experiment.p;
                [audioEvents_sec,timeLag,nSTD,audioDetectionGap_sec] = nsft_getAudioEvents_05(p);
                experiment.audioEvents_sec = audioEvents_sec;
                experiment.audioEvents_timeLag_sec = timeLag;
                experiment.audioEvents_nSTD = nSTD;
                experiment.audioEvents_audioDetectionGap_sec = audioDetectionGap_sec;
                experiment=clean_nsft_audio_events(experiment);
            case 'ICa-ICp_SUCROSE'
                experiment=sucrose_eventBasedAnalysis_PB(experiment);
            case 'ICa-ICp_QUININE-SUCROSE'
                experiment=eventBasedAnalysis_SucroseQuinine_OneServoTwoPositions_PB(experiment);                
                
                
        end
        
        experiment  = resultsWriteTransientsFrequency(experiment);
        
        %         experiment = generateEventList(experiment);
        %         experiment = eventBasedAnalysis_PB(experiment);
        
        %% SAVING DATA
        save([p.outputFolder filesep p.dataFileTag '.mat'],'experiment');
        
        p = experiment.p;
        
        tProcessingStop=toc;
        fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
        
        
        
    end
    
end
writetable(experiment.p.results,experiment.p.bach_resultFile);


