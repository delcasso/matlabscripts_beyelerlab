function experiment = loadExpData_OB(p)

%%  LOADING, SYNCHRONIZING AND CLEANING THE DATA
vData=[];
[p,vData] = getVideoTrackingData_v3(p);
%         vData.bg=getBackGroundQuick(p);
vData.bg=getBackGroundSlow(p);
videoInfo=getVideoInfo(p);
vData.videoInfo = videoInfo;

behavioralStartFilePath=[p.dataRoot filesep p.dataFileTag '-behavioralStart.tsv'];
if ~exist([behavioralStartFilePath])

switch p.apparatus.type
    case 'EPM'        
        [iBehavioralStarts,vData]=detectBehavioralStart_HalfMinimumThreshold(p,vData); %For photometry we detect the experimenter placing the animal on the maze, one minute after the exp started.
%         [iBehavioralStarts,vData]=detectBehavioralStart(p,vData); %For photometry we detect the experimenter placing the animal on the maze, one minute after the exp started.
    case 'NSFT'
        [iFirstCross,iCrossSup,iCrossInf,Th,vData]=detectBehavioralStart_simpleThresholdCrossingBothSides(p,vData);
        iBehavioralStarts = iFirstCross;
    case 'OFT'
        [iFirstCross,iCrossSup,iCrossInf,Th,vData]=detectBehavioralStart_simpleThresholdCrossingBothSides(p,vData);
        iBehavioralStarts = iFirstCross;
    case 'SI'
        [iFirstCross,iCrossSup,iCrossInf,Th,vData]=detectBehavioralStart_simpleThresholdCrossingBothSides(p,vData);
        iBehavioralStarts = iFirstCross;
    case 'TASTE'
        [iFirstCross,iCrossSup,iCrossInf,Th,vData]=detectBehavioralStart_simpleThresholdCrossingBothSides(p,vData);
        iBehavioralStarts = iFirstCross;
    case 'RTPP'
        [iFirstCross,iCrossSup,iCrossInf,Th,vData]=detectBehavioralStart_simpleThresholdCrossingBothSides(p,vData);
        iBehavioralStarts = iFirstCross;
end

else
    behavioralStart_sec =  csvread(behavioralStartFilePath,16,0);
    iBehavioralStarts = floor(behavioralStart_sec * vData.videoInfo.FrameRate);
end

if isempty(iBehavioralStarts), iBehavioralStarts=0; end

%% DETECT ANIMAL FALL OF MAZE
        nExperimenterIntervention = size(iBehavioralStarts,2);
        if iBehavioralStarts(end)> (p.behaviorCameraFrameRate_Hz*120)
            if (nExperimenterIntervention>1) && strcmp(p.apparatus.type,'EPM')
                warning('Abnormal Intervention of Experimenter, did animal fall of EPM ?');
                nFrames = size(vData.mainX,1);
                nFramesToRemove = nFrames - iBehavioralStarts(2) + (20*60);
                [vData]=removeFrames_OB(vData,nFramesToRemove,'End',p);% if the animal fall, we remove the frames after the experimenter came into the room
            end
        else
            iBehavioralStarts(1) =   iBehavioralStarts(end);
        end


%% DETECT EXPERIMENTER STARTS BEHAVIOR
nFramesToRemove = iBehavioralStarts(1); %remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording

[vData]=removeFrames_OB(vData,nFramesToRemove,'Beginning',p);%remove the first minute of the signal due to bleeching at the beginning fot he fiber-photometry recording

%% CLEAN VIDEO TRACKING DATA
vData=cleanPosBasedOnSpeedThreshold(vData,p); % remove all position when animal speed exceed 20 pixel per sec.
%% CREATES TIME VECTOR
%         nFrames = size(vData.mainX,1);T = 1: nFrames;T = T./ p.HamamatsuFrameRate_Hz;  pData.T = T; vData.nFrames = nFrames; %recreate time vector based on fiber-photometry frame rate


experiment.p = p;
experiment.vData=vData;
end