% function fp_howto()
clc
fprintf('\n');
fprintf('---------------------------------\n');
fprintf('| ANALYSIS GUIDELINES |\n');
fprintf('---------------------------------\n');
fprintf('\n');
fprintf('#1 Rename Files\n');
fprintf('\t using MatlabScripts_BeyelerLab\\FILE_TOOLS\\rename_files_ex.m\n');
fprintf('\t !!! YOU CAN ERASE (LOOSE) EVERYTHING\n');
fprintf('\t forst comment the line\n');
fprintf('\t\t%%[status,msg,msgID] = movefile(source,dest);\n');
fprintf('\tand check the values of ''source'' and ''dest''\n');
fprintf('\n');
fprintf('\n');
% end