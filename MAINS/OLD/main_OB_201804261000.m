% initialization, close all figures, delete all variables, clear the command window
close all;clear;clc
%set up timer to time differnet analysis steps
tic;t0=toc;

% The is a unique identifier for selecting the proper parameters, look
% inside the getBatchAnalysisConfig_OB.m to understand better
batchID = 'ICa-ICp_EPM';
%batchID = '20181126_IC-SweetBitter-G2_NSFT'

% Where are the data, use machine flag
% option 1 = 'SD_remote', data are on the nas 1 server mounted as Z: on my laptop 
% option 2 = 'SD_local', data have been copied on C:, the principal hard drive of my laptop 
machine = 'SD_remote';

p=[];
[dataRoot,outputFolder,apparatus,videoExt,p,results]=getBatchAnalysisConfig_OB(batchID,machine,p);

%% PARAMETERS
c = clock();
p.batch_ouputFile = [outputFolder filesep sprintf('%04d%02d%02d-%02d%02d%02d',c(1),c(2),c(3),c(4),c(5),floor(c(6))) '.txt'];
p.batch_ouputFile_headerWritten=0;
p.outputFolder=outputFolder;
p.figureFolder = [p.outputFolder filesep 'figures'];
p.apparatus=apparatus;
p.videoExt=videoExt;
p.cameraMode = 'synchronous';
p.ledDetectionThreshold = 50; %percent of led signal max
p.HamamatsuFrameRate_Hz= 20;
p.behaviorCameraFrameRate_Hz=20;
p.speedThreshold=20; % Use to clean the position data automatically, based on abnormal animal speed
p.body2licko_distanceMax_cm = 6;
p.savePDF = 0;
p.saveFIG = 0;
p.savePNG = 0;
p.forceRedrawing = 0;
p.forceBehavioralStart = 0;
p.getVideoTrackingData_plot=0;
p.getVideoTrackingData_force=0;
p.forceGetBodyParts=0;
p.OccupancyMap_sigmaGaussFilt=5;
p.PhotometrySignalMap_sigmaGaussFilt=5;
p.deltaFF_slidingWindowWidth = 1200;
p.lookingForMouse = 'M422';
p.bonzaiDone = 1;
p.eventBasedAnalysisEdges_msec = [-5000:50:5000];
p.firstLickTh_msec = 500;


save([p.outputFolder filesep 'analysisParams.mat'],'p');

%%CREATES OUTPUT FOLDERS
% if ~exist(p.outputFolder,'dir'),mkdir(outputFolder);end
if ~exist(p.figureFolder,'dir'),mkdir(p.figureFolder);end

nFolders = size(dataRoot,2);
%% TO PROCESS ALL FOLDERS
for iFolder=1:nFolders
    
    p.dataRoot = dataRoot{iFolder};
    if ~exist(p.dataRoot,'dir'), fprintf('%s doesn''t exist',p.dataRoot);pause;end
    p.apparatus = apparatus{iFolder};
    p.videoExtension=videoExt{iFolder};
    nApparatus = size(apparatus,2);
    if nFolders < nApparatus
        % is there a model of the apparatus for normalizatoin
        if strcmp(apparatus{nFolders+1}.Model,'ForNormalization'),p.apparatusModelForNormalization = apparatus{nFolders+1};end
    end
    
    %% MAIN PROGRAM
    fileList = dir([p.dataRoot filesep '*.' p.videoExtension]);nFiles = size(fileList,1);
    %% PROCESS EACH DATA FILE INDIVIDUALLY
    for iFile=1:nFiles
        p.filename = fileList(iFile).name;[p.fPath, p.dataFileTag, p.ext] = fileparts(p.filename);% Parse Filename To Use Same Filename to save analysis results
        %% TO PROCESS ONE FILE IN PARTICUALR
        processThisFile=1;
        
        % If you want to process only one file
        if ~isempty(p.lookingForMouse),processThisFile=0;if strcmp(p.dataFileTag,p.lookingForMouse),processThisFile=1;end;end
                
        % Depending of the histologfy status we skeep the analysis for the current file
        status = getHistologyStatus(p.journal,p.dataFileTag);
        fprintf('here histo status has been commented, so bad histo are still processed\n')
        if isempty(status) %| status<1 
            processThisFile=0;
        end        
        
        %% MAIN PROCESSING
        
        %Skeeping file already processed
        if ~processThisFile,  fprintf('Skeeping File %d/%d [%s]\n',iFile,nFiles,p.dataFileTag);continue;end
        
        tProcessingStart=toc;
        fprintf('[%.1f sec.]\tProcessing File %d/%d [%s]\n',tProcessingStart,iFile,nFiles,p.dataFileTag);
        
        fprintf('\t\t\tloadExpData_OB ');
        experiment = loadExpData_OB(p);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
        fprintf('\t\t\tstandardizePositionData ');
        experiment = standardizePositionData(experiment);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
        fprintf('\t\t\tbuildBasicMaps_OB ');
        experiment=buildBasicMaps_OB(experiment);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
        fprintf('\t\t\tbuildBasicMaps_OB ');
        drawBasicMaps_OB(experiment);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
        fprintf('\t\t\tgetApparatusZones ');
        experiment = getApparatusZones(experiment);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart);
        
        fprintf('\t\t\ttransformPositionsInZones_OB ');
        experiment = transformPositionsInZones_OB(experiment);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
        fprintf('\t\t\textractLaserStatus_OB ');
        experiment = extractLaserStatus_OB(experiment);
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
               
        fprintf('\t\t\tgetZonesStatistics_OB ');
        if strcmp(p.apparatus.type,'RTPP')
            experiment = getZonesStatistics_RTPP_OB(experiment);
            p = experiment.p;
        else
            experiment = getZonesStatistics_OB(experiment);
            p = experiment.p;
        end
        
        ii = find(p.journal.MouseNum==str2double(p.dataFileTag(2:end)));
        switch p.apparatus.type
            case 'OFT'
                results.TimeBorder_sec(ii)=experiment.vData.zoneTime_fr(1)/p.behaviorCameraFrameRate_Hz;
                results.TimeCenter_sec(ii)=experiment.vData.zoneTime_fr(2)/p.behaviorCameraFrameRate_Hz;
        end
        
        fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
     
                
%         fprintf('\t\t\tgetZonesStatistics_OB ');
%         experiment = getZonesStatistics_OB(experiment);
%         fprintf('done in [%.1f sec.]\n',toc-tProcessingStart)
        
%         
%         switch p.apparatus.type
%             case 'EPM'
%                 experiment=buildDirectionalMapsForEPM(experiment);
%                 experiment=buildLinearMapsForEPM(experiment);     
%         end
%         
%         experiment = generateEventList(experiment);
%         experiment = eventBasedAnalysis_PB(experiment);
                
        %% SAVING DATA
        save([p.outputFolder filesep p.dataFileTag '.mat'],'experiment');
        
        tProcessingStop=toc;
        fprintf('done in %.1f\n',tProcessingStop-tProcessingStart);
        
    end
end





